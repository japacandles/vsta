#!/bin/bash -x

# chart.sh for creating charts of cme futures
# May 18th, 2019
# Caution: HUGE technical debt and sloppy coding ahead !
#          Major refactoring called for ...
#
exg=$1
type=$2
commodity=$3

# save the vsta_chart_dir
#vsta_chart_dir=$VSTA_CHART_DIR

# Initialize vsta-global vars
. $VSTA_DIR/configs.sh

exg=$(pwd)
exchange=${exg#$VSTA_CHARTPROG_DIR*}
#echo $exchange; read

# Initialize vsta-charting configs and vars
. $VSTA_CHARTPROG_DIR/configs.sh
. $VSTA_CHARTPROG_DIR/vars.sh

# Initialize any exchange specific configs and vars
if [ -f ./configs.sh ]; then
    . ./configs.sh
fi
if [ -f ./vars.sh ]; then
    . ./vars.sh
fi

# make sure that a dir exists for the charts, data
# and the auto-generated programs
if [ $commodity ]; then   
    export VSTA_CHART_DIR=$VSTA_CHART_DIR/$exchange/$type/$commodity
else
    export VSTA_CHART_DIR=$VSTA_CHART_DIR/$exchange/$type
fi
rm -rv $VSTA_CHART_DIR
mkdir -pv $VSTA_CHART_DIR
#echo $VSTA_CHART_DIR; read

Rfile="$VSTA_CHART_DIR/chart.R"
echo "browser()" > $Rfile
echo "library(quantmod)" >> $Rfile
echo >> $Rfile

# create the data files with .dat extension 
for c in ${commodities[@]} ; do
    sec_list_csv=$(echo $c | awk -F= '{print $2}')
    c=$(echo $c | awk -F= '{print $1}')

    #echo $c
    #echo $sec_list_csv
    
    if [ "$c" == "$commodity" ]; then # Found !

	#echo "Now in $type, looking at $VSTA_DATA_DIR/$exchange/$type/$sec_list_csv "; read
	if [ -f "$VSTA_DATA_DIR/$exchange/$type/$sec_list_csv" ]; then # single stock or index

	    #echo "Now in $type, looking at $commodity"; read
	    
	    security2="$VSTA_DATA_DIR/$exchange/$type/$sec_list_csv"
	    lines=$(wc -l $security2 | awk '{print $1}')
	    if [ $lines -gt 20 ]; then
		
		tmp=$(basename $security2)
		lines=$(wc -l $security2 | awk '{print $1}')
		
		# in case there are more than 1000 records, we want a
		# really long term chart
		if [ $lines -gt 1000 ]; then
		  
		    tmp2=$tmp"--1000.dat"
		    $TOP_DIR/r-prep.sh $security2 1000 > $VSTA_CHART_DIR/$tmp2
		    
		    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
		    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
		    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
		    
		    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
		    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
		    echo "chartSeries(s, theme = \"black\", name = \"$commodity $month $year from $startDate to $endDate SMAs 200, 50\", type = c(\"line\"), TA=c(addSMA(n=200, col=\"blue\"), addSMA(n=50, col=\"red\")))" >> $Rfile
		    echo "dev.off()" >> $Rfile
		    echo >> $Rfile
		    
		fi
		
		# approx 2 years
		if [ $lines -gt 400 ]; then
		    tmp2=$tmp"--400.dat"
		    $TOP_DIR/r-prep.sh $security2 400 > $VSTA_CHART_DIR/$tmp2
		    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
		    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
		    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
		    
		    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
		    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
		    echo "chartSeries(s, theme = \"black\", name = \"$commodity $month $year from $startDate to $endDate SMAs 200, 50\", type = c(\"line\"), TA=c(addSMA(n=200, col=\"yellow\"), addSMA(n=50, col=\"red\")))" >> $Rfile
		    echo "dev.off()" >> $Rfile
		    echo >> $Rfile
		    
		fi
		
		# approx 1 year
		if [ $lines -gt 250 ]; then
		    tmp2=$tmp"--250.dat"
		    $TOP_DIR/r-prep.sh $security2 250 > $VSTA_CHART_DIR/$tmp2
		    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
		    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
		    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
		    
		    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
		    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
		    echo "chartSeries(s, theme = \"white\", name = \"$commodity $month $year from $startDate to $endDate SMAs 50, 20\", TA=c(addVo(), addSMA(n=50, col=\"blue\"), addSMA(n=20, col=\"red\"), addBBands()))" >> $Rfile
		    echo "dev.off()" >> $Rfile
		    echo >> $Rfile
		    
		else
		    
		    # anything less than a year
		    tmp2=$tmp"--all.dat"
		    $TOP_DIR/r-prep.sh $security2 $lines > $VSTA_CHART_DIR/$tmp2
		    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
		    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
		    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
		    
		    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
		    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
		    echo "chartSeries(s, theme = \"white\", name = \"$commodity $month $year from $startDate to $endDate\", TA=c(addVo(), addBBands()))" >> $Rfile
		    echo "dev.off()" >> $Rfile
		    echo >> $Rfile
		fi
	    fi # this ends the if that tests if the data file has over 20 lines
	    
	else # we are dealing with a future (having an expiry date
	    
	    for year in ${years[@]}; do
		for month in ${months[@]}; do
		    for security in $(echo $sec_list_csv | sed "s/,/ /g"); do
			
			security2="$VSTA_DATA_DIR/$exchange/$type/$security-$month-$year"
			if [ -f $security2 ]; then # check if the file exists
			    lines=$(wc -l $security2 | awk '{print $1}')
			    if [ $lines -gt 20 ]; then
				
				tmp=$(basename $security2)
				lines=$(wc -l $security2 | awk '{print $1}')
				
				# in case there are more than 1000 records, we want a
				# really long term chart
				if [ $lines -gt 1000 ]; then
				    tmp2=$tmp"--1000.dat"
				    $TOP_DIR/r-prep.sh $security2 1000 > $VSTA_CHART_DIR/$tmp2
				    
				    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
				    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    
				    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
				    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    echo "chartSeries(s, theme = \"black\", name = \"$security $month $year from $startDate to $endDate SMAs 200, 50\", type = c(\"line\"), TA=c(addSMA(n=200, col=\"blue\"), addSMA(n=50, col=\"red\")))" >> $Rfile
				    echo "dev.off()" >> $Rfile
				    echo >> $Rfile
				    
				fi
				
				# approx 2 years
				if [ $lines -gt 400 ]; then
				    tmp2=$tmp"--400.dat"
				    $TOP_DIR/r-prep.sh $security2 400 > $VSTA_CHART_DIR/$tmp2
				    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
				    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    
				    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
				    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    echo "chartSeries(s, theme = \"black\", name = \"$security $month $year from $startDate to $endDate SMAs 200, 50\", type = c(\"line\"), TA=c(addSMA(n=200, col=\"yellow\"), addSMA(n=50, col=\"red\")))" >> $Rfile
				    echo "dev.off()" >> $Rfile
				    echo >> $Rfile
				    
				fi
				
				# approx 1 year
				if [ $lines -gt 250 ]; then
				    tmp2=$tmp"--250.dat"
				    $TOP_DIR/r-prep.sh $security2 250 > $VSTA_CHART_DIR/$tmp2
				    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
				    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    
				    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
				    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate SMAs 50, 20\", TA=c(addVo(), addSMA(n=50, col=\"blue\"), addSMA(n=20, col=\"red\"), addBBands()))" >> $Rfile
				    echo "dev.off()" >> $Rfile
				    echo >> $Rfile
				    
				else
				    
				    # anything less than a year
				    tmp2=$tmp"--all.dat"
				    $TOP_DIR/r-prep.sh $security2 $lines > $VSTA_CHART_DIR/$tmp2
			    	    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
				    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    
				    echo "png(filename = \"$pic\", width = 2000, height = 1000)" >> $Rfile
				    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addVo(), addBBands()))" >> $Rfile
				    echo "dev.off()" >> $Rfile
				    echo >> $Rfile
				    
				fi
			    fi
			fi
			
		    done # end loop of securities
		done # list of variations of the securities - E-mini and E-micro are variations of Gold
	    done # list of years
	    
	fi # we do not need an "else" stanza because we simply loop back
    fi # this ends the if where we check if it is a index/stock or a future (which has an expiry date)
done # list of commodites array

#
# Run the R file
cd $VSTA_CHART_DIR
R CMD BATCH chart.R
cd $exg
echo $?
