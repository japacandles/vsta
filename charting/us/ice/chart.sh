#!/bin/bash -x

# chart.sh for creating charts of CME futures
# May 18th, 2019
# Caution: HUGE technical debt and sloppy coding ahead !
#          Major refactoring called for ...
#
exg=$1
type=$2
commodity=$3

# save the vsta_chart_dir
#vsta_chart_dir=$VSTA_CHART_DIR

# Initialize vsta-global vars
. $VSTA_DIR/configs.sh

exg=$(pwd)
exchange=${exg#$VSTA_CHARTPROG_DIR*}
#echo $exchange; read

# Initialize vsta-charting configs and vars
. $VSTA_CHARTPROG_DIR/configs.sh
. $VSTA_CHARTPROG_DIR/vars.sh

# Initialize any exchange specific configs and vars
if [ -f ./configs.sh ]; then
    . ./configs.sh
fi
if [ -f ./vars.sh ]; then
    . ./vars.sh
fi

# make sure that a dir exists for the charts, data
# and the auto-generated programs
if [ $commodity ]; then   
    export VSTA_CHART_DIR=$VSTA_CHART_DIR/$exchange/$type/$commodity
else
    export VSTA_CHART_DIR=$VSTA_CHART_DIR/$exchange/$type
fi
rm -rv $VSTA_CHART_DIR
mkdir -pv $VSTA_CHART_DIR
#echo $VSTA_CHART_DIR; read

# create the data files with .dat extension 
for c in ${commodities[@]} ; do
    sec_list_csv=$(echo $c | awk -F= '{print $2}')
    c=$(echo $c | awk -F= '{print $1}')

    #echo $c
    #echo $sec_list_csv

    #if [ "$commodity" == "all" ]; then
    #	c="$commodity"
    #fi
    
    if [ "$c" == "$commodity" ]; then
	
	Rfile="$VSTA_CHART_DIR/chart.R"
	echo "browser()" > $Rfile
	echo "library(quantmod)" >> $Rfile
	echo >> $Rfile

	for security in $(echo $sec_list_csv | sed "s/,/ /g"); do
		
		for year in ${years[@]}; do
		    for month in ${months[@]}; do
			#for security in $(echo $sec_list_csv | sed "s/,/ /g"); do
			
			#security2=$VSTA_DATA_DIR/$exchange/$type/"$security-$month-$year"
			for security2 in $(find $VSTA_DATA_DIR/$exchange/$type/ -type f | grep "$security-$month-$year" ); do
			    #security3=$VSTA_DATA_DIR/$exchange/$type/"$security-$month-$year"
                            security2=$(echo $security2 | sed "s~//~/~g")
			    #echo "Looking at $security2 1"; read

                            # If CHARTABLE has been defined then a drastically reduced number of charts will be created
                           if [ "$CHARTABLE" -ne 0 ]; then
                                #echo Looking for $security2 2; read
                                grep $security2 "$CHARTABLES_FILE"
                                RETVAL=$?
                                if [ "$RETVAL" -ne 0 ]; then
                                    continue
                                fi
                           fi
                           
			    lines=$(wc -l $security2 | awk '{print $1}')
			    if [ $lines -gt 20 ]; then
				
				tmp=$(basename $security2)
				lines=$(wc -l $security2 | awk '{print $1}')
				
				# in case there are more than 1000 records, we want a
				# really long term chart
				if [ $lines -gt 1000 ]; then
				    tmp2=$tmp"--1000.dat"
				    $TOP_DIR/r-prep.sh $security2 1000 > $VSTA_CHART_DIR/$tmp2
				    
				    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
				    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    
				    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
				    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    echo "chartSeries(s, theme = \"black\", name = \"$security $month $year from $startDate to $endDate SMAs 200, 50\", type = c(\"line\"), TA=c(addSMA(n=200, col=\"blue\"), addSMA(n=50, col=\"red\")))" >> $Rfile
				    echo "dev.off()" >> $Rfile
				    echo >> $Rfile
				    
				fi
				
				# approx 2 years
				if [ $lines -gt 400 ]; then
				    tmp2=$tmp"--400.dat"
				    $TOP_DIR/r-prep.sh $security2 400 > $VSTA_CHART_DIR/$tmp2
				    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
				    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    
				    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
				    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    echo "chartSeries(s, theme = \"black\", name = \"$security $month $year from $startDate to $endDate SMAs 200, 50\", type = c(\"line\"), TA=c(addSMA(n=200, col=\"yellow\"), addSMA(n=50, col=\"red\")))" >> $Rfile
				    echo "dev.off()" >> $Rfile
				    echo >> $Rfile
				    
				fi
				
				# approx 1 year
				if [ $lines -gt 250 ]; then
				    tmp2=$tmp"--250.dat"
				    $TOP_DIR/r-prep.sh $security2 250 > $VSTA_CHART_DIR/$tmp2
				    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')
                                    
				    #pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    #echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
				    #echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    #echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate SMAs 50, 20\", TA=c(addVo(), addSMA(n=50, col=\"blue\"), addSMA(n=20, col=\"red\"), addBBands()))" >> $Rfile
				    #echo "dev.off()" >> $Rfile
				    #echo >> $Rfile

                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")
				    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=NULL)" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile

                                    # price with BB
                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2""-BB.png" | sed "s/\/\//\//g")                                
                                    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addBBands()))" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile
                                    
                                    # price with RSI
                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2""-RSI.png" | sed "s/\/\//\//g")                               
                                    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addRSI()))" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile
                                    
                                    # price with vol
                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2""-Vol.png" | sed "s/\/\//\//g")                               
                                    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addVo()))" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile
                                    
				else
				    
				    # anything less than a year
				    tmp2=$tmp"--all.dat"
				    $TOP_DIR/r-prep.sh $security2 $lines > $VSTA_CHART_DIR/$tmp2
			    	    startDate=$(head -n 2 $VSTA_CHART_DIR/$tmp2 | tail -n 1 | awk '{print $1}')
				    endDate=$(tail -n 1 $VSTA_CHART_DIR/$tmp2 | awk '{print $1}')

				    #pic=$(echo "$VSTA_CHART_DIR/$tmp2"".png" | sed "s/\/\//\//g")				    
				    #echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
				    #echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
				    #echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addVo(), addBBands()))" >> $Rfile
				    #echo "dev.off()" >> $Rfile
				    #echo >> $Rfile

                                    # only price
                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2""Simple-.png" | sed "s/\/\//\//g")                                    
                                    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=NULL)" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile

                                    # price with BB
                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2""-BB.png" | sed "s/\/\//\//g")                                
                                    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addBBands()))" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile
                                    
                                    # price with RSI
                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2""-RSI.png" | sed "s/\/\//\//g")                               
                                    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addRSI()))" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile
                                    
                                    # price with vol
                                    pic=$(echo "$VSTA_CHART_DIR/$tmp2""-Vol.png" | sed "s/\/\//\//g")                               
                                    echo "png(filename = \"$pic\", width = 1024, height = 512)" >> $Rfile
                                    echo "s <- read.zoo(\"$VSTA_CHART_DIR/$tmp2\", sep = \" \", header = TRUE)" >> $Rfile
                                    echo "chartSeries(s, theme = \"white\", name = \"$security $month $year from $startDate to $endDate\", TA=c(addVo()))" >> $Rfile
                                    echo "dev.off()" >> $Rfile
                                    echo >> $Rfile
                                    
				fi # if lines -gt 250
			    fi # if lines -gt 20
			done # the if where we test if a data file exists
			
		    done # end of loop through months
		done # end of loop through years
		
		#cd $VSTA_CHART_DIR
		#touch .RData && rm -vf .RData && R --no-save --vanilla CMD BATCH chart.R
		
	    #fi # we do not need an "else" stanza because we simply loop back

	    echo "Now creating redemptio chart for $security"; #read
	    rm -vf /tmp/$security".dat" /tmp/$security".gnuplot" /tmp/$security".png"; #read
	    cd $VSTA_CHARTPROG_DIR/vsta-skel
	    #echo ./make-redemptio-chart-data-prog.sh $DATE /tmp/$security".dat" /tmp/$security".gnuplot" $VSTA_DATA_DIR/$exchange/$type/$security*; read
	    ./make-redemptio-chart-data-prog.sh $DATE /tmp/$security".dat" /tmp/$security".gnuplot" $VSTA_DATA_DIR/$exchange/$type/$security*
	    gnuplot /tmp/$security".gnuplot"
	    cp -v /tmp/$security".png" $VSTA_CHART_DIR/ ; #read
	    cd -
	    	    
	done
    fi
done	

#
# Run the R file
cd $VSTA_CHART_DIR
touch .RData && rm -vf .RData && R --no-save --vanilla CMD BATCH chart.R
cd $exg
echo $?
