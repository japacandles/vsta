commodities=("Gold=Gold" \
		 "Energy=Crude-Oil,Henry-Hub-Natural-Gas" \
		 "Silver=Silver" \
		 "Copper=Copper" \
		 "Bitcoin=bitcoin-reference-rate,ethereum-reference-rate,Bitcoin,Micro-Bitcoin" \
		 "AUDUSD=Australian-Dollar" \
		 "GBPUSD=British-Pound" \
		 "CADUSD=Canadian-Dollar" \
		 "JPYUSD=Japanese-Yen" \
		 "CHFUSD=Swiss-Franc" \
		 "NZDUSD=New-Zealand-Dollar" \
		 "EURUSD=Euro-FX" \
		 "Grains=Corn,Soybean,KC-HRW-Wheat,Chicago-SRW-Wheat" \
		 "USIDX=E-mini-Nasdaq-100,E-mini-SP-500" \
	    )
