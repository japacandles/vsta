
browser()
library(quantmod)

pdf(file = "/tmp/europe.pdf", onefile = T, title = "European Indexes", width = 38, height = 20 )

DAX3m <- read.zoo("/tmp/dax-60", sep=" ", header=TRUE); chartSeries(DAX3m, name = "DAX 3 months", theme="white", TA=c(addVo(),addROC(n=2),addBBands()))
DAX <- read.zoo("/tmp/dax", sep=" ", header=TRUE); chartSeries(DAX, name = "DAX", theme="white", TA=c(addVo(),addROC(n=2),addBBands()))
DAX1year <- read.zoo("/tmp/dax-260", sep=" ", header=TRUE); chartSeries(DAX1year, type = c("line"), name = "DAX 1 Year", theme="black")
DAX5year <- read.zoo("/tmp/dax-1300", sep=" ", header=TRUE); chartSeries(DAX5year, type = c("line"), name = "DAX 5 Year", theme="black")
DAX10year <- read.zoo("/tmp/dax-2500", sep=" ", header=TRUE); chartSeries(DAX10year, type = c("line"), name = "DAX 10 Year", theme="black")
#DaxSep2016 <- read.zoo("/tmp/DAX-FUTURE-Sep-2016", sep=" ", header=TRUE); chartSeries(DaxSep2016, name = "DAX Sep 2016", theme ="white", TA=c(addVo()))
#DaxDec2016 <- read.zoo("/tmp/DAX-FUTURE-Dec-2016", sep=" ", header=TRUE); chartSeries(DaxDec2016, name = "DAX Dec 2016", theme ="white", TA=c(addVo()))
#DaxMar2017 <- read.zoo("/tmp/DAX-FUTURE-Mar-2017", sep=" ", header=TRUE); chartSeries(DaxMar2017, name = "DAX Mar 2017", theme ="white", TA=c(addVo()))
#DaxJun2017 <- read.zoo("/tmp/DAX-FUTURE-Jun-2017", sep=" ", header=TRUE); chartSeries(DaxJun2017, name = "DAX Jun 2017", theme ="white", TA=c(addVo()))

FTSE3m <- read.zoo("/tmp/ftse-60", sep=" ", header=TRUE); chartSeries(FTSE3m, name = "FTSE 3 months", theme="white", TA=c(addVo(),addROC(n=2),addBBands()))
FTSE <- read.zoo("/tmp/ftse", sep=" ", header=TRUE); chartSeries(FTSE, name = "FTSE ", theme="white", TA=c(addVo(),addROC(n=2),addBBands()))
FTSE1year <- read.zoo("/tmp/ftse-260", sep=" ", header=TRUE); chartSeries(FTSE1year, type = c("line"), name = "FTSE 1 Year", theme="black")
FTSE5year <- read.zoo("/tmp/ftse-1300", sep=" ", header=TRUE); chartSeries(FTSE5year, type = c("line"), name = "FTSE 5 Year", theme="black")
FTSE10year <- read.zoo("/tmp/ftse-2500", sep=" ", header=TRUE); chartSeries(FTSE10year, type = c("line"), name = "FTSE 10 Year", theme="black")
#FtseSep2016 <- read.zoo("/tmp/FTSE_100-INDEX-FUTURE-SEP-2016", sep=" ", header=TRUE); chartSeries(FtseSep2016, name = "FTSE Sep 2016", theme="white", TA=c(addVo()))
#FtseDec2016 <- read.zoo("/tmp/FTSE_100-INDEX-FUTURE-DEC-2016", sep=" ", header=TRUE); chartSeries(FtseDec2016, name = "FTSE Dec 2016", theme="white", TA=c(addVo()))
#FtseMar2017 <- read.zoo("/tmp/FTSE_100-INDEX-FUTURE-MAR-2017", sep=" ", header=TRUE); chartSeries(FtseMar2017, name = "FTSE Mar 2017", theme="white", TA=c(addVo()))
#FtseJun2017 <- read.zoo("/tmp/FTSE_100-INDEX-FUTURE-JUN-2017", sep=" ", header=TRUE); chartSeries(FtseJun2017, name = "FTSE Jun 2017", theme="white", TA=c(addVo()))

dev.off()
