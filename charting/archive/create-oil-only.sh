!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

# Long term data of WTI Crude continuous futures
i=$DATADIR/us/eia/futures/wti-crude

file=$(basename $i)
     
./r-prep-gold.sh $i 700
awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-700"
   
./r-prep-gold.sh $i 240
awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-240"
    
./r-prep-gold.sh $i 60
awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-60"

# Long term data of Crude continuous futures
i=$DATADIR/misc/quandl/futures/ICE_T1

file=$(basename $i)
     
./r-prep.sh $i 700
awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-700"
   
./r-prep.sh $i 240
awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-240"
    
./r-prep.sh $i 60
awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-60"

# futures data from CME ; ETN data from NYSE Arca
for i in $(ls $DATADIR/us/cme/futures/Crude-Oil-MAY-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-JUN-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-JUL-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-AUG-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-SEP-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-OCT-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-NOV-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-DEC-2019 \
	      $DATADIR/us/cme/futures/Crude-Oil-JAN-2020 \
	      $DATADIR/us/cme/futures/Crude-Oil-FEB-2020 \
	      $DATADIR/us/cme/futures/Crude-Oil-MAR-2020 \
	      $DATADIR/us/cme/futures/Crude-Oil-JUN-2020 \
	      $DATADIR/us/cme/futures/Crude-Oil-DEC-2020 \
	      $DATADIR/us/cme/futures/Crude-Oil-JUN-2021 \
	      $DATADIR/us/cme/futures/Crude-Oil-DEC-2021 \
	      $DATADIR/us/cme/futures/Crude-Oil-FEB-2022 \
	      $DATADIR/us/cme/futures/Crude-Oil-MAR-2022 \
	      $DATADIR/us/cme/futures/Crude-Oil-APR-2022 \
	      $DATADIR/us/nyseArca/futures/UWT \
	      $DATADIR/us/nyseArca/futures/DWT \
	  )
do
    
    ./r-prep.sh $i 180
done


R CMD BATCH ~/vsta/charting/oil-only.R
echo All done 
cd $DIR
