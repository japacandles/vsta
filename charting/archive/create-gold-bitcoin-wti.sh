#!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$(pwd)
awk '{print $1, $4, $4, $4, $4, $5}' $DATADIR/se/nasdaqNordic/futures/bitcoin-tracker-etn | tee /var/tmp/bitcoin-tracker-etn-ohlc
awk '{print $1, $4, $4, $4, $4, $5}' $DATADIR/se/nasdaqNordic/futures/ethereum-tracker-etn | tee /var/tmp/ethereum-tracker-etn-ohlc

cd $CHARTINGDIR
./r-prep.sh $DATADIR/se/nasdaqNordic/futures/bitcoin-tracker-etn-ohlc 220
./r-prep.sh $DATADIR/se/nasdaqNordic/futures/ethereum-tracker-etn-ohlc 220
#./r-prep.sh /var/tmp/wti-crude-continuous-future 220
#./r-prep.sh /var/tmp/gold-continuous-future 220

for i in $(seq 1 7)
do
	./r-prep.sh $HOME/ICE_T$i 250
	./r-prep.sh $HOME/CME_GC$i 250
done

R CMD BATCH ~/vsta/charting/gold-bitcoin-wti.R
echo All done 
cd $DIR

