!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

# Long term charts for gold
for i in $DATADIR/uk/lbma/futures/gold_am
do

    file=$(basename $i)
    
    ./r-prep-gold.sh $i 2500
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-2500"
    
    ./r-prep-gold.sh $i 1300
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-1300"
    
    ./r-prep-gold.sh $i 700
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-700"
    
    ./r-prep-gold.sh $i 240
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-240"
    
    ./r-prep-gold.sh $i 60
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-60"

done

# Long term charts of Crude continuous futures
for i in $DATADIR/misc/quandl/futures/ICE_T1  $DATADIR/misc/quandl/futures/CME_GC1
do

    file=$(basename $i)
     
    ./r-prep.sh $i 700
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-700"
   
   ./r-prep.sh $i 240
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-240"
    
    ./r-prep.sh $i 60
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-60"

done

#file=bitcoin-reference-rate
for file in "bitcoin-reference-rate" "ethereum-reference-rate"
do
	./r-prep-gold.sh $DATADIR/us/cme/futures/$file 92
	awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-92"
	./r-prep-gold.sh $DATADIR/us/cme/futures/$file 366
	awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-366"
done

# futures charts from CME
#for i in $(ls $DATADIR/us/cme/futures/commodities/Gold-Apr-17-GCJ17 \
for i in $(ls $DATADIR/us/cme/futures/E-micro-*2018 \
				$DATADIR/us/cme/futures/E-mini*2018 \
				$DATADIR/us/cme/futures/Gold*2018 \
				$DATADIR/us/cme/futures/E-micro-*2019 \
				$DATADIR/us/cme/futures/E-mini*2019 \
				$DATADIR/us/cme/futures/Gold*2019 \
				$DATADIR/us/cme/futures/E-micro-*2020 \
				$DATADIR/us/cme/futures/E-mini*2020 \
				$DATADIR/us/cme/futures/Gold*2020 \
				$DATADIR/us/cme/futures/Gold*2021 \
				$DATADIR/us/cme/futures/Crude*2018 \
				$DATADIR/us/cme/futures/Crude*2019 \
				$DATADIR/uk/lbma/futures/gold_am.Mon-Fri \
				$DATADIR/uk/lbma/futures/gold_am.Tue-Mon \
				$DATADIR/uk/lbma/futures/gold_am.Wed-Tue \
				$DATADIR/uk/lbma/futures/gold_am.Thu-Wed \
				$DATADIR/uk/lbma/futures/gold_am.Fri-Thu \
				$DATADIR/us/cme/futures/Bitcoin*2018
				)
do

	./r-prep.sh $i 180
done


R CMD BATCH ~/vsta/charting/gold.R
#R CMD BATCH ~/vsta/charting/silver.R
echo All done 
cd $DIR
