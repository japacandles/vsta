#!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR
for i in $(ls $DATADIR/us/nyse/futures/dji \
	      $DATADIR/us/nyse/futures/sp500 \
	      $DATADIR/us/nasdaq/futures/nasdaq-100 )
	      #$DATADIR/br/bmfbovespa/futures/bvsp)
do  


	./r-prep.sh $i 5000
	twentyYear=$(basename $i)
	awk '{print $1, $2, $3, $4, $5}' /tmp/$twentyYear > /tmp/$twentyYear"-5000"

	./r-prep.sh $i 2500
	tenYear=$(basename $i)
	awk '{print $1, $2, $3, $4, $5}' /tmp/$tenYear > /tmp/$tenYear"-2500"

	./r-prep.sh $i 1300
	fiveYear=$(basename $i)
	awk '{print $1, $2, $3, $4, $5}' /tmp/$fiveYear > /tmp/$fiveYear"-1300"

	./r-prep.sh $i 260
	oneYear=$(basename $i)	
	awk '{print $1, $2, $3, $4, $5, $6}' /tmp/$oneYear > /tmp/$oneYear"-260"

	./r-prep.sh $i 60
	threeMonths=$(basename $i)	
	awk '{print $1, $2, $3, $4, $5, $6}' /tmp/$threeMonths > /tmp/$threeMonths"-60"

	./r-prep.sh $i 180

done 

for i in $(ls $DATADIR/us/cme/futures/*{SP,Dow,Nasdaq}*19 \
	      $DATADIR/us/cme/futures/*{SP,Dow,NASDAQ}*20 \
	      $DATADIR/us/nasdaqGm/futures/{S,T}QQQ)
do
	./r-prep.sh $i 180
done

echo Creating the Americas chartbook 
R CMD BATCH americas.R
cd $DIR
