!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

tempdir=$(mktemp -d)

#file=bitcoin-reference-rate
for file in "bitcoin-reference-rate" "ethereum-reference-rate"
do
	./r-prep-gold.sh $DATADIR/us/cme/futures/$file 92
	awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-92"
	./r-prep-gold.sh $DATADIR/us/cme/futures/$file 366
	awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-366"
done

# Crypto ETNs
for s in $(ls $DATADIR/se/nasdaqNordic/futures/*etn)
do
	z=$(basename $s)
	tail -n 200 $s | awk '{print $1, $4, $4, $4, $4, $5}' | tee $tempdir/"$z-ohlc"
done
for s in $(ls $tempdir/*ohlc )
do
	./r-prep.sh $s 200
done

# futures charts from CME
#for i in $(ls $DATADIR/us/cme/futures/commodities/Gold-Apr-17-GCJ17 \
    for i in $(ls $DATADIR/us/cme/futures/Bitcoin-MAY-2019 \
		  $DATADIR/us/cme/futures/Bitcoin-JUN-2019 \
		  $DATADIR/us/cme/futures/Bitcoin-JUL-2019 \
		  $DATADIR/us/cme/futures/Bitcoin-SEP-2019 \
	      )
do

	./r-prep.sh $i 180
done


R CMD BATCH ~/vsta/charting/bitcoin-only.R
echo All done 
cd $DIR
