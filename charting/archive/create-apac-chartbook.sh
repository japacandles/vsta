#!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

# Long term charts for asx200, hang seng , and nikkei 225
#for i in `ls $DATADIR/au/asx/futures/asx-aord $DATADIR/au/asx/futures/asx-200 $DATADIR/jp/tse/futures/n225 $DATADIR/kr/krx/futures/kospi $DATADIR/hk/hkex/futures/hang_seng `
for i in `ls $DATADIR/jp/tse/futures/n225 $DATADIR/hk/hkex/futures/hang_seng `
do  

        file=$(basename $i)

        ./r-prep.sh $i 5000
        awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-5000"

        ./r-prep.sh $i 2500
        awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-2500"

        ./r-prep.sh $i 1300
        awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-1300"

        ./r-prep.sh $i 260
        awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-260"

        ./r-prep.sh $i 60
        awk '{print $1, $2, $3, $4, $5, $6}' /tmp/$file > /tmp/$file"-60"
done

# Short term charts for all the others
#for i in $(ls $DATADIR/au/asx/futures/asx-aord \
#              $DATADIR/au/asx/futures/asx-200 \
#	      $DATADIR/jp/tse/futures/n225 \
#	      $DATADIR/kr/krx/futures/kospi \
#	      $DATADIR/hk/hkex/futures/hang_seng \
#	  )
for i in $(ls $DATADIR/jp/tse/futures/n225 \
	      $DATADIR/hk/hkex/futures/hang_seng \
	  )
do  

	#file=$(basename $i)

	./r-prep.sh $i 180
done

echo Creating the APAC chartbook 
R CMD BATCH apac.R
cd $DIR


#				  $DATADIR/au/asx/futures/SPI200-Mar17 \
#				  $DATADIR/au/asx/futures/SPI200-Apr17 \
#				  $DATADIR/au/asx/futures/SPI200-Jun17 \
