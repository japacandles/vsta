!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

# Long term charts for gold
for i in $DATADIR/uk/lbma/futures/silver \
	     $DATADIR/uk/lbma/futures/platinum_am \
	     $DATADIR/uk/lbma/futures/palladium_am
do
    
    file=$(basename $i)
    
    ./r-prep-gold.sh $i 2500
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-2500"
    
    ./r-prep-gold.sh $i 1300
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-1300"
    
    ./r-prep-gold.sh $i 700
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-700"
    
    ./r-prep-gold.sh $i 240
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-240"
    
    ./r-prep-gold.sh $i 60
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-60"
    
done

# futures charts from CME
for i in $(ls $DATADIR/uk/lbma/futures/silver.Mon-Fri \
	      $DATADIR/uk/lbma/futures/silver.Tue-Mon \
	      $DATADIR/uk/lbma/futures/silver.Wed-Tue \
	      $DATADIR/uk/lbma/futures/silver.Thu-Wed \
	      $DATADIR/uk/lbma/futures/silver.Fri-Thu \
	      $DATADIR/uk/lbma/futures/platinum_am.Mon-Fri \
	      $DATADIR/uk/lbma/futures/platinum_am.Tue-Mon \
	      $DATADIR/uk/lbma/futures/platinum_am.Wed-Tue \
	      $DATADIR/uk/lbma/futures/platinum_am.Thu-Wed \
	      $DATADIR/uk/lbma/futures/platinum_am.Fri-Thu \
	      $DATADIR/uk/lbma/futures/palladium_am.Mon-Fri \
	      $DATADIR/uk/lbma/futures/palladium_am.Tue-Mon \
	      $DATADIR/uk/lbma/futures/palladium_am.Wed-Tue \
	      $DATADIR/uk/lbma/futures/palladium_am.Thu-Wed \
	      $DATADIR/uk/lbma/futures/palladium_am.Fri-Thu \
	      $DATADIR/us/nyseArca/futures/SLV \
	      $DATADIR/us/nyseArca/futures/PALL \
	      $DATADIR/us/nyseArca/futures/PPLT \
	  )
do
    
    ./r-prep.sh $i 180
done


R CMD BATCH ~/vsta/charting/other-precious.R
echo All done 
cd $DIR
