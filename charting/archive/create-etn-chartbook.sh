#!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$(pwd)
cd $CHARTINGDIR

tempdir=$(mktemp -d)

for s in $(ls $DATADIR/se/nasdaqNordic/futures/*etn)
do
	z=$(basename $s)
	tail -n 200 $s | awk '{print $1, $4, $4, $4, $4, $5}' | tee $tempdir/"$z-ohlc"
done

for s in $(ls $tempdir/*ohlc $DATADIR/us/nasdaqGm/futures/* $DATADIR/us/nyseArca/futures/*)
do
	./r-prep.sh $s 200
done

R CMD BATCH etn.R
cd $DIR
