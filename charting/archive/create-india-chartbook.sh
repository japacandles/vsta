#!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

#
# Bombay Stock Exchange
#

# Create long term (5/10 year) data files
#for i in `ls $DATADIR/in/bse/futures/BSE-SENSEX-SPOT $DATADIR/in/bse/futures/BSE-100-SPOT `
#do
#	idx=$(basename $i)
#
#	./r-prep.sh $i 1300
#	awk '{ print $1, $2, $3, $4, $5 }' /tmp/$idx > /tmp/$idx"-1300"
#	./r-prep.sh $i 2500
#	awk '{ print $1, $2, $3, $4, $5 }' /tmp/$idx > /tmp/$idx"-2500"
#done

# Create short term index, and index futures' data files
#for i in `ls $DATADIR/in/bse/futures/*-SPOT $DATADIR/in/bse/futures/BSE100-BSIJUN2014-26-Jun-2014 $DATADIR/in/bse/futures/SENSEX-BSXJUN2014-26-Jun-2014 $DATADIR/in/bse/futures/SENSEX-BSXJUL2014-31-Jul-2014 $DATADIR/in/bse/futures/SENSEX-BSXAUG2014-28-Aug-2014 `
#do  
#	./r-prep.sh $i 180
#done 



#echo Creating BSE\'s chartbook 
#R CMD BATCH bse.R

# National Stock Exchange

# Create long term (5/10/20 year) data files
for i in `ls $DATADIR/in/nse/futures/NIFTY-CPSE-SPOT $DATADIR/in/nse/futures/NIFTY-IT-SPOT $DATADIR/in/nse/futures/NIFTY-BANK-SPOT $DATADIR/in/nse/futures/NIFTY-50-SPOT`
do
	idx=$(basename $i)

	./r-prep.sh $i 60
	awk '{ print $1, $2, $3, $4, $5, $6 }' /tmp/$idx > /tmp/$idx"-60"
	./r-prep.sh $i 250
	awk '{ print $1, $2, $3, $4, $5, $6 }' /tmp/$idx > /tmp/$idx"-250"
	./r-prep.sh $i 1300
	awk '{ print $1, $2, $3, $4, $5 }' /tmp/$idx > /tmp/$idx"-1300"
	./r-prep.sh $i 2500
	awk '{ print $1, $2, $3, $4, $5 }' /tmp/$idx > /tmp/$idx"-2500"
	./r-prep.sh $i 5000
	awk '{ print $1, $2, $3, $4, $5 }' /tmp/$idx > /tmp/$idx"-5000"
done

# Create short term index, and index futures' data files
for i in $(ls $DATADIR/in/nse/futures/NIFTY-50-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-BANK-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-IT-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-CPSE-SPOT \
	      $DATADIR/in/nse/futures/*-JUN-2019 \
	      $DATADIR/in/nse/futures/*-MAY-2019 \
	      $DATADIR/in/nse/futures/*-JUL-2019 \
	      $DATADIR/in/nse/futures/NIFTY-AUTO-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-FMCG-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-INFRA-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-ENERGY-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-METAL-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-MIDCAP-50-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-NEXT-50-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-MNC-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-PHARMA-SPOT \
	      $DATADIR/in/nse/futures/NIFTY-REALTY-SPOT  )

do  
	./r-prep.sh $i 180
done 

#for i in `ls /tmp/S\&P*`                                               # Commented out on Jan 1, 2019  
#do                                                                     # The S&P 500 futures are not
#	echo $i                                                         # traded any more on NSE
#	oldfile=$i 
#	newfile=`echo $oldfile | sed 's/\&//g' `; echo $newfile
#	 mv -v $oldfile $newfile
#done

echo Creating NSE\'s chartbook
R CMD BATCH $CHARTINGDIR/nse.R
echo All done 
cd $DIR
