#!/bin/bash -x

# April 16th, 2017, Sunday, 14:00 (afternoon) Pune :)
# Sometimes it is desirable to force a particular scratch file to
# generate a candlefile here and now. An obvious example would be the
# Thu before Good Friday. The London AM fix will terminate both
# the Fri-Thu _and_ the Mon-Fri data files.
# So, the moment we have the Thu am data, we can coerce the Mon-Fri.scratch
# file to update the Mon-Fri candle datafile
#
# Input: 

# the name of the weekly data-file that has to be force updated
f=$1

# the scratch-file is thereabouts :)
scratchFile=$f".scratch"

#wc -l $f $s

# Determine the DOHLC
o=$(head -n 1 $scratchFile | awk '{print $2}' ) ; echo "Open is " $o
c=$(tail -n 1 $scratchFile | awk '{print $2}' ) ; echo "Close is " $c
weekDate=$(tail -n 1 $scratchFile | awk '{print $1}' ) ; echo "Week's date is " $weekDate
h=$(awk -v high=0 ' { if (high < $2) {high = $2 ; } } END { print high; } ' $scratchFile ) ; echo "High is " $h
l=$(awk -v low=$h ' { if (low > $2) {low = $2 ; } } END { print low; } ' $scratchFile ) ; echo "Low is " $l

# append to data-file
echo $weekDate $o $h $l $c | tee -a $f

echo Now we need to truncate the scratch file $scratchFile
> $scratchFile

