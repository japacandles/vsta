!/bin/bash -x

. $HOME/vsta/dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

# Long term charts for gold
for i in $DATADIR/uk/lbma/futures/gold_am
do
    
    file=$(basename $i)
    
    ./r-prep-gold.sh $i 2500
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-2500"
    
    ./r-prep-gold.sh $i 1300
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-1300"
    
    ./r-prep-gold.sh $i 700
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-700"
    
    ./r-prep-gold.sh $i 240
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-240"
    
    ./r-prep-gold.sh $i 60
    awk '{print $1, $2, $3, $4, $5}' /tmp/$file > /tmp/$file"-60"
    
done

# futures charts from CME
for i in $(ls $DATADIR/misc/quandl/futures/CME_GC1 \
	      $DATADIR/us/nyseArca/futures/UGL \
	      $DATADIR/us/nyseArca/futures/GLL \
	      $DATADIR/us/cme/futures/Gold-FEB-2019 \
	      $DATADIR/us/cme/futures/Gold-APR-2019 \
	      $DATADIR/us/cme/futures/Gold-JUN-2019 \
	      $DATADIR/us/cme/futures/Gold-AUG-2019 \
	      $DATADIR/us/cme/futures/Gold-OCT-2019 \
	      $DATADIR/us/cme/futures/Gold-DEC-2019 \
	      $DATADIR/us/cme/futures/Gold-FEB-2020 \
	      $DATADIR/us/cme/futures/Gold-APR-2020 \
	      $DATADIR/us/cme/futures/Gold-JUN-2020 \
	      $DATADIR/us/cme/futures/Gold-DEC-2020 \
	      $DATADIR/us/cme/futures/Gold-JUN-2021 \
	      $DATADIR/us/cme/futures/Gold-DEC-2021 \
	      $DATADIR/uk/lbma/futures/gold_am.Mon-Fri \
	      $DATADIR/uk/lbma/futures/gold_am.Tue-Mon \
	      $DATADIR/uk/lbma/futures/gold_am.Wed-Tue \
	      $DATADIR/uk/lbma/futures/gold_am.Thu-Wed \
	      $DATADIR/uk/lbma/futures/gold_am.Fri-Thu \
	  )
do
    
    ./r-prep.sh $i 180
done


R CMD BATCH ~/vsta/charting/gold-only.R
echo All done 
cd $DIR
