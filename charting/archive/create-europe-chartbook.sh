#!/bin/bash -x

. $HOME/vsta//dbms/vsta-skel/configs.sh

DIR=$PWD
cd $CHARTINGDIR

for i in `ls $DATADIR/de/deutsche-boerse-frankfurt/futures/dax $DATADIR/uk/london/futures/ftse`
do 

	./r-prep.sh $i 60
	threeMonths=$(basename $i)
	awk '{ print $1, $2, $3, $4, $5, $6 }' /tmp/$threeMonths > /tmp/$threeMonths"-60"

	./r-prep.sh $i 260
	oneYear=$(basename $i)
	awk '{ print $1, $2, $3, $4, $5, $6 }' /tmp/$oneYear > /tmp/$oneYear"-260"

	./r-prep.sh $i 1300
	fiveYear=$(basename $i)
	awk '{ print $1, $2, $3, $4, $5 }' /tmp/$fiveYear > /tmp/$fiveYear"-1300"

	./r-prep.sh $i 2500
	tenYear=$(basename $i)
	awk '{ print $1, $2, $3, $4, $5 }' /tmp/$tenYear > /tmp/$tenYear"-2500"

	./r-prep.sh $i 180
done 

#for i in $DATADIR/de/eurex-eod/futures/*2016  $DATADIR/de/eurex-eod/futures/*2017
#do
#	./r-prep.sh $i 180
#done

#for i in $DATADIR/uk/nyx-nyse-liffe-eod/futures/FTSE_100-INDEX-FUTURE-*-2016
#do
#	./r-prep.sh $i 180
#done

echo Creating the Europe chartbook 
R CMD BATCH europe.R
cd $DIR
