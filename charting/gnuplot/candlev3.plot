reset
set terminal x11
set multiplot
set size 1, 0.7
set origin 0, 0.3
set grid
set offsets 0, 1, 1, 1
plot "/tmp/SCRIP" using 0:2:3:4:5 notitle with candlesticks lt 3

set size 1,0.3
set origin 0, 0
#unset ytics
set grid
#plot "/tmp/SCRIP" using 0:6 notitle with impulse lt 6
plot "/tmp/SCRIP" using 0:6 notitle with lines lt 6

unset multiplot
pause -1
