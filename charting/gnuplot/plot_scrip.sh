#!/bin/sh 

EXG=$1
SCRIP=$2
STARTREC=$3
AWK=`which awk`

$AWK -vSTARTREC=$STARTREC ' { if ( FNR > STARTREC ) {print NR, $2, $4, $3, $5}} ' ~/vsta/data/in/$EXG/futures/$SCRIP > SCRIP
$AWK -vSTARTREC=$STARTREC ' { if ( FNR > STARTREC ) {print NR, $6}} ' ~/vsta/data/in/$EXG/futures/$SCRIP > VOLUME
gnuplot candlev-afterR.plot

