#!/bin/sh 

EXG=$1
SCRIP=$2
STARTREC=$3
AWK=`which awk`

$AWK -vSTARTREC=$STARTREC ' { if ( FNR > STARTREC ) {print NR, $2, $3, $4, $5}} ' ../../../data/$EXG/$SCRIP > SCRIP
$AWK -vSTARTREC=$STARTREC ' { if ( FNR > STARTREC ) {print NR, $6}} ' ../../../data/$EXG/$SCRIP > VOLUME
perl -ei 's/SCRIP/$SCRIP' candlev2.plot
gnuplot candlev2.plot

