reset
#set terminal png
set multiplot
set size 1, 0.7
set origin 0, 0.3
set grid
plot "/tmp/SCRIP" using 0:5 notitle  with lines lt 3

set size 1, 0.3
set origin 0, 0
plot "/tmp/SCRIP" using 0:6 notitle with impulse lt 6
#plot "/tmp/SCRIP" using 0:7 notitle with lines lt 6

unset multiplot
pause -1
