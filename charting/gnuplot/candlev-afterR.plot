unset border
set multiplot
set size 1, 0.7
set origin 0, 0.3
#set style candlesticks
set grid
plot "SCRIP" with candlesticks
set size 1, 0.3
set origin 0,0
#set data style boxes
set grid
plot "VOLUME" with impulses
set nomultiplot
pause -1

