unset border
unset label
set multiplot
set boxwidth 0.5 relative
set size 1, 0.6
set origin 0, 0.4
set data style candlesticks
set grid
plot "/tmp/SCRIP"
set size 1, 0.4
set origin 0,0
set data style boxes
set grid
plot "/tmp/VOLUME"
set nomultiplot
pause -1

