#!/bin/bash -x

exg=$1
type=$2
commodity=$3

# exit with usage message if called without a exg arg
if [ "$exg" == "" ]; then
    echo "Usage: ./chart.sh ../country/exchange"
    echo "   Example:"
    echo "   ./chart.sh ../jp/tse"
    exit 1
fi
exchange=$(echo $exg | sed "s/\.\.//g")

# initialize environment variables
. $VSTA_DIR/configs.sh

TOP_DIR=$(pwd) ; export TOP_DIR

cd $exg

# if commodity is not specified, we want to 
# chart everythig defined in that vars.sh
if [ "$commodity" == "" ]; then
    . ./vars.sh
    for line in ${commodities[@]}; do
	commodity=$(echo $line | awk -F'=' '{print $1}')
	echo commodity is $commodity; #read

	sec_list_csv=$(echo $line | awk -F'=' '{print $2}')
	for security in $(echo $sec_list_csv | sed 's/,/ /g'); do
	    ls $VSTA_DATA_DIR/$exchange/$type | grep $security
	    if [ $? -eq 0 ]; then
		cd $TOP_DIR
		./chart.sh $exg $type $commodity
				
	    fi
	done
    done
else # since commodity _was_ specified, thats what we will chart
    ./chart.sh $exg $type $commodity
fi

echo $?
cd $TOP_DIR
