#!/bin/bash -x

##############################################################################################
#
# r-prep.sh
#
# Apr 14, 2012, Sat
# This script makes the data-file (ARGV[1]) compatible with the zoo time-series package
# in R.
# Example usage:
# r-prep.sh /opt/vsta/data/uk/london/futures/ftse 100
# will create /tmp/ftse out of the last 100 records of ftse
# Now inside R, run the commands
# library('quantmod')
# ftse <- read.zoo("/tmp/ftse", header = TRUE, sep = " ")
# chartSeries(ftse, theme="white", TA="addVo();addSMA(21);addSMA(62);addRSI();addBBands();")
# This will generate a rather nice chart.
#
# Changelog:
# Aug 30, 2014, Sat
# I don't have volumes for some days. On those days, R
# returns a subscript out of range (not 6 fields) error
# So I modified the awk line to check if the 5th field existed at all
# and if it did not, to set it to 0
##############################################################################################

file=$1

WHICH=/usr/bin/which
ECHO=$($WHICH echo)
BASENAME=$($WHICH basename)
TAIL=$($WHICH tail)
AWK=$($WHICH awk)
SED=$($WHICH sed)
TEE=$($WHICH tee)

PERIODS=$2
SCRIP=$($BASENAME $file)
#echo Scrip is $SCRIP

$ECHO "Date Open High Low Close Volume" | $TEE /tmp/$SCRIP
$TAIL -$PERIODS $file | $AWK '{ print $1, $2, $2, $2, $2, 0 }' | $TEE -a /tmp/$SCRIP
# check if $6 (volume) exists, and if it does not, set it to zero
#$TAIL -$PERIODS $file | $TEE -a /tmp/$SCRIP

