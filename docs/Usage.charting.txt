
Very Simple Technical Analysis Charting Usage Guide
===================================================

1. To generate all the charts, 

   cd $VSTA_DIR/charting/vsta-skel/ ; rm -rvf /var/tmp/vsta/charting/* ; mkdir -pv /var/tmp/vsta/charting ; for u in ../uk/london/ ../uk/lbma ../de/eurex/ ../de/deutsche-boerse-frankfurt/ ../us/nyse/ ../us/nasdaq/ ../us/cme/ ../misc/forex/ ; do ./chart.sh $u futures ; done

   All charts will be generated under /var/tmp/vsta/charting .
   
   The E-mini-SP-500 futures' charts will be under
   /var/tmp/vsta/charting/us/cme/futures/USIDX
   The E-mini-Nasdaq futures charts will be there too :)

   The S&P 500 Index itself is charted under
   /var/tmp/vsta/charting/us/nyse/futures/SP500/

   The futures' charts of crude oil are under
   /var/tmp/vsta/charting/us/cme/futures/Crude/

   I encourage you to explore /var/tmp/vsta/charting after running the
   above command
   
2. To generate charts of only S&P 500 index
   cd $VSTA_DIR/charting/vsta-skel/
   ./chart.sh ../us/nyse/ futures SP500

   The charts are under
   /var/tmp/vsta/charting/us/nyse/futures/SP500

3. To educate yourself about how the above command works, please read
   $VSTA_DIR/charting/us/nyse/vars.sh and
   $VSTA_DIR/charting/us/nyse/chart.sh

   It will help when you progress to the Developers' Guide
