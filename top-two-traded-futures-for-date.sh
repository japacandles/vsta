#!/bin/bash

#set -x

# top-two-traded-futures-by-dates.sh
# 
#
commodity=$1
d=$2 # date in YYYYmmdd or YYYY-mm-dd format : OPTIONAL

# if commodity is not provided, show usage and exit
print_usage () {
    
    echo "top-two-traded-futures-by-dates.sh"
    echo "Usage: top-two-traded-futures-by-dates.sh $COMMODITY"
    echo "Example: top-two-traded-futures-by-dates.sh /path/to/data/Gold"
    echo "or"
    echo "Example: top-two-traded-futures-by-dates.sh /path/to/data/OrangeJuice YYYYmmdd"
    echo "For a given commodity, this program outputs a list"
    echo "of dates on which that commodity's futures were traded"
    echo "and the names of those futures which had the highest"
    echo "volumes on those dates"
    echo "Example 1:"
    echo "top-two-traded-futures-by-dates.sh /path/to/data/Gold"
    echo " ... "
    echo " ... "

}

# If the user hasn't supplied a commodity
# print usage and exit
if [ -z "$commodity" ]; then
    echo "Missing argument"
    print_usage
    exit 1
fi

# If the commodity file doesn't exist
# say that, print usage and exit
ls $commodity* 2>&1 > /dev/null
if [ $? -ne 0 ]; then
    echo "Data files $commodity don't exist"
    print_usage
    exit 2
fi

# if we are here, there is at least one data file :)

# we begin by defining some vars that we will use throughout
DATA_FILE_LIST=/var/tmp/tradeable-futures
comm_files=$(basename $commodity)
comm_files=/tmp/$comm_files

# create a list of datafiles from which to look for dates
grep "$commodity" $DATA_FILE_LIST > $comm_files

# we create a file into which we simply dump all the dates
# from all the datafiles. We will call this file of dates $foo :)
foo=$(mktemp); > $foo
# we assume that each data file will be a spc delimited DOHLC{V}
for f in `cat $comm_files`; do
    awk '{print $1}' $f >> $foo
done
# make sure that each date occurs only once in $foo
sort -nu $foo -o $foo

# now that $foo has each date on which at least one future
# was traded, we loop through it and find out which two futures
# had the highest volume on that date
for d in $(cat $foo); do
    for line in $(grep $d `cat $comm_files` | sort -nr -k6 | head -n2 | sed 's/ /,/g'); do
        f=$(echo $line | awk -F: '{print $1}')
        v=$(echo $line | awk -F, '{print $6}')
        echo $d $f $v
    done
done

exit 0
