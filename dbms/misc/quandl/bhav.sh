#!/bin/bash -x

# Sep 8, 2018, Sunday
# Extremely rudimentary bhav script to get continuous 
# futures of Crude oil and Gold from quandl.
# to be called as
# . $HOME/vsta/dbms/vsta-skel/configs.sh ; cd DBMSDIR=$VSTADIR/dbms/vsta-skel ; ./bhav.sh ../misc/quandl

# read local conf to get the api key and the urls
. quandl.conf
#echo $KEY $URL

this_dir=$(pwd)
datadir=`echo $this_dir | sed 's/dbms/data/'`"/futures"
#ls -l $datadir

d1=$(TZ=America/Chicago date -d '1 week ago' +%Y-%m-%d)
d2=$(TZ=America/Chicago date +%Y-%m-%d)

for commodity in ${commodities[@]}; do
#for c in ICE_T1 CME_GC1
#do
	datafile=$datadir/$commodity
	#echo $datafile

	# the whole data : uncomment this to create the data file
	#curl -v --ipv4 -s "$URL/$commodity.csv?end_date=$d2\&api_key=$KEY" | sed 's/,/ /g' | awk '{print $1, $2, $3, $4, $5, $8, $9}' | grep -v Date | tac | tee -a $datafile
	
	# last day : to be used every day
	curl -v --ipv4 -s "$URL/$commodity.csv?start_date=$d1\&end_date=$d2\&api_key=$KEY" | sed 's/,/ /g' | awk '{print $1, $2, $3, $4, $5, $8, $9}' | grep -v Date | tac | tee -a $datafile

	sort -u $datafile -o $datafile
done

