#!/bin/bash

# 4 Jan 2010
# Simply gets and appends the dax data
# using bloomberg-index.pl and bb.sh

datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/dax"
echo $datafile
cd $TOP_DBMS_DIR
./bb.sh Europe/Frankfurt /tmp dax:ind | tee -a $datafile