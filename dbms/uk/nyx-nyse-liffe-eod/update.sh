#!/bin/bash

bhavcopy=$1
if [ ! -f $bhavcopy ]
then
	echo "Bhavcopy $bhavcopy doesn't exist"
	exit 1
fi

inDir=$(dirname $bhavcopy)
outDir=$(echo $inDir | sed 's/bhavcopies/futures/')

#echo $bhavcopy $inDir $outDir

for record in $(cat $bhavcopy)
do

	future=$(echo $record | awk -F, '{print $1}')
	future=$(echo $future | tr -s '[:lower:]' '[:upper:]')
	outFile=$outDir/$future

	dt=$(echo $record | awk -F, '{print $2}')
	open=$(echo $record | awk -F, '{print $3}')
	high=$(echo $record | awk -F, '{print $4}')
	low=$(echo $record | awk -F, '{print $5}')
	close=$(echo $record | awk -F, '{print $6}')
	volume=$(echo $record | awk -F, '{print $7}')
	openInterest=$(echo $record | awk -F, '{print $8}')

	echo Now writing to $outFile
	echo $dt $open $high $low $close $volume $openInterest | \
		tee -a $outFile
	
done
