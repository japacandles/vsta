#!/bin/bash

bhavCopyDate=$(TZ=Europe/CET date +%Y-%m-%d)
#echo $bhavCopyDate

bhavDir=$(echo $PWD | sed 's/dbms/data/')
#echo $bhavDir

bhavCopyFile=$bhavDir"/bhavcopies/"$bhavCopyDate
#echo $bhavCopyFile

D=$(echo $bhavCopyDate | sed 's/-//g')
#echo $D

#perl $TOP_DBMS_DIR/parse-nyx-futures.pl 29089 $D | tee $bhavCopyFile
perl $TOP_DBMS_DIR/parse-barchart.pl FTSE_100 $D | tee $bhavCopyFile
