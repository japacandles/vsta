#!/bin/bash -x

#
# make-nyx-eod-bhavcopies.sh
# Usage: 
# make-nyx-eod-bhavcopies.sh /full/path/to/bhavcopy
# This assumes that the path above contains the word "original" and the
# directory to which the output bhavcopy is to be sent
#
# 
# Wed, Dec 25, 2013
# The bhavcopy is now to be downloaded from 
# http://www.liffe.com/reports/eod?item=Histories
# http://www.liffe.com/reports/eod?item=Histories&archive=-1069674927
# The bhavcopy of Dec 23, 2013 is at
# http://www.liffe.com/data/ds131223ff.csv
#
# Sun, Oct 13, 2013
# Takes the name of a bhavcopy downloaded from
# https://globalderivatives.nyx.com/en/nyse-liffe/end-of-day-files
# and creates a bhavcopy that vsta can update from.
# In this particular case, we look *only* for the FTSE-100 futures
#

inFile=$1
echo Infile is $inFile
#cat $inFile
if [ ! -f $inFile ]
then
	echo "Input file $inFile doesn't exist"
	exit 1
fi

input=$(basename $inFile)
a1=$(echo $input | sed 's/ds//g')
a2=$(echo $a1 | sed 's/ff.csv//g')
a3="20"$a2

outFile=$(echo $a3 | \
	awk 'BEGIN { FS=NULL; OFS=NULL; }
		{print $1, $2, $3, $4, "-", $5, $6, "-", $7, $8}')

inDir=$(dirname $inFile)
outDir=$(echo $inDir | sed 's/original-//g')

#echo $input $inFile $outFile

dt=$(echo $outFile | sed 's/-//g');

# KLUDGE ! FIXME
# I am looking for ",Z  ,"
# I should look for something thats on stdin
for line in $(grep ",Z  ," $inFile | sed 's/ //g')
do
	#echo "Line is " "$line"

	# security name
	n1=$(echo $line | awk -F, '{print $2}')
	#echo "Name is " $n1

	# KLUDGE ! FIXME
	# follows from the above kludge :(
	name="FTSE-100-INDEX-FUTURE-";

	# expiry
	exp1=$(echo $line | awk -F, '{print $3}')
	exp2=$(echo $exp1 | \
	    awk 'BEGIN {FS=NULL; OFS=NULL;}
               {print $1,$2,$3,"-",$4,$5}')
	expMonth=$(echo $exp2 | awk 'BEGIN {FS="-";OFS=NULL;}{print $1}')
	expYear=$(echo $exp2 | awk 'BEGIN {FS="-";OFS=NULL;}{print $2}')
	expM=$(echo $expMonth )
	expY=$(echo "20"$expYear)
	expiry=$(echo $expM"-"$expY)
	#echo "Expiry is " $expiry

	security=$name$expiry
	#echo "Security is " $security

	# volume
	volume=$(echo $line | awk -F, '{print $6}')

	# open Interest
	openInterest=$(echo $line | awk -F, '{print $8}')

	# open
	open=$(echo $line | awk -F, '{print $9}')

	# Any record that doesn't have an open
	# obviously wasn't traded today
	# so we should simply skip the entire record
	if [ "$open" = "" ]
	then
		continue
	fi

	# high
	high=$(echo $line | awk -F, '{print $12}')

	# low
	low=$(echo $line | awk -F, '{print $13}')

	# settlement
	settlement=$(echo $line | awk -F, '{print $14}')

	#echo "Now for the line"
	echo $security","$dt","$open","$high","$low","$settlement","$volume","$openInterest 2> /tmp/err | \
		tee -a $outDir/$outFile

done
