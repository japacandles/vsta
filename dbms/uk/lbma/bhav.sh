#!/bin/bash -x

# Mar 5, 2017, Sunday, Pune, 07:30 AM :)
# to create data files of gold and silver fix prices from lbma

. ../../configs.sh 

# this is the data file to which we will append
#commodity=$1
#datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/$commodity"
#wc -l $datafile

# this is the file to which we will dump the html
a=$(mktemp)

# today's date
d=$(TZ=Europe/London date -d "1 day ago" +%Y-%m-%d)
echo -n "$d " | tee -a $datafile

# get the quotes in html from kitco
wget -A "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0" https://www.kitco.com/gold.londonfix.html -O $a.html

# finally, parse the html and append to the appropriate datafile
#cd $DBMSDIR/vsta-skel

# this is the data file to which we will append
for commodity in "$@"
do
    echo $commodity
    datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/$commodity"
    wc -l $datafile
    cd $DBMSDIR/vsta-skel
    echo -n "$d " >> $datafile
    perl parse-kitco-daily-gold-silver.pl $a.html $commodity | tee -a $datafile
    cd -
done
