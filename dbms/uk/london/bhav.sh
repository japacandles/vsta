#!/bin/bash

# 8 Dec 2015
# moved back to bloomberg because I am tired of reuters choking on ftse
#
# 3 June 2015
# Moved to reuters because bloomberg stopped providing ohlcv quotes
#
# 4 Jan 2010
# a. gets and appends the dax data
#    using bloomberg-index.pl and bb.sh
#    OR
#    yf3.pl and yf.sh
# b. creates bhavcopy of FTSE futures from euronext

datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/ftse"
echo updating $datafile with FTSE
cd $TOP_DBMS_DIR
#./bb.sh Europe/London /tmp ukx:ind | tee -a $datafile
DATE=$(TZ=Etc/GMT date +%Y%m%d)
echo -n "$DATE " | tee -a $datafile
#perl parse-reuters.pl ftse | tee -a $datafile
#perl bloomberg-index-2015-10-02.pl UKX:IND | tee -a $datafile
#perl parse-reuters.pl ftse | tee -a $datafile
perl parse-money-cnn.pl world_markets ftse100 | tee -a $datafile
