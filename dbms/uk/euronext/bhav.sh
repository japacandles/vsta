#!/bin/bash

# 31 July 2011
# returns the retval of the web-scraper

# 4 Jan 2010
# a. creates a bhavcopy of fdax futures

# Need to save this dir
this_dir=$PWD
cd $TOP_DBMS_DIR

# ftse futures
bhavdate=`zdump /usr/share/zoneinfo/Europe/London | sed 's/  / /g' | awk -f futures-date-fmt.awk`
#echo bhavdate is $bhavdate
datafile=`echo $this_dir | sed 's/dbms/data/'`"/bhavcopies""/$bhavdate"
echo updating $datafile with euronext ftse  futures
#./parse-euronext-futures.pl z dlon | tee -a $datafile
./parse-euronext-futures.pl z dlon >> $datafile
#./parse-euronext-futures.pl z dlon
RETVAL=$?
cat $datafile
exit $RETVAL
