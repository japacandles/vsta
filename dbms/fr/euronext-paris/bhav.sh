#!/bin/bash

#
# Sep 18, 2012, Tue, 07:31 AM
# Including the CAC40 as a tracked index in vsta
# This script should be run at the same time as the dax and 
# the ftse bhav scripts
#

WHICH="/usr/bin/which"
ZDUMP=$($WHICH zdump)
DBMSDIR=$PWD
ZONEDIR="/usr/share/zoneinfo"

datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/cac40"
echo updating $datafile with CAC40
cd $TOP_DBMS_DIR

timezone="Europe/Paris"

DATE=$($ZDUMP $ZONEDIR/$timezone | awk -f $TOP_DBMS_DIR/yf.awk)
(echo -n "$DATE ";./parse-money-cnn.pl world_markets cac40) | tee -a $datafile


