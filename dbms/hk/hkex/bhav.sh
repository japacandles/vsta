#!/bin/bash

# 3 june 2015
# Moved to Rueters 'coz bloomberg stopped providing ohlcv quotes
#
# Sep 18, 2012, Tue, 07:31 AM
# Including the Hang Seng as a tracked index in vsta

WHICH="/usr/bin/which"
#ZDUMP=$($WHICH zdump)
DBMSDIR=$PWD
ZONEDIR="/usr/share/zoneinfo"

datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/hang_seng"
echo updating $datafile with Hang Seng
cd $TOP_DBMS_DIR

timezone="Asia/Hong_Kong"

#DATE=$($ZDUMP $ZONEDIR/$timezone | awk -f $TOP_DBMS_DIR/yf.awk)
DATE=$(TZ="$ZONEDIR/$timezone" date +%Y%m%d)
#(echo -n "$DATE ";./parse-money-cnn.pl world_markets hang_seng) | tee -a $datafile
# CNN Money seems to have made changes on Sep 26, 2013.
# Switching to bloomberg
#(echo -n "$DATE "; ./bb.sh Asia/Hong_Kong hsi:ind) | tee -a $datafile
#(./bb.sh Asia/Hong_Kong "" hsi:ind) | tee -a $datafile

echo -n "$DATE " | tee -a $datafile
#perl parse-reuters.pl hsi | tee -a $datafile
perl parse-money-cnn.pl world_markets hang_seng | tee -a $datafile
