#!/bin/bash -x

source $TOP_DBMS_DIR/configs.sh

d=$(TZ=America/New_York date +%Y%m%d) ; 
for s in $(find ~/vsta/data/se/nasdaqNordic -type f); 
do 
	n=$(basename $s); 
	echo -n "$d " | tee -a $s ; 
	perl $TOP_DBMS_DIR/parse-yahoo-finance.pl $n | sed 's/,/ /g' | tee -a $s ; 
done
