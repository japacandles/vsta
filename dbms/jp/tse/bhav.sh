#!/bin/bash

# 3 June 2015
# Moved to Reuters because bloomberg has stopped providing 
# ohlcv quotes
#
# 31 July 2011
# Returns the retval of the web-scraper
# 4 Jan 2010
# a. gets and appends the Nikkei 225 data
#    using bloomberg-index.pl and bb.sh
#    OR
#    yf3.pl and yf.sh
# b. creates bhavcopy of FTSE futures from euronext

datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/n225"
echo updating $datafile with Nikkei
cd $TOP_DBMS_DIR
d=$(TZ="Asia/Tokyo" date +%Y%m%d)
#I=$(perl parse-reuters.pl n225)
I=$(perl parse-money-cnn.pl world_markets nikkei225)
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
	exit $RETVAL
fi
echo $d $I | tee -a $datafile
#echo $I >> $datafile
exit $RETVAL
