#!/usr/bin/perl

#
# yf-au.pl
# Rewriting the program to scrape data
# from finance.yahoo.com
#
# Changelog
# Sun, Mar 3, 2013
# Initial commit
#

use strict;
use warnings;
use diagnostics;
use utf8;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;

my $idx = shift;
my $idx_name = shift;
my %hash;
my $tmp;
my $url = 'http://au.finance.yahoo.com/q?s=^' . $idx;

my $u = LWP::UserAgent->new();
$u->agent("Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");

my $r = $u->get($url);
if ($r->is_success) {
    #print $r->decoded_content;

    my $p = HTML::TokeParser->new(\$r->decoded_content);

    $p->get_tag("h2"); $p->get_tag("h2"); 
    $p->get_tag("p");
    $p->get_tag("h2"); $p->get_tag("h2"); 
    $p->get_tag("span"); $p->get_tag("span"); $p->get_tag("span");

    # close
    $hash{'close'} = $p->get_trimmed_text("/span");
    $hash{'close'} =~ s/,//g;

    # timestamp
    $p->get_tag("span"); $p->get_tag("span"); 
    $p->get_tag("span"); $p->get_tag("span");
    $hash{'timestamp'} = $p->get_trimmed_text("/span");
    $hash{'timestamp'} =~ s/ /-/g;

    $p->get_tag("table");
    $p->get_tag("tr"); $p->get_tag("tr"); 
    
    # open
    $p->get_tag("td");
    $hash{'open'} = $p->get_trimmed_text("/td");
    $hash{'open'} =~ s/,//g;

    # low
    $p->get_tag("td");
    $p->get_tag("span"); $p->get_tag("span"); 
    $hash{'low'} = $p->get_trimmed_text("/span");
    $hash{'low'} =~ s/,//g;

    $p->get_tag("span"); $p->get_tag("span"); 
    $hash{'high'} = $p->get_trimmed_text("/span");
    $hash{'high'} =~ s/,//g;

    #print Dumper(\%hash);
    print join " ", $hash{'open'}, $hash{'high'}, 
    	$hash{'low'}, $hash{'close'}, $hash{'timestamp'}, "\n";

} else {

    print "Problems getting $idx from $url\n";

}

