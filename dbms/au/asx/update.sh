#!/bin/bash

bhavcopy=$1
date=`basename $bhavcopy | awk -F- '{print $3$2$1}'`
datadir=`echo $PWD | sed 's/dbms/data/;'`"/futures/"

#echo datadir is $datadir
for i in `cat $1`; 
do  
    
    echo $i | grep --quiet FUTIDX  ; 
    if [ $? -eq 0 ]; then 
    	#echo line; 
    	#echo $i;  
    
	line=`echo $i | sed 's/,/ /g'`; 
        #echo new line;
        #echo $line;  

	file=`echo $line | awk '{print $2"-"$3}'`; 
	#echo file is $file; 
	record=`echo $line | awk -v nr=6 'BEGIN {ORS=" "} 
                                     {for (x=nr; x<=NF; x++) 
					 #{if (x == 10) continue;
					     print $x#}
					 }'`; 
	echo appending to file $datadir/$file
	echo $date $record | tee -a $datadir/$file;
    fi 
done
