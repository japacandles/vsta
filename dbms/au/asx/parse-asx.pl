#!/usr/bin/perl

# parse-asx.pl
# Input: name of the html page where the asx
#        futures data is displayed
#        Example: to get ASX 200 futures, the page is rtp15sfAP.html 
#                 So
#                 /path/to/vsta/dbms/au/asx/parse-asx.pl rtp15sfAP.html 
# Output : the price data in bhavcopy format
#          Sample output
#INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,CONTRACTS,TIMESTAMP,
#FUTIDX,SPI200,Mar13,0,XX,4999.0,5016.0,4995.0,5008.0,4108,16/2/13-07:59,
#

#
# Changelog
# Feb 17, 2013, Sunday, 00:49 AM :)
# Initial code, to be tested on Monday, Feb 18, 2013

use strict;
use diagnostics;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;

my %hash;
my $security;
my $expiry;

my $asxBaseUrl = 'http://www.sfe.com.au/content/prices/';
my $secPage = shift;
my $url = $asxBaseUrl . $secPage ;

# print $url, "\n";

my $ua = LWP::UserAgent->new();
my $response = $ua->get($url);
if ($response->is_success) {

    my $p = HTML::TokeParser->new(\$response->decoded_content);

    # The first table, 8th row contains the name of the security 
    $p->get_tag("table");
    $p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr");
    $p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr");
    $p->get_tag("td");
    $security = $p->get_trimmed_text("/td");
    $security =~ s/\s+//g; # remove spaces

    # The second table ; the first row is captions
    $p->get_tag("table"); $p->get_tag("tr");

    # The infinite loop to read the rest of the rows
    #print Dumper(\%hash);
    while(1) {

	#print Dumper(\%hash);
	$p->get_tag("tr");

	# expiry
	$p->get_tag("td");
	my $expiry = $p->get_trimmed_text;
	$expiry =~ s/\s+//g;
	my $key = $security . "-" . $expiry;

	# The next two rows are ask and bid
	$p->get_tag("td"); $p->get_tag("td");

	# open
	$p->get_tag("td");
	$hash{$key}->{'open'} = $p->get_trimmed_text("/td");
	if ($hash{$key}->{'open'} !~ /\d/) {
	    delete ($hash{$key});
	    $p->get_tag("/tr");
	    
	    # Since open data isn't there, we should skip to the
	    # next record - unless this is the last record. If it 
	    # is the last record, we should quit
	    my $token1 = $p->get_token();
	    my $token2 = $p->get_token();
	    if ($token2->[1] =~ /table/) {
		#print (Dumper($token2));
		last;
	    } else {
		$p->unget_token(($token1, $token2));
		next;
	    }
	}

	# high
	$p->get_tag("td");
	$hash{$key}->{'high'} = $p->get_trimmed_text("/td");

	# low
	$p->get_tag("td");
	$hash{$key}->{'low'} = $p->get_trimmed_text("/td");

	# close
	$p->get_tag("td");
	$hash{$key}->{'close'} = $p->get_trimmed_text("/td");
 
	# last  trade date
	$p->get_tag("td");
	my $last_trade_date = $p->get_trimmed_text("/td");

	# last  trade time
	$p->get_tag("td");
	my $last_trade_time = $p->get_trimmed_text("/td");

	$hash{$key}->{'timestamp'} = $last_trade_date . "-" . $last_trade_time;

	# change
	$p->get_tag("td");

	# volume
	$p->get_tag("td");
	$hash{$key}->{'volume'} = $p->get_trimmed_text("/td");

	$p->get_tag("/tr");

	# Check whether this is the last record
	# if it is, then we should leave this infinite loop
	# if not, we should simply loop again
	my $token1 = $p->get_token();
	my $token2 = $p->get_token();
	if ($token2->[1] =~ /table/) {
	    #print (Dumper($token2));
	    last;
	}
	$p->unget_token(($token1, $token2));
    }
} else {

    print "Problems getting $url\n";
    exit(1);

}

# priont the bhavcopy
# print Dumper(\%hash);
print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,CONTRACTS,TIMESTAMP,", "\n";
for my $key (keys %hash) {
    my ($sec, $exp) = split /-/, $key;
    print join ",", "FUTIDX", $sec, $exp, "0,XX",
    $hash{$key}->{'open'}, $hash{$key}->{'high'},
    $hash{$key}->{'low'}, $hash{$key}->{'close'},
    $hash{$key}->{'volume'}, $hash{$key}->{'timestamp'}, "\n";
}
