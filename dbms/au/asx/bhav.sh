#!/bin/bash 

#
# Mar 19, 2016, Tuesday, 19:40
# tradingroom.com.au is no longer usable
# Moving to marketindex.com.au 
#
# Feb 16, 2014, Sunday, 00:25 :-)
# Since papdan has discontinued their "preview" site, its will
# be impossible to get australian depth. So, the volume will be
# taken from the parse-tradingroom-au.pl output.
# I am not refactoring away all the papdan stuff yet because I 
# think they'll start providing the data again :-)
#
# 28 Sep 2013
# Refactored
# Added support for tradingroom.com.au
#
# 4 Mar 2013
# Added support for asx20, asx50, asx100 and asx300
#
# 7 Jul 2012
# Updates asx-aord and asx-200 future data

RM=$(which rm)
WGET=$(which wget)
ECHO=$(which echo)
AWK=$(which awk)
DATE=$(which date)
TOUCH=$(which touch)
SED=$(which sed)
TEE=$(which tee)

# Need to save this dir
this_dir=$PWD

datadir=$($ECHO $PWD | $SED 's/dbms/data/')"/futures/"

echo Datadir is $datadir
cd $TOP_DBMS_DIR

#####################

# Date of running this script
d=$(TZ=Australia/Sydney $DATE +%Y%m%d)

DBMSDIR=$HOME/vsta/dbms/vsta-skel
DATADIR=$HOME/vsta/data/au/asx/futures
#DATADIR=/tmp
BHAVCOPYDIR=$HOME/vsta/data/au/asx/bhavcopies
#BHAVCOPYDIR=/tmp

d=$(TZ="Australia/Sydney" date +%Y%m%d)

echo -n "$d " | tee -a $DATADIR/asx-20
#perl $DBMSDIR/parse-marketindex.pl xtl asx-20 | tee -a $DATADIR/asx-20
echo -n "$d " | tee -a $DATADIR/asx-50
#perl $DBMSDIR/parse-marketindex.pl xfl asx-50 | tee -a $DATADIR/asx-50
echo -n "$d " | tee -a $DATADIR/asx-100
#perl $DBMSDIR/parse-marketindex.pl xto asx-100 | tee -a $DATADIR/asx-100
echo -n "$d " | tee -a $DATADIR/asx-200
#perl $DBMSDIR/parse-marketindex.pl xjo asx-200 | tee -a $DATADIR/asx-200
echo -n "$d " | tee -a $DATADIR/asx-300
#perl $DBMSDIR/parse-marketindex.pl xko asx-300 | tee -a $DATADIR/asx-300
echo -n "$d " | tee -a $DATADIR/asx-aord
#perl $DBMSDIR/parse-marketindex.pl xao asx-all | tee -a $DATADIR/asx-aord

#####################

bhavcopy=$(TZ="Australia/Sydney" date +%d-%m-%Y)
echo Bhavcopy is at $BHAVCOPYDIR/$bhavcopy
#$this_dir/parse-asx.pl  rtp15sfAP.html | tee -a $BHAVCOPYDIR/$bhavcopy

echo Current dir is $PWD
exit 0
