#!/bin/bash

# 16 Feb 2011
# Simply gets and appends the dax data
# using bloomberg-index.pl and bb.sh
# OR
# yf3.pl and yf.sh

this_dir=$PWD
datadir=`echo $this_dir | sed 's/dbms/data/'`"/futures"
datafile=$datadir"/bvsp"
echo updating $datafile with Bovespa
cd $TOP_DBMS_DIR
#./bb.sh America/New_York /tmp ibov:ind | tee -a $datafile
d=$(TZ=America/Sao_Paulo date +%Y%m%d)
echo -n "$d " | tee -a $datafile
perl parse-reuters.pl BVSP | tee -a $datafile
