#!/bin/bash

#  
# Sep 22, 2015, Tuesday
# For Kospi Composite (Korea)

set -x

DATE=$(TZ="Asia/Seoul" date +%Y%m%d)

datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/kospi"

echo updating $datafile with Kospi 100
echo -n "$DATE " | tee -a $datafile

cd $TOP_DBMS_DIR
#/usr/bin/GET https://eng.krx.co.kr/m1/m1_1/m1_1_1/JHPENG01001_01.jsp | tee /tmp/kospi.html
#/usr/bin/wget https://eng.krx.co.kr/m1/m1_1/m1_1_1/JHPENG01001_01.jsp -o /tmp/kospi.html
#/usr/bin/perl parse-kospi.pl /tmp/kospi.html | tee -a $datafile
#/usr/bin/perl parse-kospi.pl | tee -a $datafilea
/usr/bin/perl parse-yahoo-finance-json.pl KS11 | sed 's/,/ /g' | tee -a $datafile
