#!/bin/bash -x

# Jan 28, 2018, Sunday
# Wrapper script to get bitcoin reference rate 
# and update my data file

source $HOME/vsta/dbms/configs.sh
#source $HOME/vsta/dbms/vsta-skel/common-functions.sh

outFile="/tmp/bitcoin-reference-$(date +%Y-%m-%d).json" 

#UA=$(user_agent)
UA=$BROWSER

#wget -U 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36' http://www.cmegroup.com//CmeWS/mvc/Bitcoin/All -O $outFile
wget -d -U "$UA"  http://www.cmegroup.com//CmeWS/mvc/Bitcoin/All -O $outFile
cat $outFile

perl parse-cme-bitcoin-json.pl $outFile bitcoin | tee -a $DATADIR/us/cme/futures/bitcoin-reference-rate
perl parse-cme-bitcoin-json.pl $outFile bitcoin | tee -a $DATADIR/us/cme/futures/bitcoin-all
perl parse-cme-bitcoin-json.pl $outFile ethereum | tee -a $DATADIR/us/cme/futures/ethereum-reference-rate

