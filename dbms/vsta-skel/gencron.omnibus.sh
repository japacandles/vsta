#!/bin/bash -x

# gencron.sh
# Jan 3, 2016, Sunday, 16:37, Pune :)
# This script is to be used on Jan 1 of every year to generate the 
# crontab of the year
# Usage: cd /path/to/vsta-skel ; ./gencron.sh
# Inputs: NONE
# Outputs: a file called /tmp/gencron-crontab-<YYYY>

#
# CONFIG SECTION
# These can be changed 
# OUTFILE, SERVER_TZ, CRONTABSPATH, MYMAIL, VSTADIR

# The file to which this crontab is dumped
OUTFILE=/tmp/gencron-crontab-$(date +%Y)
# Create the file and truncate it to zero size
> $OUTFILE

# The server is on this timezone. I have set this to GMT by default
SERVER_TZ="UTC"

# crontab needs to have its the path defined
CRONTABSPATH="/usr/lib64/qt-3.3/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:$HOME/bin"

# The mail to which all output is to be sent
MYMAIL="YOU@EXAMPLE.COM"

# location of vsta
VSTADIR=$HOME/vsta

#Perl 5 libs
PERL5LIB=$HOME/perl5/lib/perl5

#
# MAIN PROGRAM STARTS HERE
#

# Set path and mail
echo "PATH=$CRONTABSPATH" | tee -a $OUTFILE
echo "MAILTO=$MYMAIL" | tee -a $OUTFILE
echo | tee -a $OUTFILE

# Australia ASX
# The ASX is the first to close
## we first populate the script /tmp/get-au-bhav
#echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB ./bhav.sh ../au/asx ; cd -" > /tmp/get-au-bhav
## Now we append the asx lines to the crontab
#echo "# Australia ASX, Sydney " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at http://www.asx.com.au/about/asx-trading-calendar-2016.htm , http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=ASX " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=Australia/Sydney --to-tz=$SERVER_TZ --time=16:45:00  --cmd /tmp/get-au-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

# Japan TSE
## we first populate the script /tmp/get-jp-bhav
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../jp/tse ; cd -" > /tmp/get-jp-bhav
## Now we append the tse lines to the crontab
echo "# Japan TSE, Tokyo" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://www.jpx.co.jp/english/corporate/calendar , http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=TYO" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=Asia/Tokyo --to-tz=$SERVER_TZ --time=15:30:00  --cmd /tmp/get-jp-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# Korea KRX
## we first populate the script /tmp/get-kr-bhav
#echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../kr/krx ; cd -" > /tmp/get-kr-bhav
## Now we append the tse lines to the crontab
#echo "# Korea KRX, Seoul " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=Asia/Seoul --to-tz=$SERVER_TZ --time=15:30:00  --cmd /tmp/get-kr-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

# Hong Kong HKEX
## we first populate the script /tmp/get-hk-bhav
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../hk/hkex ; cd -" > /tmp/get-hk-bhav
## Now we append the tse lines to the crontab
echo "# HKEX, Hong Kong " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at https://www.hkex.com.hk/eng/market/sec_tradinfo/tradcal/nont10.htm , http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=HKG " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=Asia/Hong_Kong --to-tz=$SERVER_TZ --time=16:15:00  --cmd /tmp/get-hk-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# India NSE
## we first populate the script /tmp/get-nse-bhav
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../in/nse ; cd -" > /tmp/get-nse-bhav
## Now we append the NSE lines to the crontab
echo "# India NSE, Mumbai" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://nseindia.com/global/content/market_timings_holidays/market_timings_holidays.htm " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=Asia/Kolkata --to-tz=$SERVER_TZ --time=15:35:00  --cmd /tmp/get-nse-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# India BSE
## we first populate the script /tmp/get-bse-bhav
#echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../in/bse ; cd -" > /tmp/get-bse-bhav
## Now we append the BSE lines to the crontab
#echo "# India BSE, Mumbai" | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at http://www.bseindia.com/markets/marketinfo/listholi.aspx?expandable=0 " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=Asia/Kolkata --to-tz=$SERVER_TZ --time=16:15:00  --cmd /tmp/get-bse-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

# Deutsche Boerse Frankfurt
## we first populate the script /tmp/get-de-bhav
echo "cd $VSTADIR/dbms/vsta-skel; PERL5LIB=$PERL5LIB  ./bhav.sh ../de/deutsche-boerse-frankfurt/; cd -" > /tmp/get-de-bhav
## Now we append the DE lines to the crontab
echo "# Deutsche Boerse Frankfurt" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=FRA " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=Europe/Berlin --to-tz=$SERVER_TZ --time=18:15:00  --cmd /tmp/get-de-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# Eurex for DAX and Mini DAX futures
## we first populate the script /tmp/get-eurex-bhav
#echo " cd $VSTADIR/dbms/vsta-skel; PERL5LIB=$PERL5LIB  ./bhav.sh ../de/eurex-eod/; cd -" > /tmp/get-eurex-bhav
## Now we append the Eurex lines to the crontab
#echo "# Eurex for DAX futures " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at  " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=Europe/Berlin --to-tz=$SERVER_TZ --time=18:45:00  --cmd /tmp/get-eurex-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

# UK London
## we first populate the script /tmp/get-uk-bhav
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../uk/london ; cd -" > /tmp/get-uk-bhav
## Now we append the UK, LSE lines to the crontab
echo "# UK LSE, London" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://www.lseg.com/areas-expertise/our-markets/london-stock-exchange/equities-markets/trading-services/business-days , http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=LSE " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=Europe/London --to-tz=$SERVER_TZ --time=17:15:00  --cmd /tmp/get-uk-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# CME Silver futures
## we first populate the script /tmp/get-cme-silver-bhav
#echo "cd $VSTADIR/dbms/vsta-skel ; ./parse-crb-futures.sh silver America/New_York; cd -" > /tmp/get-cme-silver-bhav
## Now we append the Silver, CME lines to the crontab
#echo "# Silver CME, Chicago" | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at http://www.cmegroup.com/tools-information/holiday-calendar/  " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=12:25:00  --cmd /tmp/get-cme-silver-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

# CME Gold futures
## we first populate the script /tmp/get-cme-gold-bhav
#echo "cd $VSTADIR/dbms/vsta-skel ; ./parse-crb-futures.sh gold America/New_York; cd -" > /tmp/get-cme-gold-bhav
## Now we append the Gold, CME lines to the crontab
#echo "# Gold CME, Chicago" | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at http://www.cmegroup.com/tools-information/holiday-calendar/ " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=12:30:00  --cmd /tmp/get-cme-gold-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

# NYX NYSE LIFFE for FTSE futures
## we first populate the script /tmp/get-nyx-bhav
#echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../uk/nyx-nyse-liffe-eod/ ; cd -  " > /tmp/get-nyx-bhav
## Now we append the FTSE, NYX Liffe lines to the crontab
#echo "# NYX NYSE Liffe, Chicago" | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at https://www.theice.com/publicdocs/Trading_Schedule.pdf " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=Europe/London --to-tz=$SERVER_TZ --time=21:15:00  --cmd /tmp/get-nyx-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

# Brasil Bovespa
## we first populate the script /tmp/get-br-bhav
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../br/bmfbovespa ; cd -" > /tmp/get-br-bhav
## Now we append the Brasil, Bovespa lines to the crontab
echo "# Brazil BVSP, Sao Paulo" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://www.bmfbovespa.com.br/en-us/rules/market-calendar/market-calendar.aspx?idioma=en-us  , http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=SAO " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Sao_Paulo --to-tz=$SERVER_TZ --time=18:20:00  --cmd /tmp/get-br-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# NYSE 
## we first populate the script /tmp/get-nyse-bhav
echo "cd $VSTADIR/dbms/vsta-skel; PERL5LIB=$PERL5LIB  ./bhav.sh ../us/nyse ; cd -" > /tmp/get-nyse-bhav
## Now we append the DJIA+SP500, NYSE lines to the crontab
echo "# NYSE, New York" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at https://www.nyse.com/markets/hours-calendars , http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=NYQ " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=America/New_York --to-tz=$SERVER_TZ --time=16:15:00  --cmd /tmp/get-nyse-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# NASDAQ
## we first populate the script /tmp/get-nasdaq-bhav
echo "cd $VSTADIR/dbms/vsta-skel; PERL5LIB=$PERL5LIB ./bhav.sh ../us/nasdaq ; cd -" > /tmp/get-nasdaq-bhav
## Now we append the Nasdaq 100 lines to the crontab
echo "# Nasdaq 100, New York" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://www.nasdaqtrader.com/Trader.aspx?id=Calendar " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=America/New_York --to-tz=$SERVER_TZ --time=16:16:00  --cmd /tmp/get-nasdaq-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# CME S&P futures
## we first populate the script /tmp/get-cme-sp500-bhav
#echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB ./parse-crb-futures.sh 's&p' America/New_York; cd -" > /tmp/get-cme-sp500-bhav
## Now we append the Gold, CME lines to the crontab
#echo "# S&P 500 CME, Chicago" | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#echo "# Holidays at http://www.cmegroup.com/tools-information/holiday-calendar/ " | tee -a $OUTFILE
#echo "# " | tee -a $OUTFILE
#perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=15:15:00  --cmd /tmp/get-cme-sp500-bhav | tee -a $OUTFILE
#echo | tee -a $OUTFILE

echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB ./bhav.sh ../us/cme/ DJIA ; cd -" > /tmp/get-cme-sp500-bhav
echo "# S&P 500 CME, Chicago" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://www.cmegroup.com/tools-information/holiday-calendar/ " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=15:15:00  --cmd /tmp/get-cme-sp500-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# CME DJI futures
## we first populate the script /tmp/get-cme-dji-bhav
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../us/cme/ SP500 ; cd - " > /tmp/get-cme-dji-bhav
## Now we append the Gold, CME lines to the crontab
echo "# DJI CME, Chicago" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://www.cmegroup.com/tools-information/holiday-calendar/ " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=15:15:00  --cmd /tmp/get-cme-dji-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# CME Nasdaq 100 futures
## we first populate the script /tmp/get-cme-nasdaq-bhav
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../us/cme/ NASD100 ; cd -" > /tmp/get-cme-nasdaq-bhav
## Now we append the Gold, CME lines to the crontab
echo "# Nasdaq 100, CME, New York" | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
echo "# Holidays at http://www.cmegroup.com/tools-information/holiday-calendar/ " | tee -a $OUTFILE
echo "# " | tee -a $OUTFILE
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=15:15:00  --cmd /tmp/get-cme-nasdaq-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# CME gold futures
## we first populate the script /tmp/get-cme-gold-bhav
echo "cd /home/mkpai/vsta/dbms/vsta-skel ; PERL5LIB=$PERL5LIB ./bhav.sh ../us/cme/ Gold " > /tmp/get-cme-gold-bhav
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=16:10:00 --cmd /tmp/get-cme-gold-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# London Gold AM price fix
echo "cd /home/mkpai/vsta/dbms/vsta-skel ; PERL5LIB=$PERL5LIB ./bhav.sh ../uk/lbma/ gold_am " > /tmp/get-lbma-gold-bhav
perl gencron.pl --skip=6 --skip=7 --from-tz=Europe/London --to-tz=$SERVER_TZ --time=11:45:00 --cmd /tmp/get-lbma-gold-bhav | tee -a $OUTFILE
echo | tee -a $OUTFILE

# Bitcoin futures on CME
echo "cd /home/mkpai/vsta/dbms/vsta-skel ; PERL5LIB=$PERL5LIB ./bhav.sh ../us/cme/ BITCOIN ; cd - " > /tmp/get-cme-btc
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=16:15:00 --cmd /tmp/get-cme-btc | tee -a $OUTFILE
echo | tee -a $OUTFILE

# Bitcoin reference rate
echo "cd $HOME/vsta/dbms/vsta-skel ; PERL5LIB=$PERL5LIB sh get-bitcoin-ref.sh ; cd - " > /tmp/brr
perl gencron.pl --from-tz=Europe/London --to-tz=$SERVER_TZ --time=16:15:00 --cmd /tmp/brr | tee -a $OUTFILE
echo | tee -a $OUTFILE

# WTI crude futures on CME
echo "cd /home/mkpai/vsta/dbms/vsta-skel ; PERL5LIB=$PERL5LIB ./bhav.sh ../us/cme/ WTICRUDE ; cd - " > /tmp/wti-crude
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=16:15:00 --cmd /tmp/wti-crude | tee -a $OUTFILE
echo | tee -a $OUTFILE

# ETFs on NYSE ARCA
echo "# ETFs traded on NYSE ARCA" | tee -a $OUTFILE
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../us/nyseArca ; cd -" > /tmp/nyseArca.sh
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=15:15:00 --cmd /tmp/nyseArca.sh | tee -a $OUTFILE
echo | tee -a $OUTFILE

# ETFs on NASDAQ GM
echo "# ETFs traded on NASDAQ GM" | tee -a $OUTFILE
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../us/nasdaqGm ; cd -" > /tmp/nasdaqGm.sh
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=15:15:00 --cmd /tmp/nasdaqGm.sh | tee -a $OUTFILE
echo | tee -a $OUTFILE

# Continuous Futures from quandl
echo "# Continuous Futures from quandl" | tee -a $OUTFILE
echo "cd $VSTADIR/dbms/vsta-skel ; PERL5LIB=$PERL5LIB  ./bhav.sh ../misc/quandl ; PERL5LIB=$PERL5LIB  ./bhav.sh ../us/eia ; cd - " > /tmp/quandl.sh
perl gencron.pl --skip=6 --skip=7 --from-tz=America/Chicago --to-tz=$SERVER_TZ --time=22:15:00 --cmd /tmp/quandl.sh | tee -a $OUTFILE
echo | tee -a $OUTFILE


