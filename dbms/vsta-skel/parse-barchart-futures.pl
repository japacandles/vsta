#!/usr/bin/perl

# parse-crb-futures.pl
# takes a symbol (DJIA or Copper etc) as a single argument on stdin
# and outputs all the futures of that symbol on crbtrader.com.
# See changelog note of June 9
# perl parse-crb-futures.pl "Dollar Index"
# to get US Dollar Index futures OR
# perl parse-crb-futures.pl Gold
# to get only gold related future prices
#
# July 14, 2013
# Misc fixes
#
# June 9, 2013
# Modified the script to accept a single argument
# So the usage is
# perl parse-crb-futures.pl "Dollar Index"
# to get US Dollar Index futures OR
# perl parse-crb-futures.pl Gold
# to get only gold related future prices
#
# May 20-21, past midnight :)
# misc tinkering to get the
# bhavcopy right
#
# May 19, 2013, Sunday, 14:06
# CME data is available from crbdata.com
#
use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
#use DateTime;
use FunctionsVstaHelper;

# Print usage if there isn't an argument
if ($#ARGV != 0) {
    print join "\n",
	"Usage: ",
	"perl parse-barchart-futures.pl <name-of-future>",
	"Examples:",
	"To get S&P 500 (including E-mini), S&P 500 VIX, S&P GSCI, S&P Midcap etc futures : perl parse-barchart-futures.pl 'S&P'",
	"To get Dow Jones futures : perl parse-barchart-futures.pl 'DJI'",
	"To get Wheat futures : perl parse-barchart-futures.pl 'Wheat'",
	"To get Crude oil futures : perl parse-barchart-futures.pl 'Crude'",
	"To get British Pound futures : perl parse-barchart-futures.pl 'British Pound'",
	"To get T-Bond futures : perl parse-barchart-futures.pl 'T-Bond'",
	"To get Bitcoin CBOE futures : perl parse-barchart-futures.pl 'Bitcoin Cboe Futures'",
	"etc",
	"In general, look up the regexes at https://www.barchart.com/futures/major-commodities",
	"and set DEBUG=1 for copius debug information",
	"\n";
    exit 1;
} 

my $DEBUG = 0; if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }

my %hash;

my $barchart_base_data_url = 'https://www.barchart.com/futures/major-commodities';
my $barchart_strange_url = 'http://crb.websol.barchart.com/default.php?&domain=crb&display_ice=1&tz=0&ed=0';
my $barchart_url = 'http://crb.websol.barchart.com/quote.php';
my $barchart_misc = '&domain=crb&display_ice=1&ed=0';

my $m = WWW::Mechanize->new();
$m->agent( random_ua_str() );
    
$m->get($barchart_base_data_url);
$m->get($barchart_strange_url);
die $! unless $m->success();

# we read the page looking for links that might lead to price data
# and populate %hash with those links
for ($m->links) {
    if ($_->url =~ /page=quote/) {
	$DEBUG && print STDERR join " ", $_->text,  $barchart_base_data_url . $_->url, "\n";
	$DEBUG && print STDERR join " ", $_->text, $barchart_url . $_->url . $barchart_misc, "\n\n";

	if (defined $ARGV[0] and $_->text =~ /$ARGV[0]/i) {
	    $DEBUG && print STDERR "Found\n";
	    parse_barchart_data($_->text, $barchart_url . $_->url . $barchart_misc, $m, \%hash);
	} elsif ( !defined $ARGV[0] ) {
	    $DEBUG && print STDERR "Not found: $_->text \n";
	    # I don't like swaps, cattle or hogs, so I skip those
	    next if ($_->text =~ /Swaps/);
	    next if ($_->text =~ /Cattle/);
	    next if ($_->text =~ /Hogs/);
	    parse_barchart_data($_->text, $barchart_url . $_->url . $barchart_misc, $m, \%hash);
	}
    }
}

$DEBUG && print STDERR Dumper(\%hash);

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP," , 
    "OPEN,HIGH,LOW,CLOSE,SETTLE_PR,VOL," ,
    "TIMESTAMP", "\n";

for (keys %hash) {
    my $idx = $_;
    $idx =~ s/(-)+/-/g; # so that "Oil & Gas" 
                        # becomes "Oil-Gas" and not "Oil---Gas"
    print join ",",
          "FUTIDX", $idx,"","0,XX",
          $hash{$_}{'open'}, 
          $hash{$_}{'high'}, 
          $hash{$_}{'low'}, 
          $hash{$_}{'close'},
          $hash{$_}{'close'},
          $hash{$_}{'volume'},
          $hash{$_}{'timestamp'},
          "\n";

}

# Subroutines

##########################################
# parse_crb_data
# creates the hash table from which the
# bhavcopy is generated
##########################################
sub parse_barchart_data {

    my ($security, $url, $m, $hashref) = @_;

    my $DEBUG = 0; if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }
    
    my $token;

    $m->get($url);

    $security =~ s/ //g;
    $security =~ s/\&/n/g;

    my $p = HTML::TokeParser->new(\$m->content);

    $p->get_tag("table"); 
    $p->get_tag("table");
    $p->get_tag("tr");

    while(1) {

	$p->get_tag("td"); my $exp = $p->get_trimmed_text("/td");

	$security =~ s/\\//g; # remove back-slashes
	$security =~ s/\'//g; # remove single quotes
	$security =~ s/\(//g; # remove brackets (
	$security =~ s/\)//g; # remove brackets )
	$security =~ s/ /-/g; # change all spaces to hyphens
	$security =~ s/\#/-/g; # change all hash-marks to hyphens
	$security =~ s/(-)+/-/g; # replace consecutive hyphens with one hyphen

	$exp =~ s/\\//g; # remove back-slashes
	$exp =~ s/\'//g; # remove single quotes
	$exp =~ s/\(//g; # remove brackets (
	$exp =~ s/\)//g; # remove brackets )
	$exp =~ s/ /-/g; # change all spaceS to hyphens
	$exp =~ s/\#/-/g; # change all hash-marks to hyphens
	$exp =~ s/(-)+/-/g; # replace consecutive hyphens with one hyphen

	$DEBUG && print STDERR "Security is ", $security, "\n";
	#print "Expiry is ", $exp, "\n";

	my $key = join "-", $security, $exp;

	$p->get_tag("td"); $hashref->{ $key }->{ 'close'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'close'} = adjust_for_fraction32($hashref->{ $key }->{ 'close'});
	#print "Close is ", $hashref->{ $security . $exp }->{ 'close'}, "\n";

	$p->get_tag("td"); $hashref->{ $key }->{ 'change'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'change'} = adjust_for_fraction32($hashref->{ $key }->{ 'change'});

	$p->get_tag("td"); $hashref->{ $key }->{ 'open'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'open'} = adjust_for_fraction32($hashref->{ $key }->{ 'open'});

	$p->get_tag("td"); $hashref->{ $key }->{ 'high'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'high'} = adjust_for_fraction32($hashref->{ $key }->{ 'high'});

	$p->get_tag("td"); $hashref->{ $key }->{ 'low'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'low'} = adjust_for_fraction32($hashref->{ $key }->{ 'low'});

	$p->get_tag("td"); $hashref->{ $key }->{ 'volume'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'volume'} = adjust_for_fraction32($hashref->{ $key }->{ 'volume'});

	$p->get_tag("td"); $hashref->{ $key }->{ 'prevClose'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'prevClose'} = adjust_for_fraction32($hashref->{ $key }->{ 'prevClose'});

	$p->get_tag("td"); $hashref->{ $key }->{ 'timestamp'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'timestamp'} = adjust_for_fraction32($hashref->{ $key }->{ 'timestamp'});

	$p->get_tag("td"); $hashref->{ $key }->{ 'misc'} = $p->get_trimmed_text("/td");
	$hashref->{ $key }->{'misc'} = adjust_for_fraction32($hashref->{ $key }->{ 'misc'});

	$p->get_tag("/tr");
	$token = $p->get_token(); #print Dumper(\$token);
	$token = $p->get_token(); #print Dumper(\$token);

	# Delete all futures whose volumes are 0 and are not the cash thing
	#if ( $exp !~ /Cash/ && 
	#     ($hashref->{ $security . $exp }->{ 'open' } == 0
	#      or $hashref->{ $security . $exp }->{ 'open' } =~ /^0-0/)) {
	#    delete($hashref->{ $security . $exp });
	#}


	# Some futures have hi = low = close, but open is set to 0
	# So we set the open = high
	$hashref->{$key}->{'open'} = $hashref->{$key}->{'high'} 
	if (($hashref->{$key}->{'high'} == $hashref->{$key}->{'low'})
	    and ($hashref->{$key}->{'low'} == $hashref->{$key}->{'close'}));

	# Some cash futures seem to have open = 0. so I am setting the open = high
	if ( $exp =~ /Cash/ and  $hashref->{ $key }->{ 'open' } == 0) {
	    $hashref->{ $key }->{ 'open' } =  $hashref->{ $key }->{ 'high'};
	}

	# Any non-cash future thats traded today will have a non-zero volume
	if (! defined ($hashref->{ $key }->{ 'volume' }) or
	    ($hashref->{ $key }->{ 'volume' } == 0)) {
	    delete($hashref->{ $key });
	}
	# FIXME: Unfortunately some non-zero volumes have not been traded today :(

	last if ($token->[0] =~ /E/ && $token->[1] =~ /tbody/);
	$p->unget_token(); $p->unget_token();
    }
}

########################################
# adjust_for_fraction32
# accepts a price as string "123-6s"
# returns 123.1875 which is 123+(6/32)
########################################
sub adjust_for_fraction32 {

    my $price = shift or return (0);

    my $DEBUG = 0; if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }
    return (0) if ($price =~ /^Q/);
    
    # remove any s at the end: s = settlement
    $price =~ s/^(.+)s$/$1/; #$DEBUG && print STDERR "Price is $price \n";

    # if there are hyphens, we need to separate the values
    if ($price =~ /-/) { 
	my ($value, $fraction) = split /-/, $price;  $DEBUG && print STDERR "Price is $price , Value is $value, fraction is $fraction \n";
	# now we divide by 32
	if (defined $fraction) { $fraction = $fraction / 32; }
	
	if ((defined $value) and ($value ne "")) { $price = $value + $fraction; }
	else { $price = $fraction; }
    }

    return $price;
}
