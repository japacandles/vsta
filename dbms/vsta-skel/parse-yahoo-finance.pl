#!/usr/bin/perl

# Jan 1, 2019,
# Rewritten again :)
# - functions are now delegated FunctionsVstaYahooFinance
# - can dump a future chain or the price data of a single security 
#   depending on input
# - Input: name of the security
# - Output: 
#    - if the name contains a "=F", then we are dealing with a futures chain, 
#      so the entire futures chain is dumped in space delimited form
#    - if the name does not contain '=", the we are dealing with a security, 
#      so we dump the data in a comma-delimited form (so that the prices,
#      which will make it easy to assemble in bhavcopy
#
#
# June 5, 2018, Tuesday, 17:00
# A new attempt to read index data from
# finance.yahoo.com

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;
use FunctionsVstaHelper;
use FunctionsVstaYahooFinance;
binmode STDOUT, ':utf8';

if ($#ARGV == -1) {

    print STDERR join "\n", "Usage: One argument is needed",
	" Use case 1: when querying for a future chain, say Gold (GC)",
	"   parse-yahoo-finance.pl GC=F",
	"   will return",
	"   GCF19.CMX GCG19.CMX GCJ19.CMX GCM19.CMX GCQ19.CMX GCV19.CMX ....",
	"   this is a spc-delimited list of futures associated with GC",
	" Use case 2: when querying for a specific future or index, say SEC",
	"   parse-yahoo-finance.pl SEC",
	"   where SEC can be",
	"   n225 : Nikkei 225",
	"   gdaxi : DAX",
	"   dji : Dow Jones Industrial Avg",
	"   stoxxe : Euro Stoxx 50",
	"   GCG19.CMX : Gold Feb 2019, traded on CME",
	"   etc etc\n",
        " Optionally, both use-cases can be combined to get a bhavcopy thusly (approximately)",
	" for c in \$\(perl parse-yahoo-finance-future-chain.pl GC=F); do echo -n \"\$c \" ; perl parse-yahoo-finance-future-chain.pl \$c; done",
	"\n";
	
    exit 1;
}

my $DEBUG=0; if (defined $ENV{'DEBUG'}) { $DEBUG = $ENV{'DEBUG'}; }

my $idx = $ARGV[0];
$idx = uc($idx);

my @ary;
my %hash;

my $content_ref = get_yahoo_finance_html_content($idx);

# individual security: does not end in "=F" or "=f" 
if ($idx !~ /=F$/) { 

    #my $content_ref = get_yahoo_finance_html_content($idx);
 
    $DEBUG && print Dumper($content_ref);
    #%hash = populate_hash_from_yahoo_finance_html($content_ref);
    populate_hash_from_yahoo_finance_html($content_ref, \%hash);
    
    $DEBUG && print "From sub \n", Dumper(\%hash);

    # if the security has non-zero volume, it has been traded
    # so we output a comma-delimted line that contains its data
    if ((defined $hash{'volume'}) and (defined $hash{'timeStamp'})
	and not (($hash{'volume'} eq '0') or ($hash{'volume'} =~ /^N/))) {

	print join ",", decomma($hash{'open'}), decomma($hash{'high'}), 
	    decomma($hash{'low'}), decomma($hash{'close'}), 
	    decomma($hash{'volume'}), $hash{'timeStamp'}, "\n";

    }

    # if the security has zero volume, it has not been traded, so we output an empty line
    else { print "\n"; }

}

# the argument ends in "=f" or "=F"
# the query is for a chain of futures
else {

    @ary = get_yahoo_finance_futures_list($content_ref);
    print join " ", @ary;

}
