#!/usr/bin/perl

# dr-nse-index-hist.pl
# In case of disaster (loss of data)
# one can obtain historical data of NSE indexes 
# from http://www.nseindia.com/products/content/equities/indices/historical_index_data.htm
# This program reformats the dump from that page in the DOHLCV format
# Usage: perl dr-nse-index-hist.pl < name-of-csv-file-from-nse 

# Changelog
# Feb 17, 2013, Sunday :)
# This works, but the early records have OHL data set to 0, which causes warnings.
# Otherwise, this is perfectly acceptable

use strict;
use diagnostics;
use Data::Dumper;

my %months = ("Jan" =>"01", 
	      "Feb" => "02", 
	      "Mar" => "03", 
	      "Apr" => "04",
	      "May" => "05", 
	      "Jun" => "06", 
	      "Jul" => "07", 
	      "Aug" => "08",
	      "Sep" => "09", 
	      "Oct" => "10", 
	      "Nov" => "11", 
	      "Dec" => "12" );

while (<STDIN>) {

    next if /^\"Date/; # skip the title line
    chomp;
    s/"//g; # Remove all quotes
    s/,//g;
    s/\s+/ /g; # Coalesce all multiple spaces into a single space
    my ($wrong_fmt_date, $open, $high, 
	$low, $close, $volume, $value) = split;

    # Debug
    #print join "|", $wrong_fmt_date, $open, $high,
    #$low, $close, $volume, $value, "\n";

    $wrong_fmt_date =~ /(.+)-(.+)-(.+)/;
    my $day = $1;
    my $month = $months{$2};
    my $year = $3;

    my $date = $year . $month . $day;

    print join " ", $date, $open, $high,
    $low, $close, $volume, $value*100, "\n";


}
