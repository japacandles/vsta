#!/usr/bin/perl

# parse-cme-futures.pl
# Feb 21, 2017, Tuesday 03:17 Maharashtra election holiday :)
# This is the real thing to parse futures pages from cme. 
# I can use the barcharts script as a backup
#
# Usage notes:
# First of all, create the empty data-file and get today's date
# host www.cmegroup.com 
# a=$(/usr/bin/mktemp) && mv $a $a.html
# d=$(TZ=America/Chicago date +%Y-%m-%d)
#
# Now for Bovespa:
# rm -vf $a.html
# wget http://www.cmegroup.com/trading/equity-index/international-index/usd-denominated-ibovespa.html -O $a.html 
# perl ~/parse-cme-futures.pl $d $a.html INTERNATIONAL
#
# For Nasdaq 100
# rm -vf $a.html
# wget http://www.cmegroup.com/trading/equity-index/us-index/e-mini-nasdaq-100.html -O $a.html
# perl ~/parse-cme-futures.pl $d $a.html DEFAULT
#
# Aids to memory:
# e-mini gold : http://www.cmegroup.com/trading/metals/precious/e-mini-gold.html INTERNATIONAL
# e-micro gold : http://www.cmegroup.com/trading/metals/precious/e-micro-gold.html INTERNATIONAL
# silver : http://www.cmegroup.com/trading/metals/precious/silver.html DEFAULT
# e-mini S&P 500 : http://www.cmegroup.com/trading/equity-index/us-index/e-mini-sandp500.html DEFAULT
# e-mini Dow Jones : http://www.cmegroup.com/trading/equity-index/us-index/e-mini-dow.html DEFAULT
# e-mini FTSE 100 : http://www.cmegroup.com/trading/equity-index/international-index/e-mini-ftse-100-index.html INTERNATIONAL
# bitcoin : http://www.cmegroup.com/trading/equity-index/us-index/bitcoin.html INTERNATIONAL
#
use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use HTML::TokeParser;

# if 3 arguments aren't there, then
# print usage and exit
if ($#ARGV != 2) {
    print "Usage:\n",
	"perl parse-cme-futures.pl <date> /path/to/datafile.html <mode>\n",
	"where \n",
	"<date> has the YYYY-mm-dd format and \n",
	"<mode> is either DEFAULT or INTERNATIONAL \n";
    exit(1);
}

my $date = shift;

my $file = shift;

my $mode = shift;

my $p = HTML::TokeParser->new($file);

my $token;
$p->get_tag("title");
my $title = $p->get_trimmed_text("/title");
$title =~ s/ /-/g;
#print $title, "\n";

$p->get_tag("table");
$p->get_tag("tr"); $p->get_tag("tr"); 

my $hashref;

while (1) {
    
	$p->get_tag("tr");
	$p->get_tag("span"); my $expiry = $p->get_trimmed_text("/span");
	$expiry =~ s/ /-/g;

	$p->get_tag("td"); if ($mode eq "DEFAULT") { $p->get_tag("td"); }
 
	$p->get_tag("td");
	$hashref->{$expiry}->{'close'} = $p->get_trimmed_text("/td");

	$p->get_tag("td"); $p->get_tag("td");

	$p->get_tag("td");
	$hashref->{$expiry}->{'open'} = $p->get_trimmed_text("/td");

	$p->get_tag("td");
	$hashref->{$expiry}->{'high'} = $p->get_trimmed_text("/td");

	$p->get_tag("td");
	$hashref->{$expiry}->{'low'} = $p->get_trimmed_text("/td");

	$p->get_tag("td");
	$hashref->{$expiry}->{'volume'} = $p->get_trimmed_text("/td");
	$hashref->{$expiry}->{'volume'} =~ s/,//g;
	    
	$p->get_tag("td");
	
	$p->get_tag("td");
	$hashref->{$expiry}->{'timestamp'} = $p->get_trimmed_text("/td");
	$hashref->{$expiry}->{'timestamp'} =~ s/ /-/g ;

	$p->get_tag("/tr");

	#print Dumper($hashref);
	
	my $token1 = $p->get_token();
	#print Dumper($token1);

	my $token2 = $p->get_token();
	#print Dumper($token2);

	if ($token2->[1] eq "tr") { $p->unget_token($token2); $p->unget_token($token1); next }
	last;
	
}

#print Dumper($hashref);

for my $expiry (keys %{$hashref}) {
    next if ($hashref->{$expiry}->{'open'} eq "-");
    
    print join ",", $title . "-" . $expiry,
	$date,
	$hashref->{$expiry}->{'open'},
	$hashref->{$expiry}->{'high'},
	$hashref->{$expiry}->{'low'},
	$hashref->{$expiry}->{'close'},
	$hashref->{$expiry}->{'volume'},
	$hashref->{$expiry}->{'timestamp'},
	"\n";
}
