#!/usr/bin/perl

# parse-money-cnn.pl
# Date: Sat, Aug 26, 2012
# To parse money.cnn.com to retrieve index data including breadth data
# masterdata.com has stopped working after Thursday, Aug 23, 2012
# So I had to look for a new data source for S&P500
# money.cnn.com was do-able :)
# Sample usage:
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl markets dow
# 13052.82 13175.51 13027.20 13157.97 442200000 24 328200000 5 103600000 1 10400000 Data as of 4:30pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl markets sandp
# 1401.99 1413.46 1398.04 1411.13 1968209100 396 1523609600 99 409916500 5 34683000 Data as of 4:45pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl markets nasdaq 
# 3045.22 3076.80 3042.22 3069.79 1298931464 1395 815596853 854 447745524 241 35589087 Data as of 5:19pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets nikkei225
# 9069.95 9090.67 9045.79 9070.76 818014000 26 162205300 191 619370700 7 36438000 Data as of 2:28am ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets hang_seng
# 19930.45 19930.45 19840.19 19880.03 1059502000 3 115200000 45 944302000 0 0 Data as of 4:01am ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets ftse100
# 5776.60 5791.39 5739.41 5776.60 484446000 51 194017300 46 288052400 3 2376300 Data as of 6:05pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets cac40
# 3432.10 3442.33 3396.81 3433.21 107296700 14 23749100 24 82047600 1 1500000 Data as of 6:20pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets dax
# 6957.60 6990.07 6885.98 6971.07 87694200 18 61360900 11 26333300 0 0 Data as of 12:30pm ET, 08/24/2012 

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use FunctionsVstaHelper;
use FunctionsVstaMoneyCnn;

# If no CNN section is specified, exit immediately
my $cnn_section = shift;
unless (defined $cnn_section) {
    print STDERR "No CNN section specified: markets world_markets ?\n";
    exit (1);
}

# If no index name is supplied, exit immediately
my $index = shift;
unless (defined $index) {
    print "No index specified\n";
    exit (1);
}

# Create my browser
my $m = money_cnn_browser();

my %hash;

read_money_cnn_main($m, $cnn_section, $index, \%hash);

#print Dumper(\%hash);
# Print in bhavcopy format
print join " ",
    $hash{'open'}, $hash{'high'}, $hash{'low'}, $hash{'close'}, $hash{'volumes'},
    defined($hash{'advances'}) ? $hash{'advances'} : 0,
    defined($hash{'advances_volumes'}) ? $hash{'advances_volumes'} : 0,
    defined($hash{'declines'}) ? $hash{'declines'} : 0,
    defined($hash{'declines_volumes'}) ? $hash{'declines_volumes'} : 0,
    defined($hash{'unchanged'}) ? $hash{'unchanged'} : 0,
    defined($hash{'unchanged_volumes'}) ? $hash{'unchanged_volumes'} : 0,
    $hash{'timestamp'}, "\n";

