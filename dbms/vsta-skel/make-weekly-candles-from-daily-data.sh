#!/bin/bash -x

# Apr 16, 2017, Sunday, Pune, 10:45 AM (morning) :-)
# This script was written at the end of March 2017 but it is getting
# annotated now in mid-April.
# The London gold_am (morning fix) is the closest thing to a spot gold price.
# It is trivial to create a line-chart out of these prices, BUT
# we can also create 5 weekly data-files out of the daily fix.
# These 5 weekly data-files are Mon-Fri, Tue-Mon, Wed-Tue, Thu-Wed, and Fri-Thu
# which will help us to see the evolution of how the weekly candles actually evolve.
# To aid us in the creation of these weekly files, we need to create scratch files
# in such a way that on a Wed, the Mon-Fri scrtach file will contain the daily
# fix prices of Mon, Tue and Wed.
# But magic happens in the Thu-Wed scratch file. First, it is updated with Wed's fix
# and then the weekly candle data is created from it. Thu's data is open and Wed's
# data is close. The highest and lowest prices in this duration complete the DOHLC
#
# This is harder than it sounds :) as you will see below what with all the
# exceptions that we encounter in the London AM gold fix data
#
# Inputs: data-file name, and date as arguments
# Outputs: scratch files and weekly candle files
# Usage:
# sh make-weekly-candles-from-daily-data.sh /path/to/data-file date-in-YYYY-MM-DD-format

# the data file is the first argument
f=$1

commodity=$(basename $f)

# the date is the second argument
d=$2

# get the price from the data file
grep $d $f
if (($? != 0))
then
	exit 1
fi
price=$(grep $d $f | awk '{print $2}')

# we convert the YYYY-mm-dd to YYYYmmdd so that
# integer comparison is possible
dNoDash=$(echo $d | sed 's/-//g')
#oneWeekAhead=$(date -d "$d +1 week" +%Y-%m-%d)
#oneWeekAheadNoDash=$(echo $oneWeekAhead | sed 's/-//g')
#echo $d $dNoDash $oneWeekAhead $oneWeekAheadNoDash

# loop through the scratch file
for scratchFile in $(ls $f*scratch)
do
    echo scratchFile is $scratchFile
    candleFile=$(dirname $scratchFile)/$(basename $scratchFile .scratch)
    echo Candle file is $candleFile 1
    
    linesInScratch=$(wc -l $scratchFile | awk '{print $1}')
    #echo lines in scratch are $linesInScratch
    
    if (( $linesInScratch == 0 ))
    then
	
	echo $scratchFile has no lines in it
	# since the scratch file has no lines in it
	# we need to first append the current data to it (the scratch file)
	echo Appending $d $price to $scratchFile
	echo $d $price | tee -a $scratchFile

	# did today complete the week ? if so
	# we will create the candle 
	declare -A weekdays
	weekdays["$f.Mon-Fri"]=0
	weekdays["$f.Tue-Mon"]=1
	weekdays["$f.Wed-Tue"]=2
	weekdays["$f.Thu-Wed"]=3
	weekdays["$f.Fri-Thu"]=4
	
	echo Candle File is ${weekdays[$candleFile]} TWO
	
	todayWeekDay=$(date -d "$d" +%u)
	echo Today is $todayWeekDay
	
	###
	if (( $todayWeekDay == ${weekdays[$candleFile]} ))
	then
	    
	    echo today has completed the week
	    echo so we will create the new candle
				
	    o=$(head -n 1 $scratchFile | awk '{print $2}' ) ; echo "Open is " $o
	    c=$(tail -n 1 $scratchFile | awk '{print $2}' ) ; echo "Close is " $c
	    weekDate=$(tail -n 1 $scratchFile | awk '{print $1}' ) ; echo "Week's date is " $weekDate
	    h=$(awk -v high=0 ' { if (high < $2) {high = $2 ; } } END { print high; } ' $scratchFile ) ; echo "High is " $h
	    l=$(awk -v low=$h ' { if (low > $2) {low = $2 ; } } END { print low; } ' $scratchFile ) ; echo "Low is " $l
	    
	    echo $weekDate $o $h $l $c | tee -a $candleFile
	    
	    echo Now we need to truncate the scratch file $scratchFile
	    > $scratchFile
	fi
	
    else # the scratch file had a non-zero number of lines

	# the first date in the scratch file
	firstDate=$(head -n 1 $scratchFile | awk '{print $1}')

	# is the first date, really when this week began ?
	# if this week is Tue-Mon and there was no trade on Tue
	# then the first date in the scratch file will be of the
	# Wed. We need to avoid that because, we will soon
	# be computing the last date of this week as T+7 days
	# So if the first rec points to a Wed in a Tue-Mon week,
	# we will make the mistake of starting _next_ week on Wed too
	
	# So we check if the day of week of the 1st rec is the
	# correct day of week and if not, then we reduce the firstDate
	# until we reach the correct day of week.
	correctDoWstart=$(echo $scratchFile | awk -F'.' '{print $2}' | awk -F'-' '{print $1}')
	realDoWstart=$(date -d "$firstDate" "+%a")
	echo This week should start on $correctDoWstart
	echo This week starts on $realDoWstart

	while (true)
	do
	    if [[ $realDoWstart != $correctDoWstart ]]
	    then
		firstDate=$(date -d "$firstDate -1 day" "+%Y-%m-%d") ; echo New firstDate is $firstDate
		realDoWstart=$(date -d "$firstDate" "+%a")
	    else
		break # this means that realDoWstart of a Tue-Mon week is now at Tue,
		      # this means that firstDate automatically contains the correct date
	    fi
	done
	echo Now firstDate is $firstDate
	
	firstDateNoDash=$(echo $firstDate | sed 's/-//g')
	
	oneWeekFromFirstDate=$(date -d "$firstDate +1 week" +%Y-%m-%d)
	oneWeekFromFirstDateNoDash=$(echo $oneWeekFromFirstDate | sed 's/-//g')
	
	if (( $dNoDash > $firstDateNoDash )) # so now we are in the correct range
	then
	    
	    echo $d exceeds $firstDate
	    if (( $dNoDash < $oneWeekFromFirstDateNoDash ))
	    then
		echo $d is within the range $firstDate and $oneWeekFromFirstDate
		echo Appending $d $price to $scratchFile
		echo $d $price | tee -a $scratchFile
		
	    else # we are ahead of where we should have been
		# so we need to create a new candle, then truncate the scratch file and
		# then append the new data to the scratch file
		echo $d exceeds the range $firstDate and $oneWeekFromFirstDate
		echo now we need to massage $scratchFile into OHLV format and append to $candleFile
				
		o=$(head -n 1 $scratchFile | awk '{print $2}' ) ; echo "Open is " $o
		c=$(tail -n 1 $scratchFile | awk '{print $2}' ) ; echo "Close is " $c
		weekDate=$(tail -n 1 $scratchFile | awk '{print $1}' ) ; echo "Week's date is " $weekDate
		h=$(awk -v high=0 ' { if (high < $2) {high = $2 ; } } END { print high; } ' $scratchFile ) ; echo "High is " $h
		l=$(awk -v low=$h ' { if (low > $2) {low = $2 ; } } END { print low; } ' $scratchFile ) ; echo "Low is " $l
		
		echo $weekDate $o $h $l $c | tee -a $candleFile
		
		echo Now we need to truncate the scratch file $scratchFile
		> $scratchFile
		
		echo lastly we need to re-create the scratchFile with the current data
		echo $d $price | tee -a $scratchFile
		
	    fi
	    
	#else # this is frankly wierd.
	       
	    #echo $d occurs before $firstDate
	    #echo But there are records in $scratchFile
	    #echo So we need to massage $scratchFile anyhow

	    #o=$(head -n 1 $scratchFile | awk '{print $2}' ) ; echo "Open is " $o
	    #c=$(tail -n 1 $scratchFile | awk '{print $2}' ) ; echo "Close is " $c
	    #weekDate=$(tail -n 1 $scratchFile | awk '{print $1}' ) ; echo "Week's date is " $weekDate
	    #h=$(awk -v high=0 ' { if (high < $2) {high = $2 ; } } END { print high; } ' $scratchFile ) ; echo "High is " $h
	    #l=$(awk -v low=$h ' { if (low > $2) {low = $2 ; } } END { print low; } ' $scratchFile ) ; echo "Low is " $l
	    
	    #echo $weekDate $o $h $l $c | tee -a $candleFile

	    #echo Now we need to truncate the scratch file $scratchFile
	    #> $scratchFile
	    
	    #echo lastly we need to re-create the scratchFile with the current data
	    #echo $d $price | tee -a $scratchFile
	fi
	
	# lastly we need to deal with the situation when
	# today _completes_ the week. In other words, if this week is
	# Thu-Wed and today is Wed, then we need to create a new candle
	# and truncate the scratch file

	# today's day of week
	dDoW=$(date -d "$d" "+%a") ; echo today is $dDoW

	# last day of this week
	thisWeekEndDoW=$(echo $scratchFile | awk -F'.' '{print $2}' | awk -F'-' '{print $2}')
	echo This week ends on $thisWeekEndDoW

	# if today completed the week, we create the new candle
	# and truncate the scratch file
	if [[ $dDoW == $thisWeekEndDoW ]]
	then
	    
	    o=$(head -n 1 $scratchFile | awk '{print $2}' ) ; echo "Open is " $o
	    c=$(tail -n 1 $scratchFile | awk '{print $2}' ) ; echo "Close is " $c
	    weekDate=$(tail -n 1 $scratchFile | awk '{print $1}' ) ; echo "Week's date is " $weekDate
	    h=$(awk -v high=0 ' { if (high < $2) {high = $2 ; } } END { print high; } ' $scratchFile ) ; echo "High is " $h
	    l=$(awk -v low=$h ' { if (low > $2) {low = $2 ; } } END { print low; } ' $scratchFile ) ; echo "Low is " $l
	    
	    echo $weekDate $o $h $l $c | tee -a $candleFile
	    
	    echo Now we need to truncate the scratch file $scratchFile
	    > $scratchFile
	    
	fi
    fi
    
    echo
    
done
