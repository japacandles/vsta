#!/usr/bin/perl

# parse-currency-converter.org.uk.pl
# July 1, 2019, Monday
#
# Input: an html file retrieved thus
# wget https://www.currency-converter.org.uk/currency-rates/historical/table/CHF-USD.html -O CHF-USD.html
# followed by
# perl parse-currency-converter.org.uk.pl CHF-USD.html
#
# Output: on stdout,
# a simple time series of currency crosses
# yyyy-mm-dd p
#

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use HTML::TokeParser;

my $f = shift;
#open FILE, "< $f" or die $!;

my $p = HTML::TokeParser->new($f) or die $!;
my $token;
my $hash;

$p->get_tag("table"); 

$p->get_tag("tr");

while (1) {

	$p->get_tag("tr");

	$p->get_tag("td"); # day of week

	$p->get_tag("td"); # date in dd/mm/yyyy fmt
	my $date = $p->get_trimmed_text("/td");
	#print "Date is ", $date, "\n";

	$date =~ /(\d{1,2})\/(\d{1,2})\/(\d{4})/;
	if ($1 > 9) { 
		$date = "$3" . "-" . "$2" . "-" ."$1";
	}
	else {
		$date = "$3" . "-" . "$2" . "-" ."0" . "$1";
	}
	#$print "Date is ", $date, "\n";

	$p->get_tag("td"); # skip how many units of first curency, example 1 CHF=

	$p->get_tag("td"); # the point of this exercise :)
	$hash->{$date} = $p->get_trimmed_text("/td");

	$p->get_tag("/tr");

	#my ($t1, $t2, $t3, $4);

	my $t1 = $p->get_token(); #print "T1 \n", Dumper($t1); 
	my $t2 = $p->get_token(); #print "T2 \n", Dumper($t2);
	my $t3 = $p->get_token(); #print "T3 \n", Dumper($t3);
	my $t4 = $p->get_token(); #print "T4 \n", Dumper($t4);

	last if ( ($t1->[0] eq "E") && ($t1->[1] eq "table") );
	$p->unget_token($t4);
	$p->unget_token($t3);
	$p->unget_token($t2);
	$p->unget_token($t1);
	
}

#print Dumper($hash);
for (sort keys %$hash) {
	print join " ", $_, $hash->{$_}, "\n";
}
