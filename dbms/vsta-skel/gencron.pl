#!/usr/bin/perl 

#
# gencron.pl
# This script is to generate a file that can be fed 
# to crontab per stock excahnge. So running the command
# gencron.pl --skip=6 --skip=7 --from-tz="America/New_York \
#            --to-tz="Asia/Kolkata" --time="16:30:00" \
#            --cmd /tmp/get-nyse-bhavcopy
# will generate output (For last week of Feb 2013)
# 30 3 25 2 * cd ~/vsta/dbms/vsta-skel ; ./bhav.sh ../us/nyse
# 30 3 26 2 * cd ~/vsta/dbms/vsta-skel ; ./bhav.sh ../us/nyse
# ...
#
use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use DateTime;
use Getopt::Long;

# Assign the cmd line 
# args to variables
my (@skiplist, $fromtz, $totz, $time, $cmd,
    $cronString, $hour, $min, $sec);
my $o = GetOptions(

    "skip=i" =>\@skiplist, # Skip over weekly holidays
                           # 1 => Mon, 7 => Sun
                           # So skipping Sat and Sun will be --skip=6 --skip=7

    "from-tz=s" => \$fromtz, # from TZ: If we want to get NYSE's bhavcopy
                             # this will be --from-tz="America/New_York"

    "to-tz=s" => \$totz,     # to TZ: This is the timezone of the server
                             # so, if the server is set to GMT,
                             # this will be --to-tz="Etc/GMT"

    "time=s" => \$time, # This is the time in the from-tz at which the bhavcopy 
                        # is to be retrieved in the to-tz
                        # So if the NYSE closes at 4 PM, the time will be
                        # --time="16:00:00"

    "cmd:s" => \$cmd,    # This is the name of the file that contains a single 
                         # line that contains the command that has to be run 
                         # so, here the file /tmp/get-nyse-bhavcopy will 
                         # contain the line
                         # cd ~/vsta/dbms/vsta-skel ; ./bhav.sh ../us/nyse
    );

($hour, $min, $sec) = split /:/, $time; # the fmt was "HH:MM:SS"

# Which days are to be skipped
# Mon = 1, Sun = 7
my %skiphash;
for (@skiplist) { $skiphash{$_} = 1; }

# Read the file into the cron's string
open FILE, "< $cmd" or die $!;
$cronString = <FILE>;
close FILE;
chomp $cronString;

my $d = DateTime->today();
my $f = $d->clone()->set_time_zone($fromtz);
$f->set(hour => $hour);
$f->set(minute => $min);

my $year = $d->year;

# The infinite loop 
# which will last for the remainder of the year
while (1) {

    if ( defined($skiphash{$f->day_of_week} )) {
	$f->add( days => 1 );
	#print join " ", "In if ", $f->minute, $f->hour, $f->day, $f->month, "*", $cmd, "\n";
	next;
    }

    #print join " ", "Normal", $f->minute, $f->hour, $f->day, $f->month, "*", $cmd, "\n";
    my $t = $f->clone()->set_time_zone($totz);
    # outputy should be in the man 5 crontab format
    print join " ", 
               $t->minute, 
               $t->hour, 
               $t->day, 
               $t->month, 
               # $t->day_of_week, # Don't specify weekday to avoid wierdness :)
               "*",
               $cronString, 
               #$totz, 
               "\n";

    $f->add( days => 1 );
    last if ($t->year > $year);

}
