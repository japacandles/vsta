#!/bin/bash

#set -x

# top-two-traded-futures-by-dates.sh
# Added on Apr 27 Apr 2021
# To list the two most traded futures of a given commodity by date
# Or
# for a given commodity on a given date, to list the two most traded futures
commodity=$1
d=$2

# if commodity is not provided, show usage and exit
print_usage () {
    
    echo "top-two-traded-futures-for-date.sh"
    echo "Usage: top-two-traded-futures-for-date.sh $COMMODITY"
    echo "Example: top-two-traded-futures-for-date.sh /path/to/data/Gold"
    echo "or"
    echo "Example: top-two-traded-futures-for-date.sh /path/to/data/OrangeJuice YYYYmmdd"
    echo "For a given commodity, this program outputs a list"
    echo "of dates on which that commodity's futures were traded"
    echo "and the names of those futures which had the highest"
    echo "volumes on those dates"
    echo "Example 1:"
    echo "top-two-traded-futures-for-date.sh /path/to/data/Gold"
    echo " ... "
    echo " ... "
    echo "Example 2:"
    echo "top-two-traded-futures-for-date.sh /path/to/data/Gold YYYY-mm-dd"
    echo " YYYY-mm-dd /path/to/data/Gold-Mar-2021 10000 "
    echo " YYYY-mm-dd /path/to/data/Gold-Jun-2021 5000 "
    
}

# If the user hasn't supplied a commodity
# print usage and exit
if [ -z "$commodity" ]; then
    echo "Missing argument"
    print_usage
    exit 1
fi

# If the commodity file doesn't exist
# say that, print usage and exit
ls $commodity* 2>&1 > /dev/null
if [ $? -ne 0 ]; then
    echo "Data files $commodity don't exist"
    print_usage
    exit 2
fi

DATA_FILE_LIST=/var/tmp/tradeable-futures
if [ ! -f $DATA_FILE_LIST ]; then
    echo "We have no reference file of tradeable futures."
    echo "Please create /var/tmp/tradeable-futures  "
    echo '(for f in `find $VSTA_DATA_ARCHIVE_DIR -type f | grep futures`; do wc -l $f; done) | sort -nr > /var/tmp/tradeable-futures'
    exit 3
fi

# if we are here, there is at least one data file :)

# we begin by defining some vars that we will use throughout
comm_files=$(basename $commodity)
comm_files=/tmp/$comm_files

# create a list of datafiles from which to look for dates
grep "$commodity" $DATA_FILE_LIST > $comm_files
if [ $? -ne 0 ]; then exit; fi

# if no date was specified
if [ -z $d ]; then
    
    # we create a file into which we simply dump all the dates
    # from all the datafiles. We will call this file of dates $foo :)
    foo=$(mktemp); > $foo
    # we assume that each data file will be a spc delimited DOHLC{V}
    for f in `cat $comm_files`; do
        awk '{print $1}' $f >> $foo
    done
    # make sure that each date occurs only once in $foo
    sort -nu $foo -o $foo
    
    # now that $foo has each date on which at least one future
    # was traded, we loop through it and find out which two futures
    # had the highest volume on that date
    for d in $(cat $foo); do
        for line in $(grep $d `cat $comm_files` | sort -nr -k6 | head -n2 | sed 's/ /,/g'); do
            f=$(echo $line | awk -F: '{print $1}')
            v=$(echo $line | awk -F, '{print $6}')
            echo $d $f $v
        done
    done
    
# if date was specified
else
    for line in $(grep $d `cat $comm_files` | sort -nr -k6 | head -n2 | sed 's/ /,/g'); do
        f=$(echo $line | awk -F: '{print $1}')
        v=$(echo $line | awk -F, '{print $6}')
        echo $d $f $v
    done  
fi
    
exit 0
