#!/usr/bin/perl

# parse-free-proxy-list
# fetches a list free-proxy-list.net
# Inputs: None
# Outputs: a list of https elite proxies in ip.ad.d.r:port format on stdout 

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;
use FunctionsVstaHelper;

my $u = LWP::UserAgent->new();
$u->agent(random_ua_str());
my $r = $u->get('https://free-proxy-list.net');

my $p;
if ($r->is_success) { $p = HTML::TokeParser->new(\$r->decoded_content); }

$p->get_tag("tbody");
while (my $token = $p->get_token) { 

    #print Dumper($token);

    $p->get_tag("tr"); 

    $p->get_tag("td");
    my $ip = $p->get_trimmed_text("/td");
    
    $p->get_tag("td");
    my $port = $p->get_trimmed_text("/td");

    $p->get_tag("/td");
    $p->get_tag("/td"); # country code
    $p->get_tag("/td"); # country name
    
    $p->get_tag("td");
    my $type = $p->get_trimmed_text("/td"); # transparent, anonymous, or elite
        
    $p->get_tag("td");
    my $google = $p->get_trimmed_text("/td");

    $p->get_tag("td");
    my $https = $p->get_trimmed_text("/td");
    
    $p->get_tag("td");
    my $time_since_last_check = $p->get_trimmed_text("/td");

    # dump the proxy details
    if (($https eq 'yes') 
	and ($type eq 'elite proxy')
	and ($time_since_last_check =~ /s/)) { # if the proxy has been tested
	                                       # a few seconds ago, it is likely
	                                       # to work

	my @adr_parts = split /\./, $ip;
	if (($#adr_parts == 3) or ($#adr_parts == 5)) { # dotted quad/hextet only
	    print "$ip:$port", "\n";
	}
    }	
    
    $p->get_tag("/tr");
    
    my $token1 = $p->get_token;
    if (($token1->[0] eq 'E')
	and ($token1->[1] eq 'tbody')) {last;}
    else { $p->unget_token($token1); }
    
}
