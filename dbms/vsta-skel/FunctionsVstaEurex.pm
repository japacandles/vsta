package FunctionsVstaEurex;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw (
    eurex_url_content
    populate_eurex_data_hash
    );
use strict;
use diagnostics;
use warnings;
use FunctionsVstaHelper;
use LWP::UserAgent;
use HTML::TokeParser;
use Data::Dumper;

sub eurex_url_content {

    my ($sec, $date) = @_;
    
    my %security_to_url_mapper = (
	'EURO-STOXX-50' => 'http://www.eurexchange.com/exchange-en/products/idx/stx/blc/18956!onlineStatsReload?productId=18956&productGroupId=846&busDate=' # defunct

	, 'FGBL' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/34656!onlineStatsReload?productId=34656&productGroupId=13328&busDate=' # Euro-Bund Futures (FGBL)

	, 'FGBM' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/34670!onlineStatsReload?productId=34670&productGroupId=13328&busDate=' # Euro-Bobl Futures (FGBM)

	, 'FGBS' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/34678!onlineStatsReload?productId=34678&productGroupId=13328&busDate=' # Euro-Schatz Futures (FGBS)

	, 'FGBX' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/42066!onlineStatsReload?productId=42066&productGroupId=13328&busDate=' # Euro-Buxl Futures (FGBX)

	, 'FBTP' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/34680!onlineStatsReload?productId=34680&productGroupId=13328&busDate=' # Long-Term Euro-BTP Futures (FBTP)

	, 'FBTS' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/42076!onlineStatsReload?productId=42076&productGroupId=13328&busDate=' # Short-Term Euro-BTP Futures (FBTS)

	, 'FOAT' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/34684!onlineStatsReload?productId=34684&productGroupId=13328&busDate=' # Euro-OAT Futures (FOAT)

	#, 'FOAM' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/42092!onlineStatsReload?productId=42092&productGroupId=13328&busDate=' # Mid-Term Euro-OAT Futures (FOAM)

	#, 'FBON' => 'https://www.eurexchange.com/exchange-en/products/int/fix/government-bonds/34690!onlineStatsReload?productId=34690&productGroupId=13328&busDate=' # Euro-BONO Futures (FBON)

	#, 'FCBI' => 'https://www.eurexchange.com/exchange-en/products/int/bond/937746!onlineStatsReload?productId=937746&productGroupId=14816&busDate=' # EURO STOXX 50 Corporate Bond Index Futures (FCBI)

	, 'FESX' => 'https://www.eurexchange.com/exchange-en/products/idx/stx/blc/34652!onlineStatsReload?productId=34652&productGroupId=13370&busDate=' # EURO STOXX 50 Index Futures (FESX)

	, 'FDXM' => 'https://www.eurexchange.com/exchange-en/products/idx/dax/34666!onlineStatsReload?productId=34666&productGroupId=13394&busDate=' # Mini-DAX Futures (FDXM)

	, 'FDAX' => 'https://www.eurexchange.com/exchange-en/products/idx/dax/34642!onlineStatsReload?productId=34642&productGroupId=13394&busDate=' # DAX Futures (FDAX)

	#, "FXXP" => 'https://www.eurexchange.com/exchange-en/products/idx/stx/bmc/41978!onlineStatsReload?productId=41978&productGroupId=13376&busDate=' # STOXX Europe 600 Index Futures (FXXP)

	, 'FSMI' => 'https://www.eurexchange.com/exchange-en/products/idx/stx/blc/34652!onlineStatsReload?productId=34652&productGroupId=13370&busDate=' # SMI® Futures (FSMI)
	,
	);
    
    my $url = $security_to_url_mapper{$sec} . $date;
    #my $url = $security_to_url_mapper{$sec};
    $ENV{'DEBUG'} && print STDERR "Sec : ", $sec, " Date : ", $date, " Url : ", $url, "\n";

    my $ua = LWP::UserAgent->new();
    #$ua->agent(random_ua_str());
    $ua->agent($ENV{'BROWSER'});

    my $response = $ua->get($url);
    if ($response->is_success) {
	$ENV{'DEBUG'} && print STDERR $response->decoded_content;
	return $response->decoded_content;
    } else { return "0"; }
    
}

sub populate_eurex_data_hash {

    my ($c) = @_;

    my ($hashref, $security, $delivery, $text);
    
    my $p = HTML::TokeParser->new(\$c) or die $!;
    
    $p->get_tag("h1"); $p->get_tag("h1"); 
    $security = $p->get_trimmed_text("/h1");
    $security =~ s/ /-/g;
    $security =~ s/®//g;

    #$p->get_tag("/script"); $p->get_tag("span"); $p->get_tag("span"); print $p->get_trimmed_text("/span"), "\n";
    
    $p->get_tag("table"); $p->get_tag("table");
    
    while (1) {
	
	$p->get_tag("tr");
	
	$p->get_tag("td"); #delivery
	$delivery = $p->get_trimmed_text("/td");
	last if ($delivery =~ /Total/);
	$delivery =~ s/ /-/g;
	#print $text, " ";
	#$p->get_text("/td");
	
	$p->get_tag("td"); #open
	$text = $p->get_trimmed_text("/td");
	last if ($text eq "Total");
	if ($text eq "0.00") { delete $hashref->{$delivery}; next ; }
	#print $text, " ";
	$hashref->{$delivery}->{'open'} = decomma($text);
	
	$p->get_tag("td"); #high
	$hashref->{$delivery}->{'high'} = decomma($p->get_trimmed_text("/td"));
	
	$p->get_tag("td"); #low
	$hashref->{$delivery}->{'low'} = decomma($p->get_trimmed_text("/td"));
	
	$p->get_tag("td"); #close
	$hashref->{$delivery}->{'close'} = decomma($p->get_trimmed_text("/td"));
	
        
	$p->get_tag("td"); # settlement price
	#$hashref->{$delivery}->{'close'} = $p->get_trimmed_text("/td"), "\n";
	
	$p->get_tag("td"); #close
	$hashref->{$delivery}->{'volume'} = decomma($p->get_trimmed_text("/td"));
	
	$p->get_tag("td"); #close
	$hashref->{$delivery}->{'oi'} = decomma($p->get_trimmed_text("/td"));
	
    }

    return $hashref;
}

1;
