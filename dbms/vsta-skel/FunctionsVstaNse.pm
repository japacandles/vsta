package FunctionsVstaNse;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw ( 
    nse_browser
    populate_urls_hash
    get_futures_urls
    get_futures_data    
    create_spot_index_list
    parse_nse_spot_data
    print_spot_bhavcopy
    );

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

# thanks https://stackoverflow.com/questions/15770114/prototype-mismatch-error-perl :)
use JSON qw//;
use JSON::PP qw//;

use FunctionsVstaHelper;

sub nse_browser {
    # create a browser so that cookies etc can be shared across subroutines
    # and add headers to imitate a true browser
    my $m = WWW::Mechanize->new();
    #$m->agent_alias('Windows Mozilla');
    #$m->agent('Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0');
    #$m->agent( random_ua_str() );
    $m->agent($ENV{'BROWSER'});
    $m->add_header('Accept' => 'image/png,image/*;q=0.8,*/*;q=0.5');
    $m->add_header('Accept-Language' => 'en-US,en;q=0.5');
    $m->add_header('Accept-Encoding' => 'gzip, deflate,br');
    $m->add_header('Referer' => 'https://nseindia.com/live_market/dynaContent/live_watch/derivative_stock_watch.htm');

    return $m;
}

# populate_urls_hash
# populates that global hash called urls
# with the urls where the futures contracts' data resides
sub populate_urls_hash {
    
    my ($browser) = @_;

    my %urls;
    my $baseNseUrl = 'http://www.nseindia.com';
    my $derivUrl = 
	'http://www.nseindia.com/live_market/dynaContent/live_watch/derivative_stock_watch.htm';

    my $DEBUG = 0;
    if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }
 
    $browser->get($derivUrl);

    $DEBUG && print STDERR Dumper(\$browser->links);
    
    my $p = HTML::TokeParser->new(\$browser->content());
    
    $p->get_tag("select");
    
    while (1) {
	
	my $token = $p->get_token();
	if (($token->[0] =~ /E/i) and ($token->[1] =~ /select/i)) {
	    $DEBUG && print STDERR "Exitting at the top\n";
	    $DEBUG && print STDERR Dumper(\$token);
	    last;
	}
	
	my $tag = $p->get_tag("option");
	if (! defined $tag->[1]->{'value'}) {
	    $DEBUG && print STDERR "option found without value\n";
	    $DEBUG && print STDERR "exitting\n";
	    last;
	}
	$DEBUG && print STDERR "\nTag description ", Dumper(\$tag), "\n";

	my $url = $baseNseUrl . $tag->[1]->{'value'} ;
	$DEBUG && print STDERR "Url is ", $url, "\n";

	if ($url =~ /FUT/i) { 
	    $urls{ $url }++ ;
	    $DEBUG && print STDERR "Urls hash ", Dumper(\%urls), "\n";
	}
	
	$p->get_tag("/option");
	
	$token = $p->get_token();
	$DEBUG && print STDERR "Token description 0", "\n", Dumper(\$token), "\n";

	if ($token->[1] =~ /option/i) {
	    $p->unget_token($token);
	    $DEBUG && print STDERR "Option found: looping back\n";
	    next;
	} elsif ($token->[1] =~ /select/i) {
	    $DEBUG && print STDERR "possible end of select\n";
	    $DEBUG && print STDERR "Exitting in the middle\n";
	    last;
	} else {
	    my $token1 = $p->get_token(); 
	    my $token2 = $p->get_token();
	    if (!defined($token2)) {
		$DEBUG && print STDERR "token 2 not defined\n";
		$DEBUG && print STDERR "exitting\n";
		last;
	    }
	    
	    $DEBUG && print STDERR "Token description 1", "\n", Dumper(\$token1), "\n";
	    $DEBUG && print STDERR "Token description 2", "\n", Dumper(\$token2), "\n";
	    $DEBUG && print STDERR "Now here 1\n";

	    if (($token1->[0] =~ /S/i) and ($token1->[1] =~ /option/i)) {
		$DEBUG && print STDERR "Now here 2 \t Looping back\n";
		$p->unget_token($token2);
		$p->unget_token($token1);
		$p->unget_token($token);
		next;
	    }

	    $DEBUG && print STDERR "Now here 3\n";
	    $p->unget_token($token);
	}
    }
    $DEBUG && print STDERR "Urls hash ", Dumper(\%urls), "\n";
    return(%urls);
}

#
# get_futures_urls
# Takes a url, and the browser object,
# GETs the url 
# and dumps the individual 
# futures'urls from HTTP::Response
sub get_futures_urls {
    my ($url, $browser) = @_;

    my %futures_urls;
    my $DEBUG = 0; if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }
    
    $DEBUG && print STDERR  "Now GETting $url inside get_futures_urls", "\n";

    $browser->get($url);

    for my $link ($browser->links) {
	if ($link->url =~ /instrument=FUTIDX/) {
	    $futures_urls{ 'http://nseindia.com' . $link->url }++
	}
    }

    $DEBUG && print STDERR "Futures urls hash ", Dumper(\%futures_urls), "\n";
    return(%futures_urls);
}

#
# get_futures_data
# takes a url, and the browser object
# GETs the url
# and dumps the data from HTTP::Response 
sub get_futures_data {
    my ($url, $browser) = @_;

    my $DEBUG = 0;
    if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }

    if ($url =~ /^http\:\/\//) { $url =~ s/^http/https/; }
    
    $DEBUG && print STDERR "Now GETting $url in get_futures_data", "\n";

    $browser->get($url);

    my $p = HTML::TokeParser->new(\$browser->content());

    $p->get_tag("div"); $p->get_tag("div");
    my $str = $p->get_trimmed_text("/div");

    my $h = JSON::decode_json($str);
    $DEBUG && print STDERR Dumper(\$h);

    return if ($h->{'data'}->[0]->{'numberOfContractsTraded'} =~ /-/);

    my $timestamp =  $h->{'lastUpdateTime'};
    $timestamp =~ s/\s+/-/g;

    my $expiry =  $h->{'data'}->[0]->{'expiryDate'};
    $expiry =~ s/(..)(...)(....)/$1-$2-$3/g;

    my $line = join " ", 
       "FUTIDX",
       $h->{'data'}->[0]->{'underlying'},
       $expiry, "0", "XX",
       $h->{'data'}->[0]->{'openPrice'},
       $h->{'data'}->[0]->{'highPrice'},
       $h->{'data'}->[0]->{'lowPrice'},
       $h->{'data'}->[0]->{'lastPrice'},
       $h->{'data'}->[0]->{'vwap'},
       $h->{'data'}->[0]->{'numberOfContractsTraded'},
       $h->{'data'}->[0]->{'turnoverinRsLakhs'},
       $h->{'data'}->[0]->{'openInterest'},
       $h->{'data'}->[0]->{'changeinOpenInterest'},
    $timestamp,;

    $line =~ s/,//g;
    $line=~ s/ /,/g;

    print $line, "\n";
}

sub create_spot_index_list {

    my $browser = shift;

    my $DEBUG = 0; if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }
    my %urls;
    
    my $url = 'http://www.nseindia.com/live_market/dynaContent/live_watch/equities_stock_watch.htm?cat=N';
    $browser->get($url);

    my $p = HTML::TokeParser->new(\$browser->content());
    $p->get_tag("select");

    while (1) {

	my $tag = $p->get_tag("option");
	$DEBUG && print STDERR Dumper($tag);
	last unless (defined $tag);
	$DEBUG && print STDERR "Tag is ", "\n", Dumper(\$tag), "\n";
	last unless (defined($tag->[1]->{'value'}));
	$DEBUG && print STDERR $tag->[1]->{'value'}, "\n";
	$urls{ $tag->[1]->{'value'} } = $p->get_trimmed_text("/option");
	$p->get_token();
	$DEBUG && print STDERR "Now urls hash is\n", Dumper(\%urls), "\n";
	my $token = $p->get_token();
	$DEBUG && print STDERR "token is\n", Dumper(\$token), "\n";

    }

    return %urls;
}

sub parse_nse_spot_data {
	
    my ($idx, $index, $browser) = @_;

    my $DEBUG = 0; if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }
    my %hash;
    
    my $url = 'http://www.nseindia.com/live_market/dynaContent/live_watch/stock_watch/' 
	. $idx . 'StockWatch.json';
    $browser->get($url);

    $DEBUG && print STDERR "Index is ", $index, "\n";
    
    my $str = $browser->content();
    my $json = JSON::PP->new();
    my $h = $json->allow_nonref->utf8->relaxed->escape_slash->loose->allow_barekey->allow_singlequote->decode($str);
    #my $h = JSON::PP::allow_nonref(JSON::PP::utf8(JSON::PP::relaxed(JSON::PP::escape_slash(JSON::PP::loose(JSON::PP::allow_barekey(JSON::PP::allow_singlequote(JSON::PP::decode($str))))))));
    #my $h = JSON::PP->allow_nonref->utf8->relaxed->escape_slash->loose->allow_barekey->allow_singlequote->decode($str);
    $DEBUG && print STDERR Dumper(\$h);
    
    $hash{$index}{'timestamp'} = $h->{'time'};
    
    $hash{$index}{'open'} = $h->{'latestData'}->[0]->{'open'};
    $hash{$index}{'high'} = $h->{'latestData'}->[0]->{'high'};
    $hash{$index}{'low'} = $h->{'latestData'}->[0]->{'low'};
    $hash{$index}{'ltp'} = $h->{'latestData'}->[0]->{'ltp'};

    my @ary = @{$h->{'data'}};
    $DEBUG && print STDERR Dumper(\@ary);
    
    for my $element (@ary) {
	$DEBUG && print STDERR $element->{'symbol'}, "\n";
	if (defined $element->{'trdVol'}) {
	    $element->{'trdVol'} =~ s/,//g;
	    $hash{$index}{'volume'} += $element->{'trdVol'}; 
	}
	 
	if (defined $element->{'ntP'}) {
	    $element->{'ntP'} =~ s/,//g;
	    $hash{$index}{'value'} += $element->{'ntP'} ;
	}

	if ((defined $element->{'ntP'}) and (defined $element->{'ptsC'})) {
	    my $tmp = $element->{'ptsC'}; $tmp =~ s/,//g;
	    if ($tmp > 0) {
		$hash{$index}{'advances'}++;
		$hash{$index}{'advances_volume'} += $element->{'trdVol'} if defined $element->{'trdVol'};
		$hash{$index}{'advances_value'} += $element->{'ntP'} if defined $element->{'ntP'};
	    } 

	    elsif  ((defined $element->{'ntP'}) and ($tmp < 0)) {
		$hash{$index}{'declines'}++;
		$hash{$index}{'declines_volume'} += $element->{'trdVol'} if defined $element->{'trdVol'};
		$hash{$index}{'declines_value'} += $element->{'ntP'} if defined $element->{'ntP'};
	    } 
	    
	    else {
		$hash{$index}{'unchanged'}++;
		$hash{$index}{'unchanged_volume'} += $element->{'trdVol'} if defined $element->{'trdVol'};
		$hash{$index}{'unchanged_value'} += $element->{'ntP'} if defined $element->{'ntP'};
	    }
	}
    }
    $DEBUG && print STDERR "Hash is ", Dumper(\%hash);
    print_spot_bhavcopy(\%hash);
    return %hash;
}

sub print_spot_bhavcopy {

    my ($h) = @_;
    my %hsh = %{$h};
    
    for my $key1 (keys %hsh) {

	my $name = $key1;
	$name =~ s/ /-/g;

	$name =~ s/junior-nifty/cnx-nifty-junior/i; # CNX Nifty Jr
	$name =~ s/cnx-bank/bank-nifty/i; # CNX Bank Nifty
	$name =~ s/Opportunities/OPPT/i; # CNX Dividend Opportunities
	$name =~ s/service-sector/service/i; # CNX Service Sector
	$name =~ s/Infrastructure/Infra/i; # CNX Infrastructure

	next unless (defined $hsh{$key1}{'open'});

	my $line = join " ", "FUTIDX",
	    uc($name), "SPOT", "0", "XX",
	    defined $hsh{$key1}{'open'} ? $hsh{$key1}{'open'} : 0,
	    defined $hsh{$key1}{'high'} ? $hsh{$key1}{'high'} : 0,
	    defined $hsh{$key1}{'low'} ? $hsh{$key1}{'low'} : 0,
	    defined $hsh{$key1}{'ltp'} ? $hsh{$key1}{'ltp'} : 0,
	    defined $hsh{$key1}{'ltp'} ? $hsh{$key1}{'ltp'} : 0,
	    defined $hsh{$key1}{'volume'} ? $hsh{$key1}{'volume'} * 100000 : 0,
	    defined $hsh{$key1}{'value'} ? $hsh{$key1}{'value'} * 100 : 0,
	    defined $hsh{$key1}{'advances'} ? $hsh{$key1}{'advances'} : 0,
	    defined $hsh{$key1}{'advances_volume'} ? $hsh{$key1}{'advances_volume'} * 100000 : 0,
	    defined $hsh{$key1}{'advances_value'} ? $hsh{$key1}{'advances_value'} * 100 : 0,
	    defined $hsh{$key1}{'declines'} ? $hsh{$key1}{'declines'} : 0,
	    defined $hsh{$key1}{'declines_volume'} ? $hsh{$key1}{'declines_volume'} * 100000 : 0,
	    defined $hsh{$key1}{'declines_value'} ? $hsh{$key1}{'declines_value'} * 100 : 0,
	    defined $hsh{$key1}{'unchanged'} ? $hsh{$key1}{'unchanged'} : 0,
	    defined $hsh{$key1}{'unchanged_volume'} ? $hsh{$key1}{'unchanged_volume'} * 100000 : 0,
	    defined $hsh{$key1}{'unchanged_value'} ? $hsh{$key1}{'unchanged_value'} * 100 : 0,
	    defined $hsh{$key1}{'timestamp'} ? $hsh{$key1}{'timestamp'} : 0 ;
	
	$line =~ s/,//g;
	$line =~ s/ /,/g;
	print $line, "\n";
    }
}

1;
