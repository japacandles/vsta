#!/bin/bash

#
# parse-crb-futures.sh
# This script is created so that the parse-crb-futures.pl script
# can get the commodity, the output and error files
# Date: Sep 14, 2013, Sunday

commodity=$1
timezone=$2

. ./configs.sh

#echo $commodity
#echo $timezone

# Nov 19, 2017 Saturday :)
# The following looks bad :(
# But its because Dow Jones 30 futures are named "Dow Indu 30 E-mini"
# Note the spaces :(
# So I had to write the hack below
echo $commodity | grep "Dow"
if [[ $? == 0 ]]
then
	filename=$DATADIR/us/cme/bhavcopies/raw/dji"-"$(TZ=$timezone date +%Y-%m-%d)
else 
	filename=$DATADIR/us/cme/bhavcopies/raw/$commodity"-"$(TZ=$timezone date +%Y-%m-%d)
fi
echo $filename

errorfile=$filename"-err"
echo $errorfile

./parse-barchart-futures.pl $commodity 1> $filename 2> $errorfile
