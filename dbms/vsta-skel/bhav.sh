#!/bin/bash

# 2 Mar 2019
# added code to go through a proxy
#
# 2 Feb 2019
# Make it possible to send multiple args to subordinate bhav.sh scripts
#
# 4 Jan 2011
#
# Top level bhav script
# This runs the bhav.sh scripts in other dirs

. ./configs.sh
. $VSTA_DIR/configs.sh
export VSTADIR DBMSDIR PERL5LIB VSTA_DIR VSTA_DBMS_DIR VSTA_DATA_DIR RECITATOR

THISDIR=$(pwd)
cd $RECITATOR

if [ "$PASCO" -gt 0 ]; then
	. ./pasco.sh
	# export BROWSER
fi

if [ "$VICARIUS" -gt 0 ]; then
	. ./vicarius.sh
	#export HTTP_PROXY HTTPS_PROXY http_proxy https_proxy
fi	

cd $THISDIR

#. ./common-functions.sh

export TOP_DBMS_DIR=$PWD
export DATADIR

COUNTRY_EXG=$1
shift

cd $COUNTRY_EXG
./bhav.sh $@
RETVAL=$?
cd -
exit $RETVAL
