package FunctionsVstaMoneyCnn;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw (
    money_cnn_browser
    read_money_cnn_main
    read_money_cnn_data
    );

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use FunctionsVstaHelper;

sub money_cnn_browser {
    
    # Create my browser
    my $m = WWW::Mechanize->new();
    # I add these headers because I randomly get "denied" errors without them
    $m->add_header("User-Agent" => random_ua_str());
    $m->add_header("Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    $m->add_header("Accept-Language" => "en-us,en;q=0.5");
    $m->add_header("Accept-Encoding" => "gzip, deflate");
    $m->add_header("Connection" => "keep-alive");

    return $m;
}
 
sub read_money_cnn_main {

    my ($m, $cnn_section, $index, $href) = @_;
    my $url;
    
    # Get page 1
    if ($cnn_section =~ /symb/) { $url = 'http://money.cnn.com/quote/quote.html?' . $cnn_section . '=' . $index . '&page=1'; }
    else { $url = 'http://money.cnn.com/data/' . $cnn_section . '/' . $index . '/?page=1'; }
    $ENV{'DEBUG'} && print STDERR $url, "\n";
    $m->get($url) or die $!;
    my ($p, $q, %hash);
    %hash = %$href;
    
    if ($m->success) { 
	$p = HTML::TokeParser->new(\$m->content);
	$q = HTML::TokeParser->new(\$m->content);
    };
    
    #print STDERR "sending $cnn_section\n";
    my $count = how_many_pages($cnn_section, $p);
    $ENV{'DEBUG'} && print STDERR "From the sub ", $count, "\n";
    
    # Parse page 1 which we already have
    read_money_cnn_data($q, 1, $href);
    
    # Now get the rest of the pages and parse them
    for my $page (2 .. $count) {
	$ENV{'DEBUG'} && print STDERR "Now getting page no ", $page, " of $count\n";
	if ($cnn_section =~ /symb/) { $url = 'http://money.cnn.com/quote/quote.html?' . $cnn_section . '=' . $index . '&page=1'; }
	else { $url = 'http://money.cnn.com/data/' . $cnn_section . '/' . $index . '/?page=1'; }
	
	#$url = 'http://money.cnn.com/data/' . $cnn_section . '/' . $index . '/?page=' . $page;
	$m->get($url) or die $!;
	if ($m->success) { 
	    $q = HTML::TokeParser->new(\$m->content);
	    read_money_cnn_data($q, $page , $href);
	}
    }
}

# This subroutine reads an HTML::TokeParser, parses 
# the 6th table and updates breadth data and volumes
sub read_money_cnn_data {

    my ($a, $page, $hashref) = @_;

    my ($flag, $change, $volume);

    if ($page == 1) {
	# The 2nd table contains close
	$a->get_tag("table"); #$a->get_tag("table"); $a->get_tag("table");
	$a->get_tag("span"); $hashref->{'close'} = decomma($a->get_trimmed_text("/span")); #print STDERR "close is ",$hashref->{'close'}, "\n";
	
	# The 4th table constains OHL data and misc stuff
	$a->get_tag("table");
	
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'prev_close'} = decomma($a->get_trimmed_text("/td"));
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'open'} = decomma($a->get_trimmed_text("/td"));
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'high'} = decomma($a->get_trimmed_text("/td"));
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'low'} = decomma($a->get_trimmed_text("/td"));
	
	# The 5th table isn't relevant
	$a->get_tag("table"); $a->get_tag("/table"); 
	$a->get_tag("div"); $a->get_tag("div"); #$a->get_tag("div"); 
	$hashref->{'timestamp'} = $a->get_trimmed_text("/span");
	
	# The 6th table is where its at
	$a->get_tag("table"); $a->get_tag("/tr");
	while (1) {
	    $a->get_tag("td"); # Name of company
	    $a->get_tag("td"); # Price
	    
	    $a->get_tag("td"); # Change
	    my $change = $a->get_trimmed_text("/td"); #print STDERR "change is $change", "\n";
	    if ($change =~ /^\+/) { 
		$flag = "ADVANCES";
	    } elsif ($change =~ /^\-/) {
		$flag = "DECLINES";
	    } else {
		$flag = "UNCHANGED";
	    }
	    
	    #print STDERR $change, " ", $flag, "\n";
	    $a->get_tag("td"); #  %Change
	    $a->get_tag("td"); # P/E
	    
	    # volume
	    $a->get_tag("td");
	    $volume = decomma($a->get_trimmed_text("/td")); #print STDERR "volume is $volume\n";
	    
	    if ($volume =~ /K$/) { chop $volume; $volume *= 1000; }
	    if ($volume =~ /M$/) { chop $volume; $volume *= 1000000; }
	    if ($volume =~ /B$/) { chop $volume; $volume *= 1000000000; }

	    $hashref->{'volumes'} += $volume ;
	    if ($flag eq "ADVANCES") { $hashref->{'advances'}++ ; $hashref->{'advances_volumes'} += $volume; }
	    if ($flag eq "DECLINES") { $hashref->{'declines'}++ ; $hashref->{'declines_volumes'} += $volume; }
	    if ($flag eq "UNCHANGED") { $hashref->{'unchanged'}++ ; $hashref->{'unchanged_volumes'} += $volume; }
	    $ENV{'DEBUG'} && print STDERR "volume: $volume change: $change\n";	    
	    #print STDERR join ",", $change, $flag, $volume,"\n";
	    
	    $a->get_tag("td"); $a->get_tag("/tr"); 
	    my $token = $a->get_token;
	    
	    last if ($token->[1] eq "tbody");
	}

    }

    else { # if page no exceeds 1

	$a->get_tag("table"); # print STDERR "table 1 : ", $a->get_trimmed_text("/table"), "\n";
	$a->get_tag("table"); # print STDERR "table 2 : ", $a->get_trimmed_text("/table"), "\n";
	$a->get_tag("table"); # print STDERR "table 3 : ", $a->get_trimmed_text("/table"), "\n";
	$a->get_tag("table"); # print STDERR "table 4 : ", $a->get_trimmed_text("/table"), "\n";

	while (1) {

	    #$a->get_tag("tr"); #print STDERR "headers col : ", $a->get_trimmed_text("/tr");

	    $a->get_tag("td"); my $n = $a->get_trimmed_text("/td"); #print STDERR "Name : ", $n, "\n"; 
	    last if ($n =~ /^\s{0,}$/);
	    
	    $a->get_tag("td"); #print STDERR "close : ", $a->get_trimmed_text("/td"), "\n";
	    $a->get_tag("td"); $change = $a->get_trimmed_text("/td"); #print STDERR "change : ", $d, "\n";
	    $a->get_tag("td"); #print STDERR "change %age : ", $a->get_trimmed_text("/td"), "\n";
	    $a->get_tag("td"); #print STDERR "p/e : ", $a->get_trimmed_text("/td"), "\n";
	    $a->get_tag("td"); $volume = $a->get_trimmed_text("/td"); #print STDERR "Volume : ", $v, "\n";

	    if ($change =~ /^\+/) {
		$flag = "ADVANCES";
	    } elsif ($change =~ /^\-/) {
		$flag = "DECLINES";
	    } else {
		$flag = "UNCHANGED";
	    }

	    if ($volume =~ /K$/) { chop $volume; $volume *= 1000; }
	    if ($volume =~ /M$/) { chop $volume; $volume *= 1000000; }
	    if ($volume =~ /B$/) { chop $volume; $volume *= 1000000000; }
	    $ENV{'DEBUG'} && print STDERR "volume: $volume change: $change\n";

	    $hashref->{'volumes'} += $volume ;
	    if ($flag eq "ADVANCES") { $hashref->{'advances'}++ ; $hashref->{'advances_volumes'} += $volume; }
	    if ($flag eq "DECLINES") { $hashref->{'declines'}++ ; $hashref->{'declines_volumes'} += $volume; }
	    if ($flag eq "UNCHANGED") { $hashref->{'unchanged'}++ ; $hashref->{'unchanged_volumes'} += $volume; }

	    my $c1 = $a->get_token(); #print STDERR Dumper($c1);
	    my $c2 = $a->get_token(); #print STDERR Dumper($c2);
	    my $c3 = $a->get_token(); #print STDERR Dumper($c3);

	    $a->unget_token($c3); $a->unget_token($c2); $a->unget_token($c1);

	    $a->get_tag("/tr");

	    my $token = $a->get_token; last if ($token->[1] eq "tbody");
	}
	
    }

    $ENV{'DEBUG'} && print "In sub\n", Dumper($hashref), "\n";

}
# This subroutine takes and HTML::TokeParser tree object 
# of the page 1 of the index page and returns the number
# of pages that this index occupies :)
sub how_many_pages {
    my ($section, $a) = @_;
    
    #print $STDERR "section is ", $section, "\n";
    #print Dumper(\$section);
    
    my $skip_input_tags;

    if ($section =~ /world_markets/) { 	$skip_input_tags = 4; } 
    else { $skip_input_tags = 5; }
    
    for (1 .. $skip_input_tags) { $a->get_tag("input"); }

    my $page_str = $a->get_text;
    $ENV{'DEBUG'} && print STDERR "Page string is ", $page_str, "\n";
    $page_str =~ /(\d+)/;
    $ENV{'DEBUG'} && print STDERR "No of pages is ", $1, "\n";
    return $1;
}

1;
