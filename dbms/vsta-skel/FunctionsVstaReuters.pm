package FunctionsVstaReuters;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw (
    get_price
    get_volume_breadth
    print_bhavcopy
    );

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use FunctionsVstaHelper;


#########################################
# get_price
# inputs : 
#   a. the content of the http request
#      from which data is to be parsed
#   b. the ref to the hash in which data
#      is to be stored
# outputs :
#   none : all the data is in the hash
#########################################
sub get_price {

    my ($r, $ref) = @_;
    my $tmp;

    #print "In get_price\n";
    my $p = HTML::TokeParser->new($r);

    $p->get_tag("form"); $p->get_tag("form");
    $p->get_tag("/form");

    # Close
    for (1 .. 4) { $p->get_tag("div"); }
    $tmp = $p->get_trimmed_text("/div");
    $tmp =~ s/,//g;
    $ref->{'close'} = $tmp;

    # Open
    for (1 .. 6) { $p->get_tag("div"); }
    $tmp = $p->get_trimmed_text("/div");
    $tmp =~ s/,//g;
    $ref->{'open'} = $tmp;

    $p->get_tag("table");
    $p->get_tag("tr"); 

    # High
    $p->get_tag("tr");
    $p->get_tag("td"); $p->get_tag("td");
    $tmp = $p->get_trimmed_text("/td");
    $tmp =~ s/,//g;
    $ref->{'high'} = $tmp;

    $p->get_tag("tr"); $p->get_tag("/tr"); # 52 week high SKIP

    # Low
    $p->get_tag("tr");
    $p->get_tag("td"); $p->get_tag("td");
    $tmp = $p->get_trimmed_text("/td");
    $tmp =~ s/,//g;
    $ref->{'low'} = $tmp;

    $p->get_tag("tr"); $p->get_tag("/tr"); # 52 week low SKIP
    $p->get_tag("/table");

    # Timestamp
    $p->get_tag("div");
    $tmp = $p->get_trimmed_text("/div");
    $tmp =~ s/\s+/-/g;
    $tmp =~ s/\W//g;
    $ref->{'timestamp'} = $tmp;

    #print Dumper($ref);
}

#########################################
# get_volume_breadth
# inputs : 
#   a. the content of the http request
#      from which data is to be parsed
#   b. the ref to the hash in which data
#      is to be stored
# outputs :
#   none : all the data is in the hash
#########################################
sub get_volume_breadth {
    my ($r, $ref) = @_;

    my ($dir, $tmp, $volume);

    #print "In get_volume_breadth\n";
    my $p = HTML::TokeParser->new($r);

    # The 4th table contains the data
    for (1 .. 4) { $p->get_tag("table"); }
    $p->get_tag("tr");

    # I need this infinite loop because I don't know how many rows
    # it can have. I do know that if I get </tr></form> then
    # this page doesn't have any more data. So I define this infinite
    # loop and also an exit condition
    while (1) {
	$p->get_tag("tr");
	
	# name of the security
	$p->get_tag("td"); #print $p->get_trimmed_text("/a"), "\n";
 
	$p->get_tag("td"); # close SKIP
	$p->get_tag("td"); # up/down figure SKIP

	# determine the direction
	# and the adv/dec numbers
	$p->get_tag("td"); 
	$tmp = $p->get_trimmed_text("/td");
	if ($tmp eq "+0.00") { 
	    $dir = "unchanged"; 
	    $ref->{'unchanged'}++ ;
	}
	elsif ($tmp =~ /\+/) { 
	    $dir = "up" ;
	    $ref->{'advances'}++ ;
	} else { 
	    $dir = "down"; 
	    $ref->{'declines'}++ ;
	}
	

	# the actual volume
	$p->get_tag("td");
	$tmp = $p->get_trimmed_text("/td");
	$tmp =~ s/,//g;
	my $volume = $tmp;	
	if ( defined $ref->{'volume'} 
	     and $ref->{'volume'} !~ /^\s{0,}$/) 
	{ $ref->{'volume'} += $volume; }
	
	# advances' volumes / declines' Volumes / unch's volumes
	if ($dir eq "up") { $ref->{'advances_volume'}+= $volume; }
	elsif  ($dir eq "down") { $ref->{'declines_volume'}+= $volume; }
	else { $ref->{'unchanged_volume'}+= $volume; }

	$p->get_tag("/tr");

	my $token1 = $p->get_token();
	#print Dumper(\$token1);
	my $token2 = $p->get_token();
	#print Dumper(\$token2);

	last if (($token2->[0] eq "E") and ($token2->[1] eq "form"));

	# We are here. This means that there is at least one more row :-)
	$p->unget_token($token2);
	$p->unget_token($token1);

    }

    #print Dumper($ref);

    # Now the fix for Nov 28, 2015 :-)
    # We sum the adv, decl and unch volumes and report that as volume
    if (not defined $ref->{'volume'} or $ref->{'volume'} == 0) {
	$ref->{'volume'} =
	    (defined($ref->{'advances_volume'}) ? $ref->{'advances_volume'} : 0)
	    + (defined($ref->{'declines_volume'}) ? $ref->{'declines_volume'} : 0)
	    + (defined($ref->{'unchanged_volume'}) ? $ref->{'unchanged_volume'} : 0);
    }
}

sub print_bhavcopy {

    my ($tmp) = @_;
    my %h = %{$tmp};

    print join " ", 
        defined $h{"open"} ? $h{"open"} : 0,
        defined $h{"high"} ? $h{"high"} : 0,
        defined $h{"low"} ? $h{"low"} : 0,
        defined $h{"close"} ? $h{"close"} : 0,
        defined $h{"volume"} ? $h{"volume"} : 0,
        defined $h{"advances"} ? $h{"advances"} : 0,
        defined $h{"advances_volume"} ? $h{"advances_volume"} : 0,
        defined $h{"declines"} ? $h{"declines"} : 0,
        defined $h{"declines_volume"} ? $h{"declines_volume"} : 0,
        defined $h{"unchanged"} ? $h{"unchanged"} : 0,
        defined $h{"unchanged_volume"} ? $h{"unchanged_volume"} : 0,
        defined $h{"timestamp"} ? $h{"timestamp"} : 0,
        "\n";
}
