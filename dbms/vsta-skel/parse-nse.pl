#!/usr/bin/perl

# parse-nse.pl
# 
# Consolidated script to retrieve spot index and index futures' data from nseindia.com.
# this script removes the duplication caused by the multiplicity of 
# parse-nse-spot, parse-nse-deriv and parse-nse-deriv-emergency scripts.
# this script also delegates the functions to FunctionsVstaNse
#
# Usage:
# DEBUG=1 perl parse-nse.pl spot
# DEBUG=1 perl parse-nse.pl futures
# DEBUG=1 perl parse-nse.pl NSE_NO_TRACEABLE_URL=1 'http://nseindia.com/live_market/dynaContent/live_watch/get_quote/GetQuoteFO.jsp?underlying=NIFTYIT&instrument=FUTIDX&expiry=27DEC2018&type=-&strike=-'
# or you can skip the DEBUG variable :)
#
# Changelog:
# Dec 23, 2018, Sunday

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
#use WWW::Mechanize;
#use HTML::TokeParser;
#use JSON;
use FunctionsVstaHelper;
use FunctionsVstaNse;

# Check usage
if ($#ARGV < 0) {

    print STDERR join "\n",
	"Usage:",
	"DEBUG=1 perl parse-nse.pl spot",
	"DEBUG=1 perl parse-nse.pl futures",
	"DEBUG=1 perl parse-nse.pl NSE_NO_TRACEABLE_URL=1 'http://nseindia.com/live_market/dynaContent/live_watch/get_quote/GetQuoteFO.jsp?underlying=NIFTYIT&instrument=FUTIDX&expiry=27DEC2018&type=-&strike=-'",
	"or you can skip the DEBUG variable :)",
	"\n";
    exit(1);
}

my %urls;
my %futures_urls;
my %hash;
my $link;

my $DEBUG = 0;
if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }

# create a browser so that cookies etc can be shared across subroutines
# and add headers to imitate a true browser
my $m = nse_browser();

# NSE_NO_TRACEABLE_URL mode
# Ideally the links for individual futures' are traceable from, say
# http://www.nseindia.com/live_market/dynaContent/live_watch/fomktwtch_FUTIDXNIFTYIT.htm
# But at the end of 2016, the NIFTY IT futures were not traceable from that url
# but I was able to get the data from the individual urls that I had to get via jugaad.
# so if we have been called with NSE_NO_TRACEABLE_URL=1 with the argument as
# http://nseindia.com/live_market/dynaContent/live_watch/get_quote/GetQuoteFO.jsp?underlying=NIFTYIT&instrument=FUTIDX&expiry=27DEC2018&type=-&strike=-
# the following stanza is executed
if ($ARGV[0] =~ /NSE_NO_TRACEABLE_URL/i) { 

    $link = $ARGV[1];

    print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,\n";

    get_futures_data($link, $m);
}

# Futures mode on :)
#
# We first get a list of futures' urls 
# example: Nifty, BankNifty, CNX-IT etc
# From this list we will derive a second list
# in which we will have a list of urls to individual 
# futures' urls, such as Nifty-Dec-2013
# Lastly we actually get the data for each of these
# futures.
# The reason that we need to get the data for individual
# futures separately, is because only the future's 
# individual page contains open interest data

elsif ($ARGV[0] =~ /FUTURES/i) {

    %urls = populate_urls_hash($m); $DEBUG && print STDERR "Urls dump is \n", Dumper(\%urls);

    print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,\n";

    for $link (sort keys %urls) {
	%futures_urls = get_futures_urls($link, $m); $DEBUG && print STDERR "Futures urls dump is \n", Dumper(\%futures_urls);
	
	for $link (sort keys %futures_urls) {
	    $DEBUG && print STDERR "Link is ", $link, "\n";
	    
	    get_futures_data($link, $m);
	}
    }
}

# Spot mode on
else {

    %urls = create_spot_index_list($m);
    
    print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,\n";
    
    for my $key (keys %urls) {
        %hash = parse_nse_spot_data($key, $urls{$key}, $m);
    }
    $DEBUG && print STDERR "Urls are\n", Dumper(\%urls);
    $DEBUG && print STDERR "Hash is\n", Dumper(\%hash), "\n";

}
