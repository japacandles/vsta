#!/usr/bin/perl

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my %hash;

# create a browser so that cookies etc can be shared across subroutines
# and add headers to imitate a true browser
my $m = WWW::Mechanize->new();
$m->agent_alias('Windows IE 6');
$m->add_header('Accept' => 'image/png,image/*;q=0.8,*/*;q=0.5');
$m->add_header('Accept-Language' => 'en-US,en;q=0.5');
$m->add_header('Accept-Encoding' => 'gzip, deflate');

my $idx = shift;
my $date = shift;

unless (defined $idx) {
    print "Usage:\n",
      "perl parse-nyx-futures.pl <index-name>\n",
      "Example:\n",
      "For FTSE 100 futures, the index name is Z-DLON, so\n",
    "perl parse-nyx-futures.pl Z-DLON\n";
    exit(1);
}

get_nyx_futures_data($m, \%hash, $date);
clean_futures_data(\%hash);
#print Dumper(\%hash);
print_bhavcopy(\%hash);

sub get_nyx_futures_data {

    my ($browser, $hashref, $d) = @_;

    my $nyxUrl = 'https://globalderivatives.nyx.com/en/products/index-futures/' .  
	$idx ;
 
    $browser->get($nyxUrl);

    my $p  = HTML::TokeParser->new(\$m->content());

    # name of the security
    $p->get_tag("h1"); 
    my $index = $p->get_trimmed_text("/h1");
    $index =~ s/\s+/-/g;
    #print "Index is ", $index, "\n";

    # timestamp
    $p->get_tag("table"); $p->get_tag("table");
    $p->get_tag("tr");$p->get_tag("tr");
    $p->get_tag("td");$p->get_tag("td"); $p->get_tag("td");
    $p->get_tag("span"); my $timestamp = $p->get_trimmed_text("/span");
    $timestamp =~ s/\s+/-/g;
    #print "Timestamp is ", $timestamp, "\n";
    
    $p->get_tag("table"); $p->get_tag("tr");

    while (1) {

	$p->get_tag("tr");

	# expiry and timestamp
	$p->get_tag("td");
	my $expiry = $p->get_trimmed_text("/td");
	$expiry =~ s/\s+/-/g;

	my $key = uc($index . "-" . $expiry);

	$p->get_tag("td"); 
	$hashref->{$key}{'timestamp'} =  $p->get_trimmed_text("/td");

	$hashref->{$key}{'date'} = $d;

	# close
	$p->get_tag("td"); 
	$hashref->{$key}{'close'} =  $p->get_trimmed_text("/td");

	# last trade volume : Ignore
	$p->get_tag("td");

	# day volume
	$p->get_tag("td");
	my $tmp = $p->get_trimmed_text("/td");
	$tmp =~ s/,//g;
	$hashref->{$key}{'volume'} = $tmp;
	
	# bid size: Ignore
	$p->get_tag("td");
	# bid price: Ignore
	$p->get_tag("td");
	# ask price: Ignore
	$p->get_tag("td");
	# ask size: Ignore
	$p->get_tag("td");
	# move direction size: Ignore
	$p->get_tag("td");

	# ask size: Ignore
	$p->get_tag("td");
	$hashref->{$key}{'open'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$key}{'high'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$key}{'low'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$key}{'settlement'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$key}{'openInterest'} = $p->get_trimmed_text("/td");
	$hashref->{$key}{'openInterest'} =~ s/,//g; 

	#print Dumper($hashref), "\n";
	$p->get_tag("/tr"); # this </tr>

	# this table ends with </tr></tbody>
	# if we are there, we need to exit
	# else we need to loop back to the top of loop
	my $token = $p->get_token();
	#print Dumper(\$token);
	if (($token->[0] =~ /S/i)
	    and ($token->[1] =~ /tr/)) {
	    $p->unget_token($token);
	    next;
	} elsif (($token->[0] =~ /E/i) 
		 and ($token->[1] =~ /tbody/i)) {
	    last;
	}
    }
}

sub clean_futures_data {

    my $hashref = shift;

    for my $key (keys %{$hashref}) {
	if ($hashref->{$key}{'open'} eq '-') { 
	    #print $key, "\n" ; 
	    delete $hashref->{$key};
	}
    }
}

sub print_bhavcopy {

    my $hashref = shift;

    for my $key (keys %{$hashref}) {
	print join ",", $key,
              defined ($hashref->{$key}{'date'}) ? $hashref->{$key}{'date'} : 0, 
	      defined ($hashref->{$key}{'open'}) ? $hashref->{$key}{'open'} : 0,
	      defined ($hashref->{$key}{'open'}) ? $hashref->{$key}{'high'} : 0,
	      defined ($hashref->{$key}{'low'}) ? $hashref->{$key}{'low'} : 0, 
	      defined ($hashref->{$key}{'close'}) ? $hashref->{$key}{'close'} : 0,
 	      defined ($hashref->{$key}{'settlement'}) ? $hashref->{$key}{'settlement'} : 0,
	      defined ($hashref->{$key}{'volume'}) ? $hashref->{$key}{'volume'} : 0, 
	      defined ($hashref->{$key}{'openInterest'}) ? $hashref->{$key}{'openInterest'} : 0,
	      defined ($hashref->{$key}{'timestamp'}) ? $hashref->{$key}{'timestamp'} : 0,
	"\n";
    }
}
