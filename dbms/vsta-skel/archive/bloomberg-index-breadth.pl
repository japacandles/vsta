#!/usr/bin/perl -w

#
# Date: Jul 1, 2012, Sunday
# Refactored bloomberg-index.pl into
# bloomberg-index-quotes.pl and
# bloomberg-index-breadth.pl
# so that I could use bloomberg for australian indexes
#
use strict;
use diagnostics;
use LWP::UserAgent;
use WWW::Mechanize;
use HTML::TokeParser;
use Data::Dumper;

# Bloomberg supplies details for a select 5 intl indexes
my %movers = (
    #"NKY:IND" => "http://www.bloomberg.com/markets/stocks/movers/nikkei-225/", # Nikkei 225
    "NKY:IND" => "http://www.bloomberg.com/quote/nky:ind/members", # Nikkei 225
    "UKX:IND" => "http://www.bloomberg.com/markets/stocks/movers/ftse-100/",   # FTSE 100
    #"DAX:IND" => "http://www.bloomberg.com/markets/stocks/movers/dax/",        # DAX
    "DAX:IND" => "http://www.bloomberg.com/quote/dax:IND/members",        # DAX
    "IBOV:IND" => "http://www.bloomberg.com/markets/stocks/movers/bovespa/",   # Bovespa
    "INDU:IND" => "http://www.bloomberg.com/markets/stocks/movers/dow/",       # Dow Jones Ind Avg
    );
#for (keys %majors) { print join " ", $_, $majors{$_}, "\n"; }

my $index = shift;
$index = uc($index);

my $token;

my %hash;

#my $bloomberg_idx_base_url = "http://www.bloomberg.com/apps/quote?ticker=";
my $bloomberg_idx_base_url = "http://www.bloomberg.com/quote/";
my $url = $bloomberg_idx_base_url . $index . "/members";

my $ua = LWP::UserAgent->new;

#if (defined $movers{$index}) {

#$url = $movers{$index};

my $m = WWW::Mechanize->new();

# For volumes
my $req = $m->get($url);

if ($m->success) { 
    
    my $html_string = $m->content;
    #print $html_string;
    
    my $p = HTML::TokeParser->new(\$html_string);
    
    # Data starts on the 16th table
    #for (0 .. 16) { $p->get_tag("table"); }
    for (0 .. 4) { $p->get_tag("table"); }
    
    # Loop through the entire table looking for data
    while (1) {
	
	$p->get_tag("td"); # Name 
	#$hash{'name'} = $p->get_trimmed_text("/td");
	#print "Name is ", $p->get_trimmed_text("/td"), "\n";
	
	$p->get_tag("td"); # Close 
	#$hash{'close'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td"); # Change
	
	$p->get_tag("td"); # % Change
	my $change = $p->get_trimmed_text("/td");
	#print "Change is $change\n";
	$change =~ s/\%$//;
	#print "Change is $change\n\n";
	#$hash{'percent_change'} = $change;
	
	$p->get_tag("td"); # Volume
	my $volume = $p->get_trimmed_text("/td");
	$volume =~ s/,//g;
	
	if ($change < 0) {
	    $hash{'declines'}++;
	    $hash{'declines_volume'} += $volume;
	    
	} elsif ($change > 0) {
	    $hash{'advances'}++;
	    $hash{'advances_volume'} += $volume;
	    
	} elsif ($change == 0) {
	    $hash{'unchanged'}++;
	    $hash{'unchanged_volumes'} += $volume;
	}
	
	$hash{'volume'} += $volume;
	
	$p->get_tag("/tr");
	#print Dumper(\%hash);
	
	my $token = $p->get_token(); $token = $p->get_token();
	last if ( ($token->[0] =~ /E/i) and ($token->[1] =~ /tbody/i));
	$p->unget_token; $p->unget_token;
	#   }
    }
}

#print Dumper(\%hash);

# Print in bhavcopy format
print join " ",
    defined($hash{'volume'}) ? $hash{'volume'}: 0,
    defined($hash{'advances'}) ? $hash{'advances'}: 0,
    defined($hash{'advances_volume'}) ? $hash{'advances_volume'}: 0,
    defined($hash{'declines'}) ? $hash{'declines'}: 0,
    defined($hash{'declines_volume'}) ? $hash{'declines_volume'}: 0,
    defined($hash{'unchanged'}) ? $hash{'unchanged'}: 0,
    defined($hash{'unchanged_volumes'}) ? $hash{'unchanged_volumes'}: 0,
    "\n";

sub decomma {
    my ($str) = @_;
    $str =~ s/,//g;
    return $str;
}
