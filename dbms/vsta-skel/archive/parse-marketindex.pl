#!/usr.bin/perl

# July 17th, 2016
# marketindex.com.au has changed the design of its website
# they now use quoteapi.com to send json
# we will trap the json and get the info we need
use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;
use JSON;

my $tmp;
my %hash;

# we need 2 arguments
if ($#ARGV != 1) {
	print "Usage:\n",
	      "parse-marketindex.pl foo bar\n",
			"Examples:\n",
			"parse-marketindex.pl xtl asx-20 | tee -a ~/vsta/data/au/asx/futures/asx-20 \n",
			"parse-marketindex.pl xfl asx-50 | tee -a ~/vsta/data/au/asx/futures/asx-50 \n",
			"parse-marketindex.pl xto asx-100 | tee -a ~/vsta/data/au/asx/futures/asx-100 \n",
			"parse-marketindex.pl xjo asx-200 | tee -a ~/vsta/data/au/asx/futures/asx-200 \n",
			"parse-marketindex.pl xko asx-300 | tee -a ~/vsta/data/au/asx/futures/asx-300 \n",
			"parse-marketindex.pl xao asx-all | tee -a ~/vsta/data/au/asx/futures/asx-aord \n";
			exit(1);
}

my $index1 = shift;
my $index2 = shift;
$index2 =~ /asx-(.+)/ ;
my $foo = $1;

my $u = LWP::UserAgent->new();
$u->agent("Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

my $indexUrl = 'https://quoteapi.com/api/v4/symbols/'. $index1 . '.asx?appID=af5f4d73c1a54a33';
my $response = $u->get($indexUrl);
#print $response->content, "\n";

my $hashref = decode_json $response->content;
#print Dumper($hashref);

for ('open', 'high', 'low', 'close', 'time') {
	$hash{$_} = $hashref->{'quote'}->{$_};
}

$hash{'index'} = $index2;

my $volumeUrl = 'https://quoteapi.com/api/v4/symbol-lists/mjg' . $foo . '?appID=af5f4d73c1a54a33';
#print $volumeUrl, "\n";

$response = $u->get($volumeUrl);
#print $response->content, "\n";

$hashref = decode_json $response->content;
#print Dumper($hashref);
my %h = %{$hashref};

for (@{$h{'items'}}) {
    #print Dumper $_;
    #print join " ", $_->{'symbol'}, $_->{'quote'}->{'change'}, "\n";

    $hash{'volume'} += $_->{'quote'}->{'volume'};
    $hash{'value'} += $_->{'quote'}->{'value'};

    if ($_->{'quote'}->{'change'} > 0) {
	$hash{'advances'}++;
	$hash{'advancesVolume'} += $_->{'quote'}->{'volume'};
	$hash{'advancesValue'} += $_->{'quote'}->{'value'};
    }

    elsif ($_->{'quote'}->{'change'} < 0) {
	$hash{'declines'}++;
	$hash{'declinesVolume'} += $_->{'quote'}->{'volume'};
	$hash{'declinesValue'} += $_->{'quote'}->{'value'};

    }
    
    else {
	$hash{'unchanged'}++;
	$hash{'unchangedVolume'} += $_->{'quote'}->{'volume'};
	$hash{'unchangedValue'} += $_->{'quote'}->{'value'};

    }
 
}

if (not defined $hash{'advances'} or $hash{'advances'} == 0) {
    $hash{'advances'} = 0, $hash{'advancesVolume'} = 0, $hash{'advancesValue'} = 0;
}
if (not defined $hash{'declines'} or $hash{'declines'} == 0) {
    $hash{'declines'} = 0, $hash{'declinesVolume'} = 0, $hash{'declinesValue'} = 0;
} 
if (not defined $hash{'unchanged'} or $hash{'unchanged'} == 0) {
    $hash{'unchanged'} = 0, $hash{'unchangedVolume'} = 0, $hash{'unchangedValue'} = 0;
}

#print Dumper(\%hash);

for ( 'open', 'high', 'low', 'close',
      'volume', 'value', 
      'advances', 'advancesVolume', 'advancesValue',
      'declines', 'declinesVolume', 'declinesValue',
      'unchanged', 'unchangedVolume', 'unchangedValue',
      'time', 'index') {
    print $hash{$_}, " ";

}
print "\n";
