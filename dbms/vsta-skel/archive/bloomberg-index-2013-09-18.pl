#!/usr/bin/perl

# bloomberg-index-2013-09-18.pl
# This script was written on the morning of
# Sep 18, 2013, Wednesday. I am at Hotel Royal Tranquility,
# Bengaluru, near Manyata Embassy Tech Park.
# Last night, bloomberg.com has changed the design of their
# data pages. So this script had to be written. I am glad 
# the design seems to be clearerthan my earlier hacks :)
#
# Changelog:
#
# Sep 18, 2013 : 
#   * Initial creation
#   * new naming convention : the name contains the date of creation :)
#   * modular design :)
#
# Sep 20, 2013, Friday
# Output to bhavcopy format

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

# Usage
my $index = shift;
unless (defined $index) {
    print STDERR "Usage:\n",
                 "bloomberg-index-2013-09-18.pl <name-of-index>\n",
                 "Example: for Dow Jones Industrial Average, the name of the index is INDU:IND\n",
                 "bloomberg-index-2013-09-18.pl INDU:IND\n";
    exit(1);
}

# Define the urls we will check
my $bloombergQuoteUrl = 'http://www.bloomberg.com/quote/' ;
my $priceUrl = $bloombergQuoteUrl . $index;
my $volumeUrl = $bloombergQuoteUrl . $index . '/members'; 

my %hash;

get_price_data($priceUrl);
if ($hash{"has_volumes"}) {
    get_volume_breadth_data($volumeUrl);
}

#print Dumper(\%hash);
print_bhavcopy(\%hash);

###############
# Subroutines
###############

# get-price_data
# populates the hash-table with price data
sub get_price_data {
    my $url = shift;
    my $tmp;

    # Create the browser
    my $u = WWW::Mechanize->new(
	agent => "Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

    $u->get($url);
    unless (defined $u->success) {
	print STDERR "Problems getting $url";
	exit(2);
    }

    my $p = HTML::TokeParser->new(\$u->content());
    my $q = HTML::TokeParser->new(\$u->content());

    # Skip the first 3 tables
    $p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table"); 

    $p->get_tag("h2");
    $hash{"security"} = $p->get_trimmed_text("/h2");

    $p->get_tag("h3");
    $hash{"securityCode"} = $p->get_trimmed_text("/h3");

    $p->get_tag("span");
    $tmp = $p->get_trimmed_text("/span");
    $tmp =~ s/,//g;
    $hash{"close"} = $tmp;

    # data continues fourth table
    $p->get_tag("table");    

    $p->get_tag("tr"); 
    
    $p->get_tag("td");
    $tmp = $p->get_trimmed_text("/td");
    $tmp =~ s/,//g;
    $hash{"open"} = $tmp;

    # Range => low to high
    $p->get_tag("td");
    $tmp = $p->get_trimmed_text("/td");
    $tmp =~ s/,//g;
    my ($low, $high) = split / - /, $tmp;
    $hash{"low"} = $low;
    $hash{"high"} = $high;

    $q->get_tag("p");
    $hash{"timestamp"} = $q->get_trimmed_text("/p");

    # Does this security have separate volume/breadth sections ?
    $hash{"has_volumes"} = 0;
    for ($u->links) {
	if ($_->url =~ /members$/) { $hash{"has_volumes"} = 1; }
    }
}

# get_volume_breadth_data
# populates the hash-table with volumes and breadth
# data. This sub will be called only if bloomberg
# provides volume and breadth data for the security 
sub get_volume_breadth_data {
    my $url = shift;

    my $upDownFlag;
    my $tmp;

    # Create the browser
    my $u = WWW::Mechanize->new(
	agent => "Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

    $u->get($url);
    unless (defined $u->success) {
	print STDERR "Problems getting $url";
	exit(3);
    }
    
    my $p = HTML::TokeParser->new(\$u->content());

    # Skip the first 5 tables
    $p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table"); 
    $p->get_tag("table"); $p->get_tag("table"); 
    
    $p->get_tag("tr");

    # iterate over each row of the table,
    # looking for volume data of components of the 
    # index. The volumes are added, then breadth
    # is computed, by checking if the security 
    # has risen, fallen or stayed unchanged
    while (1) {

	$p->get_tag("tr");
	$upDownFlag = "";
	
	# Did this stock advance, decline or stay where it was ?
	$p->get_tag("td"); $p->get_tag("td"); $p->get_tag("td");
	$tmp = $p->get_trimmed_text("/td");
	if ($tmp =~ /\+/) { $upDownFlag = "UP"; }
	elsif  ($tmp =~ /\-/) { $upDownFlag = "DOWN"; }
	else { $upDownFlag = "UNCHANGED" }
	
	# Get the volume as it is
	$p->get_tag("td"); $p->get_tag("td"); 
	$tmp = $p->get_trimmed_text("/td");
	$tmp =~ s/,//g;
	$hash{"volume"} += $tmp;
	
	# Build the breadth numbers
	if ($upDownFlag eq "UP") {
	    $hash{"advances"}++;
	    $hash{"advances_volume"} += $tmp;
	} elsif ($upDownFlag eq "DOWN") {
	    $hash{"declines"}++;
	    $hash{"declines_volume"} += $tmp;
	} else {
	    $hash{"unchanged"}++;
	    $hash{"unchanged_volume"} += $tmp;
	}
	
	#print Dumper(\%hash);
	$p->get_tag("/tr");
	my $token1 = $p->get_token;
	my $token2 = $p->get_token;
	#print Dumper($token1);
	#print Dumper($token2);
	if (($token2->[0] eq 'E') and ($token2->[1] eq 'tbody')) { last; }
	else {
	    $p->unget_token($token2); 
	    $p->unget_token($token1);
	}
    }
    
}

sub print_bhavcopy {

    my ($tmp) = @_;
    my %h = %{$tmp};

    print join " ", 
	defined $h{"open"} ? $h{"open"} : 0,
	defined $h{"high"} ? $h{"high"} : 0,
	defined $h{"low"} ? $h{"low"} : 0,
	defined $h{"close"} ? $h{"close"} : 0,
	defined $h{"volume"} ? $h{"volume"} : 0,
	defined $h{"advances"} ? $h{"advances"} : 0,
	defined $h{"open"} ? $h{"advances_volume"} : 0,
	defined $h{"declines"} ? $h{"declines"} : 0,
	defined $h{"declines_volume"} ? $h{"declines_volume"} : 0,
	defined $h{"unchanged"} ? $h{"unchanged"} : 0,
	defined $h{"unchanged_volume"} ? $h{"unchanged_volume"} : 0,
	defined $h{"timestamp"} ? $h{"timestamp"} : 0,
	"\n";
}
