#!/usr/bin/perl -w

# 
# Aug 6, 2010, Friday 00:08 AM :)
#
use strict;
use diagnostics;
use LWP::UserAgent;
use HTML::TokeParser;
use Data::Dumper;

my $symbol = shift;
#my $infile = shift;
my ($timestamp, $open, $high, $low, $close,
	$advances, $declines, $unchanged, 
	$advanceVolumes, $declineVolumes, $unchangedVolumes,  
	$totalVolumes, 
	$ad, $vol);
my $count = 0;
my $token = "" ;

my $bloombergUrl = 'http://www.bloomberg.com/apps/quote?ticker=';
my $url = $bloombergUrl . $symbol;
#print $url;

my $ua = LWP::UserAgent->new();
my $request = HTTP::Request->new(GET => $url);
my $response = $ua->request($request);
my $htmlString = $response->as_string;

my $p = HTML::TokeParser->new(\$htmlString) or die $!;
$p->empty_element_tags(1);

# Loop through spans to get last-traded info
while (1) {
	#print $count++, "\n"; 
	$p->get_tag("span");
	if ($p->get_trimmed_text("span") =~ /Last Update:/) {
		$p->get_tag("span"); 
		$timestamp = $p->get_trimmed_text("/span");
		print "Timestamp is ", $timestamp, "\n";
		$p->get_tag("span"); 
		$timestamp .= " " . $p->get_trimmed_text("/span");
		print "Timestamp is ", $timestamp, "\n";
		last;
	}
}

# Get to the 1st table
$p->get_tag("table"); 

# Data starts in the row 2
$p->get_tag("tr"); $p->get_tag("tr");

# Row 2, col 2 is the close
$p->get_tag("/td"); 
$close = $p->get_text("/td");$p->get_tag("/tr");
$close =~ s/,//g; $close =~ s/\s+//g;
#print $close, "\n";

# Row 3 is about change from previous. SKIP
$p->get_tag("/tr");

# Row 4 is about open
$p->get_tag("/td");
$open = $p->get_text("/td"); $p->get_tag("/td");
$open =~ s/,//g; $open =~ s/\s+//g;
#print $open, "\n";

# Row 5 is about high
$p->get_tag("/td");
$high = $p->get_text("/td"); $p->get_tag("/td");
$high =~ s/,//g; $high =~ s/\s+//g;
#print $high, "\n";

# Row 6 is about low
$p->get_tag("/td");
$low = $p->get_text("/td"); $p->get_tag("/td");
$low =~ s/,//g; $low =~ s/\s+//g;
#print $low, "\n";

print join " ", $open, $high, $low, $close, "\n";
