#!/usr/bin/perl

# parse-nse-spot-2013-10-14.pl
# no inputs
# discovers all the spot indices on nse
# and dumps their data in bhavcopy format to stdout
#
# Changelog:
#
# Sat Dec 22, 2018
# Use random user agent
#
# Sun Oct 14, 2013
# Initial commit

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use JSON::PP;
use FunctionsVstaHelper;
use FunctionsVstaNse;

my %urls;
my %hash;

# create a browser so that cookies etc can be shared across subroutines
# and add headers to imitate a true browser
#my $m = WWW::Mechanize->new();
#$m->agent_alias('Windows IE 6');
#$m->add_header('Accept' => 'image/png,image/*;q=0.8,*/*;q=0.5');
#$m->add_header('Accept-Language' => 'en-US,en;q=0.5');
#$m->add_header('Accept-Encoding' => 'gzip, deflate');
my $m = nse_browser();

create_spot_index_list($m);
for my $key (keys %urls) {
    parse_nse_spot_data($key, $urls{$key}, $m);
}
#print "Urls are\n", Dumper(\%urls);
#print "Hash is\n", Dumper(\%hash), "\n";
print_bhavcopy(\%hash);


sub create_spot_index_list {

    my $browser = shift;

    my $url = 'http://www.nseindia.com/live_market/dynaContent/live_watch/equities_stock_watch.htm?cat=N';
    $browser->get($url);

    my $p = HTML::TokeParser->new(\$browser->content());
    $p->get_tag("select");

    while (1) {

	my $tag = $p->get_tag("option");
	#print Dumper($tag);
	last unless (defined $tag);
	#print "Tag is ", "\n", Dumper(\$tag), "\n";
	last unless (defined($tag->[1]->{'value'}));
	#print $tag->[1]->{'value'}, "\n";
	$urls{ $tag->[1]->{'value'} } = $p->get_trimmed_text("/option");
	$p->get_token();
	#print "Now urls hash is\n", Dumper(\%urls), "\n";
	my $token = $p->get_token();
	#print "token is\n", Dumper(\$token), "\n";

    }
}

sub parse_nse_spot_data {

    my ($idx, $index, $browser) = @_; 
    my $url = 'http://www.nseindia.com/live_market/dynaContent/live_watch/stock_watch/' 
	. $idx . 'StockWatch.json';
    $browser->get($url);

    #print "Index is ", $index, "\n";
    
    my $str = $browser->content();
    my $json = JSON::PP->new();
    my $h = $json->allow_nonref->utf8->relaxed->escape_slash->loose->allow_barekey->allow_singlequote->decode($str);
    #print Dumper(\$h);
    
    $hash{$index}{'timestamp'} = $h->{'time'};
    
    $hash{$index}{'open'} = $h->{'latestData'}->[0]->{'open'};
    $hash{$index}{'high'} = $h->{'latestData'}->[0]->{'high'};
    $hash{$index}{'low'} = $h->{'latestData'}->[0]->{'low'};
    $hash{$index}{'ltp'} = $h->{'latestData'}->[0]->{'ltp'};

    my @ary = @{$h->{'data'}};
    #print Dumper(\@ary);
    
    for my $element (@ary) {
	#print $element->{'symbol'}, "\n";
	if (defined $element->{'trdVol'}) {
	    $element->{'trdVol'} =~ s/,//g;
	    $hash{$index}{'volume'} += $element->{'trdVol'}; 
	}
	 
	if (defined $element->{'ntP'}) {
	    $element->{'ntP'} =~ s/,//g;
	    $hash{$index}{'value'} += $element->{'ntP'} ;
	}

	if ((defined $element->{'ntP'}) and (defined $element->{'ptsC'}) and ($element->{'ptsC'} > 0)) {
	    $hash{$index}{'advances'}++;
	    $hash{$index}{'advances_volume'} += $element->{'trdVol'} if defined $element->{'trdVol'};
	    $hash{$index}{'advances_value'} += $element->{'ntP'} if defined $element->{'ntP'};
	} 

	elsif  ((defined $element->{'ntP'}) and ($element->{'ptsC'} < 0)) {
	    $hash{$index}{'declines'}++;
	    $hash{$index}{'declines_volume'} += $element->{'trdVol'} if defined $element->{'trdVol'};
	    $hash{$index}{'declines_value'} += $element->{'ntP'} if defined $element->{'ntP'};
	} 

	else {
	    $hash{$index}{'unchanged'}++;
	    $hash{$index}{'unchanged_volume'} += $element->{'trdVol'} if defined $element->{'trdVol'};
	    $hash{$index}{'unchanged_value'} += $element->{'ntP'} if defined $element->{'ntP'};
	}
    }
}

sub print_bhavcopy {

    my ($h) = @_;
    my %hsh = %{$h};
    for my $key1 (keys %hsh) {

	my $name = $key1;
	$name =~ s/ /-/g;

	$name =~ s/junior-nifty/cnx-nifty-junior/i; # CNX Nifty Jr
	$name =~ s/cnx-bank/bank-nifty/i; # CNX Bank Nifty
	$name =~ s/Opportunities/OPPT/i; # CNX Dividend Opportunities
	$name =~ s/service-sector/service/i; # CNX Service Sector
	$name =~ s/Infrastructure/Infra/i; # CNX Infrastructure

	next unless (defined $hsh{$key1}{'open'});

	my $line = join " ", "FUTIDX",
	    uc($name), "SPOT", "0", "XX",
	    defined $hsh{$key1}{'open'} ? $hsh{$key1}{'open'} : 0,
	    defined $hsh{$key1}{'high'} ? $hsh{$key1}{'high'} : 0,
	    defined $hsh{$key1}{'low'} ? $hsh{$key1}{'low'} : 0,
	    defined $hsh{$key1}{'ltp'} ? $hsh{$key1}{'ltp'} : 0,
	    defined $hsh{$key1}{'ltp'} ? $hsh{$key1}{'ltp'} : 0,
	    defined $hsh{$key1}{'volume'} ? $hsh{$key1}{'volume'} * 100000 : 0,
	    defined $hsh{$key1}{'value'} ? $hsh{$key1}{'value'} * 100 : 0,
	    defined $hsh{$key1}{'advances'} ? $hsh{$key1}{'advances'} : 0,
	    defined $hsh{$key1}{'advances_volume'} ? $hsh{$key1}{'advances_volume'} * 100000 : 0,
	    defined $hsh{$key1}{'advances_value'} ? $hsh{$key1}{'advances_value'} * 100 : 0,
	    defined $hsh{$key1}{'declines'} ? $hsh{$key1}{'declines'} : 0,
	    defined $hsh{$key1}{'declines_volume'} ? $hsh{$key1}{'declines_volume'} * 100000 : 0,
	    defined $hsh{$key1}{'declines_value'} ? $hsh{$key1}{'declines_value'} * 100 : 0,
	    defined $hsh{$key1}{'unchanged'} ? $hsh{$key1}{'unchanged'} : 0,
	    defined $hsh{$key1}{'unchanged_volume'} ? $hsh{$key1}{'unchanged_volume'} * 100000 : 0,
	    defined $hsh{$key1}{'unchanged_value'} ? $hsh{$key1}{'unchanged_value'} * 100 : 0,
	    defined $hsh{$key1}{'timestamp'} ? $hsh{$key1}{'timestamp'} : 0 ;
	
	$line =~ s/,//g;
	$line =~ s/ /,/g;
	print $line, "\n";
    }
}
