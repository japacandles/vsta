#!/usr/bin/perl -w

#
# Changelog:
#
# 2 Aug 2010
# Added a "or die $!" clause to the top of
# createPreliminaryHash and
# getDataFromUrl
# to try and stop the yahoo timeout errors
#
# 28 July 2010
# Trivial refactoring lines 277-279 
# from
# if ($c->{data} =~ /$month\s+$day/) { $relevance = 1; }
#     if ($c->{data} =~ /AM/) { $relevance = 1; }
#     if ($c->{data} =~ /PM/) { $relevance = 1; }
# to
# if ($c->{data} =~ /$month\s+$day/ || $c->{data} =~ /AM/ || $c->{data} =~ /PM/)
#
#
# 27 July 2010
# Changed line 208 to
#     if (defined($n->text) && $n->text =~ /Next/) {
# This was necessary because there were situations where
# $n->text was undef
#
# 16 Aug 2009
# The Finance::YahooQuote getonequote() allows me to get volumes
#
#
#
# 18 Mar 2009
# Yahoo Finance started to use some "<span id=foo>" tags 
# in the volume cell. Had to adjust the func sumVolFromHTML thus
#     $c->{data} =~ s/\<span .*\"\>//;
#     $c->{data} =~ s/\<\/span\>//;
#
#
# 26 Jan 2009
# Trivial refactoring
# From
# if (($cellCount == 5) && ($relevance == 1)) {
# ...
#	if ($ad == 1 && ($relevance == 1)) ...
# To
# if (($cellCount == 5) && ($relevance == 1)) {
# ...
#	if ($ad == 1)) ...
#
#
# 1 Dec 2008
# Corrected spelling Dev => Dec in %month2no
#

use strict;
use diagnostics;
use Carp;
use Finance::YahooQuote;
use WWW::Mechanize;
use HTML::TableContentParser;
use Data::Dumper;

my @urls;
my $idx = shift;
my $baseURL = 'http://finance.yahoo.com';
my $componentsURL  = '/q/cp?s=';
my $scripCount; 
my $date;

use vars qw(
	$baseURL
	%urls
	$componentsURL
	$m
	$scripCount
	@quotes
	%ohlcv
	$date
);

# Using YahooQuote to get ohlc data
my @quotes = getonequote($idx); 
#print join "\n", @quotes, "============\n";

#
# From man Finance::YahooQuote
#
$ohlcv{ 'Name' } = $quotes[1];
$ohlcv{ 'Close' } = $quotes[2];
$ohlcv{ 'Volume' } = $quotes[7];
$ohlcv{ 'Open' } = $quotes[12];
my @tmp = split / /, $quotes[13];
$ohlcv{ 'Low' } = $tmp[0];
$ohlcv{ 'High' } = $tmp[$#tmp];
$date = $quotes[3];

my $m = WWW::Mechanize->new();
Date();
createPreliminaryHash();

for (keys %urls) { getDataFromURL( $urls{ $_ } ); } 

#print Dumper(\%ohlcv);
#&dumpData();
printBhavcopy();

##########################################################

#
# Subroutines
#

sub printBhavcopy {
	print $ohlcv{ 'Date' }, " ",
		$ohlcv{ 'Open' }, " ",
		$ohlcv{ 'High' }, " ", 
		$ohlcv{ 'Low' }, " ",
		$ohlcv{ 'Close' }, " "; 
	if (defined($ohlcv{ 'Volume' })) { print $ohlcv{ 'Volume' }, " "; } # Sometimes volumes are not available
	if (defined($ohlcv{ 'Advances' })) { print $ohlcv{ 'Advances' }, " "; } # Niether are advances
	if (defined($ohlcv{ 'Advances Volume' })) { print $ohlcv{ 'Advances Volume' }, " "; } # so obviously advances volumes aren't there either
	print "\n";
}

sub Date {

	my %month2no = (
		'Jan' => '01', 'Feb' => '02', 'Mar' => '03',
		'Apr' => '04', 'May' => '05', 'Jun' => '06',
		'Jul' => '07', 'Aug' => '08', 'Sep' => '09',
		'Oct' => '10', 'Nov' => '11', 'Dec' => '12');

	my %no2month = (
		1 => 'Jan', 2 => 'Feb', 3 => 'Mar',
		4 => 'Apr', 5 => 'May', 6 => 'Jun',
		7 => 'Jul', 8 => 'Aug', 9 => 'Sep',
		10 => 'Oct', 11 => 'Nov', 12 => 'Dec');

	my @tmp = split /\//, $date;
	
	my $month = $month2no{ $no2month{ $tmp[0] } };

	my $day = $tmp[1];
	if ($day < 10) { $day = "0". $day; };
	
	my $year = $tmp[$#tmp];

	# print join " ", "Debug ", $date, $month, $day, $year, "\n";
	$ohlcv{ 'Date' } = $year . $month . $day;
}

#
# Yahoo Finance limits the component list to
# 50 per page with a next button
# This porcedure simply creates the first list
sub createPreliminaryHash {
	$m->get($baseURL . $componentsURL . $idx) || die $!;
	for my $n ($m->links) {
		$urls{ $n->text } = $n  if ($n->url =~ /alpha\=/);
	}
	
	#print "The relevant links on this page are\n";
	#for (keys %urls) { print $_, " ", $urls{ $_ }->url, "\n"; }
}

#
# Merely boilerplate code for other procs
# from man HTML::TableContentParser
#
sub dumpData {
	my ($tableCount, $rowCount, $cellCount) = ();
	print "Now, about the content of the tables\n";
	my $p = HTML::TableContentParser->new();
	my $html = $m->content();
	my $tables = $p->parse($html);
	for my $t (@$tables) {
		$rowCount = 0;
		print "Table number ", $tableCount++, "\n";
		for my $r (@{$t->{rows}}) {
			$cellCount = 0;
			print "Row number ", $rowCount++, "\n";
			print "Row: " , "\n";
	        	for my $c (@{$r->{cells}}) {
				print "Cell number ", $cellCount++, "\n";
	        		print "[$c->{data}] ", "\n";
			}		
	 		print "\n";
	 	}
	}
}

#
# This procedure gets individual web pages
# For each page this proc calls another proc
# to sum the volumes
# It also recursively calls itself in case it
# finds a "Next" link
#
sub getDataFromURL {

	my $url = shift;

	#print "Getting ", $url->text, " ", $baseURL . $url->url, "\n";
	$m->get( $baseURL . $url->url ) || die $!;

	# Sum up all the volumes
	sumVolFromHTML($m->content);
	#dumpData($m->content);

	# Is there a next ?
	my $nextURL;
	#for my $n ( $m->links ) {
	for my $n ( $m->links ) {
		if (defined($n->text) && $n->text =~ /Next/) {
			#print "Next link is ", $n->url, "\n";
			$nextURL = $n;
		}
	}
	if (defined ($nextURL)) { getDataFromURL( $nextURL ); }
} 

sub sumVolFromHTML {

	my $html = shift;

	# We need to have the date in the correct format so that
	# we are aware if the security was traded on the day
	# whose index values we are using
	my %months = (
		1 => 'Jan', 2 => 'Feb', 3 => 'Mar',
		4 => 'Apr', 5 => 'May', 6 => 'Jun',
		7 => 'Jul', 8 => 'Aug', 9 => 'Sep',
		10 => 'Oct', 11 => 'Nov', 12 => 'Dec');

	my @tmp = split /\//, $date;
	my $month = $months{ $tmp[0] };
	my $day = $tmp[1];
	my $dt = $month . " " . $day;
	#print "Workable date is ", $dt, "\n";

	my ($tableCount, $rowCount, $cellCount) = ();
	my $p = HTML::TableContentParser->new();
	my $tables = $p->parse($html);

	for my $t (@$tables) {
		$rowCount = 0;
		$tableCount++;
		#print "Table Count is ", $tableCount, "\n";
		if ($tableCount == 12) {
			for my $r (@{$t->{rows}}) {
				$cellCount = 0;
				$rowCount++;
				my $relevance = 0; # Unset relevance flag
				my $ad = 0; # Unset adv/decl flag
				if ($rowCount > 1) {
	        			for my $c (@{$r->{cells}}) {
						$cellCount++;
						#print join ",,",  "Cell contents are ",
						#	$tableCount, $rowCount, $cellCount,  $c->{data}, "\n";

						#
						# Remove the "<small></small>"
						#
						#next if ($c->{data} =~ /\<small\>\<\/small\>/);
						
						if ($cellCount == 3) {
							# if security was traded today, set relevance flag
							if (defined($c->{data}) && ($c->{data} =~ /$month\s+$day/ || $c->{data} =~ /AM/ || $c->{data} =~ /PM/))
								{ $relevance = 1; }
						}
						if (($cellCount == 4) && ($relevance == 1)) {
							if ($c->{data} =~ /Up/) { $ohlcv{ 'Advances' }++; $ad = 1; }
							if ($c->{data} =~ /Down/) { $ohlcv{ 'Declines' }++; $ad = 0; }
						}
						if (($cellCount == 5) && ($relevance == 1)) {
						#if ($cellCount == 5) {
							$c->{data} =~ s/,//g;
							$c->{data} =~ s/\<span .*\"\>//;
							$c->{data} =~ s/\<\/span\>//;
							$ohlcv{ 'Volume' } += $c->{data};
							#if ($ad == 1 && ($relevance == 1)) { $ohlcv{ 'Advances Volume' } += $c->{data}; } 
							if ($ad == 1) { $ohlcv{ 'Advances Volume' } += $c->{data}; } 
							else { $ohlcv{ 'Declines Volume' } += $c->{data}; }
						}	
					}
				}
			}
		}
	}
}
