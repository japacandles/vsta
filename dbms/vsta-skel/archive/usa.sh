#!/bin/bash

#
# usa.sh
#
# 25 May 2010, Tuesday
#
# Added bovespa
#
# 25 Oct 2008, Saturday
#
# This progy updates the records of the major USA indexes.
# It should be run at 6 AM IST, Tuesday to Saturday
#

. ./configs.sh
#DIR="/NotBackedUp/nse/fo"

echo Dow Jones Industrial Average
#/usr/bin/perl $DBMSDIR/yf3.pl ^dji | tee -a $DATADIR/newyork-nyse/dji
/usr/bin/perl $DBMSDIR/bloomberg-index.pl indu:ind | tee -a $DATADIR/newyork-nyse/dji

echo Standard and Poor 500
/usr/bin/perl $DBMSDIR/yf3.pl ^gspc | tee -a $DATADIR/newyork-nyse/sp500

echo New York Composite Index NYA
/usr/bin/perl $DBMSDIR/yf3.pl ^nya | tee -a $DATADIR/newyork-nyse/nya

echo Nasdaq Composite
/usr/bin/perl $DBMSDIR/yf3.pl ^ixic | tee -a $DATADIR/newyork-nasdaq/nasdaq-comp

echo Bovespa
#/usr/bin/perl $DBMSDIR/yf3.pl ^bvsp | tee -a $DATADIR/saopaulo-bovespa/bvsp
/usr/bin/perl $DBMSDIR/bloomberg-index.pl ibov:ind | tee -a $DATADIR/saopaulo-bovespa/bvsp

