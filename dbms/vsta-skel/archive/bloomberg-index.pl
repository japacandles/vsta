#!/usr/bin/perl -w

#
# Sat, May 25, 2013
# bloomberg.com has switched the positions of open and low
#
# Date: Thu, Jan 26, 2012
# Again, almost a full rewrite
# Bloomberg have created a page primarily of <div>s
#
# Date: Sun, Aug 21, 2011
# Almost omplete rewrite
#
# Date: Fri, Jun 15, 2012
# Now data is in 5th table, not 16th as earlier
#
use strict;
use diagnostics;
use LWP::UserAgent;
use WWW::Mechanize;
use HTML::TokeParser;
use Data::Dumper;

# Bloomberg supplies details for a select 5 intl indexes
my %movers = (
    "NKY:IND" => "http://www.bloomberg.com/markets/stocks/movers/nikkei-225/", # Nikkei 225
    "UKX:IND" => "http://www.bloomberg.com/markets/stocks/movers/ftse-100/",   # FTSE 100
    #"DAX:IND" => "http://www.bloomberg.com/markets/stocks/movers/dax/",        # DAX
    "DAX:IND" => "http://www.bloomberg.com/quote/dax:IND/members",        # DAX
    "IBOV:IND" => "http://www.bloomberg.com/markets/stocks/movers/bovespa/",   # Bovespa
    "INDU:IND" => "http://www.bloomberg.com/markets/stocks/movers/dow/",       # Dow Jones Ind Avg
    );
#for (keys %majors) { print join " ", $_, $majors{$_}, "\n"; }

my $index = shift;
$index = uc($index);

my %hash;

#my $bloomberg_idx_base_url = "http://www.bloomberg.com/apps/quote?ticker=";
my $bloomberg_idx_base_url = "http://www.bloomberg.com/quote/";
my $url = $bloomberg_idx_base_url . $index;

my $ua = LWP::UserAgent->new;

# For OHLC
my $req = HTTP::Request->new( GET => $url );
my $res = $ua->request( $req );

if ($res->is_success) { 
    #print $res->as_string; 
    my $html_string = $res->as_string;

    my $p = HTML::TokeParser->new(\$html_string);

    # The 65th (!) div
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 5
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 10
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 15
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 20
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 25
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 30
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 35
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 40
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 45
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 50
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 55
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 60
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 65

    # Last trade time
    $p->get_tag("span"); $p->get_tag("span"); $p->get_tag("span");
    $hash{'last_traded'} = $p->get_trimmed_text("/span");

    $p->get_tag("div"); $p->get_tag("div"); # 67
    $p->get_tag("span");  
    $hash{'close'} =  decomma($p->get_trimmed_text("/span"));

    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 70
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 75
    #$p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 78

    #print Dumper($p);
    
    $p->get_tag("span"); $p->get_tag("span"); $p->get_tag("span");
    $hash{'high'} = decomma($p->get_trimmed_text("/span"));

    # Open
    $p->get_tag("div"); $p->get_tag("span"); $p->get_tag("span");$p->get_tag("span");  
    $hash{'open'} = decomma($p->get_trimmed_text("/span"));

    # Low
    $p->get_tag("div"); $p->get_tag("span"); $p->get_tag("span");  $p->get_tag("span"); 
    $hash{'low'} = decomma($p->get_trimmed_text("/span"));

    #print Dumper(\%hash);

} else {
    print "Problems getting the url for index ", $index, "\n";
    exit(1);
}

if (defined $movers{$index}) {

    $url = $movers{$index};

    my $m = WWW::Mechanize->new();

    # For volumes
    my $req = $m->get($url);

    if ($m->success) { 

	my $html_string = $m->content;
	#print $html_string;

	my $p = HTML::TokeParser->new(\$html_string);

	# Data starts on the 16th table
	#for (0 .. 16) { $p->get_tag("table"); }
	for (0 .. 4) { $p->get_tag("table"); }

	# Loop through the entire table looking for data
	while (1) {
	    
	    $p->get_tag("td"); # Name 
	    #$hash{'name'} = $p->get_trimmed_text("/td");
	    #print "Name is ", $p->get_trimmed_text("/td"), "\n";
	    
	    $p->get_tag("td"); # Close 
	    #$hash{'close'} = $p->get_trimmed_text("/td");
	    
	    $p->get_tag("td"); # Change
	    
	    $p->get_tag("td"); # % Change
	    my $change = $p->get_trimmed_text("/td");
	    #print "Change is $change\n";
            $change =~ s/\%$//;
	    #print "Change is $change\n\n";
	    #$hash{'percent_change'} = $change;
	    
	    $p->get_tag("td"); # Volume
	            my $volume = $p->get_trimmed_text("/td");
	    $volume =~ s/,//g;
	    
	    if ($change < 0) {
		$hash{'declines'}++;
		$hash{'declines_volume'} += $volume;
		
	    } elsif ($change > 0) {
		$hash{'advances'}++;
		$hash{'advances_volume'} += $volume;
		
	    } elsif ($change == 0) {
		$hash{'unchanged'}++;
		$hash{'unchanged_volumes'} += $volume;
	    }
	    
	    $hash{'volume'} += $volume;
	    
	    $p->get_tag("/tr");
	    #print Dumper(\%hash);
	    
	    my $token = $p->get_token(); $token = $p->get_token();
	    last if ( ($token->[0] =~ /E/i) and ($token->[1] =~ /tbody/i));
	    $p->unget_token; $p->unget_token;
	}
    }
}

#print Dumper(\%hash);

# Print in bhavcopy format
print join " ",
    defined($hash{'open'}) ? $hash{'open'}: 0,
    defined($hash{'high'}) ? $hash{'high'}: 0,
    defined($hash{'low'}) ? $hash{'low'}: 0,
    defined($hash{'close'}) ? $hash{'close'}: 0,
    defined($hash{'volume'}) ? $hash{'volume'}: 0,
    defined($hash{'advances'}) ? $hash{'advances'}: 0,
    defined($hash{'advances_volume'}) ? $hash{'advances_volume'}: 0,
    defined($hash{'declines'}) ? $hash{'declines'}: 0,
    defined($hash{'declines_volume'}) ? $hash{'declines_volume'}: 0,
    defined($hash{'unchanged'}) ? $hash{'unchanged'}: 0,
    defined($hash{'unchanged_volumes'}) ? $hash{'unchanged_volumes'}: 0,
    defined($hash{'last_traded'}) ? $hash{'last_traded'}: 0,
    "\n";

sub decomma {
    my ($str) = @_;
    $str =~ s/,//g;
    return $str;
}
