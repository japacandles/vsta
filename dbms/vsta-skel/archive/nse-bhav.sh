#!/bin/bash

#
# CHANGELOG
#
# 20 Dec 2012
# Bringing in an old set of scripts.
# json-parse.pl is adapted from a page on some website.
# attribution in the script.
# It retrieves the spot index quotes and prints out the data
# in bhavcopy format.
# nse-parse-fo-json.pl retrieves the f&o data and prints
# out the data in bhavcopy format. 
#
# 31 July 2011
# Return the retval of the web-scraping script
#
# 30 Apr 2010
# Wrote parse-nse-fo.pl which can be called at 4:00 PM IST 
# rather than 6:00 PM because it scrapes the individual 
# FUTIDX pages
#
# 9 Mar 2010
# nseindia switched to the nifty-sparks page
# nse-data-sparks will replace nse-data
#
# 1 Dec 2009
# nseindia.com suddenly started keepingf only zipped 
# copies of the F&O bhavcopies
#

RM="/bin/rm"

DIR=$PWD
DATADIR=`echo $DIR | sed 's/dbms/data/'`"/bhavcopies"
#/usr/bin/perl $DIR/nse-data.pl
#/usr/bin/perl $DIR/bse-data.pl

YEAR=`date "+%Y"`
#YEAR=2010
MONTH=`date "+%b" | tr a-z A-Z`
#MONTH="APR"
DAY=`date "+%d"`
#DAY="16"
month=`date "+%m"`

echo Getting Nifty 
#/usr/bin/perl $DIR/nse-data.pl | tee -a $DIR2/$DAY-$month-$YEAR
echo updating $DATADIR/$DAY-$month-$YEAR with nifty spot
#/usr/bin/perl $DIR/nse-data-sparks.pl >> $DATADIR/$DAY-$month-$YEAR

$RM -f /tmp/*json;
niftyStockWatchUrl="http://nseindia.com/live_market/dynaContent/live_watch/stock_watch/"
( 
	echo INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,
	for i in nifty juniorNifty niftyMidcap50 bankNifty cnxAuto cnxCommodities \
		cnxConsumption cnxDividendOppt cnxEnergy cnxFinance cnxFMCG cnxInfra \
		cnxMedia cnxMetal cnxMNC cnxPharma cnxPSE cnxPSU cnxRealty cnxService cnxit  
	do 
		jsonFile=$i"StockWatch.json"; 
		jsonUrl=$niftyStockWatchUrl$jsonFile;
		#echo "The url is " $jsonUrl 
		wget -q -U " " $jsonUrl -O /tmp/$jsonFile; 
		perl $DIR/json-parse.pl /tmp/$jsonFile; 
	done
) | tee -a $DATADIR/$DAY-$month-$YEAR

echo Getting Nifty futures
#URL='http://nseindia.com/content/historical/DERIVATIVES/'${YEAR}'/'${MONTH}'/'fo${DAY}${MONTH}${YEAR}bhav.csv
###URL='http://nseindia.com/content/historical/DERIVATIVES/'${YEAR}'/'${MONTH}'/'fo${DAY}${MONTH}${YEAR}bhav.csv.zip
#elinks -dump $URL |  grep -e "FUTIDX,MINIFTY," -e "FUTIDX,NIFTY," - | sed -e 's/ //g' - | tee -a  $DIR/$DAY-$month-$YEAR
###/usr/bin/GET $URL > /tmp/`basename $URL`
###/usr/bin/unzip -p /tmp/`basename $URL` | grep -e "FUTIDX,MINIFTY," -e "FUTIDX,NIFTY," - | tee -a  $DIR/$DAY-$month-$YEAR
echo updating $DATADIR/$DAY-$month-$YEAR with nifty futures
#perl $DIR/parse-nse-fo.pl | tee -a $DATADIR/$DAY-$month-$YEAR
#perl $DIR/parse-nse-fo.pl >> $DATADIR/$DAY-$month-$YEAR
#RETVAL=$?
#cat $DATADIR/$DAY-$month-$YEAR
/usr/bin/perl $DIR/nse-parse-fo-json.pl | tee -a $DATADIR/$DAY-$month-$YEAR
RETVAL=$?
exit $RETVAL
