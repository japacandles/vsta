#!/usr/bin/perl

# parse-money-cnn.pl
# Date: Sat, Aug 26, 2012
# To parse money.cnn.com to retrieve index data including breadth data
# masterdata.com has stopped working after Thursday, Aug 23, 2012
# So I had to look for a new data source for S&P500
# money.cnn.com was do-able :)
# Sample usage:
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl markets dow
# 13052.82 13175.51 13027.20 13157.97 442200000 24 328200000 5 103600000 1 10400000 Data as of 4:30pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl markets sandp
# 1401.99 1413.46 1398.04 1411.13 1968209100 396 1523609600 99 409916500 5 34683000 Data as of 4:45pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl markets nasdaq 
# 3045.22 3076.80 3042.22 3069.79 1298931464 1395 815596853 854 447745524 241 35589087 Data as of 5:19pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets nikkei225
# 9069.95 9090.67 9045.79 9070.76 818014000 26 162205300 191 619370700 7 36438000 Data as of 2:28am ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets hang_seng
# 19930.45 19930.45 19840.19 19880.03 1059502000 3 115200000 45 944302000 0 0 Data as of 4:01am ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets ftse100
# 5776.60 5791.39 5739.41 5776.60 484446000 51 194017300 46 288052400 3 2376300 Data as of 6:05pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets cac40
# 3432.10 3442.33 3396.81 3433.21 107296700 14 23749100 24 82047600 1 1500000 Data as of 6:20pm ET, 08/24/2012 
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn.pl world_markets dax
# 6957.60 6990.07 6885.98 6971.07 87694200 18 61360900 11 26333300 0 0 Data as of 12:30pm ET, 08/24/2012 

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

# If no CNN section is specified, exit immediately
my $cnn_section = shift;
unless (defined $cnn_section) {
    print STDERR "No CNN section specified: markets world_markets ?\n";
    exit (1);
}

# If no index name is supplied, exit immediately
my $index = shift;
unless (defined $index) {
    print "No index specified\n";
    exit (1);
}

# Create my browser
my $m = WWW::Mechanize->new();
# I add these headers because I randomly get "denied" errors without them
$m->add_header("User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");
$m->add_header("Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
$m->add_header("Accept-Language" => "en-us,en;q=0.5");
$m->add_header("Accept-Encoding" => "gzip, deflate");
$m->add_header("Connection" => "keep-alive");

# Get page 1
my $url = 'http://money.cnn.com/data/' . $cnn_section . '/' . $index . '/?page=1';
print STDERR $url, "\n";
$m->get($url) or die $!;
my ($p, $q, %hash);
if ($m->success) { 
	$p = HTML::TokeParser->new(\$m->content);
	$q = HTML::TokeParser->new(\$m->content);
};

#print STDERR "sending $cnn_section\n";
my $count = how_many_pages($cnn_section, $p);
print STDERR "From the sub ", $count, "\n";

# Parse page 1 which we already have
read_data($q, \%hash);

# Now get the rest of the pages and parse them
for my $page (2 .. $count) {
    print STDERR "Now getting page no ", $page, " of $count\n";
    $url = 'http://money.cnn.com/data/' . $cnn_section . '/' . $index . '/?page=' . $page;
    $m->get($url) or die $!;
    if ($m->success) { 
	$q = HTML::TokeParser->new(\$m->content);
	read_data($q, \%hash);
    }
}

#print Dumper(\%hash);
# Print in bhavcopy format
print join " ",
    $hash{'open'}, $hash{'high'}, $hash{'low'}, $hash{'close'}, $hash{'volumes'},
    defined($hash{'advances'}) ? $hash{'advances'} : 0,
    defined($hash{'advances_volumes'}) ? $hash{'advances_volumes'} : 0,
    defined($hash{'declines'}) ? $hash{'declines'} : 0,
    defined($hash{'declines_volumes'}) ? $hash{'declines_volumes'} : 0,
    defined($hash{'unchanged'}) ? $hash{'unchanged'} : 0,
    defined($hash{'unchanged_volumes'}) ? $hash{'unchanged_volumes'} : 0,
    $hash{'timestamp'}, "\n";

exit (0);

#
# SUBROUTINES
#

# remove comma from input string
sub decomma {
    my $str = shift;
    $str =~ s/,//g;
    return($str);
}

# This subroutine reads an HTML::TokeParser, parses 
# the 6th table and updates breadth data and volumes
sub read_data {
    my ($a, $hashref) = @_;

    my ($flag, $volume);

    # The 2nd table contains close
    $a->get_tag("table"); #$a->get_tag("table"); $a->get_tag("table");
    $a->get_tag("span"); $hashref->{'close'} = decomma($a->get_trimmed_text("/span")); #print STDERR "close is ",$hashref->{'close'}, "\n";
    
    # The 4th table constains OHL data and misc stuff
    $a->get_tag("table");
    
    $a->get_tag("td"); $a->get_tag("td"); $hashref->{'prev_close'} = decomma($a->get_trimmed_text("/td"));
    $a->get_tag("td"); $a->get_tag("td"); $hashref->{'open'} = decomma($a->get_trimmed_text("/td"));
    $a->get_tag("td"); $a->get_tag("td"); $hashref->{'high'} = decomma($a->get_trimmed_text("/td"));
    $a->get_tag("td"); $a->get_tag("td"); $hashref->{'low'} = decomma($a->get_trimmed_text("/td"));

    # The 5th table isn't relevant
    $a->get_tag("table"); $a->get_tag("/table"); 
    $a->get_tag("div"); $a->get_tag("div"); #$a->get_tag("div"); 
    $hashref->{'timestamp'} = $a->get_trimmed_text("/span");

    # The 6th table is where its at
    $a->get_tag("table"); $a->get_tag("/tr");
    while (1) {
	$a->get_tag("td"); # Name of company
	$a->get_tag("td"); # Price
	
	$a->get_tag("td"); # Change
	my $change = $a->get_trimmed_text("/td"); #print STDERR "change is $change", "\n";
	if ($change =~ /^\+/) { 
	    $flag = "ADVANCES";
	} elsif ($change =~ /^\-/) {
	    $flag = "DECLINES";
	} else {
	    $flag = "UNCHANGED";
	}
	
	#print STDERR $change, " ", $flag, "\n";
	$a->get_tag("td"); #  %Change
	$a->get_tag("td"); # P/E
	
	# volume
	$a->get_tag("td");
	$volume = decomma($a->get_trimmed_text("/td")); #print STDERR "volume is $volume\n";

	if ($volume =~ /K$/) { chop $volume; $volume *= 1000; }
	if ($volume =~ /M$/) { chop $volume; $volume *= 1000000; }
	if ($volume =~ /B$/) { chop $volume; $volume *= 1000000000; }
	print STDERR "volume is $volume\n";
	
	$hash{'volumes'} += $volume ;
	if ($flag eq "ADVANCES") { $hash{'advances'}++ ; $hash{'advances_volumes'} += $volume; }
	if ($flag eq "DECLINES") { $hash{'declines'}++ ; $hash{'declines_volumes'} += $volume; }
	if ($flag eq "UNCHANGED") { $hash{'unchanged'}++ ; $hash{'unchanged_volumes'} += $volume; }
	
	#print STDERR join ",", $change, $flag, $volume,"\n";
	
	$a->get_tag("td"); $a->get_tag("/tr"); 
	my $token = $a->get_token;
	
	last if ($token->[1] eq "tbody");
    }

}
# This subroutine takes and HTML::TokeParser tree object 
# of the page 1 of the index page and returns the number
# of pages that this index occupies :)
sub how_many_pages {
    my ($section, $a) = @_;
    
    #print $STDERR "section is ", $section, "\n";
    #print Dumper(\$section);
    
    my $skip_tables;

    if ($section =~ /world_markets/) { 	$skip_tables = 4; } 
    else { $skip_tables = 5; }
    
    for (1 .. $skip_tables) { $a->get_tag("table"); }
    $a->get_tag("/table");
    
    $a->get_tag("/span");
    $a->get_tag("div");
    my $page_str = $a->get_trimmed_text("/div");
    print STDERR "Page string is ", $page_str, "\n";
    $page_str =~ /(\d+)/;
    print STDERR "No of pages is ", $1, "\n";
    return $1;
}
