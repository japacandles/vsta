#!/usr.bin/perl
#
#https://eng.krx.co.kr/m1/m1_1/m1_1_1/JHPENG01001_01.jsp
use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;

my $tmp;
my %hash;

my $kospiUrl ='https://eng.krx.co.kr/m1/m1_1/m1_1_1/JHPENG01001_01.jsp';
my $u = LWP::UserAgent->new();
$u->agent("Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

my $response = $u->get('https://eng.krx.co.kr/m1/m1_1/m1_1_1/JHPENG01001_01.jsp');
#my $data = decode_utf8($response->content);

my $p = HTML::TokeParser->new(\$response->content);

$p->get_tag("table"); $p->get_tag("table");

# close
$p->get_tag("tr"); $p->get_tag("tr");
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
#print $tmp;
$hash{'close'} = $tmp;

# timestamp
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'timestamp'} = $tmp;

# Change ; skip
$p->get_tag("tr"); $p->get_tag("tr");

# Volume
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'volume'} = $tmp;

# Value
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'value'} = $tmp;

# Open
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'open'} = $tmp;

# Low
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'low'} = $tmp;

# High
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'high'} = $tmp;

# Unchanged
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'unchanged'} = $tmp;

# Advances
$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'advances'} = $tmp;

# Declines
$p->get_tag("tr"); $p->get_tag("td"); $tmp = $p->get_trimmed_text("/td");
$tmp =~ s/,//g; $tmp =~ s/\D$//g;
$hash{'declines'} = $tmp;

#print Dumper(\%hash);
print_bhavcopy(\%hash);

sub print_bhavcopy {

    my $h = shift;
    print join " ", $h->{'open'}, $h->{'high'},
                    $h->{'low'}, $h->{'close'},
                    $h->{'volume'}, $h->{'value'},
                    $h->{'advances'}, $h->{'declines'}, 
                    $h->{'unchanged'}, "\n";
}
