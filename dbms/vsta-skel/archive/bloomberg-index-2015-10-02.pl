#!/usr/bin/perl

#
# bloomberg-index-2015-10-02.pl
#
# Mar 22, 2016, Pune, Tuesday 17:38
# Bloomberg has moved their json around a bit again
# 
# Dec 8, 2015, Pune, Tuesday :) 13:31
# Bloomberg has moved their json around a bit
# 
# Oct 2, 2015, Pune 23:46 :-)
# This script should have been written at least 2 months ago
# The bloomberg website now seems to provide lots of json
# so I don't need to claw my way through html as much :-)
#
use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use JSON;

# Usage
my $index = shift;
if ((not defined $index) or ($index !~ /:IND$/i)) {
    print STDERR "Usage:\n",
                 "bloomberg-index-2015-10-02.pl <name-of-index>\n",
                 "Example:\n", 
                 "for Dow Jones Industrial Average, the name of the index is INDU:IND\n",
		 "for FTSE 100, the name of the index is UKX:IND\n";
    exit(1);
}

$index = uc($index);

# Define the urls we will check
my $bloombergQuoteUrl = 'http://www.bloomberg.com/quote/' ;
my $priceUrl = $bloombergQuoteUrl . $index;
my $volumeUrl = $bloombergQuoteUrl . $index . '/members'; 

my %hash;

get_price_data($priceUrl);
if ($hash{"has_volumes"}) {
    #print "Calling breadth with $volumeUrl\n";
    get_volume_breadth_data($volumeUrl);
}

#print Dumper(\%hash);
print_bhavcopy(\%hash);

###############
# Subroutines
###############

# get-price_data
# populates the hash-table with price data
sub get_price_data {
    my $url = shift;
    my $tmp;
    my $token ;
    
    # Create the browser
    my $u = WWW::Mechanize->new(
        agent => "Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

    $u->get($url);
    unless (defined $u->success) {
        print STDERR "Problems getting $url";
        exit(2);
    }

    # First of all, does this index have a "members" page from
    # which we can get breadth data ?
    #print Dumper($u->links), "\n";
    $hash{"has_volumes"} = 0;
    for ($u->links) {
	if ($_->[5]->{'href'} =~ /quote\/$index\/members/) {
	    #print "volumes found\n";
	    $hash{"has_volumes"} = 1;
	    last;
	}
    }
    
    my $p = HTML::TokeParser->new(\$u->content());
    
    $p->get_tag("body");

    # 'tis the 9th "script" tag
    for ($tmp = 0; $tmp < 8; $tmp++) {
	$token = $p->get_tag("script");
	#print join "\n", $tmp, Dumper($token);
	#print $p->get_trimmed_text("/script"); 
    }
    my $full_script_text = $p->get_trimmed_text("/script");
    
    # the relevant json snip is between "basicQuote" ad "detailedQuote"
    $full_script_text =~ /\"basicQuote\"\:(.*)\,\"detailedQuote\"/ ; 
    #print "full text\n", $full_script_text, "\n";
    my $basic_json = $1;
    #print "basic json\n", $basic_json, "\n";
    
    # parse the json to get the data internal to it
    my $data_struct = from_json($basic_json);
    #print Dumper($data_struct);

    # populate the hash
    $hash{"Open"} = $data_struct->{"openPrice"};
    $hash{"High"} = $data_struct->{"highPrice"};
    $hash{"Low"} = $data_struct->{"lowPrice"};
    $hash{"Close"} = $data_struct->{"price"};
    $hash{"Volume"} = $data_struct->{"volume"};
    $hash{"timeStamp"} = $data_struct->{"priceDate"} . " " . $data_struct->{"nyTradeEndTime"};

    #print "hash in get_price_data\n", Dumper(\%hash);
}

# get_volume_breadth_data
# populates hash with breadth data
sub get_volume_breadth_data {

    my $token;
    
    my ($volumeUrl) = @_;
    #print "Volume available from $volumeUrl\n";
    
    # Create the browser
    my $u = WWW::Mechanize->new(
        agent => "Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

    $u->get($volumeUrl);
    unless (defined $u->success) {
        print STDERR "Problems getting $volumeUrl";
        exit(2);
    }
    
    my $p = HTML::TokeParser->new(\$u->content());

    $p->get_tag("body");

    # the 4th script contains the json
    for (0 .. 6) {
	$token = $p->get_tag("script");
	#print join "\n", $_ , Dumper($token);
	#print $p->get_trimmed_text("/script");
    }
    my $full_script_text = $p->get_trimmed_text("/script");
    #$full_script_text =~ /"indexMembers":(.*),"id":"$index"},"indexStatistics":/;
    $full_script_text =~ /\{\"indexMembers\"\:(.*)\]\,\"id\"/ ;
    #print "full text\n", $full_script_text, "\n";
    my $basic_json = $1;
    $basic_json .= "]";
    #print "basic json\n", $basic_json, "\n";
    
    # parse the json to get the data internal to it
    my $data_struct = from_json($basic_json);
    #print Dumper($data_struct);
    
    #for (@{$data_struct->{'indexMembers'}}) {
    for (@{$data_struct}) {
	$hash{"volume"} += $_->{"volume"};
	if ($_->{"priceChange1Day"} > 0) {
	    $hash{"Advances"}++;
	    $hash{"AdvancesVolume"} += $_->{"volume"};
	} elsif  ($_->{"priceChange1Day"} < 0) {
	    $hash{"Declines"}++;
	    $hash{"DeclinesVolume"} += $_->{"volume"};
	} else {
	    $hash{"Unchanged"}++;
	    $hash{"UnchangedVolume"} += $_->{"volume"};
	}
    }
	   
}

# self-explanatory :-)
sub print_bhavcopy {

    my ($tmp) = @_;
    my %h = %{$tmp};

    print join " ", 
        defined $h{"Open"} ? $h{"Open"} : 0,
        defined $h{"High"} ? $h{"High"} : 0,
        defined $h{"Low"} ? $h{"Low"} : 0,
        defined $h{"Close"} ? $h{"Close"} : 0,
        defined $h{"volume"} ? $h{"volume"} : defined $h{"Volume"} ? $h{"Volume"} : 0,
        defined $h{"Advances"} ? $h{"Advances"} : 0,
        defined $h{"AdvancesVolume"} ? $h{"AdvancesVolume"} : 0,
        defined $h{"Declines"} ? $h{"Declines"} : 0,
        defined $h{"DeclinesVolume"} ? $h{"DeclinesVolume"} : 0,
        defined $h{"Unchanged"} ? $h{"Unchanged"} : 0,
        defined $h{"UnchangedVolume"} ? $h{"UnchangedVolume"} : 0,
        defined $h{"timeStamp"} ? $h{"timeStamp"} : 0,
        "\n";
}
