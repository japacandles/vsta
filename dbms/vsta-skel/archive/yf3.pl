#!/usr/bin/perl -w

# 
# Nov 27, 2010
# yahoo finance changed the table structure in the "Components"
#
# Sep 19, 2010
# Shifted the trade-date to the end of the line
# So that I could get the date right by calling this script
# via yf.sh
#
# Sep 4, 2010
# I'm tired of 
# a. yahoo changing the design XXX.finance.yahoo.com
# b. YahooQuote not keeping up
# c. arbitrary hacks to get volumes, A/D etc
# 

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my $symbol = shift;
my $tmp;
my ($open, $high, $low, $close,	$trade_time, 
    $change, $change_percent, $close_prev, 
    $high52wk, $low52wk,
    $adv, $adv_volume, 
    $dec, $dec_volume, 
    $unc, $unc_volume, 
    $volume) = ();

my $m = WWW::Mechanize->new();
my $url = 'http://finance.yahoo.com';

$m->get($url);

# Punch the symbol
if ($m->success) {
    $m->submit_form(
	form_number => 1,
	fields => { 's' => "$symbol" }, 
	);
} else { die $!; }

# Get OHLC and basic stuff
if ($m->success) {
    # Debug
    # print $m->content();
    parse_basic_page();
} else { die $!; }

# Get volume and A/D
for $tmp ($m->links) {
    if (defined($tmp->text) and $tmp->text =~ /^Components/) {
	#print "Main ", $tmp->url, "\n";
	parse_components($url . $tmp->url);
	last;
    }
}

# Debug
print join " ", $open, $high, $low, $close,
    defined $volume ? $volume : 0,
    defined $adv ? $adv : 0, 
    defined $adv_volume ? $adv_volume : 0, 
    defined $dec ? $dec : 0, 
    defined $dec_volume ? $dec_volume : 0 , 
    defined $unc ? $unc : 0, 
    defined $unc_volume ? $unc_volume: 0, 
    $close_prev, $low52wk, $high52wk, $trade_time,
    "\n";

###############
# Subroutines
###############

sub decomma {
    my $foo = shift;
    $foo =~ s/,//g;
    return $foo;
}

sub parse_basic_page {

    my $p = HTML::TokeParser->new(\$m->content) || die $!;

    # The first table
    $p->get_tag("table"); 

    # close; First row, 2nd coloumn
    $p->get_tag("tr"); $p->get_tag("th"); $p->get_tag("/th");
    $p->get_tag("td"); $p->get_tag("span");
    $close = decomma($p->get_trimmed_text("/span"));
    $p->get_tag("/td"); $p->get_tag("/tr");

    # Trade time; 2nd row
    $p->get_tag("tr"); $p->get_tag("th"); $p->get_tag("/th");
    $p->get_tag("td"); $p->get_tag("span");
    $trade_time = decomma($p->get_trimmed_text("/span"));
    $p->get_tag("/td"); $p->get_tag("/tr");
    
    # Change; 3rd row, 2nd coloumn
    $p->get_tag("tr"); $p->get_tag("th"); $p->get_tag("/th");
    $p->get_tag("td"); $p->get_tag("b");
    $change = decomma($p->get_trimmed_text("/b"));
    $p->get_tag("b");
    $change_percent = decomma($p->get_trimmed_text("/b"));
    
    $p->get_tag("/table");
    
    # Second table
    $p->get_tag("table");
    
    # Previous close; 1st row, 2nd coloumn
    $p->get_tag("tr"); 
    $p->get_tag("th"); $p->get_tag("/th");
    $p->get_tag("td"); $close_prev = decomma($p->get_trimmed_text("/td"));
    $p->get_tag("/tr");

    # Open; 2nd row, 2nd column
    $p->get_tag("tr"); 
    $p->get_tag("th"); $p->get_tag("/th");
    $p->get_tag("td"); $open = decomma($p->get_trimmed_text("/td"));
    $p->get_tag("/tr");

    # Daily hi-lo (range); 3rd row, 2nd coloumn
    $p->get_tag("tr");
    $p->get_tag("th"); $p->get_tag("/th");
    $p->get_tag("td");
    $p->get_tag("span"); $low = decomma($p->get_trimmed_text("/span"));
    $p->get_tag("span"); $high = decomma($p->get_trimmed_text("/span"));
    $p->get_tag("/tr");

    # 52 week hi-lo (range); 4th row, 2nd coloumn
    $p->get_tag("tr");
    $p->get_tag("th"); $p->get_tag("/th");
    $p->get_tag("td");
    $p->get_tag("span"); $low52wk = decomma($p->get_trimmed_text("/span"));
    $p->get_tag("span"); $high52wk = decomma($p->get_trimmed_text("/span"));

}

sub parse_components {

    my $q = shift;
    my ($name, $this_volume, $tmp, $dir);
 
    #print "Checking ", $q, "\n";
    $m->get($q);

    if ($m->success) {
	my $p = HTML::TokeParser->new(\$m->content) || die $!;

	# The data begins in the 9th table
	$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
	$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
	$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
	# 2nd row
	$p->get_tag("tr"); $p->get_tag("tr");

	# Loop through the rows of the table
	while (1) {
	    
	    $dir = "";
	    $this_volume = 0;
	    
	    # Name; col 1
	    $p->get_tag("td"); #$name = $p->get_trimmed_text("/td"); print $name, "\n";
	    $tmp = $p->get_tag("td"); #print $p->get_trimmed_text("/td"), "\n"; # Full name
	    $p->get_tag("td"); #print $p->get_trimmed_text("/td"), "\n"; # Close and traded time
	    
	    # A/D info
	    $p->get_tag("td");
	    $tmp = $p->get_trimmed_text("/td");
	    #print $tmp, "\n"; # debug
	    if ($tmp =~ /Up/) {	$adv++; }
	    elsif ($tmp =~ /Down/) { $dec++; }
	    else { $unc++; }
	    
	    # This section was commented out on Nov 27, 2010
	    # after td, get_tag("span") and get_next_token
	    #$tmp = $p->get_token();
	    #if ($tmp->[1] =~ /span/i) {
	    #   print Dumper($tmp);
	    #	$tmp = $p->get_token();
	    #	# First of all, look for the unchanged
	    #	if ($tmp->[0] =~ /T/) { $unc++; $dir = "unch"; }
		
	    #	# Up/Down
	    #	else { 
	    #	    if ($tmp->[1] =~ /img/i) {
	    #		#print $tmp->[4], "\n";
	    #		if ($tmp->[4] =~ /Up/i) { $adv++; $dir = "up"; }
	    #		# if ($tmp->[4] =~ /Down/i)		  
	    #		else  { $dec++; $dir = "down"; }
	    #	    }
	    #	}
	    #}

	    # Volume
	    $p->get_tag("td");
	    $this_volume = $p->get_trimmed_text("/td");
	    $this_volume = decomma($this_volume);
            $this_volume = 0 if ($this_volume eq "");
	    $volume += $this_volume;

	    if ($tmp =~ /Up/) { $adv_volume += $this_volume; }
	    elsif ($tmp =~ /Down/) { $dec_volume += $this_volume; }
	    else { $unc_volume += $this_volume; }

	    # This section also was commented out on Nov 27
	    # $this_volume = decomma($p->get_trimmed_text("/td"));
	    # if ($this_volume =~ /\D+/) { $this_volume = 0; }
	    # if ($this_volume eq "") { $this_volume = 0; }
	    # if (defined $this_volume) {
	    #	if ($dir eq "up") { $adv_volume += $this_volume; }
	    #	elsif ($dir eq "unch") { $unc_volume += $this_volume; }
	    #	else { 
	    #	    #print join " ", "Name => ", $name,
	    #	    #"Decline volume => ", $dec_volume,
	    #	    #"This volume => ", $this_volume, "\n";
	    #	    $dec_volume += $this_volume; 
	    #	}
	    #	$volume += $this_volume;
	    #}
	    $p->get_tag("/td"); $p->get_tag("/tr");
	    
	    #print join ":", 
	    #  defined $name ? $name : "", 
  	    #  defined $this_volume ? $this_volume : 0,
	    #  defined $adv ? $adv : 0, 
	    #  defined $adv_volume ? $adv_volume : 0, 
	    #  defined $dec ? $dec : 0, 
	    #  defined $dec_volume ? $dec_volume : 0, 
	    #  defined $unc ? $unc : 0, 
	    #  defined $unc_volume ? $unc_volume: 0, 
	    #  defined $volume ? $volume : 0, 
	    #    "\n";
	    
	    # Have we gone to the end of this table ?
	    # If not, loop back
	    $tmp = $p->get_token();
	    #print "Next token is ", $tmp->[0], "\n";
	    if ($tmp->[0] eq 'S') { next; }
	    else { last ; }
	}
    }
    
    # Go the the "Next" link if available
    for $tmp ($m->links) {
	if (defined($tmp->text) and $tmp->text =~ /^Next/) {
	    #print "Main ", $tmp->url, "\n";
	    parse_components($url . $tmp->url);
	    last;
	}
    }
}
