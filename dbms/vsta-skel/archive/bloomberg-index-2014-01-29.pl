#!/usr/bin/perl

#
# Feb 4, 2014, Tuesday
# bloomberg.com has changed their site a bit again.
# They have moved the closing price up by one <span>
#
# bloomberg-index-2014-01-29.pl
# This script was written on the evening of
# Jan 29, 2014, Wednesday.
# bloomberg moved their close price down by 3 <span>s :-)
# 
# bloomberg-index-2013-12-10.pl
# Dec 10, 2013 
# Last night, bloomberg.com has changed the design of their
# data pages. So this script had to be written. 
#
# Changelog:
#
# Dec 10 2013 : 
#   * Initial creation

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

# Usage
my $index = shift;
unless (defined $index) {
    print STDERR "Usage:\n",
                 "bloomberg-index-2013-12-10.pl <name-of-index>\n",
                 "Example: for Dow Jones Industrial Average, the name of the index is INDU:ID";
    exit(1);
}

# Define the urls we will check
my $bloombergQuoteUrl = 'http://www.bloomberg.com/quote/' ;
my $priceUrl = $bloombergQuoteUrl . $index;
my $volumeUrl = $bloombergQuoteUrl . $index . '/members'; 

my %hash;

get_price_data($priceUrl);
if ($hash{"has_volumes"}) {
    get_volume_breadth_data($volumeUrl);
}

#print Dumper(\%hash);
print_bhavcopy(\%hash);

###############
# Subroutines
###############

# get-price_data
# populates the hash-table with price data
sub get_price_data {
    my $url = shift;
    my $tmp;

    # Create the browser
    my $u = WWW::Mechanize->new(
	agent => "Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

    $u->get($url);
    unless (defined $u->success) {
	print STDERR "Problems getting $url";
	exit(2);
    }

    my $p = HTML::TokeParser->new(\$u->content());
    my $q = HTML::TokeParser->new(\$u->content());

    $p->get_tag("h3"); 
    $p->get_tag("span"); $p->get_tag("span"); #$p->get_tag("span");;
    $hash{"close"} = $p->get_trimmed_text("/span");
    $hash{"close"} =~  s/,//g;

    $p->get_tag("table"); #$p->get_tag("table"); $p->get_tag("table"); 

    $p->get_tag("td");
    $hash{"open"} = $p->get_trimmed_text("/td");
    $hash{"open"} =~ s/,//g;

    $p->get_tag("td");
    my $range = $p->get_trimmed_text("/td");
    $range =~ s/,//g;
    $range =~ s/ //g;
    ($hash{"low"}, $hash{"high"}) = split /-/, $range;

    $q->get_tag("p");

    $hash{"timestamp"} = $q->get_trimmed_text("/p");

    # Does this security have separate volume/breadth sections ?
    $hash{"has_volumes"} = 0;
    for ($u->links) {
	if ($_->url =~ /members$/) { $hash{"has_volumes"} = 1; }
    }
}

# get_volume_breadth_data
# populates the hash-table with volumes and breadth
# data. This sub will be called only if bloomberg
# provides volume and breadth data for the security 
sub get_volume_breadth_data {
    my $url = shift;

    my $upDownFlag;
    my $tmp;

    # Create the browser
    my $u = WWW::Mechanize->new(
	agent => "Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

    $u->get($url);
    unless (defined $u->success) {
	print STDERR "Problems getting $url";
	exit(3);
    }
    
    my $p = HTML::TokeParser->new(\$u->content());

    # Skip the first table
    $p->get_tag("table"); $p->get_tag("table"); #$p->get_tag("table"); 
    #$p->get_tag("table"); $p->get_tag("table"); 
    
    $p->get_tag("tr");

    # iterate over each row of the table,
    # looking for volume data of components of the 
    # index. The volumes are added, then breadth
    # is computed, by checking if the security 
    # has risen, fallen or stayed unchanged
    while (1) {

	$p->get_tag("tr");
	$upDownFlag = "";
	
	# Did this stock advance, decline or stay where it was ?
	$p->get_tag("td"); $p->get_tag("td"); $p->get_tag("td");
	$tmp = $p->get_trimmed_text("/td");
	if ($tmp =~ /\+/) { $upDownFlag = "UP"; }
	elsif  ($tmp =~ /\-/) { $upDownFlag = "DOWN"; }
	else { $upDownFlag = "UNCHANGED" }
	
	# Get the volume as it is
	$p->get_tag("td"); $p->get_tag("td"); 
	$tmp = $p->get_trimmed_text("/td");
	$tmp =~ s/,//g;
	$hash{"volume"} += $tmp;
	
	# Build the breadth numbers
	if ($upDownFlag eq "UP") {
	    $hash{"advances"}++;
	    $hash{"advances_volume"} += $tmp;
	} elsif ($upDownFlag eq "DOWN") {
	    $hash{"declines"}++;
	    $hash{"declines_volume"} += $tmp;
	} else {
	    $hash{"unchanged"}++;
	    $hash{"unchanged_volume"} += $tmp;
	}
	
	#print Dumper(\%hash);
	$p->get_tag("/tr");
	my $token1 = $p->get_token;
	my $token2 = $p->get_token;
	#print Dumper($token1);
	#print Dumper($token2);
	if (($token2->[0] eq 'E') and ($token2->[1] eq 'tbody')) { last; }
	else {
	    $p->unget_token($token2); 
	    $p->unget_token($token1);
	}
    }
    
}

sub print_bhavcopy {

    my ($tmp) = @_;
    my %h = %{$tmp};

    print join " ", 
	defined $h{"open"} ? $h{"open"} : 0,
	defined $h{"high"} ? $h{"high"} : 0,
	defined $h{"low"} ? $h{"low"} : 0,
	defined $h{"close"} ? $h{"close"} : 0,
	defined $h{"volume"} ? $h{"volume"} : 0,
	defined $h{"advances"} ? $h{"advances"} : 0,
	defined $h{"open"} ? $h{"advances_volume"} : 0,
	defined $h{"declines"} ? $h{"declines"} : 0,
	defined $h{"declines_volume"} ? $h{"declines_volume"} : 0,
	defined $h{"unchanged"} ? $h{"unchanged"} : 0,
	defined $h{"unchanged_volume"} ? $h{"unchanged_volume"} : 0,
	defined $h{"timestamp"} ? $h{"timestamp"} : 0,
	"\n";
}
