#!/usr/bin/perl

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my %hash;

# create a browser so that cookies etc can be shared across subroutines
# and add headers to imitate a true browser
my $m = WWW::Mechanize->new();
$m->agent_alias('Windows IE 6');
$m->add_header('Accept' => 'image/png,image/*;q=0.8,*/*;q=0.5');
$m->add_header('Accept-Language' => 'en-US,en;q=0.5');
$m->add_header('Accept-Encoding' => 'gzip, deflate');

my $idx = shift;
my $date = shift;

unless (defined $idx) {
    print "Usage:\n",
      "perl parse-nyx-futures.pl <index-no> <date-YYYYmmdd>\n",
      "Example:\n",
      "For FTSE 100 futures, the index no is 29089, so\n",
    "perl parse-nyx-futures.pl 29089 20130931\n";
    exit(1);
}

get_nyx_futures_data($m, \%hash, $date);
clean_futures_data(\%hash);
#print Dumper(\%hash);
print_bhavcopy(\%hash);

sub get_nyx_futures_data {

    my ($browser, $hashref, $d) = @_;

    my $nyxUrl = 'https://globalderivatives.nyx.com/contract/content/' .  
	$idx . 
	'/settlement-prices' ;

    $browser->get($nyxUrl);

    my $p  = HTML::TokeParser->new(\$m->content());

    # name of the security
    $p->get_tag("h1"); 
    my $index = $p->get_trimmed_text("/h1");
    $index =~ s/\s+/-/g;

    # timestamp
    $p->get_tag("h3"); 
    my $timestamp = $p->get_trimmed_text("/h3");
    $timestamp =~ s/\s+/-/g;

    $p->get_tag("table");
    $p->get_tag("tr");$p->get_tag("tr");
    
    while (1) {
	$p->get_tag("td");
	my $expiry = $p->get_trimmed_text("/td");
	
	# The last row is a "Totals" row. If we are here,
	# we need to exit this infinite loop
	last if ($expiry eq "Total:");
	
	$expiry =~ s/\s+/-/g;

	$hashref->{$index . "-" . $expiry}{'timestamp'} = $timestamp;
	
	$hashref->{$index . "-" . $expiry}{'date'} = $d;

	$p->get_tag("td");
	$hashref->{$index . "-" . $expiry}{'open'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$index . "-" . $expiry}{'high'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$index . "-" . $expiry}{'low'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$index . "-" . $expiry}{'close'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td"); $p->get_tag("td");
	
	$p->get_tag("td");
	$hashref->{$index . "-" . $expiry}{'volume'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("td");
	$hashref->{$index . "-" . $expiry}{'openInterest'} = $p->get_trimmed_text("/td");
	
	$p->get_tag("/tr"); # this </td>
	
    }
}

sub clean_futures_data {

    my $hashref = shift;

    for my $key (keys %{$hashref}) {
	if ($hashref->{$key}{'open'} == 0) { 
	    #print $key, "\n" ; 
	    delete $hashref->{$key};
	}
    }
}

sub print_bhavcopy {

    my $hashref = shift;

    for my $key (keys %{$hashref}) {
	print join ",", $key,
              defined($hashref->{$key}{'date'}) ? $hashref->{$key}{'date'} : "DATE", 
	      defined($hashref->{$key}{'open'}) ? $hashref->{$key}{'open'} : 0,
	      defined($hashref->{$key}{'high'}) ? $hashref->{$key}{'high'} : 0,
	      defined($hashref->{$key}{'low'}) ? $hashref->{$key}{'low'} : 0, 
	      defined($hashref->{$key}{'close'}) ? $hashref->{$key}{'close'} : 0, 
	      defined($hashref->{$key}{'volume'}) ? $hashref->{$key}{'volume'} : 0, 
	      defined($hashref->{$key}{'openInterest'}) ? $hashref->{$key}{'openInterest'} : 0,
	      defined($hashref->{$key}{'timestamp'}) ? $hashref->{$key}{'timestamp'} : "TIMESTAMP",
	"\n";
    }
}
