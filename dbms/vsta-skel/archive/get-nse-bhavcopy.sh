#!/bin/bash -x

# Define error log
download_dir="/tmp"
error_log=$download_dir/"curl_error_log"

# Commands
WHICH="/usr/bin/which"
DATE=$($WHICH date)
ECHO=$($WHICH echo)
TR=$($WHICH tr)
WGET=$($WHICH wget)
ZCAT=$($WHICH zcat)
GREP=$($WHICH grep)
CURL=$($WHICH curl)
RM=$($WHICH rm)

# Browser stuff
user_agent_identifier="Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.20) Gecko/20110804 Red Hat/3.6.20-2.el6_1 Firefox/3.6.20"
base_url='http://www.nseindia.com/content/historical/DERIVATIVES/'

# What file are we downloading from the base url ?
day_of_month=$($DATE +%d)
#day_of_month="14"
mon=$($DATE +%b)
#mon="Feb"
month=$($ECHO $mon | $TR a-z A-Z)
year=$($DATE +%Y)
dir="$year/$month/"
file="fo"$day_of_month$month$year"bhav.csv.zip"
url=$base_url$dir$file

# Whats the file name that we are downloading to ?
download_file_day_of_month=$day_of_month
download_file_month=$($DATE +%m)
#download_file_month="09"
download_file_year=$year
download_file=$download_file_day_of_month-$download_file_month-$download_file_year

# The futures lines in the bhavcopy have "FUTIDX" in it
# The options lines in the bhavcopy have "OPTIDX" in it
# We only want the Futures lines
string_to_search="FUTIDX"

# Need to log errors
echo >> $error_log
echo $($DATE) "$download_file" >> $error_log

#echo $url
#echo Downloading $url to /tmp/$download_file
($CURL -A "$user_agent_identifier" $url | $ZCAT  | $GREP $string_to_search -) 2>> $error_log > $download_dir/$download_file

# Delete the bhavcopy if there was a download error
if [ $? -ne 0 ]
then
    #echo Deleting $download_dir/$download_file
    $RM $download_dir/$download_file
    exit 1
fi
echo $download_dir/$download_file
exit 0
