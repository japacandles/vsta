#!/bin/bash

#
# eu.sh
# 29 Oct 2008, Wednesday
#
# This progy updates the records of the major European indexes.
# It should be run at 6 AM IST, Tuesday to Saturday
#


. ./configs.sh

echo Dow Jones Euro Stoxx 50
/usr/bin/perl $DBMSDIR/yf3.pl ^stoxx50e | tee -a $DATADIR/zurich/dj-euro-stoxx

echo DAX
/usr/bin/perl $DBMSDIR/bloomberg-index.pl dax:ind | tee -a $DATADIR/frankfurt/dax

echo FTSE
#/usr/bin/perl $DBMSDIR/yf3.pl ^ftse | tee -a $DATADIR/london/ftse
/usr/bin/perl $DBMSDIR/bloomberg-index.pl ukx:ind | tee -a $DATADIR/london/ftse
