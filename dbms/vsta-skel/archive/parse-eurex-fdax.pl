#!/usr/bin/perl

#
# fdax.pl
# Date: 1 Jan 2011
# Create a bhavcopy script for DAX futures
# traded on Eurex
#

use strict;
use diagnostics;
use WWW::Mechanize;
use HTML::TokeParser;
use Data::Dumper;

my %hash;
my %urlhash;
my $baseURL = 'http://www.eurexchange.com/market/quotes/IDX/DAX/FDAX_en.html';
my $futureSymbol = shift ; # needed because I'll extend this prog for stoxx etc
$futureSymbol = uc($futureSymbol);
my $m = WWW::Mechanize->new();

$m->get( $baseURL );
die $! unless ($m->success);

for ($m->links) {
    if ((defined($_->url)) and ($_->url =~ /$futureSymbol/i) and ($_->text =~ / /)) {
	$urlhash{$_->text} = $_->url;
    }
}

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,VOL,OPENINT,TIMESTAMP\n";
for my $exp (keys %urlhash) {
    #$m->follow_link( text_regex => qr/$futureSymbol/i, n => 1 );
    $m->get($urlhash{$exp});
    die $! unless ($m->success);

    # Debug
    #print $m->content;

    # Now we should be on the page where the near future is shown
    # and we see the URLs of the other futures

    my $p = HTML::TokeParser->new(\$m->content);

    # Price info is in the 11th table
    $p->get_tag("table");$p->get_tag("table");$p->get_tag("table");
    $p->get_tag("table");$p->get_tag("table");$p->get_tag("table");
    $p->get_tag("table");$p->get_tag("table"); $p->get_tag("table");
    $p->get_tag("table"); $p->get_tag("table");
    # row 2
    $p->get_tag("tr"); $p->get_tag("tr");

    $hash{'open'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'high'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'low'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'bid_vol'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'bid_price'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'ask_price'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'ask_vol'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'diff'} =  $p->get_trimmed_text("/td"); $p->get_tag("td"); 
    $p->get_tag("/td");
    $hash{'close'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'timestamp'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'timestamp'} .=  " " . $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'settlement'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'volume'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'oi'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    $hash{'oi_date'} =  $p->get_trimmed_text("/td"); $p->get_tag("td");
    
    $hash{'volume'} =~ s/,//g;
    $hash{'oi'} =~ s/,//g;

    #Debug
    # print Dumper(\%hash);

    $exp =~ s/ /-/g;
    print join ",", "FUTIDX",$futureSymbol,$exp,"XX,0",
        $hash{'open'}, $hash{'high'}, $hash{'low'}, 
        $hash{'close'}, $hash{'close'}, 
        $hash{'volume'}, $hash{'oi'}, 
        $hash{'timestamp'}, "\n";
}
