#!/usr/bin/perl

# parse-barchart.pl
# 20 November 2014, 13:30, Thursday
# Nyse Liffe has been acquired by ICE
# So the scripts to parse nyx futures, have stopped working.
#
# Input: name of the future with spaces turned to underscores
#        Example: FTSE_100 is used for "FTSE 100"

# I then read http://www.barchart.com/futures/all_futures.php
# and get a list of all links that contain 'commoditiesfutures' and
# which are at least 3 deep. The name of the link is the key and the
# url is the value. I then GET the url and parse it for future values

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use FunctionsVstaHelper;
binmode STDOUT, ':utf8';

# Usage 
if ($#ARGV < 0) {
    print "Usage:\n",
      "parse-barchart.pl name_of_index YYYYMMDD\n",
      "where FTSE 100 Index will be input as FTSE_100_INDEX\n";
    exit(1);
}

my $future = $ARGV[0];
my $date = $ARGV[1];
my (%url_hash, $html, $token, $token2, %hash);

# create the browser
my $u = WWW::Mechanize->new( agent => random_ua_str() );
#print Dumper(\$u);

# parse http://www.barchart.com/futures/all_futures.php
# and create a hash whose key is the name of the future
# and then value is the url where the those futures can be evaluated
create_url_hash($u, \%url_hash);
#print Dumper(\%url_hash);

unless(defined $url_hash{$future}) {
    print "Undefined future $future\n";
    exit 2;
}
#print join " ", $future, $url_hash{$future}, "\n";

get_future_data($u, $future, \%url_hash, \%hash);
#print Dumper(\%hash);

for my $name (keys %hash) {
    next if ($hash{$name}{'open'} eq '0.0');

    # the name of the future is like
    #  XH15 (Mar '15)
    # We need it to be
    # FTSE-100-INDEX-FUTURE-MAR-2015
    my $n = $name;
    $n =~ s/\(//g;
    $n =~ s/\)//g;
    $n =~ s/\'//g;
    my ($first, $second, $third) = split / /, $n;
    #$second = uc($second);
    $third = 2000 + $third;

    $n = $future . "-INDEX-FUTURE-" . $second . "-" . $third;

    print join ",", $n, $date, $hash{$name}{'open'}, $hash{$name}{'high'}, 
                    $hash{$name}{'low'}, $hash{$name}{'close'}, $hash{$name}{'volume'}, "\n";
}
#
# Subroutines
#

#
# create_url_hash
# Input:  the ref to browser object AND
#         the ref to the hash which will be populated with the names and urls
#         of the futures
# Output: none. the hash gets populated
sub create_url_hash {

    my ($b, $h) = @_;
    my $barchart_all_futures_url = 'http://www.barchart.com/futures/all_futures.php';

    $b->get($barchart_all_futures_url);	

    unless ($b->success) {
	exit 3;
    }

    my $p = HTML::TokeParser->new(\$b->content);
    $p->get_tag("td"); 
    if ($u->success()) {
	for ($u->links) {
	    my $url = $_->url;
	    my @ary = split '/', $url ;
	    
	    next if ($url !~ /commodityfutures/);
	    next if ($#ary < 3);
	    
	    my $name = $ary[2];
	    $name =~ s/_Futures$//g;
	    
	    #print $url, $#ary, "\n";
	    $h->{ $name } = 'http://www.barchart.com' . $url ;
	}
    }
}

#
# get_future_data
# Input : the ref to browser object, AND
#          the name of the future, AND
#          the ref to the url hash, AND
#          the ref to the futures' data hash
# Output: none. the data hash will be populated
sub get_future_data {
    
    my ($b, $f, $h, $d) = @_;
    my $token;
    my ($tmp, $name);

    #print "Getting ", $h->{$f}, "\n";
    $b->get($h->{ $f });
    unless ($b->success()) {
	exit 3;
    }
    
    #print $b->content();
    my $p = HTML::TokeParser->new(\$b->content);
    
    for (1 .. 4) { $p->get_tag("table"); }
    for (1 .. 3) { $p->get_tag("tr"); }
    
    while (1) {
	
	$p->get_tag("td"); 
	$name = $p->get_trimmed_text("/td");

	$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td"); $tmp =~ s/,//g; $tmp =~ s/s$//g;
	$d->{$name}->{'close'} = $tmp;

	$p->get_tag("td"); # change from previous

	$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td"); $tmp =~ s/,//g; $tmp =~ s/s$//g;
	$d->{$name}->{'open'} = $tmp;

	$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td"); $tmp =~ s/,//g; $tmp =~ s/s$//g;
	$d->{$name}->{'high'} = $tmp;

	$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td"); $tmp =~ s/,//g; $tmp =~ s/s$//g;
	$d->{$name}->{'low'} = $tmp;

	$p->get_tag("td"); # previous

	$p->get_tag("td"); $tmp = $p->get_trimmed_text("/td"); $tmp =~ s/,//g; $tmp =~ s/s$//g;
	$d->{$name}->{'volume'} = $tmp;
	
	#print Dumper($d);

	$p->get_tag("/tr");

	$p->get_token(); 
	$token = $p->get_token(); #print Dumper(\$token);

	last if ( ($token->[0] eq 'E') and ($token->[1] eq 'tbody') );
 	next;
    }
}
