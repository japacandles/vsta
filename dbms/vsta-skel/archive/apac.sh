#!/bin/bash

#
# eu.sh
# 29 Oct 2008, Wednesday
#
# This progy updates the records of the major APAC indexes.
# It should be run at 6 PM IST, Monday to Friday
# Note: Nikkei updates volumes at 6 AM in the morning, so a manual update is 
# needed at that time
#

. ./configs.sh
#DIR="/NotBackedUp/nse/fo"

echo AU All Ord
/usr/bin/perl $DBMSDIR/yf3.pl ^aord | tee -a $DATADIR/sydney/asx-all-ordinaries

echo AU ASX 200
/usr/bin/perl $DBMSDIR/yf3.pl ^axjo | tee -a $DATADIR/sydney/asx-200

echo Nikkei 225
#/usr/bin/perl $DBMSDIR/yf3.pl ^n225 | tee -a $DATADIR/tokyo/n225
/usr/bin/perl $DBMSDIR/bloomberg-index.pl nky:ind | tee -a $DATADIR/tokyo/n225

#echo Shanghai Composite
#/usr/bin/perl $DBMSDIR/yf3.pl ^ssec | tee -a $DATADIR/shanghai/shanghai-comp

echo Hang Seng
/usr/bin/perl $DBMSDIR/yf3.pl ^hsi | tee -a $DATADIR/hongkong/hangseng

#echo Straits Times
#/usr/bin/perl $DBMSDIR/yf3.pl ^sti | tee -a $DATADIR/singapore/straits

