#!/bin/bash

# For refernce: http://www.masterdata.com/Reports/Combined/CompositeReports/

WHICH="/usr/bin/which"
ZDUMP=$($WHICH zdump)
DBMSDIR=$PWD
ZONEDIR="/usr/share/zoneinfo"

if [ -n "$1" ]
then
    timezone="$1"
else
    timezone="Asia/Kolkata"
fi

if [ -n "$2" ]
then
    ticker="$2"
else
    echo "Usage: basename $0 <timezone> <ticker>"
    exit 1
fi

#echo $timezone
#echo $datadir
#date=$($ZDUMP $ZONEDIR/$timezone | awk -f $DBMSDIR/yf.awk)
date=$(TZ="$ZONEDIR/$timezone" date +%Y%m%d)
echo -n "$date "
$DBMSDIR/parse-masterdata.pl $ticker

 
