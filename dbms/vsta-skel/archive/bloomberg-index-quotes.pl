#!/usr/bin/perl -w

#
# Date: Jul 1, 2012, Sunday
# Refactored bloomberg-index.pl into
# bloomberg-index-quotes.pl and
# bloomberg-index-breadth.pl
# so that I could use bloomberg for australian indexes
#
use strict;
use diagnostics;
use LWP::UserAgent;
use WWW::Mechanize;
use HTML::TokeParser;
use Data::Dumper;
use DateTime;

# We will use DateTime to get the date in YYYYmmDD
#my $timeZone = shift;
#my $time = DateTime->from_epoch( epoch => time() );
#my $dateTime = $time->strftime("%Y%m%d");

my $index = shift;
$index = uc($index);

my $token;
my %hash;

my $bloomberg_idx_base_url = "http://www.bloomberg.com/apps/quote?ticker=";
my $url = $bloomberg_idx_base_url . $index;

my $ua = LWP::UserAgent->new;

# For OHLC
my $req = HTTP::Request->new( GET => $url );
my $res = $ua->request( $req );

if ($res->is_success) { 
    #print $res->as_string; 
    my $html_string = $res->as_string;

    my $p = HTML::TokeParser->new(\$html_string);

    # The 65th (!) div
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 5
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 10
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 15
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 20
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 25
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 30
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 35
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 40
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 45
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 50
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 55
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 60
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 65

    # Last trade time
    $p->get_tag("span"); $p->get_tag("span"); $p->get_tag("span");
    $hash{'last_traded'} = $p->get_trimmed_text("/span");
    $hash{'last_traded'} =~ s/ /-/g;

    $p->get_tag("div"); $p->get_tag("div"); # 67
    $p->get_tag("span");  
    $hash{'close'} =  decomma($p->get_trimmed_text("/span"));

    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 70
    $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 75
    #$p->get_tag("div"); $p->get_tag("div"); $p->get_tag("div"); # 78

    #print Dumper($p);
    
    $p->get_tag("span"); $p->get_tag("span"); $p->get_tag("span");
    $hash{'high'} = decomma($p->get_trimmed_text("/span"));

    # Low
    $p->get_tag("div"); $p->get_tag("span"); $p->get_tag("span");  $p->get_tag("span"); 
    $hash{'low'} = decomma($p->get_trimmed_text("/span"));

    # Open
    $p->get_tag("div"); $p->get_tag("span"); $p->get_tag("span");$p->get_tag("span");  
    $hash{'open'} = decomma($p->get_trimmed_text("/span"));

    #print Dumper(\%hash);

} else {
    print "Problems getting the url for index ", $index, "\n";
    exit(1);
}


# Print in bhavcopy format
print join " ",
    #defined($dateTime) ? $dateTime : 0,
    defined($hash{'open'}) ? $hash{'open'}: 0,
    defined($hash{'high'}) ? $hash{'high'}: 0,
    defined($hash{'low'}) ? $hash{'low'}: 0,
    defined($hash{'close'}) ? $hash{'close'}: 0,
    defined($hash{'last_traded'}) ? $hash{'last_traded'}: 0,
    "\n";

sub decomma {
    my ($str) = @_;
    $str =~ s/,//g;
    return $str;
}
