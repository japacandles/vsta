#!/usr/bin/perl -w

#
# Date: Jul 1, 2012, Sunday
# I found that yahoo finance does a yql query to 
# preview.papdan.com, so I do too :)
#
# Sep 28, 2013, Saturday
# Refactored

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my %hash;
my $index = shift;

get_breadth_web_data($index);
#print Dumper(\%hash);
print_breadth_bhavcopy(\%hash);

#
# Subroutines
#

# get_breadth_web_data()
# Takes the name of the index as input,
# GETs the HTTP::Response for that index and
# calls parse_breadth_content with that
# response data
sub get_breadth_web_data {

    my ($idx) = @_;
    my $url = 'http://preview.papdan.com/papdan/data/get_index_tables_price.php?content=' . "\'" . uc($index) . "\'";

    my $b = WWW::Mechanize->new(
	agent => 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0' );

    $b->get($url);
    unless ($b->success) { die $!; }
    
    parse_breadth_content(\$b->content);
}

#
# parse_breadth_content()
# Takes the HTTP::Response object and populates
# the global variable %hash
sub parse_breadth_content {

    my ($breadth) = @_;

    my $str;
    my $flag;
    my $token;
    
    my $p = HTML::TokeParser->new($breadth);
    
    # The first table
    $p->get_tag("table"); 
    
    # The first row is just column headers
    $p->get_tag("/tr");
    
    while (1) {
	#$p->get_tag("tr");
	$token = $p->get_token;
	#print Dumper $token;
	
	# the first col is an image
	$p->get_tag("/td");
	
	# the second col is the symbol
	$p->get_tag("td");
	$str = $p->get_trimmed_text("/td");
	#print "Symbol is ", $str, "\n";
	
	# the third col is company name
	$p->get_tag("td");
	#$str = $p->get_trimmed_text("/td");
	#print "Company is ", $str, "\n";
	
	# the fourth col is the closing price
	$p->get_tag("td");
	#$str = $p->get_trimmed_text("/td");
	#print "Close is ", $str, "\n";
	
	# the fifth col is the change
	$p->get_tag("td");
	$str = $p->get_trimmed_text("/td");
	#print "Change is ", $str, "\n";
	if ($str =~ /\+/) { $flag = "UP" }
	elsif ($str =~ /\-/) { $flag = "DOWN" }
	else { $flag = "UNCH" ; }
	
	# the sixth col is the percent change
	$p->get_tag("td");
	#$str = $p->get_trimmed_text("/td");
	#print "Percent Change is ", $str, "\n";
	
	# the seventh col is open
	$p->get_tag("td");
	#$str = $p->get_trimmed_text("/td");
	#print "open is ", $str, "\n";
	
	# the eighth col is high
	$p->get_tag("td");
	#$str = $p->get_trimmed_text("/td");
	#print "high is ", $str, "\n";
	
	# the ninth col is low
	$p->get_tag("td");
	#$str = $p->get_trimmed_text("/td");
	#print "low is ", $str, "\n";
	
	# the tenth col is volume
	$p->get_tag("td");
	$str = $p->get_trimmed_text("/td");
	#print "volume is ", $str, "\n";
	
	$hash{'volumes'} += decomma($str);
	if ($flag eq "UP") { 
	    $hash{'advances'}++;
	    $hash{'advances_volumes'} += $hash{'volumes'};
	} elsif ($flag eq "DOWN") {
	    $hash{'declines'}++;
	    $hash{'declines_volumes'} += $hash{'volumes'};
	} else {
	    $hash{'unchanged'}++;
	    $hash{'unchanged_volumes'} += $hash{'volumes'};
	}
	
	#print Dumper(\%hash);
	
	$p->get_tag("/tr");
	$token = $p->get_token();
	#print Dumper $token;
	last if ($token->[1] eq "table");
	$token = $p->unget_token();
	#print Dumper $token;
	#$token = $p->unget_token();
	#print Dumper $token;
	
	
    }
}

# print_breadth_bhavcopy()
# outputs the contents of the global
# variable %hash in bhavcopy format
sub print_breadth_bhavcopy {

    print join " ", 
          defined($hash{'volumes'}) ? $hash{'volumes'} : 0,
          defined($hash{'advances'}) ? $hash{'advances'} : 0,
          defined($hash{'advances_volumes'}) ? $hash{'advances_volumes'} : 0,
          defined($hash{'declines'}) ? $hash{'declines'} : 0 ,
          defined($hash{'declines_volumes'}) ? $hash{'declines_volumes'} : 0,
          defined($hash{'unchanged'}) ? $hash{'unchanged'} : 0 ,
          defined($hash{'unchanged_volumes'}) ? defined($hash{'unchanged_volumes'}) : 0, ;
}

# decomma()
# takes a string and returns the same string
# with all commas removed
sub decomma {
    my $str = shift;
    $str =~ s/,//g;
    return $str;
}
