#!/usr/bin/perl -w

#
# 5 Jan 2011
# Euronext simplified page
#
# 11 July 2011
# Euronext now seems to have put the data
# in the first table instead of the third
#
use strict;
use diagnostics;
use Data::Dumper;
use HTTP::Request::Common qw/GET/;
use HTML::TokeParser;
use LWP::UserAgent;

my %hash;

my $futureSymbol = shift;
$futureSymbol = uc($futureSymbol);

my $exchange = shift;
$exchange = uc($exchange);

my $euronext_derivative_webapp = 'https://euronext.nyx.com/EN/derivatives';
my $url = $euronext_derivative_webapp . '?' .
    'Class_type=' .'F' . '&' .
    'Class_symbol=' . $futureSymbol . '&' .
    'Class_exchange=' . $exchange;

my $request = GET $url;

my $ua = LWP::UserAgent->new();
my $response = $ua->request($request);

#Debug
#print $response->content;

unless ($response->is_success) { exit 3; }

my $p = HTML::TokeParser->new(\$response->content);

# The index name is the first thing displayed
$p->get_tag("body");
$hash{'security'} =  $p->get_trimmed_text("/font");
$hash{'security'} =~ s/\(.*\)//g;
$hash{'security'} =~ s/\s+/-/g;
# Sometimes they forget to add FUTURE to the name :)
$hash{'security'} = $hash{'security'} . '-FUTURE' 
    unless ($hash{'security'} =~ /FUTURE$/);

# Data is in
# the 1st table
$p->get_tag("table"); #$p->get_tag("table"); $p->get_tag("table"); 

# Loop through the table
while (1) {

    # col 1: expiry 
    $p->get_tag("td");
    $p->get_tag("a");
    $hash{'expiry'} = $p->get_trimmed_text("/a");
    $hash{'expiry'} =~ s/ /-/g;

    # col 2: timestamp
    $p->get_tag("td");
    $hash{'timestamp'} = $p->get_trimmed_text("/td");
    
    # col 3: close
    $p->get_tag("td");
    $hash{'close'} = $p->get_trimmed_text("/td");

    # col 4: last trade vol: SKIP
    $p->get_tag("td");
    $p->get_tag("/td");

    # col 5: volume
    $p->get_tag("td");
    $hash{'volume'} = $p->get_trimmed_text("/td");
    $hash{'volume'} =~ s/,//g;

    # col 6: bid size : SKIP
    $p->get_tag("td");
    $p->get_tag("/td");

    # col 7 : bid : SKIP
    $p->get_tag("td");
    $p->get_tag("/td");

    # col 8 : ask : SKIP
    $p->get_tag("td");
    $p->get_tag("/td");

    # col 9 : ask size : SKIP
    $p->get_tag("td");
    $p->get_tag("/td");

    # col 10 : percent change : SKIP
    $p->get_tag("td");
    $p->get_tag("/td");

    # col 11 : Open
    $p->get_tag("td");
    $hash{'open'} = $p->get_trimmed_text("/td");

    # col 12 : High
    $p->get_tag("td");
    $hash{'high'} = $p->get_trimmed_text("/td");

    # col 13 : Low
    $p->get_tag("td");
    $hash{'low'} = $p->get_trimmed_text("/td");

    # col 14 : settlement price : SKIP
    $p->get_tag("td");
    $p->get_tag("/td");

    # col 15 : Open interest
    $p->get_tag("td");
    $hash{'oi'} = $p->get_trimmed_text("/td");
    $hash{'oi'} =~ s/,//g;

    #Debug
    #print Dumper(\%hash)
    #	unless (($hash{'timestamp'} =~ '-') or 
    #		($hash{'timestamp'} eq '')) ;

    # Dump in bhavcopy format
    print join ",", "FUTIDX", 
       $hash{'security'}, $hash{'expiry'}, "XX,0",
       $hash{'open'}, $hash{'high'}, $hash{'low'}, $hash{'close'}, 
       $hash{'close'}, $hash{'volume'}, $hash{'oi'}, $hash{'timestamp'}, 
      "\n"
    unless (($hash{'timestamp'} =~ '-') or 
	    ($hash{'timestamp'} eq '')) ;

    $p->get_token();
    my $tmp = $p->get_token();
    last unless (defined $tmp->[0]);
}
