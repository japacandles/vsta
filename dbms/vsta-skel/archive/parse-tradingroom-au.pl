#!/usr/bin/perl

# parse-tradingroom-au.pl
# This is an alternative way to get data
# about Australian indexes
# This script calls tradingroom.com.au
#
# Changelog:
# 
# * Friday, Sep 28, 2013
# Initial design
#
# * Saturday, Sep 29, 2013
# Refactoring and initial commit

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my $index = shift;
unless (defined $index) {
    print "Usage:\n",
      "parse-tradingroom-au.pl <name-of-index>\n",
      "Example: for the asx all ordinaries (XAO)\n",
      "parse-tradingroom-au.pl XAO\n";
    exit (1);
}

my %hash;

# Send out for the data :)
get_web_content($index);

# Debug
#print Dumper(\%hash);
print_bhavcopy(\%hash);

#
# Subroutines
# 

# get_web_content() 
# takes the name of the index as input,
# calls tradingroom.com.au with that name,
# get the data,
# and calls parse_web_content()
sub get_web_content {

    my ($idx) = @_;
 
    # Create the url
    # The name of the index has to be in uppercase
    # The tradingroom.com.au doesn't work with "xao", but 
    # does work with "XAO"
    my $url = 'http://www.tradingroom.com.au/apps/mkt/indexDetails.ac?idx=' . uc($idx) ;
    
    # Create the browser
    my $b = WWW::Mechanize->new(
	agent => 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0' );
    
    # Send the GET request
    $b->get($url);
    unless ($b->success) { die $!; }
    
    parse_web_content(\$b->content);
}

# parse_web_content()
# takes the HTTP::Response::Object from get_web_content()
# and populates the global variable hash %hash
# with the data
sub parse_web_content {

    my ($http_content) = @_;
    
    # Parse the content
    my $p = HTML::TokeParser->new($http_content);

    # data begins in table 2
    $p->get_tag("table"); $p->get_tag("table");

    $p->get_tag("tr"); $p->get_tag("td"); 
    $hash{"timestamp"} = $p->get_trimmed_text("/td");
    $hash{"timestamp"} =~ s/,//g; # de-comma
    $hash{"timestamp"} =~ s/ /-/g; # substitute spaces with -
    $p->get_tag("/tr");
    
    $p->get_tag("tr"); $p->get_tag("td"); 
    $hash{"open"} = $p->get_trimmed_text("/td");
    $hash{"open"} =~ s/,//g; # de-comma
    $p->get_tag("/tr");
    
    $p->get_tag("tr"); $p->get_tag("td"); 
    $hash{"high"} = $p->get_trimmed_text("/td");
    $hash{"high"} =~ s/,//g; # de-comma
    $p->get_tag("/tr");
    
    $p->get_tag("tr"); $p->get_tag("td"); 
    $hash{"low"} = $p->get_trimmed_text("/td");
    $hash{"low"} =~ s/,//g; # de-comma
    $p->get_tag("/tr");
    
    $p->get_tag("tr"); $p->get_tag("td"); 
    $hash{"close"} = $p->get_trimmed_text("/td");
    $hash{"close"} =~ s/,//g; # de-comma
    $p->get_tag("/tr");
    
    # skip the nest record
    $p->get_tag("tr"); #$p->get_tag("td"); 
    #$hash{"movement"} = $p->get_trimmed_text("/td");
    $p->get_tag("/tr");
    
    $p->get_tag("tr"); $p->get_tag("td"); 
    $hash{"turnover"} = $p->get_trimmed_text("/td");
    $hash{"turnover"} =~ s/,//g;
    $p->get_tag("/tr");
    
    $p->get_tag("tr"); $p->get_tag("td"); 
    $hash{"volume"} = $p->get_trimmed_text("/td");
    $hash{"volume"} =~ s/,//g;
    $p->get_tag("/tr");
    
}

# print_bhavcopy()
# This sub prints out a space-delimited 
# dump of the global %hash
sub print_bhavcopy {

    my ($h) = @_;

    print join " ",
       defined($h->{"open"}) ? $h->{"open"} : 0,
       defined($h->{"high"}) ? $h->{"high"} : 0,
       defined($h->{"low"}) ? $h->{"low"} : 0,
       defined($h->{"close"}) ? $h->{"close"} : 0,
       defined($h->{"volume"}) ? $h->{"volume"} : 0,
       defined($h->{"turnover"}) ? $h->{"turnover"} : 0 ,
    defined($h->{"timestamp"}) ? $h->{"timestamp"} : 0;

							    
}
