#!/usr/bin/perl

#
# parse-yahoo-finance-json.pl
# Sep 22, 2015, Tuesday
# 
#

# q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20("%5ENDX")%0A%09%09&format=json&diagnostics=true&env=http%3A%2F%2Fdatatables.org%2Falltables.env&callback=

use strict;
use diagnostics;
use LWP::UserAgent;
use HTTP::Request::Common qw(GET);
use JSON;
use Data::Dumper;

my $symbol = shift;

my $u = LWP::UserAgent->new();
$u->agent("Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");
my $yql_url = "https://query.yahooapis.com/v1/public/yql?";
my $yql_query = 'q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20("%5E' ;
$yql_query .= $symbol;
$yql_query .= '")%0A%09%09&format=json&diagnostics=true&env=http%3A%2F%2Fdatatables.org%2Falltables.env&callback=';

my $get = $yql_url . $yql_query;
#print $get, "\n";

my $req = HTTP::Request->new( GET => $get );
my $res = $u->request($req);
#print $res->as_string;

#my $file = shift;
# read the json file and create a giant string out of it
#open FILE, "< $file" or die $!;
#my $str;
#while (<FILE>) {
#	$str .= $_ ;
#}

my $str = $res->decoded_content;
my $dataStruct = decode_json $str;

print_bhavcopy($dataStruct);

##############
# Subroutines
##############
 
# print_bhavcopy
# self explanatory :)
sub print_bhavcopy {

   my $d = shift;
   #print Dumper(\$d);

   print join " ", 
       defined $d->{'query'}->{'results'}->{'quote'}->{'Open'} ? $d->{'query'}->{'results'}->{'quote'}->{'Open'}: " ",
       defined $d->{'query'}->{'results'}->{'quote'}->{'DaysHigh'} ? $d->{'query'}->{'results'}->{'quote'}->{'DaysHigh'}: " ",
       defined $d->{'query'}->{'results'}->{'quote'}->{'DaysLow'} ? $d->{'query'}->{'results'}->{'quote'}->{'DaysLow'}: " ",
       defined $d->{'query'}->{'results'}->{'quote'}->{'LastTradePriceOnly'} ? $d->{'query'}->{'results'}->{'quote'}->{'LastTradePriceOnly'}: " ",
       defined $d->{'query'}->{'results'}->{'quote'}->{'Volume'} ? $d->{'query'}->{'results'}->{'quote'}->{'Volume'}: "0",
       defined $d->{'query'}->{'results'}->{'quote'}->{'Value'} ? $d->{'query'}->{'results'}->{'quote'}->{'Value'}: "0",
       defined $d->{'query'}->{'results'}->{'quote'}->{'Advances'} ? $d->{'query'}->{'results'}->{'quote'}->{'Advances'}: "0",
       defined $d->{'query'}->{'results'}->{'quote'}->{'AdvancesVolume'} ? $d->{'query'}->{'results'}->{'quote'}->{'AdvancesVolume'}: "0",
       defined $d->{'query'}->{'results'}->{'quote'}->{'Declines'} ? $d->{'query'}->{'results'}->{'quote'}->{'Declines'}: "0",
       defined $d->{'query'}->{'results'}->{'quote'}->{'DeclinesVolume'} ? $d->{'query'}->{'results'}->{'quote'}->{'DeclinesVolume'}: "0",
       defined $d->{'query'}->{'results'}->{'quote'}->{'LastTradeDate'} ? $d->{'query'}->{'results'}->{'quote'}->{'LastTradeDate'}: "Null-Date",
       defined $d->{'query'}->{'results'}->{'quote'}->{'LastTradeTime'} ? $d->{'query'}->{'results'}->{'quote'}->{'LastTradeTime'}: "Null-Time",
       "\n";
}
