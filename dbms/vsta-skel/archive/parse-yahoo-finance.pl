#!/usr/bin/perl

#
# parse-yahoo-finance.pl
# Rewriting the program to scrape data
# from finance.yahoo.com
#
# Changelog
#
# Mon Dec 8, 2014, 22:54
# Since Yahoo! can be relied on to change its page structure
# I need to change my approach. This is my first effort
# towards that. I have partly moved to parsing by <span>
# rather than parsing by <table> etc
#
# Sun Aug 10, 2014 17:49 :-)
# yahoo finance changed its page structure again :-(
#
# Sun Apr 13, 2014, 22:36 :-)
# yahoo finance changed its page structure again :-(
#
# Sat Feb 15, 22:00, Saturday :-)
# yahoo finance changed its page structure again :-(
#
# Sun Oct 7, 2013
# Refactored, checked with stoxxe and dji
# 
# Sat Sep 28, 2013
# Refactored, and 
# support for breadth
#
# Sun, Mar 3, 2013
# Initial commit
#

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my $idx = shift;
my $idx_name = shift;
my %hash;
my $components_exist = "";
my %urls;
my $tmp;

parse_main_page($idx);
if ($components_exist) {
    #print "Now parsing components\n";
    populate_components_urls_hash($components_exist);
    parse_components_pages(%urls);
}

#print STDERR Dumper(\%hash);
print_bhavcopy(\%hash);

sub parse_main_page {

    my ($index) = @_;
    my $tag;

    my $priceUrl = 'http://finance.yahoo.com/q?s=^' . $index;

    my $u = WWW::Mechanize->new(
	agent => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");

    $u->get($priceUrl);
    if ($u->success) {
	
	#print $u->content;
	#print Dumper($u->links);

	# If there exists a link called "Components" then 
	# include that in a list of links that have to be followed
	for my $link ($u->links) {
	    if (defined($link->text) and ($link->text =~ /^Components/)) {
		$components_exist = $link->url;
	    }
	}

	my $p = HTML::TokeParser->new(\$u->content);


	while (1) {

		# we want to skip all tokens unless we find <span>
		my $token = $p->get_token();
		next unless (defined($token->[0]) 
		             and ($token->[0] eq 'S') 
		             and defined($token->[1]) 
			     and ($token->[1] eq 'span'));

		# we want to study only those <span> where the class id 'time_rtq_ticket'
		next unless (defined($token->[2]) 
		             and defined($token->[2]->{'class'})
		             and ($token->[2]->{'class'} eq 'time_rtq_ticker'));
		#print Dumper(\$token);

		# close
		$token = $p->get_token();
		#print "token 1\n", Dumper(\$token);
		$token = $p->get_token();
		#print "token 2\n",Dumper(\$token);
		$hash{'close'} = $token->[1];
		$hash{'close'} =~ s/,//g;

		# timestamp
		for ( 1 .. 18 ) { $token = $p->get_token(); }
		$hash{'timestamp'} = $token->[1];
		$hash{'timestamp'} =~ s/ /-/g;

		# previous close : SKIP
		for ( 1 .. 24 ) { $token = $p->get_token(); }

		# open
		for ( 1 .. 8 ) { $token = $p->get_token(); }
		$hash{'open'} = $token->[1];
		$hash{'open'} =~ s/,//g;
		
		# low
		for ( 1 .. 12 ) { $token = $p->get_token(); }
		$hash{'low'} = $token->[1];
		$hash{'low'} =~ s/,//g;
		
		# high 
		for ( 1 .. 6 ) { $token = $p->get_token(); }
		$hash{'high'} = $token->[1];
		$hash{'high'} =~ s/,//g;
		
		#print Dumper(\%hash);
		return ;
	
		#for my $count ( 1 .. 100 ) {
		#	$token = $p->get_token();
		#	print $count, "\n", Dumper(\$token);
		#}

	}

    } else {
	
	print "Problems getting $idx from $priceUrl\n";
	exit(2);
    }
}

sub populate_components_urls_hash {
    
    my ($components_url) = @_;

    $components_url = 'http://finance.yahoo.com' . $components_url;

    #print "Trying $components_url\n";

    my $u = WWW::Mechanize->new(
	agent => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");

    $u->get($components_url);
    if ($u->success) {
	
	#print $u->content;
	#print Dumper($u->links);
	
	for my $link ($u->links) {
	    #print $link->text, " ", $link->url, "\n";
	    if (defined($link->text) and ($link->url =~ /alpha/)) {
		$urls{'http://finance.yahoo.com/' . $link->url} = $link->text;
	    }
	}

	#print Dumper(\%urls);

    }
}

sub parse_components_pages {

    my (%h) = @_;
    my $token;

    for my $url (keys %h) {
	#print "Now getting data from ", $url, "\n";

	my $u = WWW::Mechanize->new(
	    agent => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");

	$u->get($url);
	if ($u->success) {
	    my $p = HTML::TokeParser->new(\$u->content);

	    for (1 .. 9) { $p->get_tag("table"); }
	    for (1 .. 2) { $p->get_tag("tr"); }

	    while (1) {

		# Sometimes Y! has a component page that has no list
		# for instance for FTSE
		# http://finance.yahoo.com//q/cp?s=%5EFTSE&alpha=6
		$token = $p->get_token();
		#print Dumper(\$token);
	    
		#for (1 .. 4) { $p->get_tag("td"); } 
		$token = $p->get_tag("img"); my $dir_flag;
		#print Dumper($token);
		if ((defined $token->[1]->{'class'}) and ($token->[1]->{'alt'} =~ /Down/)) { 
		    $dir_flag = "DOWN";
		} elsif ((defined $token->[1]->{'class'}) and ($token->[1]->{'alt'} =~ /Up/)) { 
		    $dir_flag = "UP";
		} else {
		    $dir_flag = "UNCH";
		}
		
		$p->get_tag("td");
		my $volume = $p->get_trimmed_text("/td");
		$volume =~ s/,//g;
		if ((!defined($volume)) 
		    or ($volume eq "")
		    or ($volume !~ /^\d/)) {
		    $volume = 0;
		}
		
		if ((!defined $hash{"volume"}) 
		    or ($hash{"volume"} eq "")
		    or ($hash{"volume"} !~ /^\d/)){
		    $hash{"volume"} = 0;
		}

		#print "Hash vol is ", $hash{"volume"}, "\n";
		#print "Vol is ", $volume, "\n";
		$hash{"volume"} += $volume;
		
		if ($dir_flag eq "DOWN") {
		    $hash{"declines"}++; 
		    $hash{"declines_volume"} += $volume;
		} elsif ($dir_flag eq "UP") {
		    $hash{"advances"}++; 
		    $hash{"advances_volume"} += $volume;
		} else {
		    $hash{"unchanged"}++; 
		    $hash{"unchanged_volume"} += $volume;
		}

		$p->get_tag("/tr");
		$token = $p->get_token(); last unless(defined $token);
		#print "Now here ", Dumper(\$token);
		last if (($token->[0] =~ /E/i) and
			 (defined $token->[1]) and
			 ($token->[1] =~ /table/i));
		$p->unget_token($token);
	    }
	} 
    }
}

sub print_bhavcopy {

    my ($h) = @_;
    #print Dumper(\%hash);

    print join " ",
       defined($h->{"open"}) ? $h->{"open"} : 0,
       defined($h->{"high"}) ? $h->{"high"} : 0,
       defined($h->{"low"}) ? $h->{"low"} : 0,
       defined($h->{"close"}) ? $h->{"close"} : 0,
       defined($h->{"volume"}) ? $h->{"volume"} : 0,
       defined($h->{"advances"}) ? $h->{"advances"} : 0,
       defined($h->{"advances_volume"}) ? $h->{"advances_volume"} : 0,
       defined($h->{"declines"}) ? $h->{"declines"} : 0,
       defined($h->{"declines_volume"}) ? $h->{"declines_volume"} : 0,
       defined($h->{"unchanged"}) ? $h->{"unchanged"} : 0,
       defined($h->{"unchanged_volume"}) ? $h->{"unchanged_volume"} : 0,
       defined($h->{"turnover"}) ? $h->{"turnover"} : 0,
    defined($h->{"timestamp"}) ? $h->{"timestamp"} : 0, ;
}
