#!/usr/bin/perl

# parse-nse-deriv-2017-06-04.pl
# No inputs
# discovers which derivatives were traded today
# and dumps the data in bhavcopy format to stdout
# 
# Changelog:
#
# Sun Jun 4, 2017
# Needed to add a couple of new headers
#
#
# Sun Oct 14, 2013
# Initial commit

use strict;
use warnings;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use JSON;
use FunctionsVstaHelper;

#
# We first get a list of futures' urls 
# example: Nifty, BankNifty, CNX-IT etc
# From this list we will derive a second list
# in which we will have a list of urls to individual 
# futures' urls, such as Nifty-Dec-2013
# Lastly we actually get the data for each of these
# futures.
# The reason that we need to get the data for individual
# futures separately, is because only the future's 
# individual page contains open interest data

my %urls;
my %futures_urls;
my $link;
my $url;

my $DEBUG = 0;
if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }

# create a browser so that cookies etc can be shared across subroutines
# and add headers to imitate a true browser
my $m = WWW::Mechanize->new();
#$m->agent_alias('Windows Mozilla');
#$m->agent('Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0');
$m->agent( random_ua_str() );
$m->add_header('Accept' => 'image/png,image/*;q=0.8,*/*;q=0.5');
$m->add_header('Accept-Language' => 'en-US,en;q=0.5');
$m->add_header('Accept-Encoding' => 'gzip, deflate,br');
$m->add_header('Referer' => 'https://nseindia.com/live_market/dynaContent/live_watch/derivative_stock_watch.htm');

populate_urls_hash($m);
#$DEBUG && print STDERR Dumper(%urls);

for $link (sort keys %urls) {
    get_futures_urls($link, $m);
}
#$DEBUG && print STDERR Dumper(\%futures_urls);

for $link (sort keys %futures_urls) {
    get_futures_data($link, $m);
}

#
# Subroutines
# 

# populate_urls_hash
# populates that global hash called urls
# with the urls where the futures contracts' data resides
sub populate_urls_hash {

    my $browser = shift;
    my $baseNseUrl = 'http://www.nseindia.com';
    my $derivUrl = 
	'http://www.nseindia.com/live_market/dynaContent/live_watch/derivative_stock_watch.htm';

    my $DEBUG = 0;
    if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }

    $browser->get($derivUrl);

    #$DEBUG && print STDERR Dumper(\$m->links);
    
    my $p = HTML::TokeParser->new(\$browser->content());
    
    $p->get_tag("select");
    
    while (1) {
	
	my $token = $p->get_token();
	if (($token->[0] =~ /E/i) and ($token->[1] =~ /select/i)) {
	    #$DEBUG && print STDERR "Exitting at the top\n";
	    #$DEBUG && print STDERR Dumper(\$token);
	    last;
	}
	
	my $tag = $p->get_tag("option");
	if (! defined $tag->[1]->{'value'}) {
	    #$DEBUG && print STDERR "option found without value\n";
	    #$DEBUG && print STDERR "exitting\n";
	    last;
	}
	#$DEBUG && print STDERR "\nTag description ", Dumper(\$tag), "\n";

	my $url = $baseNseUrl . $tag->[1]->{'value'} ;
	#$DEBUG && print STDERR "Url is ", $url, "\n";

	if ($url =~ /FUT/i) { 
	    $urls{ $url }++ ;
	    #$DEBUG && print STDERR "Urls hash ", Dumper(\%urls), "\n";
	}
	
	$p->get_tag("/option");
	
	$token = $p->get_token();
	#$DEBUG && print STDERR "Token description 0", "\n", Dumper(\$token), "\n";

	if ($token->[1] =~ /option/i) {
	    $p->unget_token($token);
	    #$DEBUG && print STDERR "Option found: looping back\n";
	    next;
	} elsif ($token->[1] =~ /select/i) {
	    #$DEBUG && print STDERR "possible end of select\n";
	    #$DEBUG && print STDERR "Exitting in the middle\n";
	    last;
	} else {
	    my $token1 = $p->get_token(); 
	    my $token2 = $p->get_token();
	    if (!defined($token2)) {
		#$DEBUG && print STDERR "token 2 not defined\n";
		#$DEBUG && print STDERR "exitting\n";
		last;
	    }
	    
	    #$DEBUG && print STDERR "Token description 1", "\n", Dumper(\$token1), "\n";
	    #$DEBUG && print STDERR "Token description 2", "\n", Dumper(\$token2), "\n";
	    #$DEBUG && print STDERR "Now here 1\n";

	    if (($token1->[0] =~ /S/i) and ($token1->[1] =~ /option/i)) {
		#$DEBUG && print STDERR "Now here 2 \t Looping back\n";
		$p->unget_token($token2);
		$p->unget_token($token1);
		$p->unget_token($token);
		next;
	    }

	    #$DEBUG && print STDERR "Now here 3\n";
	    $p->unget_token($token);
	}
    }
}

#
# get_futures_urls
# Takes a url, and the browser object,
# GETs the url 
# and dumps the individual 
# futures'urls from HTTP::Response
sub get_futures_urls {
    my ($url, $browser) = @_;

    my $DEBUG = 0;
    if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }
    
    #SDEBUG && print STDERR  "1 Now GETting $url", "\n";

    $browser->get($url);

    for my $link ($browser->links) {
	if ($link->url =~ /instrument=FUTIDX/) {
	    #$futures_urls{ 'http://www.nseindia.com' . $link->url }++
	    $futures_urls{ 'http://nseindia.com' . $link->url }++
	}
    }
}

#
# get_futures_data
# takes a url, and the browser object
# GETs the url
# and dumps the data from HTTP::Response 
sub get_futures_data {
    my ($url, $browser) = @_;

    my $DEBUG = 0;
    if (defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }

    if ($url =~ /^http\:\/\//) { $url =~ s/^http/https/; }
    
    #$DEBUG && print STDERR "2 Now GETting $url", "\n";

    $browser->get($url);

    my $p = HTML::TokeParser->new(\$browser->content());

    $p->get_tag("div"); $p->get_tag("div");
    my $str = $p->get_trimmed_text("/div");

    my $h = decode_json($str);
    #$DEBUG && print STDERR Dumper(\$h);

    return if ($h->{'data'}->[0]->{'numberOfContractsTraded'} =~ /-/);

    my $timestamp =  $h->{'lastUpdateTime'};
    $timestamp =~ s/\s+/-/g;

    my $expiry =  $h->{'data'}->[0]->{'expiryDate'};
    $expiry =~ s/(..)(...)(....)/$1-$2-$3/g;

    my $line = join " ", 
       "FUTIDX",
       $h->{'data'}->[0]->{'underlying'},
       $expiry, "0", "XX",
       $h->{'data'}->[0]->{'openPrice'},
       $h->{'data'}->[0]->{'highPrice'},
       $h->{'data'}->[0]->{'lowPrice'},
       $h->{'data'}->[0]->{'lastPrice'},
       $h->{'data'}->[0]->{'vwap'},
       $h->{'data'}->[0]->{'numberOfContractsTraded'},
       $h->{'data'}->[0]->{'turnoverinRsLakhs'},
       $h->{'data'}->[0]->{'openInterest'},
       $h->{'data'}->[0]->{'changeinOpenInterest'},
    $timestamp,;

    $line =~ s/,//g;
    $line=~ s/ /,/g;

    print $line, "\n";
}
