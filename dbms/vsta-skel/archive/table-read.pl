#!/usr/bin/perl -w

use HTML::TableContentParser;
$p = HTML::TableContentParser->new();

my ($table_count, $row_count, $column_count);

my $file = shift;
open FILE, "< $file" or die "Cannot open $file ", $!;

while (<FILE>) { $html .= $_ };

$tables = $p->parse($html);

for $t (@$tables) {
	$table_count++;
	$row_count = 0;
	$column_count = 0;
	print "Table : ", $table_count, "\n";

	for $h (@{$t->{table_headers}}) {
		print $h, "\n";
	}

	for $r (@{$t->{rows}}) {
		$row_count++;
		$column_count = 0;
		print "Row: ", $row_count, "\n";
		for $c (@{$r->{cells}}) {
			$column_count++;
			$string = $c->{data};
			#$string =~ s/\,//g;
			#$string =~ /(\d+\.\d+)/;
			#print "\n", "Column: ", $column_count, " ", $1;
			print "\n", "Column: ", $column_count, " ", $string;
		}
		print "\n";
	}
}
