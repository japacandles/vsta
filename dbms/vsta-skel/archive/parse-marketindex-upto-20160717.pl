#!/usr.bin/perl
#

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;

my $tmp;
my %hash;

my $index = shift;

my $u = LWP::UserAgent->new();
$u->agent("Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0");

my $indexUrl = 'http://www.marketindex.com.au/' . $index;
my $response = $u->get($indexUrl);
#my $data = decode_utf8($response->content);

my $p = HTML::TokeParser->new(\$response->content);
#my $token;
my $direction;

# first of all, we look for the ohlc info

$hash{'index'} = $index;
# the 3rd "p" tag gives us the close
$p->get_tag("p"); $p->get_tag("p"); $p->get_tag("p");
$tmp = $p->get_trimmed_text("/p");
$hash{'close'} = decomma($tmp);

$p->get_tag("p");
$hash{'timestamp'} = $p->get_trimmed_text("/p");

# skip the 4th, 5th, 6th, and 7th "p"
$p->get_tag("p"); $p->get_tag("p"); $p->get_tag("p");
$tmp = $p->get_trimmed_text("/p");
$hash{'open'} = decomma($tmp);

$p->get_tag("p"); $p->get_tag("p");
$tmp = $p->get_trimmed_text("/p");
$hash{'high'} = decomma($tmp);

$p->get_tag("p"); $p->get_tag("p");
$tmp = $p->get_trimmed_text("/p");
$hash{'low'} = decomma($tmp);

# next we look for the volume and breadth info

$p->get_tag("table"); 

# the second row
$p->get_tag("tr");

while (1) {
    $p->get_tag("tr");

    for (1 .. 5) { $p->get_tag("td"); }
    $direction = $p->get_trimmed_text("/td");
    if ($direction =~ /\+/) { $hash{'advances'}++ ; }
    elsif ($direction =~ /\-/) { $hash{'declines'}++ ; }
    else { $hash{'unchanged'}++ ; }

    for (1 .. 4) { $p->get_tag("td"); }
    my $volume = $p->get_trimmed_text("/td");
    $volume = decomma($volume);
    $hash{'volume'} += $volume;

    if ($direction =~ /\+/) { $hash{'advances_volume'} += $volume ; }
    elsif ($direction =~ /\-/) { $hash{'declines_volume'} += $volume ; }
    else { $hash{'unchanged_volume'} += $volume ; }
    
    $hash{'value'} = 0;
    $p->get_tag("/tr");

    my $token = $p->get_token(); #print Dumper($token);
    my $token2 = $p->get_token(); #print Dumper($token2);
    #my $token3 = $p->get_token(); print Dumper($token3);

    last if ($token2->[1] =~ /tbody/);
    
    #$p->unget_token($token3); 
    $p->unget_token($token2); 
    $p->unget_token($token);
}

#print Dumper(\%hash);
print_bhavcopy(\%hash);

sub decomma {
    my ($str) = @_;
    $str =~ s/,//g;
    return $str;
}    

sub print_bhavcopy {

    my ($h) = @_;
    print join " ", $h->{'open'}, $h->{'high'},
                    $h->{'low'}, $h->{'close'},
                    $h->{'volume'}, $h->{'value'},
                    defined $h->{'advances'} ? $h->{'advances'} : 0,
                    defined $h->{'advances_volume'} ? $h->{'advances_volume'} : 0,
                    defined $h->{'declines'} ? $h->{'declines'} : 0 ,
                    defined $h->{'declines_volume'} ? $h->{'declines_volume'} : 0 ,
                    defined $h->{'unchanged'} ? $h->{'unchanged'} : 0 ,
                    defined $h->{'unchanged_volume'} ? $h->{'unchanged_volume'} : 0 ,
    defined $h->{'timestamp'} ? $h->{'timestamp'} : "", $h->{'index'},
                    "\n";
    
}
