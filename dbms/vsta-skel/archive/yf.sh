#!/bin/bash

WHICH="/usr/bin/which"
ZDUMP=$($WHICH zdump)
DBMSDIR=$PWD
ZONEDIR="/usr/share/zoneinfo/"

if [ -n "$1" ]
then
    timezone="$1"
else
    timezone="Asia/Kolkata"
fi

if [ -n "$2" ]
then
    datadir="$2"
else
    datadir="/tmp"
fi

if [ -n "$3" ]
then
    ticker="$3"
else
    echo "Usage: basename $0 <timezone> <datadir> <ticker>"
    exit 1
fi

#echo $timezone
#echo $datadir
date=$($ZDUMP $ZONEDIR/$timezone | awk -f $DBMSDIR/yf.awk)
echo -n "$date "
$DBMSDIR/yf3.pl $ticker
