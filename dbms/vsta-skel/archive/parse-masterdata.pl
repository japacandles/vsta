#!/usr/bin/perl

#
# parse-masterdata.pl
# Date: Mar 14, 2011
# 01:23 AM :)
#
# I don't seem to get S&P 500 volumes, A/D from yahoo finance or 
# bloomberg. But masterdata.com seems to be OK
#
use strict;
use diagnostics;
use LWP::UserAgent;
use HTML::TokeParser;

binmode STDOUT, ':utf8';

my $idx = shift;
my %hash;
my $tmp;

my $url = 'http://www.masterdata.com/Reports/Combined/CompositeReports/' . $idx;

# get the page
my $u = LWP::UserAgent->new();
my $response = $u->get($url);
die unless $response->is_success();
my $html = $response->content();

my $p = HTML::TokeParser->new(\$html) or die $!;

# The date is in table 3
$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
$p->get_tag("font"); $p->get_tag("font"); $p->get_tag("font");
$hash{'date_string'} = $p->get_trimmed_text("/font");

# Data begins in table 5
$p->get_tag("table"); $p->get_tag("table");

# Open: Row 2, col 2 
$p->get_tag("tr"); $p->get_tag("tr");
$p->get_tag("td"); $p->get_tag("td");
$p->get_tag("b"); $hash{'open'} = $p->get_trimmed_text("/b");

# High: Row 3, col 2 
$p->get_tag("tr");
$p->get_tag("td"); $p->get_tag("td");
$p->get_tag("b"); $hash{'high'} = $p->get_trimmed_text("/b");

# Low: Row 4, col 2 
$p->get_tag("tr");
$p->get_tag("td"); $p->get_tag("td");
$p->get_tag("b"); $hash{'low'} = $p->get_trimmed_text("/b");

# Close: Row 5, col 2 
$p->get_tag("tr");
$p->get_tag("td"); $p->get_tag("td");
$p->get_tag("b"); $hash{'close'} = $p->get_trimmed_text("/b");

# Total volume: Row 9, col 2
$p->get_tag("tr"); $p->get_tag("tr"); 
$p->get_tag("tr"); $p->get_tag("tr");
$p->get_tag("td"); $p->get_tag("td");
$p->get_tag("b"); $hash{'volume'} = $p->get_trimmed_text("/b");

# Now for some fun :)
# table 7 contains A/D data
# but that begins after a nested table (no 8)
# The purpose of table 8 is not clear to me 
# So we'll just skip to its end
$p->get_tag("table"); $p->get_tag("table");
$p->get_tag("table"); $p->get_tag("/table");

# Adv: Row 9, the first <b>
$p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr");
$p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr");
$p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr");
$p->get_tag("b"); $tmp = $p->get_trimmed_text("b");
$hash{'advances'} = strip_arrow($tmp);

# Decl: Row 10, the first <b>
$p->get_tag("tr");
$p->get_tag("b"); $tmp = $p->get_trimmed_text("b");
$hash{'declines'} = strip_arrow($tmp);

# Unchanged: Row 11, the first <b>
$p->get_tag("tr");
$p->get_tag("b"); $tmp = $p->get_trimmed_text("b");
$hash{'unchanged'} = strip_arrow($tmp);

# Adv volumes: row 13, the first <b>
$p->get_tag("tr");$p->get_tag("tr");
$p->get_tag("b"); $tmp = $p->get_trimmed_text("b");
$hash{'advances-volume'} = strip_arrow($tmp);

# Decl volumes: row 14, the first <b>
$p->get_tag("tr");
$p->get_tag("b"); $tmp = $p->get_trimmed_text("b");
$hash{'declines-volume'} = strip_arrow($tmp);

# Unch volumes: row 15, the first <b>
$p->get_tag("tr");
$p->get_tag("b"); $tmp = $p->get_trimmed_text("b");
$hash{'unchanged-volume'} = strip_arrow($tmp);

# Debug
#for (keys %hash) { 
#    $hash{$_} =~ s/,//g;
#    print $_, "\t", $hash{$_}, "\n"; 
#}

# Print in bhavcopy fmt
# All volumes multiplied by 100 because of yahoo finance inaccuracies :(
for (keys %hash) { $hash{$_} =~ s/,//g; } # de-comma
print join " ",
    $hash{'open'}, $hash{'high'}, $hash{'low'}, $hash{'close'},
    $hash{'volume'} * 100,
    $hash{'advances'}, $hash{'advances-volume'} * 100,
    $hash{'declines'}, $hash{'declines-volume'} * 100,
    $hash{'unchanged'}, $hash{'unchanged-volume'} * 100,
    $hash{'date_string'}, 
    "\n";


#
# Had to write sub strip_arrow because
# A/D numbers have a arrow utf8 image
# needed to strip that out
# 
sub strip_arrow {
    my $tmp = shift;
    $tmp =~ s/,//g; # de-comma
    $tmp =~ /(\d+)/;
    $tmp = $1;
    return $tmp;
}
