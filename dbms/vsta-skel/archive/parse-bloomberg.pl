#!/usr/bin/perl -w

use strict;
use diagnostics;
use HTML::TokeParser;
use Data::Dumper;

my $infile = shift;
my ($advances, $declines, $advanceVolumes, 
	$declineVolumes, $totalVolumes, $ad, $vol) = 
		(0,0,0,0,0,0,0);
my $token = "" ;

my $p = HTML::TokeParser->new($infile) or die $!;
$p->empty_element_tags(1);

# Get to the 12th table
$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");

# Data starts in the second row
$p->get_tag("tr"); $p->get_tag("tr");

while ($token ne "E") {
	$p->get_tag("/td"); $p->get_tag("td"); # col 1: security name: SKIP
	$p->get_tag("/td"); $p->get_tag("td"); # col 2: closing price: SKIP

	$p->get_tag("span"); # col 3: a/d value
	$ad = $p->get_trimmed_text("/span"); 
	last if ($ad =~ /\[IMG\]/); # UGLY HACK FIXME
	$ad =~ s/,//g; # remove all commas
	$p->get_tag("/td");

	$p->get_tag("/td"); $p->get_tag("td"); # col 4: adv/dec percent: SKIP

	$vol = $p->get_trimmed_text("/td"); # col 5: volume
	$vol =~ s/,//g; # remove all commas
	$p->get_tag("/td");

	# print $ad, "\t", $vol, "\n";

	if ($ad > 0) {
		$advances++;
		$advanceVolumes += $vol;
	} else {
		$declines++;
		$declineVolumes += $vol;
	}

	$totalVolumes += $vol;
	
	#print join " ",$ad, $vol, $advances, $advanceVolumes, 
	#	$declines, $declineVolumes, $totalVolumes, "\n";
	
	$p->get_tag("/tr"); # skip everything else

	$token = $p->get_token()->[0] ;
}; 

print join " ", $advances, $advanceVolumes, $declines, 
	$declineVolumes, $totalVolumes, "\n";

