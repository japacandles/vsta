#!/bin/bash -x

# common-functions.sh
# Dec 22, 2018, Sat 17:00 M
# functions that can be included by other dbms scripts


# Select a random user agent string for this session
# https://stackoverflow.com/questions/1194882/how-to-generate-random-number-in-bash
# ref https://www.cyberciti.biz/faq/finding-bash-shell-array-length-elements
user_agent () {
    a=( \
	"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko" \
	    "Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko"  \
	    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 7.0; InfoPath.3; .NET CLR 3.1.40767; Trident/6.0; en-IN)"  \
	    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)"  \
	    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1"  \
	    "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0"  \
	    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0" \
	    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36" \
	    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36"  \
	    "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36" \
	)
    
    #UA='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    UA=${a[((RANDOM % ${#a[@]} - 1))]}
    echo "$UA"
    
}

# random_free_proxy
# - assumes that /var/tmp/proxies exists and has a non-zero number of lines
# - creates a hash of the proxies from /var/tmp/proxies file
# - loops through proxies hash
# - returns a randomly chosen proxy
random_free_proxy ()
{
    #declare -a ary
    #ary=( "five" "four" "three" "two" "one" )
    #aryLength=${#ary[@]}

    #echo "Number of elements is " $aryLength

    #echo "original ary"
    #for ((index=0; index<aryLength; index++))
    #do
    #   echo $index "${ary[$index]}"
    #done

    #echo "now randomizing ..."
    #for index in ${!ary[@]}
    #do
    
    #echo "index is $index"
    #randomIndex=$(( RANDOM % $aryLength ))
    #echo "attempting to swap element no " $randomIndex
    #tmp="${ary[$randomIndex]}"; echo "tmp is $tmp"
    #ary[$randomIndex]="${ary[(( $aryLength - 1 ))]}"
    #ary[(( $aryLength - 1 ))]=$tmp

    #echo "new ary"
    #for ((index=0; index<aryLength; index++))
    #do
    #    echo $index "${ary[$index]}"
    #done

    #done

    numProxies=${#proxies[@]}
    for index in ${!proxies[@]}
    do
	randomIndex=$(( RANDOM % $numProxies ))
	tmp="${proxies[$randomIndex]}"
	proxies[$randomIndex]="${proxies[(( $numProxies - 1 ))]}"
	proxies[(( $numProxies - 1 ))]=$tmp
    done
	 
    proxy=${proxies[((RANDOM % ${#proxies[@]} - 1))]}
    echo "$proxy"
}

# random_test_destination
# - creates an array of destination sites on which we will test the proxy
# - loops through the destinations' array
# - returns a randomly chosen destination
# Not sure about why this function is needed :(
random_test_destination ()
{
    # there are https urls that permit HEAD requests
    # if HEAD through the proxy, returns 200 OK, the proxy works
    dests=( \
	    "https://www.ibm.com" \
		"https://www.oracle.com" \
		"https://www.nyse.com" \
		"https://www.microsoft.com" \
		"https://www.nseindia.com" \
		"https://www.bseindia.com" \
		"https://www.bbc.co.uk" \
		"https://www.msnbc.com" \
	)
    
    dest=${dests[((RANDOM % ${#dests[@]} - 1))]}
    echo "$dest"   
}

# get_working_proxy
# cycles through free proxies and stops at the first working proxy
# - repeatedly calls random_free_proxy
#   - for each proxy, we try to GET whatsmyip
#      - match: return this proxy as working proxy
#      - no match: next get_free_proxy
# [TODO] what if the entire list of free proxies has been exhausted ?
#        if so, we should unset the proxy variables
get_working_proxy ()
{

    # explanation for infinite loop
    # we are going to test max_tries if /var/tmp/proxies exists
    # every time we go into this loop we increment this_try
    # we break out of this loop if
    #  /var/tmp/proxies exists
    #  OR
    #  this_try > max_tries
    max_tries=5
    this_try=0

    PROXY_FOUND="FALSE"
    proxy=""
    
    while (true)
    do
	# do this loop only if this_try < max_tries
	let "this_try++" 
	if [ "$this_try" -lt "$max_tries" ]
	then	
	    
	    # does proxy list exist
	    if [ -f /var/tmp/proxies ]
	    then
		
		# if there is a non-zero number of proxies
		numProxies=$(wc -l /var/tmp/proxies)
		if [ $numProxies -gt 0 ] 
		then
		    
		    # create an array of proxies from which a random proxy will be chosen
		    readarray proxies < /var/tmp/proxies
		    #echo "Number of elements in proxies is " ((${#proxies[@]} - 1))
		    
		    # now that proxies is an actual array, we need to randomize the elements
		    # the crude hack here is to swap random elements with the last element
		    # and do this swap as many times as the length of the proxies array
		    # we are probably living in a state of sin !
		    numProxies=${#proxies[@]}
		    for index in ${!proxies[@]}
		    do
			randomIndex=$(( RANDOM % $numProxies ))
			tmp="${proxies[$randomIndex]}"
			proxies[$randomIndex]="${proxies[(( $numProxies - 1 ))]}"
			proxies[(( $numProxies - 1 ))]=$tmp
		    done
	 	    
		    # iterate through the proxies array looking for a proxy that works
		    # break prematurely if such a proxy is found.
		    # if no working proxy is found, we will set *****
		    for proxy in ${proxies[@]}
		    do
			
			#echo Proxy is $proxy
			ip=$(echo $proxy | awk -F: '{print $1}')
			
			a=$(user_agent)
			#echo "User agent is " "$a"
			
			tmpfile=$(mktemp)
			
			HTTP_PROXY=http://$proxy HTTPS_PROXY=http://$proxy http_proxy=http://$proxy https_proxy=http://$proxy curl -s -A "$a" https://whatsmyip.com > $tmpfile
			grep -q "$ip" $tmpfile
			retval=$?
			#echo Ret Val is $retval
			if [[ $retval -eq 0 ]]
			then
			    set -a
			    HTTP_PROXY="http://$proxy"
			    HTTPS_PROXY="http://$proxy"
			    http_proxy="http://$proxy"
			    https_proxy="http://$proxy"
			    set +a
			    PROXY_FOUND="TRUE"
			    break
			fi
		    done
		    
		    if [ "$PROXY_FOUND" == "FALSE" ]
		    then
			break # 
			
		    fi
		    
		    # the /var/tmp/proxies exists, but is empty
		    #     
		else
		    
		    # if the number of tries has been exceeded
		    if [ $this_try_number -ge $num_max_tries ]
		    then
			unset HTTP_PROXY HTTPS_PROXY http_proxy https_proxy
			break
			
		    else
		    
			# wait for a random number of seconds (max 20 secs) and
			# check again
			sleep( (( RANDOM % 20 )) )
		    fi
		fi

	    else

		# if /var/tmp/proxies doesn't exist
		# wait for a random number of seconds (max 20 secs) and
		# check again
		sleep( (( RANDOM % 20 )) )
	    fi	
    done

    # if we have reached this point, there isn't a working proxy
    unset HTTP_PROXY HTTPS_PROXY http_proxy https_proxy
    
}
