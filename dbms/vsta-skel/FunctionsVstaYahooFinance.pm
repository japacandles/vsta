package FunctionsVstaYahooFinance;

require Exporter;
@ISA = qw(Exporter);
@EXPORT= qw (
    populate_hash_from_yahoo_finance_html
    get_yahoo_finance_html_content
    get_yahoo_finance_futures_list
    );

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use LWP::UserAgent;
use HTML::TokeParser;
use FunctionsVstaHelper;
binmode STDOUT, ':utf8';

sub populate_hash_from_yahoo_finance_html {
    my ($c, $href) = @_;

    my $DEBUG = 0; if (defined $ENV{'DEBUG'}) { $DEBUG = $ENV{'DEBUG'}; }
    
    my %h = %{$href};

    $ENV{'DEBUG'} and print "IN SUB after entering", "\n", Dumper(\%h);
    
    my $p = HTML::TokeParser->new($c);

    my ($token, $text);
    while ($token = $p->get_token) {
	$ENV{'DEBUG'} && print Dumper($token), "\n";
	$text = $p->get_text;
	#$ENV{'DEBUG'} && print join "\n", 'TEXT', $text, "\n";
	
	next if ($token->[1] =~ /script/g);
	
	if ( (defined $token->[2]) && (ref($token->[2]) =~ /HASH/) && (defined $token->[2]->{'data-reactid'}) ) {
	    #$ENV{'DEBUG'} && print join "\n", "REACTID: ", ref($token->[2]), Dumper($token->[2]), "\n";

            # CLOSE
	    if ( ($token->[2]->{'data-reactid'} eq '31') && ($token->[1] =~ /span/) 
	    	&& ($text !~ /Holders/) 
		&& ($text !~ / days ago/)
                 && ($text =~ /^\d/)) { $href->{'close'} = $text; next; }

            # TIMESTAMP
	    #if ( ($token->[2]->{'data-reactid'} eq '37') && ($token->[1] =~ /span/) ) { $href->{'timeStamp'} = $text; next ;}
	    if ( ($token->[2]->{'data-reactid'} eq '34') && ($token->[1] =~ /span/) && ($text =~ /^A/) ) { $href->{'timeStamp'} = $text; next ;}

            # HI-LO RANGE
	    if (defined $token->[2]->{'data-test'}) {
		if ($token->[2]->{'data-test'} =~ /DAYS_RANGE/) {
		    my $range = $text;
		    ($href->{'low'}, $href->{'high'}) = split / - /, $range;
		} 

                # OPEN
		elsif ($token->[2]->{'data-test'} =~ /OPEN/) {
		    $p->get_token; $text = $p->get_text;
		    $href->{'open'} = $text;
		    
		} 

                # VOLUME
		elsif ($token->[2]->{'data-test'} =~ /TD_VOLUME/) {
		    $p->get_token; $text = decomma($p->get_text);
		    $href->{'volume'} = $text;
		}
	    }
	}
    }
    
    $ENV{'DEBUG'} and print "IN SUB before returning", "\n", Dumper(\%h);
    #return(%h);
}

# get_yahoo_finance_html_content
# input   : the name of a yahoo finance security (like ^gdaxi or UGL)
# returns : HTTP::Response object after GETting the finance.yahoo uel of that security
sub get_yahoo_finance_html_content {
    my $i = shift;

    my $DEBUG = 0; if (defined $ENV{'DEBUG'}) { $DEBUG = $ENV{'DEBUG'}; }
    
    $i = uc($i);
    my $url;
    
    if ($i =~ /=/) {

	$url = 'https://finance.yahoo.com/quote/' . $i . '/futures?p=' . $i;
    }
    
    else {
	
	$url = 'https://finance.yahoo.com/quote/' . $i . '?' . 'p=' . $i;
    }
    
    #print join ' ', $i, $url, "\n";
    
    my $ua = LWP::UserAgent->new();
    #$ua->agent(random_ua_str());
    $ua->agent($ENV{'BROWSER'});
    
    my $response = $ua->get($url);
    if ($response->is_success) { return(\$response->decoded_content); }
    else {
	print $response->status_line, "\n"; 
	return (\$response->status_line); 
    }
}


# get_yahoo_finance_html_content
# input   : takes a HTTP::Response object
# returns : list of all securities that are in the futures chain in 
#           the input HTTP::Response
sub get_yahoo_finance_futures_list {

    my $c = shift;

    my $DEBUG = 0; if (defined $ENV{'DEBUG'}) { $DEBUG = $ENV{'DEBUG'}; }
    
    my @ary; my $tmp;
    my $p = HTML::TokeParser->new($c);

    while (my $token = $p->get_tag("a")) {
	my $url = $token->[1]{'href'} || "-";
	#$DEBUG && print STDERR "Url is : $url \n";

	if (
	    ($url =~ /quote/)
	    and ($url !~ /\^/)
	    and ($url !~ /chart/) 
	    and ($url !~ /futures/) 
	    and ($url !~ /\?p=(.+)=(.+)/)
	    and ($url !~ /quote\/(.+)%3DF(.+)\?p=(.+)%3D(.+)/)) {
	    
	    #$DEBUG && print STDERR "Pushing Url is : $url \n";
	    #push @ary, 'https://finance.yahoo.com' . $url ;

	    ($tmp, $url) = split /=/, $url;
	    if ($url !~ /%/) { push @ary, $url; }
	}
    }
 
    return @ary;
}

1;

