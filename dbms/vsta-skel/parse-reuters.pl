#!/usr/bin/perl

#
# parse-reuters.pl
# Jan 30, 2014, Thursday, 01:30 AM :-)
# This script parses the reuters.com website (duh!)
# and gets the data 
#
# Nov 28th, 2015, Saturday, 10:46 AM, Pune
# Reuters has made some change. As a result, we need to arrive
# at total volumes as a sum of other volumes. Otherwise it is
# reported as zero

use strict;
use diagnostics;
use warnings;
use WWW::Mechanize;
use HTML::TokeParser;
use Data::Dumper;
use FunctionsVstaHelper;
use FunctionsVstaReuters;

my $index = shift;

# Usage :-) and die
unless (defined $index) {
    print STDERR 
	"Usage: parse-reuters.pl <index-name>\n",
	"Example: for Nikkei 225\n",
	"parse-reuters.pl n225\n",
	"will generate output in the following format\n",
	"date open high low close volume advances advances-volumes declines declines-volumes unch unch-volumes timestamp\n",
	"Other indexes whose data can be obtained thus are\n",
	"FTSE 100 => ftse\n",
	"DAX => gdaxi\n",
	"AEX (Amsterdam) => aex\n",
	"IBEX 35 => ibex\n",
	"BVSP => bovespa\n",
	"Hang Seng => hsi\n",
	"Straits Times Index => ftsti\n";

    exit (1);
}

# check debug settings
my $DEBUG = 0;
if (defined $ENV{'DEBUG'}) {$DEBUG = $ENV{'DEBUG'}; }

# Since we are here the index string has been found.
# So we convert it to upper-case
$index = uc($index);

# Reuters prefixes a period to each index name.
# For example N225 is queried via ".N225"
# So we set up the base url
my $reuters = 'http://www.reuters.com';
my $reutersUrl = $reuters . '/finance/markets/index?symbol=.';

# The url that we are going to get the data from
my $indexUrl = $reutersUrl . $index;
#print $indexUrl, "\n";

# Create the browser
#my $u = WWW::Mechanize->new(agent => random_ua_str());
my $u = WWW::Mechanize->new(agent => $ENV{'BROWSER'});

my %bhavHash;
my $next_exists;

#
# The layout is such that the first table contains the price data
# and the second table contains the volume/breadth data
# So, I want to get the breadth data from each page whether or not
# it contains a "NEXT" link. The I follow the "Next" link.
# If a page contains no "Next" link, its obviously the last page,
# so I take the price data from it
# Since I don't know how many pages will have "Next", I have to 
# put things in an infinite loop and exit whenever I hit a page
# without a "Next" link in it
while (1) {

    #print $indexUrl, "\n";

    $u->get($indexUrl);
    if ($u->success()) {
	my %tmphash;
	my $response = $u->response();
	
	get_volume_breadth(\$u->content(), \%bhavHash);
	
	for ($u->links) {
	    if (defined($_->text) and ($_->text =~ /^Next/)) { 
		$tmphash{ $reuters . $_->url }++;
		$next_exists++ ;
	    }
	}
	#print Dumper(\%tmphash);
	unless ($next_exists) { get_price(\$u->content(), \%bhavHash); last; }
	
	$next_exists = 0;
	for (keys %tmphash) { $indexUrl = $_ ; next ;}

    }
}

#print Dumper(\%bhavHash);
print_bhavcopy(\%bhavHash);
