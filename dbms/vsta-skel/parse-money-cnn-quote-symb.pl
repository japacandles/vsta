#!/usr/bin/perl

# parse-money-cnn-quote-symb.pl
# Date: Sun, Dec 4, 2016
# Based on parse-money.-cnn.pl
# To parse money.cnn.com to retrieve data where only symbol is provided
# Sample usage:
# For the Nasdaq 100 index (NDX)
# [mkpai@mkpai ~]$ perl /tmp/parse-money-cnn-quote-symb.pl ndx

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use FunctionsVstaHelper;

# If no CNN section is specified, exit immediately
#my $cnn_section = shift;
#unless (defined $cnn_section) {
#    print STDERR "No CNN section specified: markets world_markets ?\n";
#    exit (1);
#}

# If no index name is supplied, exit immediately
my $index = shift;
unless (defined $index) {
    print "No index specified\n";
    exit (1);
}

my $DEBUG = 0;
if ( defined $ENV{'DEBUG'} ) { $DEBUG = $ENV{'DEBUG'}; }

# Create my browser
my $m = WWW::Mechanize->new();
# I add these headers because I randomly get "denied" errors without them
#$m->add_header("User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");
$m->add_header( random_ua_str() );
$m->add_header("Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
$m->add_header("Accept-Language" => "en-us,en;q=0.5");
$m->add_header("Accept-Encoding" => "gzip, deflate");
$m->add_header("Connection" => "keep-alive");

# Get page 1
my $url = 'http://money.cnn.com/quote/quote.html?symb=' . $index .'&page=1';
$DEBUG && print STDERR $url, "\n";
$m->get($url) or die $!;
my ($p, $q, %hash);
if ($m->success) { 
	$p = HTML::TokeParser->new(\$m->content);
	$q = HTML::TokeParser->new(\$m->content);
};

#print STDERR "sending $cnn_section\n";
#my $count = how_many_pages($cnn_section, $p);
my $count = how_many_pages($p);
$DEBUG && print STDERR "From the sub ", $count, "\n";

# Parse page 1 which we already have
read_data($q, 1, \%hash);

# Now get the rest of the pages and parse them
for my $page (2 .. $count) {
    $DEBUG && print STDERR "Now getting page no ", $page, " of $count\n";
    $url = 'http://money.cnn.com/quote/quote.html?symb=' . $index . '&page=' . $page;
    $m->get($url) or die $!;
    if ($m->success) { 
	$q = HTML::TokeParser->new(\$m->content);
	read_data($q, $page, \%hash);
    }
}

#print Dumper(\%hash);
# Print in bhavcopy format
print join " ",
    $hash{'open'}, $hash{'high'}, $hash{'low'}, $hash{'close'}, $hash{'volumes'},
    defined($hash{'advances'}) ? $hash{'advances'} : 0,
    defined($hash{'advances_volumes'}) ? $hash{'advances_volumes'} : 0,
    defined($hash{'declines'}) ? $hash{'declines'} : 0,
    defined($hash{'declines_volumes'}) ? $hash{'declines_volumes'} : 0,
    defined($hash{'unchanged'}) ? $hash{'unchanged'} : 0,
    defined($hash{'unchanged_volumes'}) ? $hash{'unchanged_volumes'} : 0,
    $hash{'timestamp'}, "\n";

exit (0);

#
# SUBROUTINES
#

# This subroutine reads an HTML::TokeParser, parses 
# the 6th table and updates breadth data and volumes
sub read_data {
    my ($a, $page, $hashref) = @_;

    my ($flag, $change, $volume);

    my $DEBUG = 0;
    if (defined $ENV{'DEBUG'}) { $DEBUG = $ENV{'DEBUG'}; }
    
    if ($page == 1) {
	
	# The 2nd table contains close
	$a->get_tag("table"); #$a->get_tag("table"); $a->get_tag("table");
	$a->get_tag("span"); $hashref->{'close'} = decomma($a->get_trimmed_text("/span")); #$DEBUG && print STDERR "close is ",$hashref->{'close'}, "\n";
	
	# The 4th table constains OHL data and misc stuff
	$a->get_tag("table");
	
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'prev_close'} = decomma($a->get_trimmed_text("/td")); #$DEBUG && print STDERR "Prev cl ", $hashref->{'prev_close'}, "\n";
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'open'} = decomma($a->get_trimmed_text("/td"));
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'high'} = decomma($a->get_trimmed_text("/td"));
	$a->get_tag("td"); $a->get_tag("td"); $hashref->{'low'} = decomma($a->get_trimmed_text("/td"));

	# The 5th table isn't relevant
	$a->get_tag("table"); $a->get_tag("/table"); 
	$a->get_tag("div"); $a->get_tag("div"); #$a->get_tag("div"); 
	$hashref->{'timestamp'} = $a->get_trimmed_text("/span");
	
	# The 6th table is where its at
	$a->get_tag("table"); $a->get_tag("/tr");
	while (1) {
	    $a->get_tag("td"); # Name of company
	    $a->get_tag("td"); # Price
	    
	    $a->get_tag("td"); # Change
	    $change = $a->get_trimmed_text("/td"); #$DEBUG && print STDERR "change is $change", "\n";
	    if ($change =~ /^\+/) { 
		$flag = "ADVANCES";
	    } elsif ($change =~ /^\-/) {
		$flag = "DECLINES";
	    } else {
		$flag = "UNCHANGED";
	    }
	    
	    #$DEBUG && print STDERR $change, " ", $flag, "\n";
	    $a->get_tag("td"); #  %Change
	    $a->get_tag("td"); # P/E
	    
	    # volume
	    $a->get_tag("td");
	    $volume = decomma($a->get_trimmed_text("/td")); #$DEBUG && print STDERR "volume is $volume\n";
	    #$DEBUG' && print STDERR "original volume is $volume\n";
	    
	    if ($volume =~ /K$/) { chop $volume; $volume *= 1000; }
	    if ($volume =~ /M$/) { chop $volume; $volume *= 1000000; }
	    if ($volume =~ /B$/) { chop $volume; $volume *= 1000000000; }
	    $DEBUG && print STDERR "volume is $volume\n";
	    
	    $hash{'volumes'} += $volume ;
	    if ($flag eq "ADVANCES") { $hash{'advances'}++ ; $hash{'advances_volumes'} += $volume; }
	    if ($flag eq "DECLINES") { $hash{'declines'}++ ; $hash{'declines_volumes'} += $volume; }
	    if ($flag eq "UNCHANGED") { $hash{'unchanged'}++ ; $hash{'unchanged_volumes'} += $volume; }
	    
	    #$DEBUG && print STDERR join ",", $change, $flag, $volume,"\n";
	    
	    $a->get_tag("td"); $a->get_tag("/tr"); 
	    my $token = $a->get_token;
	    
	    last if ($token->[1] eq "tbody");
	}
    }

    else { # if page no exceeds 1

	$a->get_tag("table"); # $DEBUG && print STDERR "table 1 : ", $a->get_trimmed_text("/table"), "\n";
	$a->get_tag("table"); # $DEBUG && print STDERR "table 2 : ", $a->get_trimmed_text("/table"), "\n";
	$a->get_tag("table"); # $DEBUG && print STDERR "table 3 : ", $a->get_trimmed_text("/table"), "\n";
	$a->get_tag("table"); # $DEBUG && print STDERR "table 4 : ", $a->get_trimmed_text("/table"), "\n";

	while (1) {
	    
	    #$a->get_tag("tr"); #$DEBUG && print STDERR "headers col : ", $a->get_trimmed_text("/tr");
	    
	    $a->get_tag("td"); my $n = $a->get_trimmed_text("/td"); #$DEBUG && print STDERR "Name : ", $n, "\n"; 
	    last if ($n =~ /^\s{0,}$/);
	    
	    $a->get_tag("td"); #$DEBUG && print STDERR "close : ", $a->get_trimmed_text("/td"), "\n";
	    $a->get_tag("td"); $change = $a->get_trimmed_text("/td"); #$DEBUG && print STDERR "change : ", $d, "\n";
	    $a->get_tag("td"); #$DEBUG && print STDERR "change %age : ", $a->get_trimmed_text("/td"), "\n";
	    $a->get_tag("td"); #$DEBUG && print STDERR "p/e : ", $a->get_trimmed_text("/td"), "\n";
	    $a->get_tag("td"); $volume = $a->get_trimmed_text("/td"); #$DEBUG && print STDERR "Volume : ", $v, "\n";

	    if ($change =~ /^\+/) { 
		$flag = "ADVANCES";
	    } elsif ($change =~ /^\-/) {
		$flag = "DECLINES";
	    } else {
		$flag = "UNCHANGED";
	    }
	    
	    if ($volume =~ /K$/) { chop $volume; $volume *= 1000; }
	    if ($volume =~ /M$/) { chop $volume; $volume *= 1000000; }
	    if ($volume =~ /B$/) { chop $volume; $volume *= 1000000000; }
	    $DEBUG && print STDERR "volume is $volume\n";
	    
	    $hash{'volumes'} += $volume ;
	    if ($flag eq "ADVANCES") { $hash{'advances'}++ ; $hash{'advances_volumes'} += $volume; }
	    if ($flag eq "DECLINES") { $hash{'declines'}++ ; $hash{'declines_volumes'} += $volume; }
	    if ($flag eq "UNCHANGED") { $hash{'unchanged'}++ ; $hash{'unchanged_volumes'} += $volume; }
	    	    
	    my $c1 = $a->get_token(); #$DEBUG && print STDERR Dumper($c1);
	    my $c2 = $a->get_token(); #$DEBUG && print STDERR Dumper($c2);
	    my $c3 = $a->get_token(); #$DEBUG && print STDERR Dumper($c3);

	    $a->unget_token($c3); $a->unget_token($c2); $a->unget_token($c1);
	    
	    $a->get_tag("/tr"); 

	    my $token = $a->get_token; last if ($token->[1] eq "tbody");
	}
    }
}

# This subroutine takes and HTML::TokeParser tree object 
# of the page 1 of the index page and returns the number
# of pages that this index occupies :)
sub how_many_pages {
    #my ($section, $a) = @_;
    my ($a) = @_;
    
    #$DEBUG && print $STDERR "section is ", $section, "\n";
    #$DEBUG && print Dumper(\$section);
    
    my $skip_tables = 4;

    #if ($section =~ /world_markets/) { 	$skip_tables = 4; } 
    #else { $skip_tables = 5; }
    
    for (1 .. $skip_tables) { $a->get_tag("table"); }
    $a->get_tag("/table");
    
    $a->get_tag("/span");
    $a->get_tag("div");
    my $page_str = $a->get_trimmed_text("/div");
    $DEBUG && print STDERR "Page string is ", $page_str, "\n";
    $page_str =~ /(\d+)/;
    $DEBUG && print STDERR "No of pages is ", $1, "\n";
    return $1;
}
