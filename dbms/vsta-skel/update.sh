#!/bin/bash 

# 4 Jan 2011
#
# Top level update script
# This runs the update.sh scripts in other dirs

. ./configs.sh

export TOP_DBMS_DIR=$PWD
export DATADIR

COUNTRY_EXG=$1
bhavcopy=$2

cd $COUNTRY_EXG; ./update.sh $bhavcopy
cd -
