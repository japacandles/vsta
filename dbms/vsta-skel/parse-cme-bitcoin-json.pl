#!/usr/bin/perl

# Jan 28, 2018, Sunday
# Input: path to json file containing the day's bitcoin reference rate
# Output: :)
# Side-effect: appending of the bitcoin reference rate to the 
# bitcoin reference rate data file

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use JSON;

my $json_file = shift;
my ($json_text, $perl_scalar);

my $security = shift;

open(FILE, "< $json_file") or die $!;
while (<FILE>) { $json_text .= $_ ; }

#print $json_text, "\n";

$perl_scalar = decode_json($json_text);

#print Dumper($perl_scalar);

if ($security eq "bitcoin") {

    my ($d, $t) = split / /, $perl_scalar->{'referenceRate'}->{'date'};
    print join " ", $d, $perl_scalar->{'referenceRate'}->{'value'}, "\n";

} elsif ($security eq "ethereum") {

    my ($d, $t) = split / /, $perl_scalar->{'ethReferenceRates'}->[0]->{'date'};
    print join " ", $d, $perl_scalar->{'ethReferenceRates'}->[0]->{'value'}, "\n";

}
