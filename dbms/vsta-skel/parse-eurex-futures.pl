#!/usr/bin/perl

# wget http://www.eurexchange.com/exchange-en/products/idx/stx/blc/EURO-STOXX-50--Index-Futures/18954
# wget http://www.eurexchange.com/exchange-en/products/idx/stx/blc/18956!onlineStatsReload?productId=18956&productGroupId=846&busDate=20180612

use strict;
use diagnostics;
use warnings;
use FunctionsVstaHelper;
use FunctionsVstaEurex;
use Data::Dumper;

if ($#ARGV < 1) {
    print "Usage: \n";
}

my ($security, $date, $mode);
my ($file, $text, $delivery, $href);

$security = $ARGV[0];
$date = $ARGV[1];
#print $date, "\n";

my $content = eurex_url_content($security, $date);
if ($content ne "0") {
    $href = populate_eurex_data_hash($content);
}

#if (defined $href) {
#    print Dumper($href);
#}

for (keys %$href) {
    print join ",", $security . "-" . $_, $date, 
	$href->{$_}->{'open'}, $href->{$_}->{'high'},
	$href->{$_}->{'low'}, $href->{$_}->{'close'}, 
	$href->{$_}->{'volume'}, $href->{$_}->{'oi'}, "\n";
}
