#!/usr/bin/perl
########################################################################################################
#
# parse-commodityonline
# General info
# - to fetch data from commodityonline.com and create a bhavcopy
# - commodityonline.com provides information in the format
#   Market (State)   Variety    Arrivals in tonnes    Modal price    Unit of price    Min/Max Price
# - the timestamp is right on the page
# - any commodity which has Arrivals set to N.R (not recorded) is to be skipped
# Stages of operation
# - the initial page shows us where the commodities are traded across the country - we get 
#   the page 1 of garlic, page 1 of wheat etc. we store this first page 
# - each commodity's first page contains urls of further pages that contain that
#   commodity's data. we store all these urls too
# - we scrape each of these urls to extract the data and dump to stdout in bhavcopy format
# - bhavcopy format is
#   Commodity:Variety:Market:State    Min   Max   Modal   Arrival
#
# 
# Dec 15, 2018, Saturday 
# - first working version :)
#
########################################################################################################

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;
use FunctionsVstaHelper;

my $DEBUG;
if (defined $ENV{'DEBUG'}) { $DEBUG = 1; }

my $m = WWW::Mechanize->new();
$m->add_header("User-Agent" => random_ua_str());

my %linkhash;
my %todo_linkhash;
my %bhavlines;

get_initial_commodities_links($m, \%linkhash);
#$DEBUG && print STDERR "Link hash is ", Dumper(\%linkhash), "\n";

for my $link (keys %linkhash) {

    #$DEBUG && print STDERR "Now GETting $link \n";
    get_links("TOP", $m, $link, \%todo_linkhash, \%bhavlines);
    
    #$DEBUG && print STDERR "To do link hash is ", Dumper(\%todo_linkhash), "\n";
    #$DEBUG && print STDERR "bhavlines are ", Dumper(\@bhavlines), "\n";

}

for (keys %bhavlines) { print join " ", $_, $bhavlines{$_}, "\n"; }

# get_links
# this functions performs two tasks
# - it takes a top-level page from commodityonline,
#   calls get_data on it to read the data from it AND
#   creates a list of subsidary pages AND
#   calls get_data on each
#   OR
#   it takes a subsidary page from commodityonline
#   and calls get_data on it
sub get_links {

    my ($tag,$b, $u, $h1, $h2) = @_;

    $DEBUG && print STDERR "Now GETting $u \n";
    my $r = $b->get($u);
    die unless ($r->is_success);

    my $p = HTML::TokeParser->new(\$r->decoded_content);
    
    if ($tag eq "TOP") {

	# we populate a todo-hash with urls
	# that are available from the commodity's landing page.
	# these links point us to next pages that contain data
	# about the commodity
	for my $link ($b->links) {
	    if ($link->url =~ /all-state/) {
		unless (defined $h1->{ $link->url }) { 
		    $h1->{ $link->url } = "TODO";
		    #$DEBUG and print STDERR "Setting ", $link->url, " to TODO\n";
		}
	    }
	}

	# retrieve the data on this page
	get_data($r->decoded_content, $h2);
	
	# Now that we have the subsidary links, we loop through them
	# fetching data from each, as we go along
	foreach my $link (keys %$h1) { 
	    if ($h1->{$link} eq "TODO") {
		$h1->{ $link} = "DONE";
		$DEBUG and print STDERR "Setting ", $link, " to DONE\n";
		get_links("SUBSIDARY", $b, $link, $h1, $h2); 
	    } 

	    else { $DEBUG and print STDERR $link, " has already been looked at\n"; }
	}
	
    }

    # this isn't a "TOP" link
    else { get_data($r->decoded_content, $h2); }

}

# get_data
# parses html content from commodityonline to find the price/volume data in it
# inputs:
# - HTML decoded content
# - hashref: this hash will be populated with the data 
#   from the html content
# outputs:
# - None, the aref itself is modified :)
sub get_data {

    my ($re, $href) = @_;
    my ($t, $tmp, $date, $time, $commodity, $market, $state, $variety, $qty, $modal, $min, $max);
    
    my $q = HTML::TokeParser->new(\$re);
    while (my $t = $q->get_token) {

	#$DEBUG && print Dumper(\$t);
	last if ((defined $t->[2]) && ($t->[2] =~ /Not Reported/));
	
	# work begins with commodity's name
	if ((defined $t->[0]) and (defined $t->[1]) and (defined $t->[2]) and (ref ($t->[2]) eq 'HASH') 
	    and ($t->[0] =~ 'S') and ($t->[1] =~ /div/) 
	    and ($t->[2]->{'class'} =~ /col-lg-7 col-md-6 mds_tit/)) {
	    #$DEBUG && print STDERR Dumper($t);
	    
	    $q->get_tag("div"); #$DEBUG && print STDERR Dumper($t);
	    $commodity = clean_str( $q->get_trimmed_text("/div") ); #$DEBUG && print STDERR "Commodity is $commodity\n";
	}
	
	# last updated
	if ((defined $t->[0]) and (defined $t->[1]) and (defined $t->[2]) 
	    and ($t->[0] =~ 'S') and ($t->[1] =~ /div/) 
	    and ($t->[2]->{'class'} =~ /la_update-style as_padd/)) {
	    #$DEBUG && print STDERR Dumper($t);
	    
	    $q->get_tag("span"); #$DEBUG && print STDERR Dumper($t);
	    $time = clean_str( fix_date($q->get_trimmed_text("/span")) ); #$DEBUG && print STDERR "Time is $time\n";

	    while (1) {

		$t = $q->get_token;
		if ((defined $t->[0]) and (defined $t->[1]) and (defined $t->[2]) 
		    and ($t->[0] =~ 'S') and ($t->[1] =~ /div/) 
		    and ($t->[2]->{'class'} =~ /la_date-style as_padd/)) {
		    #$DEBUG && print STDERR Dumper($t);	
		    $time .= "-" . clean_str( $q->get_trimmed_text("/div") ); #$DEBUG && print STDERR "Time is $time\n";
		    last;
		}
	    }
	}

	# actual price and qty data
	if ((defined $t->[0]) and (defined $t->[1]) and (defined $t->[2]) 
	    and ($t->[0] =~ 'S') and ($t->[1] =~ /div/) 
	    and ($t->[2]->{'class'} =~ /dt_ta_11/)) {
	    
	    $q->get_tag("a"); $market = clean_str( $q->get_trimmed_text("font") ); #$DEBUG && print STDERR "Market is $market\n";
	    $state = clean_str( $q->get_trimmed_text("/font") ); #$DEBUG && print STDERR "State is $state\n";
	    $q->get_tag("div"); $variety = clean_str( $q->get_trimmed_text("/div") ); #$DEBUG && print STDERR "Variety is $variety\n";
	    
	    $q->get_tag("div"); $qty = clean_str( $q->get_trimmed_text("/div") ); 
	    if ($qty =~ /(.+)Tonnes/) { 
		$qty = $1; #$DEBUG && print STDERR "Qty is $qty\n";
		next if ($qty == 0); # no point in continuing this line if there has been no trade
	    }
	    elsif ($qty =~ /NR/) { next; } 
	    else { $qty = "FOO" };
	    
	    $q->get_tag("div"); $modal = clean_str( $q->get_trimmed_text("/div") ); #$DEBUG && print STDERR "Modal is $modal\n";
	    $q->get_tag("div");
	    $q->get_tag("div"); ($min, $max) = split "/",( $q->get_trimmed_text("/div") ); $min = clean_str($min); $max = clean_str($max); #$DEBUG && print STDERR "Min max is $min $max\n";

	    # create the bhavcopy line
	    # format -  Commodity:Variety:Market:State    Min   Max   Modal   Arrival
	    my $bhavline = join ":", $commodity, $variety, $market, $state;
	    #$bhavline = join " ", $bhavline, , $min, $max, $modal, $qty, $time; #$DEBUG && print STDERR "bhavline is $bhavline\n";
	    $href->{$bhavline} = join " ", $min, $max, $modal, $qty, $time; #$DEBUG && print STDERR "bhavline is $bhavline\n";
	    
	}
	
    }
    
}

# get_initial_commodities_links
# - inputs:
#   -- browser object
#   -- hash-ref
# - modifies
#   -- the hash-ref
#      the hash-ref has now links to each commodity's landing page
# - returns : NONE : everything is a side-effect :)
sub get_initial_commodities_links {

    my ($b, $href) = @_;

    my $u = 'https://www.commodityonline.com/mandiprices/' ;
    my $r = $b->get($u);
    die unless ($r->is_success);

    my $p = HTML::TokeParser->new(\$r->decoded_content);    

    while (my $t = $p->get_token) {

	#$DEBUG && print STDERR Dumper($t);
	
	if (($t->[0] =~ /S/) and ($t->[1] =~ /option/) and ($t->[2]->{'value'} =~/all-state/)) {
	    $href->{ $t->[2]->{'value'} } = 1;
    }

	last if (($t->[0] =~ /E/) and ($t->[1] =~ /select/));
    }
}

# clean_str
# takes a string and removes all spaces, ampersands, backslashes 
# and brackets from it.
# returns the cleaned string
# wonder if this belongs in VstaHelper
sub clean_str {

    my ($str) = @_;

    $str =~ s/\s{1,}//g; 
    $str =~ s!\(!-!g; 
    $str =~ s!\)!-!g;
    $str =~ s/\&/and/g;
    $str =~ s/ /--/g;
    $str =~ s!\/!!g;
    $str =~ s/-$//g;

    return $str;
}

# fix_date
# accepts a space delimited date in the format: 1 Jun 1984
# returns: 19840601
# I suspect this will need to be made more general
# * accept different delimiters
# * accept different date formats for example : June 1 1984
#   and therefore will need to be in VstaHelper
sub fix_date {

    my $date = shift;
    
    my %month_name_to_number = (
	
	'Jan' => '01', 'Feb' => '02', 'Mar' => '03', 
	'Apr' => '04', 'May' => '05', 'Jun' => '06',
	'Jul' => '07', 'Aug' => '08', 'Sep' => '09',	
	'Oct' => '10', 'Nov' => '11', 'Dec' => '12', 

	'JAN' => '01', 'FEB' => '02', 'MAR' => '03', 
	'APR' => '04', 'MAY' => '05', 'JUN' => '06',
	'JUL' => '07', 'AUG' => '08', 'SEP' => '09',	
	'OCT' => '10', 'NOV' => '11', 'DEC' => '12',

	'January' => '01', 'February' => '02', 'March' => '03', 
	'April' => '04'  , 'May' => '05'     , 'June' => '06',
	'July' => '07'   , 'August' => '08'  , 'September' => '09',	
	'October' => '10', 'November' => '11', 'December' => '12', 

	'JANUARY' => '01', 'FEBRUARY' => '02', 'MARCH' => '03', 
	'APRIL' => '04'  , 'MAY' => '05'     , 'JUNE' => '06',
	'JULY' => '07'   , 'AUGUST' => '08'  , 'SEPTEMBER' => '09',	
	'OCTOBER' => '10', 'NOVEMBER' => '11', 'DECEMBER' => '12',
	
	);

    my ($day, $month, $year) = split / /, $date;

    if (($day !~ /^0/) and ($day < 10)) { $day = "0" . $day; } # for the first 9 days of the month
    if ($year =~ /^\d\d$/) { $year = "20" . $year; } # if year is given as YY instead of YYYY, prefix it with '20'
    
    return $year . $month_name_to_number{$month} . $day;
}
