#!/usr/bin/perl

# parse-gold-silver.pl
# Dec 22, 2018, Sat
# This prog was committed in Feb 2017 and I didn't
# annotate it then. So I have to do my best to
# bring the documentation up to date now :)
#
# This is to parse the historical lbma spot prices and create a
# data file out of those
#
# Changelog:
#
# Dec 22, 2018
# use random user-agent
# Feb 2017
# initial commit
use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use HTML::TokeParser;
use LWP::UserAgent;
use FunctionsVstaHelper;

binmode STDOUT, ':utf8';

my $u = LWP::UserAgent->new;
#$u->agent('Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0');
$u->agent(random_ua_str());

my $r = $u->get('http://goldsilver.com/historical-london-fix/');

#print $r->decoded_content;

my $p = HTML::TokeParser->new(\$r->decoded_content) or die $!;

$p->get_tag("table");
$p->get_tag("tr"); $p->get_tag("tr");

$p->get_tag("td");
my $date = $p->get_trimmed_text("/td");
#print $date, "\n";

$p->get_tag("td");
my $gold_am = $p->get_trimmed_text("/td");

$p->get_tag("td");
my $gold_pm = $p->get_trimmed_text("/td");

$p->get_tag("td");
my $silver = $p->get_trimmed_text("/td");

my $bhavs = join " ", $date, $gold_am, $gold_pm, $silver, "\n";

$bhavs =~ s/\$//g; # there are actual $ signs :) we need to remove 'em
$bhavs =~ s/,//g;  # also commas that need to be removed
$bhavs =~ s/ /,/g; # lastly we embed commas back, for csv processing to work :)

print $bhavs;
