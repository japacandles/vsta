#!/usr/bin/perl

# This program is for creating a data file from the data available
# from kitco.com
# 
# The process is 
#
# 1. get the 2016 data  
#    wget http://www.kitco.com/londonfix/gold.londonfix16.html?sitetype=fullsite
#    mv gold.londonfix16.html?sitetype=fullsite gold.londonfix16.html
# 2. run this script "thusly",
#    perl parse-kitco-history-html.pl gold.londonfix16.html | sed 's/ /-/g' | tac | tee -a /tmp/foo
#    The tac is because kitco publishes the latest prices highest on the page
# 3. The kitco data fropm 1999 onwards is usable in this way
use strict;
use warnings;
use diagnostics;
use HTML::TokeParser;
use Data::Dumper;

binmode STDOUT, ':utf8';

my $f = shift;
open (my $fh, "<:utf8", $f) or die $!;

my $p = HTML::TokeParser->new($fh) or die $!;

for ( 1 .. 15 ) { $p->get_tag("table"); }
for ( 1 .. 3 ) { $p->get_tag("tr"); }

while (1) {

    #print "looping again\n";
    
    $p->get_tag("td");
    my $d = $p->get_trimmed_text("/td");
    next if ($d =~ /^$/); # skip blank dates
    next if ($d =~ /^\s+$/); # skip dates that contain only spaces
    $d =~ s/,//g;
    #last unless ($d =~ /^\d+/); # skip dates that start with non-numeric values 
    #print "D is $d\n";
    #my $date = make_date($d);
    my $date = $d;
    
    $p->get_tag("td");
    my $gold_am = $p->get_trimmed_text("/td");
    #last if ($gold_am =~ /^\s{0,}$/);
    #print join " ", $date, $gold_am, "\n";
    #next if ($gold_am !~ /\d+/);
    
    $p->get_tag("td"); # skip the Gold PM
    my $gold_pm = $p->get_trimmed_text("/td");
    #last if ($gold_pm =~ /^\s{0,}$/);
    #print join " ", $date, $gold_pm, "\n";
    #next if ($gold_pm !~ /\d+/);
    
    $p->get_tag("td");
    my $silver = $p->get_trimmed_text("/td");
    
    $p->get_tag("td");
    my $platinum_am = $p->get_trimmed_text("/td");
    
    $p->get_tag("td"); # skip the Platinum PM
    my $platinum_pm = $p->get_trimmed_text("/td");
    
    $p->get_tag("td");
    my $palladium_am = $p->get_trimmed_text("/td");
    
    $p->get_tag("td");
    my $palladium_pm = $p->get_trimmed_text("/td");
    
    $p->get_tag("/tr");
    
    print join ",", 
	$date, 
	$gold_am,
	$gold_pm,
	$silver, 
	$platinum_am,
	$platinum_pm, 
	$palladium_am,
	$palladium_pm, 
	"\n";
	
    my $t1 = $p->get_token();
    #print Dumper($t1);
    
    my $t2 = $p->get_token();
    #print Dumper($t2);
    
    if ($date !~ /^\d/) { # before 2000, kitco used month names like "January", "February" etc
	                  # after 2000, kitco started using month numbers like "01", "02" etc
	
	my $t3 = $p->get_token();
	#print Dumper($t3);
    
	my $t4 = $p->get_token();
	#print Dumper($t4);
    
	my $t5 = $p->get_token();
	#print Dumper($t5);

	if ($t5->[0] eq 'T'){
	    #print "exiting at t5\n";
	    last;	
	} else {
	    $p->unget_token($t5, $t4, $t3, $t2, $t1);
	    next;
	}
    }
  
    elsif ($t2->[1] eq "table") {
	#print "exiting at t2\n";
	last;
    }
    
    $p->unget_token($t2, $t1);
    
    #next ; # looping back 
}
