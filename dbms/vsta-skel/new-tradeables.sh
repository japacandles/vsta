#!/bin/bash

# new-tradeables
# Identifies new candidates for inclusion in the
# list of tradeables
# The outout should be grepped

. $VSTA_DIR/configs.sh
EXISTING_TRADEABLE_FILE=/var/tmp/tradeable-futures

for f in $(find $VSTA_DATA_ARCHIVE_DIR -type f | grep futures ); do
    lines=$(wc -l $f | awk '{print $1}')
    if [ $lines -gt 100 ]; then
        #echo Looking for $f
        #echo grep "$f" $EXISTING_TRADEABLE_FILE
        grep "$f" $EXISTING_TRADEABLE_FILE 2>&1 > /dev/null
        if [ $? -ne 0 ]; then
            echo $f 
        fi
    fi  
done
