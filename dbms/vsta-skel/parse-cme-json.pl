#!/usr/bin/perl

# Apr 08, 2018, Sunday
# Input: path to json file containing the day's json bhavs from cme
# Output:
# CSV bhavcopy on stdout
# 

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use JSON;

my $date = shift;
my $json_file = shift;
my ($json_text, $perl_scalar);

open(FILE, "< $json_file") or die $!;
while (<FILE>) { $json_text .= $_ ; }

#print $json_text, "\n";

$perl_scalar = decode_json($json_text);

for (@{$perl_scalar->{'quotes'}}) {

    next if $_->{'open'} =~ /-/;

    print join ",",  
	decomma(deFuture(replace_space_with_hyphen($_->{'productName'}))) . replace_space_with_hyphen($_->{'expirationMonth'}), $date,
	$_->{'open'}, $_->{'high'}, $_->{'low'}, $_->{'last'}, 
	decomma($_->{'volume'}), replace_space_with_hyphen($_->{'updated'}),"\n";
}

sub decomma {
    my ($str) = @_;
    $str =~ s/,//g;
    return $str;
}

sub replace_space_with_hyphen {
    my ($str) = @_;
    $str =~ s/ /-/g;
    return $str;
}

sub deFuture {
    my ($str) = @_;
    $str =~ s/Futures//g;
    return $str;
}
