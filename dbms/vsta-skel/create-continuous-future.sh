#!/bin/bash 

# This script attempts too create a continuous future file 
# It takes a file that contains a list of futures' files
# I have decided that the last 5 days of the future should not be traded
# but the position should be rolled over.

#set -x

fileOfFiles=$1
fileCount=$(wc -l $fileOfFiles | awk '{print $1}')

#echo Number of files $fileCount
filesToHead=$(($fileCount - 1))
#echo to process $filesToHead

lastFile=$(tail -n 1 $fileOfFiles)
#echo last file is $lastFile

# the first file is to included in toto
file=$(head -n 1 $fileOfFiles)
#echo $file
cat $file

# whats the last date ?
lastLine=$(tail -n 1 $file)
lastDate=$(echo $lastLine | awk '{print $1}')

for file in $(tail -n $filesToHead $fileOfFiles)
#for file in $(cat $fileOfFiles)
do 

	echo $file | grep -q "\#"
	if [ $? -eq 0 ]
	then 
		continue
	fi

	if [ $file == $lastFile ]
	then
		continue
	fi

	a=$(wc -l $file)
	lineCount=$(echo $a | awk '{print $1}')

	# include all lines until the final five
	lineCountHead=$(($lineCount - 5))

	line0=$(grep -n $lastDate $file | awk -F: '{print $1}')
	lineCountTail=$(($lineCountHead - $line0))

	#echo $file $lastDate $lineCount $lineCountHead $lineCountTail
	#read
	head -n $lineCountHead $file | tail -n $lineCountTail

	lastDate=$(head -n $lineCountHead $file | tail -n 1 | awk '{print $1}')
done

# we are at the last file
a=$(wc -l $file)
lineCount=$(echo $a | awk '{print $1}')
line0=$(grep -n $lastDate $file | awk -F: '{print $1}')
lineCountTail=$(($lineCount - $line0))
#echo $file $lastDate $lineCount $lineCountTail
tail -n $lineCountTail $file
