#!/usr/bin/perl

# parse-kitco-daily-gold-silver.pl parses kitco to obtain today's gold,
# silver, platinum and palladium fix prices from kitco.
# In the case of gold and platinum, the script needs to be called
# with _am or _pm
# 
# Inputs:
# a. name of the datafile to parse
# b. name of the commodity
#
# Output
# stdout : <date> <price>
# stderr : whatever

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use HTML::TokeParser;
use utf8;

my $file = shift;
my $commodity = shift;

my %hash;

my $str;
open(FILE, "< $file") or die $!;
while (<FILE>) { $str .= $_ ; }
utf8::decode($str);

my $p = HTML::TokeParser->new(\$str);

my $token;
for (1 .. 14) { $token = $p->get_tag("table"); }

$p->get_tag("tr"); 
$p->get_tag("td"); $hash{'date'} = $p->get_trimmed_text("/td");

for (1 .. 3) { $p->get_tag("tr"); }

$p->get_tag("td"); if ($commodity eq "gold_am") { $hash{$commodity} = $p->get_trimmed_text("/td"); }
$p->get_tag("td"); if ($commodity eq "gold_pm") { $hash{$commodity} = $p->get_trimmed_text("/td"); }
$p->get_tag("td"); if ($commodity eq "silver") { $hash{$commodity} = $p->get_trimmed_text("/td"); }

$p->get_tag("td"); #$hash{'date'} = $p->get_trimmed_text("/td");

$p->get_tag("td"); if ($commodity eq "platinum_am") { $hash{$commodity} = $p->get_trimmed_text("/td"); }
$p->get_tag("td"); if ($commodity eq "platinum_pm") { $hash{$commodity} = $p->get_trimmed_text("/td"); }
$p->get_tag("td"); if ($commodity eq "palladium_am") { $hash{$commodity} = $p->get_trimmed_text("/td"); }
$p->get_tag("td"); if ($commodity eq "palladium_pm") { $hash{$commodity} = $p->get_trimmed_text("/td"); }

#print Dumper(\%hash);
print join " ", $hash{$commodity}, $hash{'date'}, "\n";
