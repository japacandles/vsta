#!/bin/bash -x

source ../../configs.sh

#echo data dir is $DATADIR

# the first argument is the bhavcopy
bhavcopy=$1

#datadir=$DATADIR/us/cme/futures/commodities
datadir=$DATADIR/us/ice/futures/

# We get the date from the name of the bhavcopy
# From "/path/to/vsta/data/us/cme/bhavcopies/raw/TBond-2019-12-26" 
# we get "2019-12-26"
b=$(dirname $bhavcopy)
c=$(basename $bhavcopy)
nf=$(echo "$c" | awk -F '-' '{print NF}')
dayF=$nf ; dayStr="{print $"$dayF"}"; echo $dayStr ; day=$(echo "$c" | awk -F'-' "$dayStr" )
monthF=$(($nf - 1)); echo $monthF; monthStr='{print $'$monthF'}'; echo $monthStr; month=$(echo "$c" | awk -F'-' "$monthStr" ); echo $month
yearF=$(($nf - 2)); echo $yearF; yearStr='{print $'$yearF'}'; echo $yearStr; year=$(echo "$c" | awk -F'-' "$yearStr" ); echo $year
d=$year$month$day
echo Date is $d; #read

# We expect the lines of the bhavcopy to be like
# FUTIDX,Cotton-2-Jul-21-CTN21,,0,XX,70.00,70.00,69.86,69.86,69.86,6,12/26/19,

for line in $(cat $bhavcopy); do

    echo $line ;#    read

    echo "$line" | grep "FUTIDX"
    if [ $? -eq 0 ]; then
	file=$(echo $line | awk -F, '{print $2}' )

	#d=$(echo $line | awk -F, '{print $2}' )
	#dt=$(echo $d | sed 's/-//g')
	dt=$d
	o=$(echo $line | awk -F, '{print $6}' )
	h=$(echo $line | awk -F, '{print $7}' )
	l=$(echo $line | awk -F, '{print $8}' )
	c=$(echo $line | awk -F, '{print $9}' )
	v=$(echo $line | awk -F, '{print $11}' )
	t=$(echo $line | awk -F, '{print $12}' )
	
	outfile=$datadir/$file
	echo outfile is $outfile
	echo $dt $o $h $l $c $v $t | tee -a $outfile
    fi
    #read
    
done
