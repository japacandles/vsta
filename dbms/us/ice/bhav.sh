#!/bin/bash -x

# Dec 12, 2019, Thu
# Usage:
# cd /path/to/vsta/dbms/vsta-skel ; ./bhav.sh ../us/ice FOO
# where FOO is one of Coffee, Cocoa, Cotton, Sugar ...

. ./vars.sh


# the first argument is the security
security=$1
d=$(TZ=America/New_York date +%Y-%m-%d)

# Generic directory stuff
this_dir=$PWD
datadir="$VSTA_DATA_DIR/us/ice/bhavcopies/"
echo $datadir
cd $TOP_DBMS_DIR

#
# If no security is specified, get all bhavcopies
#
if [ "$security" == "" ]; then

    for commodity in ${commodities[@]}; do
        commodity=$(echo $commodity | sed 's/_/ /g') # the sed here is to convert Orange_Juice to "Orange Juice"
        echo Commodity is $commodity
        OUT=$datadir/$(echo $commodity | sed 's/ //g')-$d # the sed here will convert "Orange Juice" to OrangeJuice 
        rm -vf $OUT

        #echo -n "$d " | tee -a $OUT
        perl parse-barchart-futures.pl "$commodity" | tee -a $OUT # we send out for "Coffee" or "Orange Juice" ...
    done

else # if security is specified, get only that security's bhavcopy

    commodity=$(echo $security | sed 's/_/ /g') # the sed here is to convert Orange_Juice to "Orange Juice"
    echo Commodity is $commodity
    OUT=$datadir/$(echo $commodity | sed 's/ //g')-$d # the sed here will convert "Orange Juice" to OrangeJuice 
    rm -vf $OUT

    #echo -n "$d " | tee -a $OUT
    perl parse-barchart-futures.pl "$commodity" | tee -a $OUT # we send out for "Coffee" or "Orange Juice" ...

fi
cd $this_dir

