#!/bin/bash

#
# Before running this script, the pre-process is
# cat ~/tmp/dji-2013-10-15 ~/tmp/nasdaq-2013-10-15 ~/tmp/s\&p-2013-10-15 \
#	| grep -v 'INSTRUMENT\|Cash\|VIX\|Midcap\|\/' \
#	| tee ~/vsta/data/us/cme/bhavcopies/2013-10-15

bhavcopy=$1
d=$2
#date=$(basename $bhavcopy)
#d=$(echo $date | sed 's/-//g')

subDir=$(basename $(dirname $bhavcopy))
echo str is $subDir

#outDir=$DATADIR/us/cme/futures/$subDir
outDir=$DATADIR/us/cme/futures/

for record in $(cat $bhavcopy)
do
	outFile=$(echo $record | awk -F, '{ print $2 }')
	open=$(echo $record | awk -F, '{ print $6 }')
	high=$(echo $record | awk -F, '{ print $7 }')
	low=$(echo $record | awk -F, '{ print $8 }')
	close=$(echo $record | awk -F, '{ print $9 }')
	volume=$(echo $record | awk -F, '{ print $11 }')
	timestamp=$(echo $record | awk -F, '{ print $12 }')

	echo "Appending to " $outDir/$outFile
	echo $d $open $high $low $close $volume $timestamp | tee -a $outDir/$outFile

done
