#!/usr/bin/perl

# barchart-to-cme-bhavcopy.pl
# Converts a barchart bhavcopy to a cmegroup bhavcopy like so ...
# perl /var/tmp/barchart-to-cme-bhavcopy.pl ~/vsta/data/us/cme/bhavcopies/raw/JPYUSD-2019-09-26 | tee -a /tmp/JPYSD-2019-09-26
#
# Takes the barchart bhavcopy file as the argument and writes the cmegroup bhavcopy on stdout
# Warning: Look at the generated bhavcopy before you update !

use strict;
use diagnostics;
use warnings;
use Data::Dumper;


my $f = $ARGV[0];

# the name of the file ends in YYYY-MM-DD
$f =~ /(\d{4})-(\d{2})-(\d{2})$/;
my $d = $1 . "-" . $2 . "-" . $3;
#print $d, "\n";

open FILE, "< $f" or die $!;
while (<FILE>) {

    my @fields = split /,/;

    # the first field is "FUTIDX" in homage to nse :)
    # the second field is the name like Gold-Dec-19-GCZ19
    # this needs to become Gold-DEC-2019
    $fields[1] =~ /(\w+)-(\w+)-(\d+)-(\w+)$/;
    #print join " ", $1, $2, $3, $4, "\n";
    my $m = uc($2);
    my $y = "20" . $3;
    my $security = $1 . "-" . $m . "-" . $y;
    #print $security, "\n";

    # fields no 3, 4 and 5 are also to be ignored
    print join ',', $security, $d, $fields[6], $fields[7], $fields[8], $fields[9], $fields[10], $fields[11], "\n";
    
}
