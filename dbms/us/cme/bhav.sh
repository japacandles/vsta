#!/bin/bash -x

# Apr 08, 2018, Sun 03:40 AM
# CME now sends out jsons
# Usage:
# cd /path/to/vsta/dbms/vsta-skel ; ./bhav.sh ../us/cme FOO
# where FOO is one of GOLD, DJIA, SP500, NASD100

# the first argument is the security
security=$1

# This is for CME, so the date has to be what it is in Chicago :)
d=$(TZ=America/Chicago date +%Y-%m-%d)
echo Date is $d

# Generic directory stuff
this_dir=$PWD
datadir=`echo $this_dir | sed 's/dbms/data/'`"/bhavcopies/raw/"
echo $datadir
#cd $TOP_DBMS_DIR
#. $TOP_DBMS_DIR/common-functions.sh

# which then leads to the location to which the
# bhavcopy is to be written
OUT=$datadir/$security-$d
rm -vf $OUT

# we need to create a tmpfile to
# which wget --wait=$MAXDELAY --random-wait will dump
a=$(mktemp)
tmpfile="$a.json"
rm -vf $a

#UA=$(user_agent)
UA=$BROWSER

echo Browser CME is "$UA"
#read

# maximum delay between requests
MAXDELAY=60

reqNo=0 # request no; odd numbered requests go to /dev/null and even numbered requests are jsons to be parsed

# and so, finally, we get to the point :)
case "$security" in

    # Indices
    
    # S&P 500 futures
    # S&P 500, mini S&P 500
    # 5 PM New York
    "SP500") for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/132/G?quoteCodes=&_=" \
		       "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/133/G?quoteCodes=null&_"
	     do
	 	 rm -rvf $tmpfile
	 	 wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	 perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	     done
	     sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT
	     ;;

    # Dow Jones Ind Avg
    # mini-Dow
    # 5 PM 
    "DJIA" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/318/G?quoteCodes=null&_=" 
	     do
		 rm -rvf $tmpfile
	 	 wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	 perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	     done
	     sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT
	     ;;

    # Nasdaq 100 futures
    # mini-nasdaq-100
    # 5 PM New York
    "NASD100" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/146/G?quoteCodes=&_=" 
		do
		    rm -rvf $tmpfile
	 	    wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	    perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
		done
		sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT	
		;;
    
    
    # Energies
    
    # WTI crude oil future
    # mini-crude, crude
    # 4 PM Chicago
    "WTICRUDE" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/425/G?pageSize=50&_=" 
		 do
	 	     rm -rvf $tmpfile
	 	     wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
                     perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
		 done
		 ;;

     "NATGAS" ) for i in  "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/444/G?pageSize=50&_=" 
		do
		    rm -rvf $tmpfile
	 	    wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	    perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
		done
		;;
    
    # Financials
    
    # T-Bond and Ultra T-bond
    # 4 PM Chicago
    "TBond" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/307/G" \
			   "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/3141/G"
              do
		  rm -rvf $tmpfile
	 	  wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	  perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	      done
	      sed -i "s/'/./g" $OUT
	      ;;

    # 30 day Fed funds
    # 4 PM Chicago ?
    "30DFF" ) for i in "https://www.cmegroup.com//CmeWS/mvc/Quotes/Future/305/G"
	      do
		  rm -rvf $tmpfile
	 	  wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	  perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	      done
	      sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT; sed -i "s/'/./g" $OUT
	      ;;

    # Eurodollar future
    # 4 PM Chicago
    "EURODOLLAR" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/1/G"
		   do
                       rm -rvf $tmpfile
	 	       wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	       perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
		   done
		   ;;

    # 2 year treasury note future
    # 4 PM Chicago
    "2YTN" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/303/G"
	     do
	         rm -rvf $tmpfile
	 	 wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	 perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	     done
	     sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT; sed -i "s/'/./g" $OUT
	     ;;
    
    # 5 year treasury note future
    # 4 PM Chicago
    "5YTN" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/329/G"
	     do
	         rm -rvf $tmpfile
	 	 wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	 perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	     done
	     sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT; sed -i "s/'/./g" $OUT
	     ;;

    # 10 year treasury note future
    # 4 PM Chicago
    "10YTN" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/316/G"
	      do
	          rm -rvf $tmpfile
	 	  wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	  perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	      done
	      sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT; sed -i "s/'/./g" $OUT
	      ;;
    
    # SOFR Interest rate futures
    # 4 PM Chicago
    "SOFR" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/8462/G" \
			  "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/8463/G"
	     do
		 rm -rvf $tmpfile
	 	 wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	 perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	     done
	     sed -i "s/'/./g" $OUT
	     ;;


    # Currencies

    # crypto currency reference rates from CME
    # 4 PM GMT
    "CRYPTOREF" ) cd $VSTADIR/dbms/vsta-skel ; 
		  PERL5LIB=$PERL5LIB sh get-bitcoin-ref.sh
		  #cd -
		  ;;
    
    # bitcoin future
    # 4 PM Chicago
    "BITCOIN" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/8478/G?quoteCodes=null&_=" \
                         "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/9024/G" \
                         "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/8995/G"
		do
		    rm -rvf $tmpfile
	 	    wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	    perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile INTERNATIONAL | tee -a $OUT
		done
		sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT	
		;;
    
    # GBP/USD future
    # 4 PM Chicago
    "GBPUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/42/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # CAD/USD future
    # 4 PM Chicago
    "CADUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/48/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # JPY/USD future
    # 4 PM Chicago
    "JPYUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/69/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # CHF/USD future
    # 4 PM Chicago
    "CHFUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/86/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # EUR/USD future
    # 4 PM Chicago
    "EURUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/58/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
                   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # AUD/USD future
    # 4 PM Chicago
    "AUDUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/37/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # MXN/USD future
    # 4 PM Chicago
    "MXNUSD" ) for i in  "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/75/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
                   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # NZD/USD future
    # 4 PM Chicago
    "NZDUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/78/G"
	       do
	           rm -rvf $tmpfile
                   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # ZAR/USD future
    # 4 PM Chicago
    "ZARUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/129/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # BRL/USD future
    # 4 PM Chicago
    "BRLUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/40/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # RUB/USD future
    # 4 PM Chicago
    "RUBUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/83/G"
	       do
	           rm -rvf $tmpfile
                   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;

    # INR/USD future
    # 4 PM Chicago
    "INRUSD" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/6897/G"
	       do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       ;;


    
    # Meats

    # Live Cattle future
    # 1:05 PM Chicago
    "Livestock" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/22/G" \
			       "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/34/G" \
			       "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/19/G"	
	          do
	              rm -rvf $tmpfile
	 	      wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	      perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	          done
	       ;;

    # Class III future
    # 4 PM Chicago
    "Milk" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/27/G"
	     do
	           rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
                   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	     done
	     ;;

    # The Softs are better handled through ICE

    # Metals

    # Gold futures
    # Gold, mini-Gold, micro-Gold and kilo-Gold
    # 4 PM Chicago
    "Gold" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/437/G?quoteCodes=&_=" 
	     do
		 rm -rvf $tmpfile
	 	 wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	 perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	     done
	     sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT
	     ;;

    # Silver futures
    # Silver, Silver-1000-oz
    # 4 PM Chicago
    "Silver" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/458/G"
	       do
		   rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	     sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT; sed -i "s/'/./g" $OUT; 
	     ;;

    # Copper futures
    # Copper, Copper-mini
    # 4 PM Chicago
    "Copper" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/438/G"
	       do
		   rm -rvf $tmpfile
	 	   wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	   perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	       done
	       sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT
	     ;;

    # Palladium futures
    # 4 PM Chicago
    "Palladium" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/445/G"
	          do
		      rm -rvf $tmpfile
	 	      wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	      perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT	 
	          done
	          sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT
	     ;;

    # Platinum futures
    # 4 PM Chicago
    "Platinum" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/446/G"
	         do
		     rm -rvf $tmpfile
	 	     wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
                     perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT	 
	         done
	         sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT
	     ;;

    # Grains : Corn, Wheat, Soybeans
    # 1:20 PM Chicago
    "Grain" ) for i in "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/300/G" \
		     "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/323/G" \
		     "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/348/G" \
		     "https://www.cmegroup.com/CmeWS/mvc/Quotes/Future/320/G" 
	    do
		rm -rvf $tmpfile
	 	wget --wait=$MAXDELAY --random-wait -U "$UA" $i -O $tmpfile
	 	perl $TOP_DBMS_DIR/parse-cme-json.pl $d $tmpfile DEFAULT | tee -a $OUT
	      done
	      sed -i 's/\&//g' $OUT; sed -i 's/\$//g' $OUT; sed -i 's/Futures-Quotes---//g' $OUT; sed -i 's/Futures---//g' $OUT; sed -i 's/CME-Group-//g' $OUT;	sed -i "s/'/./g" $OUT; 
	;;
    
		    
    
esac

