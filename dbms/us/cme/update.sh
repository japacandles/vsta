#!/bin/bash -x

source ../../configs.sh

#echo data dir is $DATADIR

# the first argument is the bhavcopy
bhavcopy=$1

#datadir=$DATADIR/us/cme/futures/commodities
datadir=$DATADIR/us/cme/futures/

# We expect the lines of the bhavcopy to be like
# Gold-JUL-2017,2017-04-28,1267.9,1270.5,1267.3,1270.2,44,16:37:37-CT-28-Apr-2017,

# ~/vsta/data/us/cme/futures/commodities/

for line in $(cat $bhavcopy)
do

	echo $line 
	#read

	file=$(echo $line | awk -F, '{print $1}' )

	d=$(echo $line | awk -F, '{print $2}' )
	dt=$(echo $d | sed 's/-//g')

	o=$(echo $line | awk -F, '{print $3}' )
	h=$(echo $line | awk -F, '{print $4}' )
	l=$(echo $line | awk -F, '{print $5}' )
	c=$(echo $line | awk -F, '{print $6}' )
	v=$(echo $line | awk -F, '{print $7}' )
	t=$(echo $line | awk -F, '{print $8}' )

	outfile=$datadir/$file
	echo outfile is $outfile
	echo $dt $o $h $l $c $v $t | tee -a $outfile

	#read

done
