#!/bin/bash

#
# 28 Nov 2013
# masterdata.com has stopped providing breadth data
# moving s&p500 to money.cnn.com
#
# 4 Jan 2010
# Simply gets and appends the dax data
# using bloomberg-index.pl and bb.sh
# OR
# yf3.pl and yf.sh

this_dir=$PWD
D=$(TZ=America/New_York date +%Y%m%d)
datadir=`echo $this_dir | sed 's/dbms/data/'`"/futures"
datafile=$datadir"/dji"
echo updating $datafile with Dow Jones
cd $TOP_DBMS_DIR
#./bb.sh America/New_York /tmp indu:ind | tee -a $datafile
echo -n $D" " | tee -a $datafile
#perl parse-reuters.pl dji | tee -a $datafile
perl parse-money-cnn.pl markets dow | tee -a $datafile

datafile=$datadir"/sp500"
echo updating $datafile with S\&P 500
cd $TOP_DBMS_DIR
#./yf.sh America/New_York /tmp ^gspc | tee -a $datafile
#./mstd.sh America/New_York '$SPX.htm' | tee -a $datafile
echo -n $D" " | tee -a $datafile
perl parse-money-cnn.pl markets sandp | tee -a $datafile
