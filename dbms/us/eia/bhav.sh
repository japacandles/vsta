#!/bin/bash -x

# Sep 8, 2018, Sunday
# Extremely rudimentary bhav script to get continuous 
# futures of Crude oil and Gold from quandl.
# to be called as
# . $HOME/vsta/dbms/vsta-skel/configs.sh ; cd DBMSDIR=$VSTADIR/dbms/vsta-skel ; ./bhav.sh ../misc/quandl

# read local conf to get the api key and the urls
. quandl.conf
#echo $KEY $URL

this_dir=$(pwd)
datadir=`echo $this_dir | sed 's/dbms/data/'`"/futures"
#ls -l $datadir

d1=$(TZ=America/Chicago date -d '2 weeks ago' +%Y-%m-%d)
d2=$(TZ=America/Chicago date +%Y-%m-%d)
for c in PET_RWTC_D 
do
	datafile=$datadir/wti-crude
	#echo $datafile
	curl -v --ipv4 -s "$URL/$c.csv?start_date=$d1\&end_date=$d2\&api_key=$KEY" | sed 's/,/ /g' | grep -v Date | tac | tee -a $datafile
	sort -u $datafile -o $datafile
done

