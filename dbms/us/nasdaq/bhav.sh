#!/bin/bash

# nasdaq-100.sh 
# Sep 22, 2015, Tuesday
# For Nasdaq 100

set -x

DATE=$(TZ="America/New_York" date +%Y%m%d)
datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/nasdaq-100"

cd $TOP_DBMS_DIR

# first get the OHLC data
echo -n "$DATE " | tee -a $datafile

/usr/bin/perl parse-money-cnn.pl symb NDX | tee -a $datafile
