#!/usr/bin/perl -w

#
# nse-data.pl
# June 20, 2008 Friday
# tidied up majorly so that I can hand over the script to Ramky :)
#

use strict;
use diagnostics;
use HTML::TokeParser;
use LWP::UserAgent;
use IO::File;
use Data::Dumper;

# Define a hash-table to store the data
my %data = (
	'Open' => 0,
	'High' => 0,
	'Low' => 0,
	'Last' => 0,
	'Prev. Close' => 0,
	'% Change ' => 0,
	'P/E *' => 0,
	'P/B *' => 0,
	'Div. yield *' => 0,
	'Shares Traded' => 0,
	'Turnover (Rs. lakhs)' => 0,
	'52 Week High' => 0,
	'52 Week Low' => 0,
	'All-time High' => 0,
	'All-time Low ' => 0,
	'Advances' => 0,
	'Declines' => 0,
	'Unchanged' => 0,
);

my $fh;

#
# Get the indexwatch page
#
my $niftyURL = 'http://www.nseindia.com/marketinfo/indices/indexwatch_nifty.jsp';
my $u = LWP::UserAgent->new();
my $r = $u->get($niftyURL);

if ($r->is_success) {

	# The following is an ugly hack
	# HTML::TokeParser expects a filehandle
	# So I create a temp file, write the info to it
	# and rewind
	$fh = IO::File->new_tmpfile;
	die "Cannot create temporary file $!" unless (defined $fh);
	print $fh $r->content;
	$fh->seek(0, 0);

# Our get wasn't a success :( so we bail out
} else { 
	print STDERR $r->status_line, "\n"; 
	print "Are you sure the URL is correct ?\n";
	exit 1;
}

# So now we read the tmp file
my $p = HTML::TokeParser->new($fh);

# All the data is stored in tabular form
# So we parse the tables reading each td
while (my $t = $p->get_tag("td")) {
	my $text = $p->get_text("/td");

	# This is another ugly hack
	# I want to get the DateTime field to be output first
	# The trick is to create the key at the very last 
	# possible moment
	if ($text =~ /As on /) { $data{'DateTime'} = $text; }

	# the page provides information about the 5 biggest gainers/losers
	# One day, I'll add a separate data structure to accomodate that
	# data too. For the moment, I am just skipping it all
	if ($text =~ /Symbol/) { $p->get_tag("/table"); }

	# self-explanatory :)
	if (exists($data{$text} )) {
		$p->get_tag("td");
		$data{$text} = $p->get_trimmed_text("/td");
	}
}

$fh->close;

#for (keys %data) {
#	print $_, "\t", $data{$_}, "\n";
#}

#print Dumper(\%data);

# Bhavcopy line
print "FUTIDX,NIFTY,SPOT,0,XX,",
	$data{'Open'}, ",",
	$data{'High'}, ",",
	$data{'Low'}, ",",
	$data{'Last'}, ",",
	$data{'Last'}, ",",
	$data{'Shares Traded'}, ",",
	$data{'Turnover (Rs. lakhs)'}, ",",
	$data{'Advances'}, ",",
	"0,0,0", "\n";

exit 0;
