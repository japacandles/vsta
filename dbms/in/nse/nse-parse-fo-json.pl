#!/usr/bin/perl -w

#
# nse-parse-fo-json.pl
#
# Date: Not sure
# The script written on Jun 16 used to parse each nse fo url twice
# Fixed in this version
#
# Date: Jun 16, 2012, Sat
# The bhavcopy is available at approx 5:30 PM but the data can be 
# scraped any time after 4 PM
#
use strict;
use diagnostics;
use WWW::Mechanize;
use JSON;

my $m = WWW::Mechanize->new();
# These headers are needed, else I get a 403 forbidden error
$m->add_header("User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");
$m->add_header("If-None-Match" => "W/\"3958-4fa24947\"");
$m->add_header("Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
$m->add_header("Accept-Language" => "en-us,en;q=0.5");
$m->add_header("Accept-Encoding" => "gzip, deflate");
$m->add_header("Connection" => "keep-alive");
#$m->add_header("If-Modified-Since" => "Thu, 03 May 2012 09:00:55 GMT");

my $main_nse_url = 'http://www.nseindia.com';
my $nse_deriv_stockwatch = 'http://nseindia.com/live_market/dynaContent/live_watch/derivative_stock_watch.htm?cat=GI';

my @nse_derivative_group_urls;
my %nse_derivative_urls;

# This page give us the urls where the derivatives are grouped
# Fow example : which url has links to NIFTY Futures' urls
$m->get($nse_deriv_stockwatch);
if ($m->success) {
    my @lines = split /\n/, $m->content();

    for (@lines) { 
	next unless ( /<option value=\"(.+)\"/ );
	#print STDERR $1, "\n";
	my $url = $1;
	$url = $main_nse_url . $url;
	if ($url =~ /FUTIDX/ ) {
	    #print STDERR "main Getting $url\n";
	    push @nse_derivative_group_urls, $url;
	}
    }

# Since we couldn't even get to nseindia.com, we exit with error
} else {
    print STDERR "Couldn't get $nse_deriv_stockwatch\n";
    exit(1);
}

# Now that we have the urls in groups, we need to create a list
# of links, one for each individual derivative 
for my $nse_derivative_group_url ( @nse_derivative_group_urls ) {

    $m->get($nse_derivative_group_url);
    if ($m->success) {
	#print STDERR "Links are\n";
	for ($m->links) {
	    next if ($_->url =~ /DerivatStyle.css/);
	    next if ($_->url =~ /#/);
	    #print  STDERR "Found link ", $main_nse_url . $_->url, "\n";
	    #push @nse_derivative_urls,  $main_nse_url . $_->url, "\n";
	    $nse_derivative_urls{ $main_nse_url . $_->url }++ ;
	}
    }
    sleep 1;
}

#print join "\n", @nse_derivative_urls, "\n";

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,",
      "CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,", "\n";

# Now to get the data from the individual urls
foreach (sort keys %nse_derivative_urls) {
    #print STDERR "2. Getting ", $_, "\n";
    get_nse_data($m, $_);
    sleep 1
}

sub get_nse_data {

    my ($n, $url)= @_; 

    #print "Getting $url\n";
    $n->get($url);
    if ($n->success) {
        #print $n->content, "\n";
        my $p = HTML::TokeParser->new(\$n->content);

        $p->get_tag("script"); $p->get_tag("div");
        my $json = $p->get_text("/div");
        #print "JSON text is\n", $json, "\n";

        my $json_str;

        my @lines = split /\n/, $json;

        for (@lines) {
            #print STDERR "Line is ",  $_, "\n";
            chomp;
            next if ($_ =~ /^\s+$/); # skip blank lines
            next if ($_ =~ /glb/); # skip any lines with string glb in it
            $json_str .= $_; 
        }   

        #print "JSON text is\n", $json_str, "\n";

        my $hashref = decode_json $json_str;
        #print Dumper $hashref, "\n";
        if ($hashref->{'data'}->[0]->{'openPrice'} =~ /-/) { return; };  

        # Print in bhavcopy format
        #INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,
        
	$hashref->{'data'}->[0]->{'expiryDate'} =~ /(..)(...)(....)/;
	my $expiry = $1 . "-" . $2 . "-" . $3;
        print join ",", "FUTIDX", $hashref->{'data'}->[0]->{'underlying'},
                        $expiry,
                        "0,XX",
                        decomma ($hashref->{'data'}->[0]->{'openPrice'}),
                        decomma ($hashref->{'data'}->[0]->{'highPrice'}),
                        decomma ($hashref->{'data'}->[0]->{'lowPrice'}),
                        decomma ($hashref->{'data'}->[0]->{'lastPrice'}),
                        decomma ($hashref->{'data'}->[0]->{'closePrice'}),
                        decomma ($hashref->{'data'}->[0]->{'numberOfContractsTraded'}),
                        decomma ($hashref->{'data'}->[0]->{'turnoverinRsLakhs'}),
                        decomma ($hashref->{'data'}->[0]->{'openInterest'}),
                        decomma ($hashref->{'data'}->[0]->{'changeinOpenInterest'}),
                        decomma ($hashref->{'lastUpdateTime'}), "\n";
    }

}

sub decomma {
    my ($str) = @_;
    $str =~ s/,//g;
    return $str;
}
