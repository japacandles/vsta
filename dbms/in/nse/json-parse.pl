#!/usr/bin/perl

######################################################
# Adapted from
# http://ifanda.cz/clanky/perl-a-cgi/program-json2xml
######################################################
 
use strict;
use diagnostics;
use Data::Dumper; # stringified perl data structures
use JSON::XS;     # JSON serialising/deserialising

# available options of this program
my $n=0;  # not to generate XML header
my $r=''; # name of the root element to wrap result
my $s=0;  # string transform
my $i=0;  # integer transform
my $l=0;  # literals transform
my $e=0;  # root array recovery

my %hash;
 
#my $infile = ''; my $outfile = ''; # input and output filenames
 
# file input
my $jshash; # declare json hash reference

my $infile = shift;
 
local $/=undef; # unset $/ for whole file read at once
 
if ($infile) { # reading from file
    open JSFILE, "<", $infile or die $infile.': problem with file!';
    $jshash = JSON::XS->new->utf8->decode(<JSFILE>); # json parsing
    close JSFILE;
}
else { # reading from STDIN
  $jshash = JSON::XS->new->utf8->decode(<STDIN>); # json parsing
}
# explicit check of input JSON and options
die("Invalid JSON input\n") if (ref $jshash eq 'ARRAY' and !($r and $e));

#print Dumper (\$jshash);
#print Dumper ($jshash->{'latestData'});
#print Dumper ($jshash->{'data'});

$hash{'timeStamp'} = decomma( $jshash->{'time'} );

$hash{'indexName'} = $jshash->{'latestData'}->[0]->{'indexName'};
$hash{'indexName'} =~ s/\s{1,}/-/g;

$hash{'open'}  = decomma( $jshash->{'latestData'}->[0]->{'open'} );
$hash{'high'}  = decomma( $jshash->{'latestData'}->[0]->{'high'} );
$hash{'low'}   = decomma( $jshash->{'latestData'}->[0]->{'low'} );
$hash{'close'} = decomma( $jshash->{'latestData'}->[0]->{'ltp'} ); 


my @ary = @{$jshash->{'data'}} ;
for (@ary) {
	#print "dump is ",  Dumper $_;
	#print join " ", $_->{'symbol'}, $_->{'per'}, "\n";
	$hash{'volume'} += decomma( $_->{'trdVol'} ) * 100000;
	$hash{'value'} += decomma( $_->{'ntP'} ) * 100 ;

	if ( $_->{'per'} > 0 ) {
		$hash{'advances'}++;
		$hash{'advances_volume'} += decomma( $_->{'trdVol'}) * 100000;
		$hash{'advances_value'} += decomma( $_->{'ntP'} ) * 100;
	} elsif ( $_->{'per'} < 0 ) {
		$hash{'declines'}++;
		$hash{'declines_volume'} += decomma( $_->{'trdVol'} ) * 100000;
		$hash{'declines_value'} += decomma( $_->{'ntP'} ) * 100;
	} else {
		$hash{'unchanged'}++;
		$hash{'unchanged_volume'} += decomma( $_->{'trdVol'} ) * 100000;
		$hash{'unchanged_value'} += decomma( $_->{'ntP'} ) * 100;
	}
}
		
#print Dumper(\%hash);

# dump in bhavcopy format
#INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,
#FUTIDX,NIFTY,SPOT,0,XX,5297.05,5333.65,5288.85,5327.30,5327.30,118915000,470971,32,79781000,360378,18,39134000,110599,

print join ",", "FUTIDX", 
	$hash{'indexName'}, "SPOT", "0,XX",
	defined($hash{'open'}) ? $hash{'open'} : 0,
	defined($hash{'high'}) ? $hash{'high'} : 0,
	defined($hash{'low'}) ? $hash{'low'} : 0,
	defined($hash{'close'}) ? $hash{'close'} : 0,
	defined($hash{'close'}) ? $hash{'close'} : 0,
	defined($hash{'volume'}) ? $hash{'volume'} : 0,
	defined($hash{'value'}) ? $hash{'value'} : 0,
	defined($hash{'advances'}) ? $hash{'advances'} : 0,
	defined($hash{'advances_volume'}) ? $hash{'advances_volume'} : 0,
	defined($hash{'advances_value'}) ? $hash{'advances_value'} : 0,
	defined($hash{'declines'}) ? $hash{'declines'} : 0,
	defined($hash{'declines_volume'}) ? $hash{'declines_volume'} : 0,
	defined($hash{'declines_value'}) ? $hash{'declines_value'} : 0,
	defined($hash{'unchanged'}) ? $hash{'unchanged'} : 0,
	defined($hash{'unchanged_volume'}) ? $hash{'unchanged_volume'} : 0,
	defined($hash{'unchanged_value'}) ? $hash{'unchanged_value'} : 0,
	defined($hash{'timeStamp'}) ? $hash{'timeStamp'} : "Unknown TimeStamp",
	"\n";


sub decomma {
	my ($str) = @_;
	$str =~ s/,//g;
	return $str;
}
