#!/bin/bash

# Sun Oct 14, 2013
# Rewriting the nse bhav.sh as part of a major code clean-up

bhavDate=$(TZ="Asia/Kolkata" date +%d-%m-%Y)
bhavcopyFile=$DATADIR/in/nse/bhavcopies/$bhavDate
echo $bhavcopyFile

spotHeader="INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,"

#echo $DATADIR

echo $spotHeader | tee $bhavcopyFile
#(perl $TOP_DBMS_DIR/parse-nse-spot-2013-10-14.pl 2>&1) | tee -a $bhavcopyFile
(perl $TOP_DBMS_DIR/parse-nse.pl spot 2>&1) | tee -a $bhavcopyFile

#(perl $TOP_DBMS_DIR/parse-nse-deriv-2017-06-04.pl 2>&1) | tee -a $bhavcopyFile
(perl $TOP_DBMS_DIR/parse-nse.pl futures 2>&1) | tee -a $bhavcopyFile
