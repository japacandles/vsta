#!/usr/bin/perl -w

use strict;
use diagnostics;

my $bhavcopy = shift; # from which file do I update

# Create the date field
my $date1 = `basename $bhavcopy`;
$date1 =~ /(\d\d)-(\d\d)-(\d\d\d\d)/;
my $date = $3 . $2 . $1;
#print $date1, " ", $date,"\n";
my $datadir = "/tmp/vsta/india/";

open BHAV, "< $bhavcopy" or die "Cannot open bhavcopy. $!";

while ($_ = <BHAV>) {

	next unless (/^FUTIDX/); # reject nonsense
	print $_, "\n";
	my ($crap1, 			# FUTIDX
		$future,		# Future Name
		$expdate,		# Expiry date
		$crap2,			# Duh ! usually 0
		$crap3,			# Duh ! usually XX
		$open,
		$high,
		$low,
		$close,
		$crap4,			# Settlement price
		$volume,
		$value,
		$oi) = split /,/, $_ ;

	my $future_file = $datadir . $future . '-' . $expdate;
	print "future file is ", $future_file, "\n";

	open FUT, ">> $future_file" or die "Cannot open Future file $future_file. $!";
	print $future_file, "\n";
	print FUT join " ", $date, $open, $high, $low, $close, 
		$volume, $value, $oi;
	print FUT "\n";
	#print join " ", $date, $open, $high, $low, $close, 
	#	$volume, $value, $oi;
	#print "\n";
	close FUT;
}
