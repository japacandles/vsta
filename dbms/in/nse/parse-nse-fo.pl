#!/usr/bin/perl -w

#
# 2 Aug 2011
# Fixed all "undefined" vars
#
# 30 Jul 2010, 9:30 AM
# Trivial bugfix on line 28
# Needed to check if $_->text was defined
# 
# 30 Apr 2010; 0:55 AM
# This program scrapes the pages of the individual futidx pages
# so that the data is available at 4:00 PM IST
# Now there is no reason to get a bhavcopy
#

use strict;
use diagnostics;
use LWP::Simple;
use WWW::Mechanize;
use HTML::TokeParser;
use Data::Dumper;

my $key; my %hash ;
my @url_ary = ('http://nseindia.com/content/fo/MostActiveContractsFUTIDX.htm', 
	'http://nseindia.com/content/fo/MostActiveContractsFUTIDXMININIFTY.htm');

my $m = WWW::Mechanize->new();

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,CONTRACTS,VAL_INLAKH,OPEN_INT,CHG_IN_OI,TIMESTAMP,\n";
for (@url_ary) {
	$m->get(  $_ );
	for ( $m->links ) {
		#print Dumper($_);
		if (defined($_->text) && $_->text =~ /FUTIDX/) {
			my $url = $_->url; 

			%hash = ();

			# rather clever turn here. thanks live-http-headers :)
			$url =~ s/foquote\.jsp/foquote\_tab\_ajax\.jsp/;
			#$url = 'http://www.nseindia.com' . $url;
			$url = 'http://nseindia.com' . $url;
			#print $url, "\n";
			$m->get( $url );	

			my $p = HTML::TokeParser->new(\$m->content);
	
			# the 4th table contains data about the futidx itself
			$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table"); 
		
			$p->get_tag("tr"); $p->get_tag("tr");
			my $instr_type = $p->get_trimmed_text("/td"); #print $instr_type, "\n";
			$instr_type = "" unless (defined $instr_type);
		
			$p->get_tag("td");
			my $underlying = $p->get_trimmed_text("/td"); 
			$underlying = "" unless (defined $underlying);
			unless ($underlying =~ /^$/) {
				$underlying =~ s/\W//g;
				$underlying =~ s/IMG//g;
			}
			#print $underlying, "\n";
		
			$p->get_tag("td");
			my $expiry_dt = $p->get_trimmed_text("/td");
			$expiry_dt = "" unless (defined $expiry_dt);
			unless ($expiry_dt =~ /^$/) {
				$expiry_dt =~ /(\d\d)(\w\w\w)(\d\d\d\d)/;
				$expiry_dt = $1 . "-" . ucfirst(lc($2)) . "-" . $3;
			}
			#print $expiry_dt, "\n";

			$p->get_tag("td"); $p->get_tag("td"); $p->get_tag("td");
			my $lot_size = $p->get_trimmed_text("/td"); #print $lot_size, "\n";
	
			# the actual data is in the 7th table
			$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("tr");

			for ( 1 .. 15 ) {
				$p->get_tag("tr");
				$p->get_tag("th"); $key = $p->get_trimmed_text("/th"); #print $key, "\t => ";
				$p->get_tag("td"); $hash{$key} = $p->get_trimmed_text("/td"); #print $hash{$key}, "\n";
				$p->get_tag("/tr");
			}
			#print Dumper(\%hash);
			my $line = join ",", 
				$instr_type, $underlying, $expiry_dt, '0,XX', 
				defined $hash{'Open Price'} ? $hash{'Open Price'} : "",
				defined $hash{'High Price'} ? $hash{'High Price'} : "",
				defined $hash{'Low Price'} ?  $hash{'Low Price'} : "",
				defined $hash{'Last Price'} ? $hash{'Last Price'} : "",
				defined $hash{'Close Price'} ? $hash{'Close Price'} : "",
				defined $hash{'Number of contracts traded'} ? $hash{'Number of contracts traded'} : "",
				defined $hash{'Turnover in Rs. Lakhs'} ? $hash{'Turnover in Rs. Lakhs'} : "",
				defined $hash{'Open Interest'} ? $hash{'Open Interest'} : "",
				defined $hash{'Change in Open Interest'} ? $hash{'Change in Open Interest'} : "",
				"\n";
			print $line;
		}
	}
}	

