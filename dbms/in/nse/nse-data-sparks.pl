#!/usr/bin/perl -w

#
# nse-data-sparks.pl
#
# Nov 27, 2010, Sunday
#
# 1. nseindia.com has inserted one more table before table listing
# the individual stocks. This new table contains A/D/U figures.
# Even so, thats just the absolute numbers, not including Adv-volumes,
# Decl-volumes etc.
# 
# 2. Reformatted in emacs :)
#
# March 6, 2010, Saturday
#
# Needed to evolve out of nse-data.pl
# because nseindia.com created niftysparks.htm
#

use strict;
use diagnostics;
use HTML::TokeParser;
use LWP::UserAgent;
use IO::File;
use Data::Dumper;

# Define a hash-table to store the data
my %data = (
    'Open' => 0,
    'High' => 0,
    'Low' => 0,
    'Last' => 0,
    'Shares Traded' => 0,
    'Turnover (Rs. lakhs)' => 0,
    'Advances' => 0,
    'Advances-volumes' => 0,
    'Advances-turnover' => 0,
    'Declines' => 0,
    'Declines-volumes' => 0,
    'Declines-turnover' => 0,
    );

my $fh;
my $multiplier;
my $counter;
my $tmp;
my $sign;

#
# Get the indexwatch page
#
my $niftyURL = 'http://nseindia.com/content/equities/niftysparks.htm';
my $u = LWP::UserAgent->new();
my $r = $u->get($niftyURL);

if ($r->is_success) {
    
    # The following is an ugly hack
    # HTML::TokeParser expects a filehandle
    # So I create a temp file, write the info to it
    # and rewind
    $fh = IO::File->new_tmpfile;
    die "Cannot create temporary file $!" unless (defined $fh);
    print $fh $r->content;
    $fh->seek(0, 0);
    
# Our get wasn't a success :( so we bail out
} else { 
    print STDERR $r->status_line, "\n"; 
    print "Are you sure the URL is correct ?\n";
    exit 1;
}

# So now we read the tmp file
my $p = HTML::TokeParser->new($fh);

# The first NIFTY listing is in the second table, second row
$p->get_tag("table"); $p->get_tag("table"); 
$p->get_tag("table"); # This the table added on Nov 27.
$p->get_tag("tr"); $p->get_tag("tr");

# OHLC is columns 4-7. So we skip the first 3 cols
$p->get_tag("td"); $p->get_tag("td"); $p->get_tag("td"); 

for ("Open", "High", "Low", "Last") {
    $p->get_tag("td");
    $data{$_} = $p->get_trimmed_text("/td");
    $data{$_} =~ s/\,//g; # get the commas out
}

# Volume info is in cols 10 and 11 and so we skip to it
$p->get_tag("td"); $p->get_tag("td");
for ('Shares Traded', 'Turnover (Rs. lakhs)') {
    $p->get_tag("td");
	if ($_ =~ /Shares Traded/) { $multiplier = 100000; }
    else { $multiplier = 100 ; }
    $data{$_} = $p->get_trimmed_text("/td");
    $data{$_} =~ s/\,//g; # get the commas out
    
    # too many <div> tags
    $data{$_} =~ /(\d+\.\d+)/;
    $data{$_} = $1;
    $data{$_} *= $multiplier;
}

# Having got OHLCV, why do we still parse the rest of the page ?
# Because we want to get the A/D numbers.
# And the good thing about the sparks page is that we see turnover 
# and volumes on the same page

# The individual scrip data start in the third table, second row onwards
$p->get_tag("table"); $p->get_tag("tr");

# The data is in rows 2 .. 51
for $counter (2 .. 51) {
    
    # The change from last trading day is in col 8
    $p->get_tag("td"); $p->get_tag("td"); $p->get_tag("td");
    $p->get_tag("td"); $p->get_tag("td"); $p->get_tag("td");
    $p->get_tag("td"); $p->get_tag("td");
    
    # Advance or decline
    $tmp = $p->get_trimmed_text("/td");
    if ($tmp > 0) { $sign = 1; }
    else { $sign = -1 }
    
    if ($sign == 1) { $data{'Advances'}++; }
    else { $data{'Declines'}++; }
    
    # Skip one more col
    $p->get_tag("td");
    
    for ('Shares Traded', 'Turnover (Rs. lakhs)') {
	$p->get_tag("td");
	
	if ($_ =~ /Shares Traded/) { $multiplier = 100000; }
	else { $multiplier = 100 ; } 
	
	$tmp = 0;
	$tmp = $p->get_trimmed_text("/td");
	$tmp =~ s/\,//g; # get the commas out
	
	# too many <div> tags
	$tmp =~ /(\d+\.\d+)/;
	$tmp = $1;
	$tmp *= $multiplier;
	
	if ($sign > 0) {
	    if ($_ =~ /Shares Traded/) { $data{'Advances-volumes'} += $tmp; }
	    else { $data{'Advances-turnover'} += $tmp; }
	} else {
	    if ($_ =~ /Shares Traded/) { $data{'Declines-volumes'} += $tmp; }
	    else { $data{'Declines-turnover'} += $tmp; }
	}
    }
    $p->get_tag("/tr");
}

#for (keys %data) {
#	print $_, "\t", $data{$_}, "\n";
#}

#print Dumper(\%data);

# Bhavcopy line
print "FUTIDX,NIFTY,SPOT,0,XX,",
    $data{'Open'}, ",",
    $data{'High'}, ",",
    $data{'Low'}, ",",
    $data{'Last'}, ",",
    $data{'Last'}, ",",
    $data{'Shares Traded'}, ",",
    $data{'Turnover (Rs. lakhs)'}, ",",
    $data{'Advances'}, ",",
    $data{'Advances-volumes'}, ",",
    $data{'Advances-turnover'}, ",",
    $data{'Declines'}, ",",
    $data{'Declines-volumes'}, ",",
    $data{'Declines-turnover'}, ",",
    "\n";

exit 0;
