#!/usr/bin/perl

#
# Changelog:
# Feb 10, 2014, 19:58 PM, Monday
# bseindia.com has added a </tbody> where I expected a </table>
# fixed parser for that

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my $bse_url = 'http://beta.bseindia.com/markets/Derivatives/DeriReports/Derimarketwatch.aspx?expandable=0';

my $url;
my %hash;

my $m = WWW::Mechanize->new();
# I add these headers because I randomly get "denied" errors without them
$m->add_header("User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");
$m->add_header("Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
$m->add_header("Accept-Language" => "en-us,en;q=0.5");
$m->add_header("Accept-Encoding" => "gzip, deflate");
$m->add_header("Connection" => "keep-alive");

$m->get($bse_url);
unless ($m->success) {
    print STDERR "Problems getting $bse_url", "\n";
    exit (1);
}

my $p = HTML::TokeParser->new(\$m->content);

#print Dumper(\$p);

$p->get_tag("table"); $p->get_tag("table"); $p->get_tag("table");
$p->get_tag("table"); $p->get_tag("table"); 

$p->get_tag("/tr");

while (1) {

    $p->get_tag("td"); my $name = $p->get_trimmed_text("/td"); #print "Name is ", $name, "\n";
    $p->get_tag("td"); my $code = $p->get_trimmed_text("/td"); #print "Code is ", $code, "\n";
    $p->get_tag("td"); my $type = $p->get_trimmed_text("/td"); #print "Type is ", $type, "\n";
    if ($type =~ /^\s{0,}IF\s{0,}$/) {

	# name	
	#$name = $name . "-" . $code . "-" . $type ;
	$name = $name . "-" . $code ;
	#print STDERR $name, "\n";
	
	# expiry
	$p->get_tag("td"); my $expiry = $p->get_trimmed_text("/td");
	$expiry =~ s/\s+/-/g;
	$hash{$name}{'expiry'} = $expiry;

	# strike price
	$p->get_tag("td"); $p->get_tag("/td");

	# traded qty
	$p->get_tag("td"); $hash{$name}{'tradedQty'} = $p->get_trimmed_text("/td");

	# open
	$p->get_tag("td"); $hash{$name}{'open'} = $p->get_trimmed_text("/td");

	# high
	$p->get_tag("td"); $hash{$name}{'high'} = $p->get_trimmed_text("/td");

	# low
	$p->get_tag("td"); $hash{$name}{'low'} = $p->get_trimmed_text("/td");

	# close
	$p->get_tag("td"); $hash{$name}{'close'} = $p->get_trimmed_text("/td");

	# net change
	$p->get_tag("td"); $p->get_tag("/td");

	# volume
	$p->get_tag("td"); $hash{$name}{'volume'} = $p->get_trimmed_text("/td");

	# value
	$p->get_tag("td"); $hash{$name}{'value'} = $p->get_trimmed_text("/td");

	# net change
	$p->get_tag("td"); $p->get_tag("/td");

	# open interest
	$p->get_tag("td"); $hash{$name}{'openInterest'} = $p->get_trimmed_text("/td");

    	#print Dumper(\%hash), "\n";
    }

    $p->get_tag("/tr"); 
    my $token1 = $p->get_token(); #print Dumper(\$token1); 
    my $token2 = $p->get_token(); #print Dumper(\$token2); 
    #last if (defined $token2 and $token2->[2] =~ /<\/table>/);
    last if (defined $token2 and $token2->[2] =~ /<\/tbody>/);
    $p->unget_token($token1); $p->unget_token($token1);
    #print Dumper($token);
    #print Dumper(\%hash), "\n";


}

#print STDERR Dumper(\%hash);

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN," , 
    "HIGH,LOW,CLOSE,SETTLE_PR,VOL_INCrs,VAL_INCrs,", "OPEN_INTEREST," ,"\n";

for (keys %hash) {
    print join ",", "FUTIDX", $_, $hash{$_}{'expiry'}, "0,XX",
                    $hash{$_}{'open'}, $hash{$_}{'high'}, $hash{$_}{'low'}, 
                    $hash{$_}{'close'}, $hash{$_}{'close'}, $hash{$_}{'volume'}, 
                    $hash{$_}{'value'}, $hash{$_}{'openInterest'}, "\n";
}
