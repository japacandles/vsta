#!/bin/bash

#
# CHANGELOG
#
# 19 May 2013
# bseindia.com changed their spot index pages
# needed to rewrite the scraper and also 
# needed to point this script to the new scraper
#
# 20 Dec 2012
# This is an addition of an old set of scripts.
# Now there are two scripts.
# One gets the quotes for the spot indexes.
# The other gets the quotes for the index's futures.
#
# 3 Jan 2011
# Modularized application
# So this particular script only creates
# the bhavcopy of bse sensex
#
# 30 Apr 2010
# Wrote parse-nse-fo.pl which can be called at 4:00 PM IST 
# rather than 6:00 PM because it scrapes the individual 
# FUTIDX pages
#
# 9 Mar 2010
# nseindia switched to the nifty-sparks page
# nse-data-sparks will replace nse-data
#
# 1 Dec 2009
# nseindia.com suddenly started keepingf only zipped 
# copies of the F&O bhavcopies
#

DIR=$PWD
DATADIR=`echo $DIR | sed 's/dbms/data/'`
DATADIR=$DATADIR"/bhavcopies"
#/usr/bin/perl $DIR/nse-data.pl
#/usr/bin/perl $DIR/bse-data.pl

YEAR=`date "+%Y"`
#YEAR=2010
MONTH=`date "+%b" | tr a-z A-Z`
#MONTH="APR"
DAY=`date "+%d"`
#DAY="16"
month=`date "+%m"`

echo Getting Sensex
echo Writing to $DATADIR/$DAY-$month-$YEAR 
/usr/bin/perl $DIR/parse-bse-indexes-spot-2013-10-23.pl | tee -a $DATADIR/$DAY-$month-$YEAR
/usr/bin/perl $DIR/parse-bse-indexes-futures.pl | tee -a $DATADIR/$DAY-$month-$YEAR
RETVAL=$?
#cat $DATADIR/$DAY-$month-$YEAR
exit $RETVAL
