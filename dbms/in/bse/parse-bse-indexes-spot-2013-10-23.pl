#!/usr/bin/perl

#
# parse-bse-spot-2013-10-23.pl
# bseindia.com has changed their page layout again
# on 23rd Oct 2013
#
# Changelog:
#
# Sep 1, 2014: BSE has changed the url and the format
#              In addition, the site returns an internal server 
#              error if queries are run one after the other
#              Hence a delay of 10s between queries
#
# Oct 23, 2013: Initial commit

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my $bse_base_url = 'http://www.bseindia.com/indices/' ;
#my $bse_indexwatch = $bse_base_url . 'IndexHighlight.aspx?expandable=2';
my $bse_indexwatch = 'http://www.bseindia.com/dropdowns/markets_ind.htm';
my $bse_release_url = 'http://www.bseindia.com/mktlive/';

my $url;
my %hash;

my $m = WWW::Mechanize->new();
# I add these headers because I randomly get "denied" errors without them
$m->add_header("User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");
$m->add_header("Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
$m->add_header("Accept-Language" => "en-us,en;q=0.5");
$m->add_header("Accept-Encoding" => "gzip, deflate");
$m->add_header("Connection" => "keep-alive");

$m->get($bse_indexwatch);
#print $m->content();
die unless($m->success);

for ($m->links()) {
    if ( $_->url =~ /indexview_new.aspx/ ) {
	#$url = $bse_base_url . $_->url;
	$url = $_->url;
	next if ($url =~ /iname=DOL/);
	$url =~ s/beta/www/g;
	print STDERR "Url is ", $url, "\n";
	parse_spot_index_page($_->text, $url, $m, \%hash);
	#sleep 10;
    }
}

#print Dumper(\%hash), "\n";

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP," , 
    "OPEN,HIGH,LOW,CLOSE,SETTLE_PR,VOL_INCrs,VAL_INCrs," ,
    "NUM_TRADES,ADV,ADV_VOL_INCrs,ADV_VAL_INCrs,DECL,DECL_VOL_INCrs," ,
    "DECL_VAL_INCrs,UNC,UNC_VOL_INCrs,UNC_VAL_INCrs", "\n";

for (keys %hash) {
    print join ",",
          "FUTIDX", $_,"SPOT","0,XX",
          $hash{$_}{'open'}, 
          $hash{$_}{'high'}, 
          $hash{$_}{'low'}, 
          $hash{$_}{'close'},
          $hash{$_}{'close'},
          $hash{$_}{'volume'},
          $hash{$_}{'value'},
          $hash{$_}{'noOfTrades'},
          $hash{$_}{'advances'},
          $hash{$_}{'advances_volumes'},
          $hash{$_}{'advances_value'},
          $hash{$_}{'declines'},
          $hash{$_}{'declines_volumes'},
          $hash{$_}{'declines_value'},
          $hash{$_}{'unchanged'},
          $hash{$_}{'unchanged_volumes'},
          $hash{$_}{'unchanged_value'},
          $hash{$_}{'timestamp'},
          "\n";
} 

################
# Subroutines
################

#
# parse_spot_index
# will create one element of a hash of hashes
# takes the name of the index, its url, the mech and the HoH
# as its args
sub parse_spot_index_page {

    my ($key, $url, $m, $hashref) = @_;

    $key =~ s/\&/-/g;
    $key =~ s/\s+/-/g;
    print STDERR $key, " ", $url, "\n";

    # Debug
    #return unless ($key =~ /^SENSEX$/);

    $m->get($url);
    unless ($m->success) {
	print STDERR "Problem getting $url\n";
	return;
    }

    # parse the HTML tree
    my $p = HTML::TokeParser->new(\$m->content());

    $p->get_tag("/table");
    $p->get_tag("/script"); $p->get_tag("/script");

    for (1 .. 10) { $p->get_tag("div"); }

    # index name
    my $security = $p->get_trimmed_text("/div");
    $security =~ s/Index Reach - S\&P //g;
    $security =~ s/ \&//g;
    $security =~ s/ /-/g;

    # last traded timestamp
    $p->get_tag("div");
    my $timestamp = $p->get_trimmed_text("/div");
    $timestamp =~ s/ \|//g;

    $hash{$security}{'timestamp'} = $timestamp;

    # The fourth table from here contains the basic
    # ohlcv,Val,numTrades info
    for (1 .. 4) { $p->get_tag("table"); }
    $p->get_tag("tr"); $p->get_tag("tr"); $p->get_tag("tr");

    $p->get_tag("td");
    $hash{$security}{'open'} = $p->get_trimmed_text("/td");

    $p->get_tag("td");
    $hash{$security}{'high'} = $p->get_trimmed_text("/td");

    $p->get_tag("td");
    $hash{$security}{'low'} = $p->get_trimmed_text("/td");

    $p->get_tag("td");
    $hash{$security}{'close'} = $p->get_trimmed_text("/td");

    $p->get_tag("td");
    $hash{$security}{'volume'} = $p->get_trimmed_text("/td");
    $hash{$security}{'volume'} *= 10000;

    $p->get_tag("td");
    $hash{$security}{'value'} = $p->get_trimmed_text("/td");
    $hash{$security}{'value'} *= 100;

    $p->get_tag("td");
    $hash{$security}{'noOfTrades'} = $p->get_trimmed_text("/td");

    # gracefully leave this table
    $p->get_tag("/table");

    # We now proceed to look for breadth data

    $p->get_tag("table");
    $p->get_tag("tr"); $p->get_tag("tr");

    while (1) {
	$p->get_tag("td");
	my $breadthComponent = $p->get_trimmed_text("/td"); # advances/declines/unchanged
	#print $breadthComponent, "\n";

	last if ($breadthComponent =~ /TOTAL/);

	$breadthComponent = lc($breadthComponent);

	$p->get_tag("td");
	$hash{$security}{ $breadthComponent } = $p->get_trimmed_text("/td");
	#print $breadthComponent, "\n";

	$p->get_tag("td");
	$hash{$security}{ $breadthComponent . '_volumes' } = $p->get_trimmed_text("/td");
	$hash{$security}{ $breadthComponent . '_volumes' } *= 10000;

	$p->get_tag("td");
	$hash{$security}{ $breadthComponent . '_value' } = $p->get_trimmed_text("/td");
	$hash{$security}{ $breadthComponent . '_value' } *= 100;

	$p->get_tag("/tr");
	my $token = $p->get_token();
	#print Dumper(\$token);
	last if ( ($token->[0] =~ /E/) and ($token->[1] =~ /table/) );
	$p->unget_token($token);
    }
}
