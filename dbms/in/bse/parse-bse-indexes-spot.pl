#!/usr/bin/perl

use strict;
use diagnostics;
use Data::Dumper;
use WWW::Mechanize;
use HTML::TokeParser;

my $bse_base_url = 'http://beta.bseindia.com/indices/' ;
my $bse_indexwatch = $bse_base_url . 'Indexwatch.aspx?expandable=0';
my $bse_release_url = 'http://www.bseindia.com/mktlive/';

my $url;
my %hash;

my $m = WWW::Mechanize->new();
# I add these headers because I randomly get "denied" errors without them
$m->add_header("User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:10.0.4) Gecko/20120421 Firefox/10.0.4");
$m->add_header("Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
$m->add_header("Accept-Language" => "en-us,en;q=0.5");
$m->add_header("Accept-Encoding" => "gzip, deflate");
$m->add_header("Connection" => "keep-alive");

$m->get($bse_indexwatch);
die unless($m->success);

for ($m->links()) {
    if ( $_->url =~ /indiceswatch.aspx/ ) {
	$url = $bse_base_url . $_->url;
	#$url = $bse_release_url . $_->url;
	parse_spot_index_page($_->text, $url, $m, \%hash);
    }
}

#print Dumper(\%hash), "\n";

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP," , 
    "OPEN,HIGH,LOW,CLOSE,SETTLE_PR,VOL_INCrs,VAL_INCrs," ,
    "NUM_TRADES,ADV,ADV_VOL_INCrs,ADV_VAL_INCrs,DECL,DECL_VOL_INCrs," ,
    "DECL_VAL_INCrs,UNC,UNC_VOL_INCrs,UNC_VAL_INCrs", "\n";

for (keys %hash) {
    print join ",",
          "FUTIDX", $_,"SPOT","0,XX",
          $hash{$_}{'open'}, 
          $hash{$_}{'high'}, 
          $hash{$_}{'low'}, 
          $hash{$_}{'close'},
          $hash{$_}{'close'},
          $hash{$_}{'volume'},
          $hash{$_}{'value'},
          $hash{$_}{'noOfTrades'},
          $hash{$_}{'advances'},
          $hash{$_}{'advances_volumes'},
          $hash{$_}{'advances_value'},
          $hash{$_}{'declines'},
          $hash{$_}{'declines_volumes'},
          $hash{$_}{'declines_value'},
          $hash{$_}{'unchanged'},
          $hash{$_}{'unchanged_volumes'},
          $hash{$_}{'unchanged_value'},
          $hash{$_}{'timestamp'},
          "\n";
} 

################
# Subroutines
################

#
# parse_spot_index
# will create one element of a hash of hashes
# takes the name of the index, its url, the mech and the HoH
# as its args
sub parse_spot_index_page {

    my ($key, $url, $m, $hashref) = @_;

    $key =~ s/\&/-/g;
    $key =~ s/\s+/-/g;
    #print STDERR $key, " ";

    # Debug
    #return unless ($key =~ /^SENSEX$/);

    $m->get($url);
    unless ($m->success) {
	print STDERR "Problem getting $url\n";
	return;
    }

    # parse the HTML tree
    my $p = HTML::TokeParser->new(\$m->content());
    my $q = HTML::TokeParser->new(\$m->content());

    # First, get the timestamp
    for (1 .. 24) { $q->get_tag("script"); }
    for (1 .. 5) { $q->get_tag("span"); }
    for (1 .. 5) { $q->get_tag("div"); }
    #print STDERR $q->get_trimmed_text("/div"), "\n";
    my $timestamp =  $q->get_trimmed_text("/div");
    $timestamp =~ s/\s+/-/g;
    $hashref->{ $key }->{'timestamp'} = $timestamp;

    # data starts in the 5th table
    $p->get_tag("table"); $p->get_tag("table");
    $p->get_tag("table"); $p->get_tag("table");
    $p->get_tag("table"); #$p->get_tag("table");

    # Reject the first record
    $p->get_tag("tr"); $p->get_tag("/tr");
    $p->get_tag("tr"); $p->get_tag("/tr");
    $p->get_tag("tr"); $p->get_tag("/tr");
    $p->get_tag("tr");

    # open
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'open'} = $p->get_trimmed_text("/td");

    # high
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'high'} = $p->get_trimmed_text("/td");

    # low
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'low'} = $p->get_trimmed_text("/td");

    # close
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'close'} = $p->get_trimmed_text("/td");

    # volume
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    my $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'volume'} = $tmp * 10000;

    # value
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'value'} = $tmp * 100;

    # no of trades
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'noOfTrades'} = $tmp * 100;

    # tag madness :)
    $p->get_tag("/tr"); $p->get_tag("/table"); $p->get_tag("/td"); 
    $p->get_tag("/tr"); $p->get_tag("tr"); #$p->get_tag("tr");
    $p->get_tag("td"); $p->get_tag("/td"); $p->get_tag("/tr");
    $p->get_tag("tr");  $p->get_tag("/tr"); 

    $p->get_tag("table"); 
    $p->get_tag("tr"); $p->get_tag("/tr"); 

    $p->get_tag("tr"); 

    # advances
    $p->get_tag("td"); $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'advances'} = $p->get_trimmed_text("/td");

    # advances' volume
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'advances_volumes'} = $tmp * 10000;

    # advances' value
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'advances_value'} = $tmp * 100;

    $p->get_tag("/tr");
    $p->get_tag("tr");

    # declines
    $p->get_tag("td"); $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'declines'} = $p->get_trimmed_text("/td");

    # declines' volume
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'declines_volumes'} = $tmp * 10000;

    # declines' value
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'declines_value'} = $tmp * 100;

    $p->get_tag("/tr");
    $p->get_tag("tr");

    # unchanged
    $p->get_tag("td"); $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'unchanged'} = $p->get_trimmed_text("/td");

    # declines' volume
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'unchanged_volumes'} = $tmp * 10000;

    # declines' value
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'unchanged_value'} = $tmp * 100;

}
