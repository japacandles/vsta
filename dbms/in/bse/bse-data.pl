#!/usr/bin/perl -w

#
# bse-data.pl
#
# Oct 27, 2010
# While printing bhavcopy-format, there is an extra comma after XX
#
# Sep 16, 2010
# a. The bhavcopy line now provides decl and unch info after adv in format
#    FOO, FOO_VOL_INCrs, FOO_VAL_INCrs
# b. Remove any need for IO::File :)
#
# May 3, 2010
# Added a data-format line before outputting the bhavcopy line
#
# July 30, 2009
# BSE has started saying
# 'Shares Traded In Crs' instead of 'Total Shares Traded In Crs'
# and
# 'Turnover Rs Crs' instead of 'Total Turnover Rs Crs'
#
# October 17, 2008
# Created function print_bhavcopy()
#
# October 17, 2008 Friday
# A consequence of the bugfix of June 27:
# if advances were 0, we get a huge ugly perl warn about undefined vals
# Checked if the advances section was defined
# before printing the bhavcopy line
#
# July 14, 2008 Monday
# Trivial bugfix. Forgot to look for a new row after 
# the opening table. Fixed.
# Also BSE has started to refer to "traded value" as
# "turnover (Rs Crs)"
#
# July 5, 2008 Sunday
# Rewrote large parts
# Created the push_arr and pop_rev subs
# The push_arr makes decomma redundant
#
# June 27, 2008 Friday
# BUGFIX: Today all 30 scrips declined
# Instead of having a 0 row for advances, bseindia, skipped the row
# altogether. So this script says that there were 30 advances !
# Never again ...
#
# June 25, 2008 Wednesday
# COSMETIC: Decomma'd all numeric values coming from the web-page
# except those where its stupid to do so :)
#
# June 20, 2008 Friday
# tidied up majorly so that I can hand over the script to Ramky :)
#

use strict;
use diagnostics;
use HTML::TokeParser;
use LWP::UserAgent;
#se IO::File;
use Data::Dumper;

# Define a hash-table to store the data
my %data ;

my $fh;
my (@headers, @values, @new_headers);

#
# Get the indexwatch page
#
my $sensexURL = 'http://www.bseindia.com/mktlive/indiceswatch.asp?iname=BSE30&sensid=30';
my $u = LWP::UserAgent->new();
my $r = $u->get($sensexURL);

if ($r->is_success) {

	# The following is an ugly hack
	# HTML::TokeParser expects a filehandle
	# So I create a temp file, write the info to it
	# and rewind
	#$fh = IO::File->new_tmpfile;
	#die "Cannot create temporary file $!" unless (defined $fh);
	#print $fh $r->content;
	#$fh->seek(0, 0);

# Our get wasn't a success :( so we bail out
} else { 
	print STDERR $r->status_line, "\n"; 
	print "Are you sure the URL is correct ?\n";
	exit 1;
}

# So now we read the tmp file
#my $p = HTML::TokeParser->new($fh);
my $p = HTML::TokeParser->new(\$r->content);

# All the data is stored in tabular form
# So we parse the tables reading each td
while (my $t = $p->get_tag("td")) {
	my $text = $p->get_text("/td");

	if ($text =~ /Last Updated On/) { $data{'DateTime'} =  $text; }

	# This is a mess
	# Unfortunately I have no choice
	# The input was a mess too
	if ($text =~ /Open/) {
		@headers = push_arr();
		#print "headers are ", Dumper(\@headers), "\n";
		@headers = reverse @headers; push @headers, 'Open'; @headers = reverse @headers;

		#print "headers are ", Dumper(\@headers), "\n";
		$p->get_tag("tr"); # added on July 14, 2008, Monday
		@values = push_arr();
		#print "values are ", Dumper(\@values), "\n";
		@data{@headers} = @values;
		
		# Re-init the headers, values arrays
		(@headers, @values) = ();
	
		# We get to the table where the adv/decl/unch are shown
		$p->get_tag("table"); 
		
		# The first row only has two rows like captions
		# Advances/Declines
		# and
		# Intraday Graph ...
		$p->get_tag("tr"); $p->get_tag("tr");
		@headers = push_arr();
		pop(@headers); # get rid of the [IMG]
		
		@headers = pop_rev(@headers);
		#print "Header\n";
		#print Dumper(\@headers);
		
		# Miraculously, some table ends here too; but I want to skip all that 
		
		my $t = $p->get_tag("tr"); 
		
		#
		# BSE varies the number of rows on pure whim
		# The "Unchanged" row is always full of zeroes
		# but it is never omitted
		# But on 27th June 2008, when all scrips declined and the
		# "Advances" row was nil, they just skipped the 
		# advances altogether
		#
		while ($t->[0] !~ /^\/TABLE/i) {
		
			@values = push_arr();
			#print "Values\n";
			#print Dumper(\@values);

			# In this loop, we create the hash keys like
			# "Advances No of Scrips" etc
			foreach my $counter (0 .. $#headers ) {
				$new_headers[$counter] = $values[0] . " " . $headers[$counter];
			}	
		
			@values = pop_rev(@values);
			#print "New Headers\n";
			#print Dumper(\@new_headers);
			
			# Merge with the data hash
			@data{@new_headers} = @values;

			$t = $p->get_tag;
		}

		#foreach (/'Advances No of Scrips', 
		#		'Advances Shares Traded In Crs',
		#		'Advances Turnover Rs Crs'/) {
		#	unless (defined(data{$_})) { $data{$_} = 0; }
		#}
	}
}

#$fh->close;

#print Dumper(\%data);
print_bhavcopy();

exit 0;

#
# This sub starts at a <td> and creates an array
# of all the cells until a </tr>
#
sub push_arr {
	my @ary;
	while (my $t = $p->get_tag) {
		if ($t->[0] =~ /^TD/i) { 
			my $txt = $p->get_trimmed_text("/td");
			$txt =~ s/\W+/ /g; # get rid of unprintable chars
			$txt =~ s/\s{2,}/ /g; # get rid of multiple spaces
			$txt =~ s/\s$//; # get rid of trailing spaces
			if ($txt =~ /\s\d\d$/) { $txt =~ s/ (\d\d)$/\.$1/g; } #we got rid of the decimals too. bring them back
			if ($txt =~ /^\d/) { $txt =~ s/\s//g; } # no spaces in numbers
			push @ary, $txt;
		}
		last if ($t->[0] =~ /^\/TR/i);
	}
	return @ary;
}

#
# Refactoring Alert
# this sub can be removed if I use splice
#
sub pop_rev {
	my @ary = @_;
	@ary = reverse(@ary);
	pop(@ary);
	@ary = reverse(@ary);
	return(@ary);
}

sub print_bhavcopy {

	# Bhavcopy line
	print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP,OPEN,HIGH,LOW,CLOSE,SETTLE_PR,VOL_INCrs,VAL_INCrs,ADV,ADV_VOL_INCrs,ADV_VAL_INCrs," . 
	    "DECL,DECL_VOL_INCrs,DECL_VAL_INCrs,UNC,UNC_VOL_INCrs,UNC_VAL_INCrs", "\n";
	print join ",", "FUTIDX,sensex,spot,0,XX",
		$data{'Open'} ,
		$data{'High'} , 
		$data{'Low'} ,
		$data{'Current Close'} , 
		$data{'Current Close'} , 
		#$data{'Total Shares Traded In Crs'} * 10000, ",",
		$data{'Shares Traded In Crs'} * 10000, 
		#$data{'Total Turnover Rs Crs'} * 100, ",",
		$data{'Turnover Rs Crs'} * 100, 

		# If advances are undef, we get an ugly perl message, so ...
		(defined($data{'Advances No of Scrips'}) ? 
			$data{'Advances No of Scrips'} : 0), 
		(defined($data{'Advances Shares Traded In Crs'}) ? 
			$data{'Advances Shares Traded In Crs'} * 10000 : 0), 
		(defined($data{'Advances Turnover Rs Crs'}) ? 
			$data{'Advances Turnover Rs Crs'} * 100 : 0), 

		# If declines are undef, we get an ugly perl message, so ...
		(defined($data{'Declines No of Scrips'}) ? 
			$data{'Declines No of Scrips'} : 0), 
		(defined($data{'Declines Shares Traded In Crs'}) ? 
			$data{'Declines Shares Traded In Crs'} * 10000 : 0), 
		(defined($data{'Declines Turnover Rs Crs'}) ? 
			$data{'Declines Turnover Rs Crs'} * 100 : 0), 

		# If unch are undef, we get an ugly perl message, so ...
		(defined($data{'Unchanged No of Scrips'}) ? 
			$data{'Unchanged No of Scrips'} : 0), 
		(defined($data{'Unchanged Shares Traded In Crs'}) ? 
			$data{'Unchanged Shares Traded In Crs'} * 10000 : 0), 
		(defined($data{'Unchanged Turnover Rs Crs'}) ? 
			$data{'Unchanged Turnover Rs Crs'} * 100 : 0), 

		"\n";
}
