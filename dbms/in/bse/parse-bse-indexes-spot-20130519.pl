#!/usr/bin/perl

# May 19, 2013, Sunday, 13:26
# BSE modified their pages again on May 6th
# This rewrite was necessary because of that

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use WWW::Mechanize;

my $bse_idx_base = 'http://www.bseindia.com/indices/IndexHighlight.aspx?expandable=1';
my %hash;

my $p = WWW::Mechanize->new();
$p->agent_alias('Windows IE 6');

$p->get($bse_idx_base);
die $! unless $p->success();

for ($p->links) {

    #if (defined $_->text) { print join "\t", $_->text, $_->url, "\n"; }

    # skip all dollex urls
    next if ($_->url =~ /iname=DOL/);
    # skip carbonex
    next if ($_->url =~ /iname=CARBON/);

    if ($_->url =~ /indiceswatch/) { 
	parse_spot_index_page($_->text, 
			      "http://www.bseindia.com/indices/" . $_->url, 
			      $p, \%hash);
    }
}

#print Dumper(\%hash);

print "INSTRUMENT,SYMBOL,EXPIRY_DT,STRIKE_PR,OPTION_TYP," , 
    "OPEN,HIGH,LOW,CLOSE,SETTLE_PR,VOL_INCrs,VAL_INCrs," ,
    "NUM_TRADES,ADV,ADV_VOL_INCrs,ADV_VAL_INCrs,DECL,DECL_VOL_INCrs," ,
    "DECL_VAL_INCrs,UNC,UNC_VOL_INCrs,UNC_VAL_INCrs", "\n";

for (keys %hash) {
    my $idx = $_;
    $idx =~ s/(-)+/-/g; # so that "Oil & Gas" 
                        # becomes "Oil-Gas" and not "Oil---Gas"
    print join ",",
          "FUTIDX", $idx,"SPOT","0,XX",
          $hash{$_}{'open'}, 
          $hash{$_}{'high'}, 
          $hash{$_}{'low'}, 
          $hash{$_}{'close'},
          $hash{$_}{'close'},
          $hash{$_}{'volume'},
          $hash{$_}{'value'},
          $hash{$_}{'noOfTrades'},
          $hash{$_}{'advances'},
          $hash{$_}{'advances_volumes'},
          $hash{$_}{'advances_value'},
          $hash{$_}{'declines'},
          $hash{$_}{'declines_volumes'},
          $hash{$_}{'declines_value'},
          $hash{$_}{'unchanged'},
          $hash{$_}{'unchanged_volumes'},
          $hash{$_}{'unchanged_value'},
          $hash{$_}{'timestamp'},
          "\n";
} 

################
# Subroutines
################

#
# parse_spot_index
# will create one element of a hash of hashes
# takes the name of the index, its url, the mech and the HoH
# as its args
sub parse_spot_index_page {

    my ($key, $url, $m, $hashref) = @_;

    $key =~ s/\&/-/g;
    $key =~ s/\s+/-/g;
    #print STDERR "key is ", $key, " ";

    # Debug
    #return unless ($key =~ /^SENSEX$/);

    my $token;

    $m->get($url);
    unless ($m->success) {
        print STDERR "Problem getting $url\n";
        return;
    }

    # parse the HTML tree
    my $p = HTML::TokeParser->new(\$m->content());

    # First, get the timestamp

    $p->get_tag("table"); $p->get_tag("/table");

    for (1 .. 22) { $p->get_tag("div"); }
    my $timestamp =  $p->get_trimmed_text("/div");
    #print STDERR "Timestamp is ", $timestamp, "\n";
    $timestamp =~ s/\s+/-/g; 
    $hashref->{ $key }->{'timestamp'} = $timestamp;

    # data starts in the 5th table, which is the 4th 
    # from where we are
    for (1 .. 4) { $p->get_tag("table"); }

    # Reject the first 2 record
    $p->get_tag("tr"); $p->get_tag("/tr");
    $p->get_tag("tr"); $p->get_tag("/tr");
    # BSE has embedded a table here for market cap data
    # skipping it entirely
    $p->get_tag("/tr");

    $p->get_tag("tr");

    # open
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'open'} = $p->get_trimmed_text("/td");

    # high
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'high'} = $p->get_trimmed_text("/td");

    # low
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'low'} = $p->get_trimmed_text("/td");

    # close
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'close'} = $p->get_trimmed_text("/td");

    # volume
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    my $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'volume'} = $tmp * 10000;

    # value
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'value'} = $tmp * 100;

    # no of trades
    $p->get_tag("td"); 
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'noOfTrades'} = $tmp * 100;

    # tag madness :)
    $p->get_tag("/tr"); $p->get_tag("/table"); $p->get_tag("/td"); 
    $p->get_tag("/tr"); $p->get_tag("tr"); #$p->get_tag("tr");
    $p->get_tag("td"); $p->get_tag("/td"); $p->get_tag("/tr");
    $p->get_tag("tr");  $p->get_tag("/tr"); 

    $p->get_tag("table"); 
    $p->get_tag("tr"); $p->get_tag("/tr"); 

    $p->get_tag("tr"); 

    # advances
    $p->get_tag("td"); $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'advances'} = $p->get_trimmed_text("/td");

    # advances' volume
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'advances_volumes'} = $tmp * 10000;

    # advances' value
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'advances_value'} = $tmp * 100;

    $p->get_tag("/tr");
    $p->get_tag("tr");

    # declines
    $p->get_tag("td"); $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'declines'} = $p->get_trimmed_text("/td");

    # declines' volume
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'declines_volumes'} = $tmp * 10000;

    # declines' value
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'declines_value'} = $tmp * 100;

    $p->get_tag("/tr");
    $p->get_tag("tr");

    # unchanged
    $p->get_tag("td"); $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $hashref->{ $key }->{'unchanged'} = $p->get_trimmed_text("/td");

    # declines' volume
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'unchanged_volumes'} = $tmp * 10000;

    # declines' value
    $p->get_tag("td");
    #print STDERR $p->get_trimmed_text("/td"), "\n";
    $tmp = $p->get_trimmed_text("/td");
    $hashref->{ $key }->{'unchanged_value'} = $tmp * 100;

}
