#!/bin/bash

# 3 June 2015
# switched to reuters because bloomberg stopped providing ohlcv quotes
#
# 4 Jan 2010
# a. gets and appends the dax data
#    using bloomberg-index.pl and bb.sh
# b. creates a bhavcopy of fdax futures

# Need to save this dir
this_dir=$PWD

# Dax spot
datafile=`echo $PWD | sed 's/dbms/data/'`"/futures""/dax"
DATE=$(TZ=Europe/Frankfurt date +%Y%m%d)
echo updating $datafile with dax
cd $TOP_DBMS_DIR
echo -n "$DATE " | tee -a $datafile
perl parse-money-cnn.pl world_markets dax | tee -a $datafile
#perl parse-reuters.pl gdaxi | tee -a $datafile
#./bb.sh Europe/Frankfurt /tmp dax:ind | tee -a $datafile
#yf.sh Europe/Frankfurt /tmp ^gdaxi | tee -a $datafile

