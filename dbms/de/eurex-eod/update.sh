#!/bin/bash

bhavcopy=$1
#date=`basename $bhavcopy | awk -F- '{print $3$2$1}'`
datadir=`echo $PWD | sed 's/dbms/data/;'`"/futures/"
#echo $bhavcopy $date $datadir

for line in $(cat $bhavcopy)
do
	FUTURE=$(echo $line | sed 's/,/ /g' | sed 's/_Index//g' | sed 's/-INDEX//g' | awk '{print $1}')
	DATE=$(echo $line | sed 's/,/ /g' | sed 's/_Index//g' | sed 's/-INDEX//g' | awk '{print $2}')
	OPEN=$(echo $line | sed 's/,/ /g' | sed 's/_Index//g' | sed 's/-INDEX//g' | awk '{print $3}')
	HIGH=$(echo $line | sed 's/,/ /g' | sed 's/_Index//g' | sed 's/-INDEX//g' | awk '{print $4}')
	LOW=$(echo $line | sed 's/,/ /g' | sed 's/_Index//g' | sed 's/-INDEX//g' | awk '{print $5}')
	CLOSE=$(echo $line | sed 's/,/ /g' | sed 's/_Index//g' | sed 's/-INDEX//g' | awk '{print $6}')
	VOLUME=$(echo $line | sed 's/,/ /g' | sed 's/_Index//g' | sed 's/-INDEX//g' | awk '{print $7}')

	DATAFILE=$datadir/$FUTURE
	echo Future file is $DATAFILE
	echo $DATE $OPEN $HIGH $LOW $CLOSE $VOLUME | tee -a $DATAFILE
done

#echo datadir is $datadir
#for i in `cat $1`; 
#do  
    
#    echo $i | grep FUTIDX  ; 
#    if [ $? -eq 0 ]; then 
    #echo line; 
    #echo $i;  
    
#	line=`echo $i | sed 's/,/ /g'`; 
#        #echo new line;
        #echo $line;  

#	file=`echo $line | awk '{print $2"-"$3}'`; 
	#echo file is $file; 
#	record=`echo $line | awk -v nr=6 'BEGIN {ORS=" "} 
#                                     {for (x=nr; x<=NF; x++) 
#					 {if (x == 10) continue;
#					     print $x}
#					 }'`; 
#	echo appending to file $datadir/$file
#	echo $date $record | tee -a $datadir/$file;
#    fi 
#done
