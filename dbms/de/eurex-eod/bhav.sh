#!/bin/bash -x

# 31 July 2011
# Returns the retval of the web-scraper

# 4 Jan 2010
# a. creates a bhavcopy of fdax futures

# 28 Nov 2015
# Started getting Mini Dax futures

# Need to save this dir
this_dir=$PWD
cd $TOP_DBMS_DIR

# fdax futures
bhavdate=$(TZ=Europe/Frankfurt date +%Y%m%d)
bhavcopy=$(TZ=Europe/Frankfurt date +%Y-%m-%d)
#echo bhavdate is $bhavdate
datafile=`echo $this_dir | sed 's/dbms/data/'`"/bhavcopies""/$bhavcopy"
echo updating $datafile with fdax futures
#./parse-eurex-fdax.pl fdax | tee -a $datafile
#./parse-eurex-fdax.pl fdax >> $datafile
#./parse-eurex-fdax.pl fdax 
perl ./parse-barchart.pl DAX_Index $bhavdate | tee -a $datafile
perl ./parse-barchart.pl Mini_DAX_Index $bhavdate | tee -a $datafile
RETVAL=$?
exit $RETVAL
