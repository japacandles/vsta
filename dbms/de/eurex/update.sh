#!/bin/bash -x

source ../../configs.sh

#echo data dir is $DATADIR

# the first argument is the bhavcopy
bhavcopy=$1

#datadir=$DATADIR/us/cme/futures/commodities
datadir=$DATADIR/de/eurex/futures/

# We expect the lines of the bhavcopy to be like
# DAX-Jun-18,20180518,13125.50,13138.00,13047.00,13082.00,79244,130280,


for line in $(cat $bhavcopy)
do

	echo $line 
	#read

	file=$(echo $line | awk -F, '{print $1}' )

	d=$(echo $line | awk -F, '{print $2}' )
	dt=$(echo $d | sed 's/-//g')

	o=$(echo $line | awk -F, '{print $3}' )
	h=$(echo $line | awk -F, '{print $4}' )
	l=$(echo $line | awk -F, '{print $5}' )
	c=$(echo $line | awk -F, '{print $6}' )
	v=$(echo $line | awk -F, '{print $7}' )
	t=$(echo $line | awk -F, '{print $8}' )

	outfile=$datadir/$file
	echo outfile is $outfile
	echo $dt $o $h $l $c $v $t | tee -a $outfile

	#read

done
