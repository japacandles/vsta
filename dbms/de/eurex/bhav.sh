#!/bin/bash -x

# June 15, 2018, Friday
# Usage:
# cd /path/to/vsta/dbms/vsta-skel ; ./bhav.sh ../de/eurex FOO
# where FOO is one of EURO-STOXX-50, DAX, MDAX ...

. ./vars.sh
#for commodity in ${commodities[@]}; do
#    echo Commodity is $commodity
#done

# the first argument is the security
security=$1
d=$(TZ=Europe/Berlin date -d "1 day ago" +%Y%m%d)
#d=$(TZ=Europe/Berlin date +%Y%m%d)

# Generic directory stuff
this_dir=$PWD
#datadir=`echo $this_dir | sed 's/dbms/data/'`"/bhavcopies/raw/"
datadir="$VSTA_DATA_DIR/de/eurex/bhavcopies/raw/"
echo $datadir
cd $TOP_DBMS_DIR

# which then leads to the location to which the
# bhavcopy is to be written
#OUT=$datadir/$security-$d
#rm -vf $OUT

#echo updating $OUT with $security
#cd $TOP_DBMS_DIR

for commodity in ${commodities[@]}; do
    echo Commodity is $commodity
    OUT=$datadir/$commodity-$d
    rm -vf $OUT

    #echo -n "$d " | tee -a $OUT
    perl parse-eurex-futures.pl $commodity $d | tee -a $OUT
done

