#!/usr/bin/perl

#
# create-bitcoincharts-security-datafile.pl
# Takes the name of the security on stdin
# and outputs the entire history of security
# in dohlcv format
# The date is in GMT at 00:00:00
# The vulume is in BTC
# 
# Changelog:
# Jul 14, 2013, Sunday, 6:16 PM
# recreated the script as it was deleted from /tmp :(

use strict;
use diagnostics;
use warnings;
use Data::Dumper;
use LWP::UserAgent;
use JSON;
use DateTime;

my $security = shift;
unless (defined $security) {
    print "Usage: create-bitcoincharts-security-datafile.pl <name-of-security>\n",
          "Example: create-bitcoincharts-security-datafile.pl mtgoxUSD\n";
    exit(1);
}

my $browser = LWP::UserAgent->new(
    agent => 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130329 Firefox/17.0',
);

my $url = "http://www.bitcoincharts.com/charts/chart.json?" . 
    "SubmitButton=Draw&r=&i=&c=0&s=&e=&Prev=&Next=&t=S&b=&" . 
    "a1=&m1=10&a2=&m2=25&x=0&i1=&i2=&i3=&i4=&v=1&cv=0&ps=0&l=0&p=0&" .
    "m=" . $security ;

my $request = HTTP::Request->new(GET => $url);

my $response = $browser->request($request);
unless ($response->is_success) {
    print "Problems getting bitcoincharts.com data for $security\n";
    exit(2);
}

# Dump the entire response
# print $response->decoded_content;
my $string = from_json($response->decoded_content);

# Dump the data structure
# print Dumper(\$string);    

for my $chunk (@$string) {
    #print Dumper(\$chunk);
    my $epochDate = $chunk->[0];
    my $open = $chunk->[1];
    my $high = $chunk->[2];
    my $low = $chunk->[3];
    my $close = $chunk->[4];
    my $volume = $chunk->[5];
    next if ($volume == 0);

    # sometimes data is strange. high is not the highest price etc
    if ($high < $open) { $high = $open; }
    if ($high < $low) { $high = $low; }
    if ($high < $close) { $high = $close; }
    if ($low > $open) { $low = $open; }
    if ($low > $high) { $low = $high; }
    if ($low > $close) { $low = $close; }
    

    my $dt = DateTime->from_epoch(epoch => $epochDate);
    print join " ", $dt->format_cldr("yyyyLLdd") , $open, $high, $low, $close, $volume, "\n";
}
